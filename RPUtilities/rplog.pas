{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  Abstract:
    Logging.
}
unit RPLog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils; 
  
type
  TRPLogStage = (
    rlsVeryVerbose,
    rlsVerbose,
    rlsInfo,
    rlsNote,
    rlsWarning,
    rlsError,
    rlsFatal
    );
    
const
  RPLogStageNames: array[TRPLogStage] of string = (
    'VeryVerbose',
    'Verbose',
    'Info',
    'Note',
    'Warning',
    'Error',
    'Fatal'
    );
    
type

  { TRPLog }

  TRPLog = class(TComponent)
  public
    procedure Add(const Msg: string; Stage: TRPLogStage = rlsInfo);
  end;

var
  MainRPLog: TRPLog;

procedure Log(const Msg: string; Stage: TRPLogStage = rlsInfo);

implementation

procedure Log(const Msg: string; Stage: TRPLogStage);
begin
  MainRPLog.Add(Msg,Stage);
end;

{ TRPLog }

procedure TRPLog.Add(const Msg: string; Stage: TRPLogStage);
var
  s: String;
begin
  s:=FormatDateTime('YYYY-MM-DD HH:NN:SS ',Now);
  s:=s+RPLogStageNames[Stage]+': '+Msg;
  writeln(s);
end;

end.

