{ This file was automatically created by Lazarus. Do not edit!
This source is only used to compile and install the package.
 }

unit SynapseLaz; 

interface

uses
  asn1util, blcksock, clamsend, dnssend, ftpsend, ftptsend, httpsend, imapsend, 
    ldapsend, nntpsend, pingsend, pop3send, slogsend, smtpsend, snmpsend, 
    sntpsend, synacode, synafpc, synautil, synsock, tlntsend, 
    LazarusPackageIntf; 

implementation

procedure Register; 
begin
end; 

initialization
  RegisterPackage('SynapseLaz', @Register); 
end.
