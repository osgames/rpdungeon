{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  Abstract:
    The server runs as daemon, i.e. endless.
}
unit RPServerDaemon;

{$mode objfpc}{$H+}

interface

uses
  Classes, blcksock, SynSock, SynaUtil, SysUtils;

type

  { TRPServerDaemon }

  TRPServerDaemon = class(TThread)
  private
    FBindAddress: string;
    FPort: string;
    ServerSocket: TTCPBlockSocket;
    procedure SetBindAddress(const AValue: string);
    procedure SetPort(const AValue: string);
  public
    constructor Create(CreateSuspended: Boolean);
    destructor Destroy; override;
    procedure Execute; override;
    property Port: string read FPort write SetPort;
    property BindAddress: string read FBindAddress write SetBindAddress;
  end;

  { TRPServerThread }

  TRPServerThread = class(TThread)
  private
    ClientSocket: TTCPBlockSocket;
  public
    Headers: TStringList;
    InputData, OutputData: TMemoryStream;
    constructor Create (hsock: tSocket; CreateSuspended: Boolean);
    destructor Destroy; override;
    procedure Execute; override;
    function ProcessHttpRequest(Request, URI: string): integer;
  end;

implementation

{ TRPServerDaemon }

procedure TRPServerDaemon.SetPort(const AValue: string);
begin
  if FPort=AValue then exit;
  FPort:=AValue;
end;

procedure TRPServerDaemon.SetBindAddress(const AValue: string);
begin
  if FBindAddress=AValue then exit;
  FBindAddress:=AValue;
end;

constructor TRPServerDaemon.Create(CreateSuspended: Boolean);
begin
  ServerSocket:=TTCPBlockSocket.Create;
  FreeOnTerminate:=true;
  Priority:=tpNormal;
  FBindAddress:='0.0.0.0';
  FPort:='17000';
  inherited Create(CreateSuspended);
end;

destructor TRPServerDaemon.Destroy;
begin
  FreeAndNil(ServerSocket);
  inherited Destroy;
end;

procedure TRPServerDaemon.Execute;
var
  ClientSock:TSocket;
begin
  with ServerSocket do begin
    CreateSocket;
    setLinger(true,10);
    bind(BindAddress,Port);
    listen;
    repeat
      if terminated then break;
      if canread(1000) then begin
        ClientSock:=Accept;
        if lastError=0 then TRPServerThread.Create(ClientSock,false);
      end;
    until false;
  end;
end;

{ TRPServerThread }

constructor TRPServerThread.Create(Hsock:TSocket; CreateSuspended: Boolean);
begin
  ClientSocket:=TTCPBlockSocket.Create;
  Headers := TStringList.Create;
  InputData := TMemoryStream.Create;
  OutputData := TMemoryStream.Create;
  ClientSocket.Socket:=HSock;
  FreeOnTerminate:=true;
  Priority:=tpNormal;
  inherited Create(CreateSuspended);
end;

destructor TRPServerThread.Destroy;
begin
  FreeAndNil(ClientSocket);
  FreeAndNil(Headers);
  FreeAndNil(InputData);
  FreeAndNil(OutputData);
  inherited Destroy;
end;

procedure TRPServerThread.Execute;
var
  timeout: integer;
  s: string;
  method, uri, protocol: string;
  size: integer;
  x, n: integer;
  resultcode: integer;
begin
  timeout := 120000;
  //read request line
  s := ClientSocket.RecvString(timeout);
  if ClientSocket.lasterror <> 0 then
    Exit;
  if s = '' then
    Exit;
  method := fetch(s, ' ');
  if (s = '') or (method = '') then
    Exit;
  uri := fetch(s, ' ');
  if uri = '' then
    Exit;
  protocol := fetch(s, ' ');
  headers.Clear;
  size := -1;
  //read request headers
  if protocol <> '' then
  begin
    if pos('HTTP/', protocol) <> 1 then
      Exit;
    repeat
      s := ClientSocket.RecvString(Timeout);
      if ClientSocket.lasterror <> 0 then
        Exit;
      if s <> '' then
        Headers.add(s);
      if Pos('CONTENT-LENGTH:', Uppercase(s)) = 1 then
        Size := StrToIntDef(SeparateRight(s, ' '), -1);
    until s = '';
  end;
  //recv document...
  InputData.Clear;
  if size >= 0 then
  begin
    InputData.SetSize(Size);
    x := ClientSocket.RecvBufferEx(InputData.Memory, Size, Timeout);
    InputData.SetSize(x);
    if ClientSocket.lasterror <> 0 then
      Exit;
  end;
  OutputData.Clear;
  ResultCode := ProcessHttpRequest(method, uri);
  ClientSocket.SendString('HTTP/1.0 ' + IntTostr(ResultCode) + CRLF);
  if protocol <> '' then
  begin
    headers.Add('Content-length: ' + IntTostr(OutputData.Size));
    headers.Add('Connection: close');
    headers.Add('Date: ' + Rfc822DateTime(now));
    headers.Add('Server: Synapse HTTP server demo');
    headers.Add('');
    for n := 0 to headers.count - 1 do
      ClientSocket.sendstring(headers[n] + CRLF);
  end;
  if ClientSocket.lasterror <> 0 then
    Exit;
  ClientSocket.SendBuffer(OutputData.Memory, OutputData.Size);
end;

function TRPServerThread.ProcessHttpRequest(Request, URI: string): integer;
var
  l: TStringlist;
begin
//sample of precessing HTTP request:
// InputData is uploaded document, headers is stringlist with request headers.
// Request is type of request and URI is URI of request
// OutputData is document with reply, headers is stringlist with reply headers.
// Result is result code
  result := 504;
  if request = 'GET' then
  begin
    headers.Clear;
    headers.Add('Content-type: Text/Html');
    l := TStringList.Create;
    try
      l.Add('<html>');
      l.Add('<head></head>');
      l.Add('<body>');
      l.Add('Request Uri: ' + uri);
      l.Add('<br>');
      l.Add('This document is generated by Synapse HTTP server demo!');
      l.Add('</body>');
      l.Add('</html>');
      l.SaveToStream(OutputData);
    finally
      l.free;
    end;
    Result := 200;
  end;
end;

end.

