{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  Abstract:
    The server runs as daemon, i.e. endless.
}
program RPServer;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, fpsock, LCLProc, LResources, CustApp, RPProtocolPkg, RPProtocol,
  RPMsg, SynapseLaz, RPServerDaemon;
  
type

  { TRPServer }

  TRPServer = class(TCustomApplication)
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure Run;
    procedure Test;
    procedure LogInfo(const Msg: string);
  end;

{ TRPServer }

constructor TRPServer.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  RegisterRPMessages;
end;

destructor TRPServer.Destroy;
begin
  UnregisterRPMessages;
  inherited Destroy;
end;

procedure TRPServer.Run;
var
  Daemon: TRPServerDaemon;
  i: Integer;
begin
  Daemon:=TRPServerDaemon.Create(true);
  if HasOption('p','port') then
    Daemon.Port:=GetOptionValue('p','port');
  if HasOption('a','address') then
    Daemon.BindAddress:=GetOptionValue('a','address');
  LogInfo('TRPServer.Run Port='+Daemon.Port+' Address='+Daemon.BindAddress);
  Daemon.Resume;
  for i:=1 to 100000 do begin
    writeln('TRPServer.Run ',i);
    sleep(1000);
  end;

  Test;
end;

procedure TRPServer.Test;
var
  AMessage: TRPWorldPropertiesMessage;
  AStream: TMemoryStream;
  LFMStream: TSTream;
  s: string;
  ReceivedMessage: TRPMessage;
begin
  AMessage:=TRPWorldPropertiesMessage.Create(nil);
  AMessage.Floor.SizeX:=1;
  AMessage.Floor.SizeY:=2;
  AMessage.Camera.Position.X:=3.1;
  AMessage.Camera.Position.Y:=3.2;
  AMessage.Camera.Position.Z:=3.3;
  AMessage.Camera.CenterOfView.X:=4.1;
  AMessage.Camera.CenterOfView.Y:=4.2;
  AMessage.Camera.CenterOfView.Z:=4.3;

  AStream:=TMemoryStream.Create;

  WriteMessageToStream(AStream,AMessage);
  AStream.Position:=0;
  ReceivedMessage:=nil;
  ReadMessageFromStream(AStream,nil,ReceivedMessage);
  AStream.Size:=0;
  WriteMessageToStream(AStream,ReceivedMessage);

  AStream.Position:=0;

  LFMStream:=TMemoryStream.Create;
  LRSObjectBinaryToText(AStream,LFMStream);

  LFMStream.Position:=0;
  SetLength(s,LFMStream.Size);
  LFMStream.Read(s[1],length(s));
  writeln(s);
  LFMStream.Free;

  AStream.Free;

  ReceivedMessage.Free;
  AMessage.Free;
end;

procedure TRPServer.LogInfo(const Msg: string);
begin
  writeln(Msg);
end;

var
  Application: TRPServer;
begin
  Application:=TRPServer.Create(nil);
  Application.Run;
  Application.Free;
end.

