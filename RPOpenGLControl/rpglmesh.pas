{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit RPGLMesh;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, Forms, Controls, FPImage, Graphics, AVL_Tree,
  Dyn_Arrays, Dialogs, Vectors, AsmTypes, GL, AsmUtils, RPGLUtils, Laz_XMLCfg,
  RPGLObjDB;

type
  TRPMeshes = class;

  { TRPMesh }

  TRPMesh = class(TAsmMesh)
  private
    FDBPath: string;
    FLoadedRPName: string;
    FObjFilename: string;
    FRefCount: integer;
    FRPName: string;
    FContainers: TFPList;
    function GetContainerCount: integer;
    function GetContainers(Index: integer): TRPMeshes;
    procedure SetLoadedRPName(const AValue: string);
    procedure SetRPName(const AValue: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure CreateBox2D(const BoxWidth, BoxHeight: GLfloat;
                             WidthSubdivisions, HeightSubdivisions: integer);
    procedure CreateWall(const BoxWidth, BoxHeight, BoxDepth: GLfloat;
                         WidthSubdivisions, HeightSubdivisions, DepthSubdivsions: integer);
    procedure CreateWall(const BoxWidth, BoxHeight, BoxDepth: GLfloat);
    procedure CreateSelection;
    procedure Release;
    procedure Reference;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    function LoadMeshFromObjFile(const Filename: string): boolean;
    function LoadMeshFromObjStream(AStream: TStream): boolean;
    function LoadMeshFromDB(const DBPath: string): boolean;
    function LoadMeshFromObjStrings(const Data: TStrings): boolean;
    procedure ClearSource;
  public
    property RPName: string read FRPName write SetRPName;
    property LoadedRPName: string read FLoadedRPName write SetLoadedRPName;
    property Containers[Index: integer]: TRPMeshes read GetContainers;
    property ContainerCount: integer read GetContainerCount;
    property ObjFilename: string read FObjFilename;
    property DBPath: string read FDBPath;
  end;


  { TRPMeshes }

  TRPMeshes = class
  private
    FItems: TAVLTree; // tree of TRPMesh
    function GetCount: integer;
  protected
    procedure ChangingName(Item: TRPMesh);
    procedure ChangedName(Item: TRPMesh);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function FindByRPName(const RPName: string): TRPMesh;
    function GetMeshFromDB(const DBPath: string): TRPMesh;
    function GetMeshFromObjFile(const RPName, Filename: string): TRPMesh;
    function FindNodeByRPName(const RPName: string): TAVLTreeNode;
    procedure Add(Mesh: TRPMesh);
    function NewRPName(const StartName: string): string;
  public
    property Count: integer read GetCount;
  end;
  
  
  TRPTextures = class;
  
  { TRPTexture }

  TRPTexture = class(TAsmTexture)
  private
    FRefCount: integer;
    FRPName: string;
    FContainers: TFPList;
    FTextureDBPath: string;
    FTextureFilename: string;
    function GetContainerCount: integer;
    function GetContainers(Index: integer): TRPTextures;
    procedure SetRPName(const AValue: string);
  public
    constructor Create(AWidth, AHeight: integer); override;
    destructor Destroy; override;
    procedure Release;
    procedure Reference;
    function LoadFromFile(const Filename: string): TModalResult;
    function LoadFromDB(const DBPath: string): TModalResult;
    procedure ClearSource;
  public
    property RPName: string read FRPName write SetRPName;
    property Containers[Index: integer]: TRPTextures read GetContainers;
    property ContainerCount: integer read GetContainerCount;
    property TextureFilename: string read FTextureFilename;
    property TextureDBPath: string read FTextureDBPath;
  end;
  

  { TRPTextures }

  TRPTextures = class
  private
    FItems: TAVLTree; // tree of TRPTexture
    function GetCount: integer;
  protected
    procedure ChangingName(Item: TRPTexture);
    procedure ChangedName(Item: TRPTexture);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function GetTextureFromFile(const RPName, Filename: string): TRPTexture;
    function GetTextureFromDB(const DBPath: string): TRPTexture;
    function FindByRPName(const RPName: string): TRPTexture;
    function FindNodeByRPName(const RPName: string): TAVLTreeNode;
    procedure Add(Texture: TRPTexture);
    function NewRPName(const StartName: string): string;
  public
    property Count: integer read GetCount;
  end;


  { TRPAsmObject }

  TRPAsmObject = class(TAsmObject)
  public
    function IsMatrixEqual(AnObject: TAsmObject): boolean;
    procedure CopyMatrix(AnObject: TAsmObject);
    procedure ComputeBoundingBox(out Box: TRPAsmBox);
    destructor Destroy; override;
  end;
  
  
var
  RPMeshes: TRPMeshes;
  RPTextures: TRPTextures;

function CompareRPMeshesByRPNames(Data1, Data2: Pointer): integer;
function CompareNameWithRPMeshRPName(Key, Data: Pointer): integer;
function CompareRPTexturesByRPNames(Data1, Data2: Pointer): integer;
function CompareNameWithRPTextureRPName(Key, Data: Pointer): integer;

implementation

function CompareRPMeshesByRPNames(Data1, Data2: Pointer): integer;
begin
  Result:=CompareText(TRPMesh(Data1).RPName,TRPMesh(Data2).RPName);
  //DebugLn('CompareRPMeshesByRPNames ',TRPMesh(Data1).RPName,' ',TRPMesh(Data2).RPName);
end;

function CompareNameWithRPMeshRPName(Key, Data: Pointer): integer;
var
  RPName: String;
begin
  RPName:='';
  Pointer(RPName):=Key;
  Result:=CompareText(RPName,TRPMesh(Data).RPName);
  //DebugLn('CompareNameWithRPMeshRPName ',RPName,' ',TRPMesh(Data).RPName);
  Pointer(RPName):=nil;
end;

function CompareRPTexturesByRPNames(Data1, Data2: Pointer): integer;
begin
  Result:=CompareText(TRPTexture(Data1).RPName,TRPTexture(Data2).RPName);
  //DebugLn('CompareRPTexturesByRPNames ',TRPTexture(Data1).RPName,' ',TRPTexture(Data2).RPName);
end;

function CompareNameWithRPTextureRPName(Key, Data: Pointer): integer;
var
  RPName: String;
begin
  RPName:='';
  Pointer(RPName):=Key;
  Result:=CompareText(RPName,TRPTexture(Data).RPName);
  //DebugLn('CompareNameWithRPTextureRPName ',RPName,' ',TRPTexture(Data).RPName);
  Pointer(RPName):=nil;
end;

{ TRPMesh }

procedure TRPMesh.SetRPName(const AValue: string);
var
  i: Integer;
begin
  if FRPName=AValue then exit;
  for i:=0 to ContainerCount-1 do
    Containers[i].ChangingName(Self);
  FRPName:=AValue;
  for i:=0 to ContainerCount-1 do
    Containers[i].ChangedName(Self);
end;

function TRPMesh.GetContainerCount: integer;
begin
  Result:=FContainers.Count;
end;

function TRPMesh.GetContainers(Index: integer): TRPMeshes;
begin
  Result:=TRPMeshes(FContainers[Index]);
end;

procedure TRPMesh.SetLoadedRPName(const AValue: string);
begin
  if FLoadedRPName=AValue then exit;
  FLoadedRPName:=AValue;
end;

constructor TRPMesh.Create;
begin
  FRefCount:=1;
  FContainers:=TFPList.Create;
end;

destructor TRPMesh.Destroy;
begin
  FContainers.Free;
  inherited Destroy;
end;

procedure TRPMesh.CreateBox2D(const BoxWidth, BoxHeight: GLfloat;
  WidthSubdivisions, HeightSubdivisions: integer);
var
  x,y: Integer;
  StepSizeX, StepSizeY: GLfloat;
begin
  if FCount>0 then ClearArrays;
  Primitive:=GL_TRIANGLES;
  BS.Radius:=Max(BoxWidth,BoxHeight);
  BoundingBox.X:=BoxWidth/2;
  BoundingBox.Y:=BoxHeight/2;
  BoundingBox.Z:=0.01;
  StepSizeX:=BoxWidth/WidthSubdivisions;
  StepSizeY:=BoxHeight/HeightSubdivisions;
  for x:=0 to WidthSubdivisions-1 do
    for y:=0 to HeightSubdivisions-1 do begin
      IndexArray.Add(AddVertex3_3_2(-BoxWidth/2+x*StepSizeX,BoxHeight/2-y*StepSizeY,0,0,0,1,0,0,NormalArray,TexCoordArray));
      IndexArray.Add(AddVertex3_3_2(-BoxWidth/2+x*StepSizeX,BoxHeight/2-(y+1)*StepSizeY,0,0,0,1,0,1,NormalArray,TexCoordArray));
      IndexArray.Add(AddVertex3_3_2(-BoxWidth/2+(x+1)*StepSizeX,BoxHeight/2-y*StepSizeY,0,0,0,1,1,0,NormalArray,TexCoordArray));

      IndexArray.Add(AddVertex3_3_2(-BoxWidth/2+(x+1)*StepSizeX,BoxHeight/2-y*StepSizeY,0,0,0,1,1,0,NormalArray,TexCoordArray));
      IndexArray.Add(AddVertex3_3_2(-BoxWidth/2+x*StepSizeX,BoxHeight/2-(y+1)*StepSizeY,0,0,0,1,0,1,NormalArray,TexCoordArray));
      IndexArray.Add(AddVertex3_3_2(-BoxWidth/2+(x+1)*StepSizeX,BoxHeight/2-(y+1)*StepSizeY,0,0,0,1,1,1,NormalArray,TexCoordArray));
    end;
  FCount:=IndexArray.Length;
  BuildGeometryInfo;
  CreateBuffers;
end;

procedure TRPMesh.CreateWall(const BoxWidth, BoxHeight, BoxDepth: GLfloat;
  WidthSubdivisions, HeightSubdivisions, DepthSubdivsions: integer);
  
  procedure Add(const AVector, ANormal: TVector3;
    const TextureX,TextureY: GLfloat);
  begin
    IndexArray.Add(AddVertex3_3_2(AVector.x,AVector.y,AVector.z,
                                  ANormal.x,ANormal.y,ANormal.z,
                                  TextureX,TextureY,
                                  NormalArray,TexCoordArray));
  end;
  
  procedure AddFace(const FacePos: TVector3;
    const Side1: TVector3; const Steps1: integer;
    const Side2: TVector3; const Steps2: integer);
  var
    ANormal: TVector3;
    LeftTop: TVector3;
    RightTop: TVector3;
    LeftBottom: TVector3;
    RightBottom: TVector3;
    x: Integer;
    y: Integer;
    StepSide1: TVector3;
    StepSide2: TVector3;
  begin
    //DebugLn('AddFace FacePos=',dbgs(FacePos),' Side1=',dbgs(Side1),' Side2=',dbgs(Side2));
    ANormal:=CrossProduct(Side1,Side2);
    StepSide1:=Side1/Steps1;
    StepSide2:=Side2/Steps2;
    Normalize(ANormal);
    for y:=0 to Steps2-1 do begin
      for x:=0 to Steps1-1 do begin
        LeftTop:=FacePos+StepSide1*x+StepSide2*y;
        LeftBottom:=LeftTop+StepSide1;
        RightTop:=LeftTop+StepSide2;
        RightBottom:=LeftBottom+StepSide2;

        // two triangles, counter clockwise
        Add(LeftTop,     ANormal, 0,0);
        Add(LeftBottom,  ANormal, 0,1);
        Add(RightTop,    ANormal, 1,0);

        Add(RightTop,    ANormal, 1,0);
        Add(LeftBottom,  ANormal, 0,1);
        Add(RightBottom, ANormal, 1,1);
      end;
    end;
  end;
  
var xDraw, yDraw, zDraw: GLfloat;
  HalfBoxWidth: GLfloat;
  HalfBoxHeight: GLfloat;
  HalfBoxDepth: GLfloat;
  LeftBottomBack: TVector3;
  SideX: TVector3;
  SideY: TVector3;
  SideZ: TVector3;
begin
  if FCount>0 then ClearArrays;
  xDraw:= -(BoxWidth/2);
  yDraw:= +(BoxHeight/2);
  zDraw:= +(BoxDepth/2);
  Primitive:=GL_TRIANGLES;
  BS.Radius:=sqrt(xDraw*xDraw+yDraw*yDraw+zDraw*zDraw);
  BS.Center.x:=0; BS.Center.y:=0; BS.Center.z:=0;
  BoundingBox.X:=BoxWidth/2;
  BoundingBox.Y:=BoxHeight/2;
  BoundingBox.Z:=BoxDepth/2;
  
  HalfBoxWidth:=BoxWidth/2;
  HalfBoxHeight:=BoxHeight/2;
  HalfBoxDepth:=BoxDepth/2;
  
  LeftBottomBack:=Vector(-HalfBoxWidth,-HalfBoxHeight,-HalfBoxDepth);
  SideX:=Vector(BoxWidth,0,0);
  SideY:=Vector(0,BoxHeight,0);
  SideZ:=Vector(0,0,BoxDepth);

  // Top Face
  AddFace(LeftBottomBack+SideY,SideZ,HeightSubdivisions,SideX,WidthSubdivisions);
  // Front Face
  AddFace(LeftBottomBack+SideZ,SideX,WidthSubdivisions,SideY,HeightSubdivisions);
  // Right Face
  AddFace(LeftBottomBack+SideX,SideY,HeightSubdivisions,SideZ,DepthSubdivsions);
  // Back Face
  AddFace(LeftBottomBack,SideY,HeightSubdivisions,SideX,WidthSubdivisions);
  // Left Face
  AddFace(LeftBottomBack,SideZ,DepthSubdivsions,SideY,HeightSubdivisions);
  // Bottom Face
  AddFace(LeftBottomBack,SideX,WidthSubdivisions,SideZ,DepthSubdivsions);

  FCount:=IndexArray.Length;
  BuildGeometryInfo;
  CreateBuffers;
end;

procedure TRPMesh.CreateWall(const BoxWidth, BoxHeight, BoxDepth: GLfloat);

  procedure Add(const AVector, ANormal: TVector3;
    const TextureX,TextureY: GLfloat);
  begin
    IndexArray.Add(AddVertex3_3_2(AVector.x,AVector.y,AVector.z,
                                  ANormal.x,ANormal.y,ANormal.z,
                                  TextureX,TextureY,
                                  NormalArray,TexCoordArray));
  end;

  procedure AddFace(const FacePos, Side1, Side2: TVector3);
  var
    ANormal: TVector3;
    LeftTop: TVector3;
    RightTop: TVector3;
    LeftBottom: TVector3;
    RightBottom: TVector3;
  begin
    //DebugLn('AddFace FacePos=',dbgs(FacePos),' Side1=',dbgs(Side1),' Side2=',dbgs(Side2));
    ANormal:=CrossProduct(Side1,Side2);
    Normalize(ANormal);
    LeftTop:=FacePos;
    LeftBottom:=LeftTop+Side1;
    RightTop:=LeftTop+Side2;
    RightBottom:=LeftBottom+Side2;
    // two triangles counter clockwise
    Add(LeftTop,     ANormal, 0,0);
    Add(LeftBottom,  ANormal, 0,1);
    Add(RightTop,    ANormal, 1,0);
    //DebugLn('AddFace RightTop=',dbgs(RightTop),' LeftBottom=',dbgs(LeftBottom),' RightBottom=',dbgs(RightBottom));
    Add(RightTop,    ANormal, 1,0);
    Add(LeftBottom,  ANormal, 0,1);
    Add(RightBottom, ANormal, 1,1);
  end;

var xDraw, yDraw, zDraw: GLfloat;
  HalfBoxWidth: GLfloat;
  HalfBoxHeight: GLfloat;
  HalfBoxDepth: GLfloat;
  LeftBottomBack: TVector3;
  SideX: TVector3;
  SideY: TVector3;
  SideZ: TVector3;
begin
  if FCount>0 then ClearArrays;
  xDraw:= -(BoxWidth/2);
  yDraw:= +(BoxHeight/2);
  zDraw:= +(BoxDepth/2);
  Primitive:=GL_TRIANGLES;
  BS.Radius:=sqrt(xDraw*xDraw+yDraw*yDraw+zDraw*zDraw);
  BS.Center.x:=0; BS.Center.y:=0; BS.Center.z:=0;
  BoundingBox.X:=BoxWidth/2;
  BoundingBox.Y:=BoxHeight/2;
  BoundingBox.Z:=BoxDepth/2;

  HalfBoxWidth:=BoxWidth/2;
  HalfBoxHeight:=BoxHeight/2;
  HalfBoxDepth:=BoxDepth/2;

  LeftBottomBack:=Vector(-HalfBoxWidth,-HalfBoxHeight,-HalfBoxDepth);
  SideX:=Vector(BoxWidth,0,0);
  SideY:=Vector(0,BoxHeight,0);
  SideZ:=Vector(0,0,BoxDepth);

  // Top Face
  AddFace(LeftBottomBack+SideY,SideZ,SideX);
  // Front Face
  AddFace(LeftBottomBack+SideZ,SideX,SideY);
  // Right Face
  AddFace(LeftBottomBack+SideX,SideY,SideZ);
  // Back Face
  AddFace(LeftBottomBack,SideY,SideX);
  // Left Face
  AddFace(LeftBottomBack,SideZ,SideY);
  // Bottom Face
  AddFace(LeftBottomBack,SideX,SideZ);

  FCount:=IndexArray.Length;
  BuildGeometryInfo;
  CreateBuffers;
end;

procedure TRPMesh.CreateSelection;
var
  bb: TVector3;
  C: TVector3;

  procedure Add(const x,y,z: GLfloat);
  begin
    IndexArray.Add(AddVertex3(C.x+x,C.y+y,C.z+z));
  end;

begin
  if FCount>0 then ClearArrays;
  //xDraw:= -(BoxWidth/2);
  //yDraw:= +(BoxHeight/2);
  //zDraw:= +(BoxDepth/2);
  Primitive:=GL_LINES;
  {BSRadius:=sqrt(xDraw*xDraw+yDraw*yDraw+zDraw*zDraw);
  BSCenter.x:=0; BSCenter.y:=0; BSCenter.z:=0;
  BoundingBox.X:=BoxWidth/2+0.1;
  BoundingBox.Y:=BoxHeight/2+0.1;
  BoundingBox.Z:=BoxDepth/2+0.1;}
  bb:=BoundingBox;
  C:=BS.Center;
  Add(bb.x,bb.y,bb.z);
  Add(bb.x-bb.x/2,bb.y,bb.z);
  Add(bb.x,bb.y,bb.z);
  Add(bb.x,bb.y-bb.y/2,bb.z);
  Add(bb.x,bb.y,bb.z);
  Add(bb.x,bb.y,bb.z-bb.z/2);
  //
  Add(-bb.x,bb.y,bb.z);
  Add(-bb.x+bb.x/2,bb.y,bb.z);
  Add(-bb.x,bb.y,bb.z);
  Add(-bb.x,bb.y-bb.y/2,bb.z);
  Add(-bb.x,bb.y,bb.z);
  Add(-bb.x,bb.y,bb.z-bb.z/2);
  //
  Add(bb.x,-bb.y,bb.z);
  Add(bb.x-bb.x/2,-bb.y,bb.z);
  Add(bb.x,-bb.y,bb.z);
  Add(bb.x,-bb.y+bb.y/2,bb.z);
  Add(bb.x,-bb.y,bb.z);
  Add(bb.x,-bb.y,bb.z-bb.z/2);
  //
  Add(bb.x,bb.y,-bb.z);
  Add(bb.x-bb.x/2,bb.y,-bb.z);
  Add(bb.x,bb.y,-bb.z);
  Add(bb.x,bb.y-bb.y/2,-bb.z);
  Add(bb.x,bb.y,-bb.z);
  Add(bb.x,bb.y,-bb.z+bb.z/2);
  //
  Add(-bb.x,-bb.y,bb.z);
  Add(-bb.x+bb.x/2,-bb.y,bb.z);
  Add(-bb.x,-bb.y,bb.z);
  Add(-bb.x,-bb.y+bb.y/2,bb.z);
  Add(-bb.x,-bb.y,bb.z);
  Add(-bb.x,-bb.y,bb.z-bb.z/2);
  //
  Add(-bb.x,bb.y,-bb.z);
  Add(-bb.x+bb.x/2,bb.y,-bb.z);
  Add(-bb.x,bb.y,-bb.z);
  Add(-bb.x,bb.y-bb.y/2,-bb.z);
  Add(-bb.x,bb.y,-bb.z);
  Add(-bb.x,bb.y,-bb.z+bb.z/2);
  //
  Add(bb.x,-bb.y,-bb.z);
  Add(bb.x-bb.x/2,-bb.y,-bb.z);
  Add(bb.x,-bb.y,-bb.z);
  Add(bb.x,-bb.y+bb.y/2,-bb.z);
  Add(bb.x,-bb.y,-bb.z);
  Add(bb.x,-bb.y,-bb.z+bb.z/2);
  //
  Add(-bb.x,-bb.y,-bb.z);
  Add(-bb.x+bb.x/2,-bb.y,-bb.z);
  Add(-bb.x,-bb.y,-bb.z);
  Add(-bb.x,-bb.y+bb.y/2,-bb.z);
  Add(-bb.x,-bb.y,-bb.z);
  Add(-bb.x,-bb.y,-bb.z+bb.z/2);
  //
  FCount:=IndexArray.Length;
  CreateBuffers;
end;

procedure TRPMesh.Release;
begin
  dec(FRefCount);
  if FRefCount=0 then Free;
end;

procedure TRPMesh.Reference;
begin
  inc(FRefCount);
end;

procedure TRPMesh.LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
begin
  fObjFilename:=XMLConfig.GetValue(Path+'ObjFilename/Value','');
  FDBPath:=XMLConfig.GetValue(Path+'DBPath/Value','');
  LoadedRPName:=XMLConfig.GetValue(Path+'RPName/Value','');
  if FDBPath<>'' then
    LoadMeshFromDB(FDBPath)
  else if fObjFilename<>'' then
    LoadMeshFromObjFile(fObjFilename);
end;

procedure TRPMesh.SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
begin
  XMLConfig.SetDeleteValue(Path+'RPName/Value',RPName,'');
  XMLConfig.SetDeleteValue(Path+'ObjFilename/Value',ObjFilename,'');
  XMLConfig.SetDeleteValue(Path+'DBPath/Value',DBPath,'');
end;

function TRPMesh.LoadMeshFromObjFile(const Filename: string): boolean;
var
  Data: TStringList;
begin
  ClearSource;
  Data:=TStringList.Create;
  try
    Data.LoadFromFile(Filename);
    Result:=LoadMeshFromObjStrings(Data);
  finally
    Data.Free;
  end;
  fObjFilename:=Filename;
end;

function TRPMesh.LoadMeshFromObjStream(AStream: TStream): boolean;
var
  Data: TStringList;
begin
  ClearSource;
  Data:=TStringList.Create;
  try
    Data.LoadFromStream(AStream);
    Result:=LoadMeshFromObjStrings(Data);
  finally
    Data.Free;
  end;
end;

function TRPMesh.LoadMeshFromDB(const DBPath: string): boolean;
var
  AStream: TStream;
begin
  ClearSource;
  AStream:=RPGLFilesDB.GetStream(DBPath);
  try
    Result:=LoadMeshFromObjStream(AStream);
  finally
    AStream.Free;
  end;
  FDBPath:=DBPath;
end;

function TRPMesh.LoadMeshFromObjStrings(const Data: TStrings): boolean;

  procedure RaiseError(const Msg: string);
  begin
    writeln('RaiseError "',Msg,'"');
    raise Exception.Create(Msg);
  end;

  procedure RaiseError(const Msg: string; LineNumber: integer);
  var
    s: String;
  begin
    s:=Msg;
    if LineNumber>=0 then
      s:=s+' Line='+IntToStr(LineNumber+1);
    RaiseError(s);
  end;

  procedure RaiseErrorTooBig(AValue, UpperBound: integer;
    LineNumber: integer =-1);
  var
    s: String;
  begin
    s:=IntToStr(AValue)+'>'+IntToStr(UpperBound);
    if LineNumber>=0 then
      s:=s+' Line='+IntToStr(LineNumber+1);
    RaiseError(s);
  end;

  procedure RaiseErrorLessThan1(AValue: integer; LineNumber: integer =-1);
  var
    s: String;
  begin
    s:=IntToStr(AValue)+'<1';
    if LineNumber>=0 then
      s:=s+' Line='+IntToStr(LineNumber+1);
    RaiseError(s);
  end;

  procedure ClearObject(out x; count: SizeInt);
  begin
    FillChar(x,count,0);
  end;

var
  ActualLine: string;
  start, i, j,
  VertexData, NormalData, TextureData, FaceData,
  VDAcnt, TDAcnt, NDAcnt,
  Groups: Integer;
  distance,
  lx, ly, lz,
  l1x, l1y, l1z,
  l2x, l2y: GLFloat;
  VD, ND, TD, FD,
  VIA, NIA, TIA: ArrayOfInteger;
  VDA, NDA, TDA: ArrayOfGLFloat;
  spaces, tris, quads: integer;
  minp, maxp: TVector3;
  ActualVIA: LongInt;
  ActualNIA: Integer;
  ActualTIA: Integer;
  FloatCntInLine: Integer;
  VDALength3: Integer;
  NDALength3: Integer;
  TDALength3: Integer;
  TexCoord: GLFloat;
begin
  ClearSource;
  if FCount>0 then ClearArrays;

  // init
  VertexData:=0; NormalData:=0; TextureData:=0; FaceData:=0;
  VDAcnt:=0; TDAcnt:=0; NDAcnt:=0; Groups:=0;
  ClearObject(VD,SizeOf(VD));
  ClearObject(ND,SizeOf(ND));
  ClearObject(TD,SizeOf(TD));
  ClearObject(FD,SizeOf(FD));
  ClearObject(VIA,SizeOf(VIA));
  ClearObject(NIA,SizeOf(NIA));
  ClearObject(TIA,SizeOf(TIA));
  ClearObject(VDA,SizeOf(VDA));
  ClearObject(NDA,SizeOf(NDA));
  ClearObject(TDA,SizeOf(TDA));


  // first run information gathering
  for i:=0 to Data.Count-1 do begin
    ActualLine:=Data[i];
    if length(ActualLine)>1 then begin
      case ActualLine[1] of
      'v':
        case ActualLine[2] of
          ' ': begin
                 inc(VertexData);
                 VD.Add(i);
               end;
          'n': begin
                 inc(NormalData);
                 ND.Add(i);
               end;
          't': begin
                 inc(TextureData);
                 TD.Add(i);
               end;
        end;
      'f': begin
             inc(FaceData);
             FD.Add(i);
           end;
      'g': begin
             inc(Groups);
           end;
      end;
    end;
  end;
  //
  //writeln(VertexData, ' Vertices');
  //writeln(TextureData, ' Texture Coords');
  //writeln(NormalData, ' Normals');
  //writeln(FaceData, ' Faces');
  //writeln(Groups,' Groups');
  if Groups>1 then begin
    writeln('Either this file contains multiple models or the model consists of multiple parts');
    //writeln('Since grouping is not yet supported model will not be loaded');
    writeln('model may not work correctly');
    //exit;
  end;

  //writeln('check primitive');
  // check primitive
  tris:=0; quads:=0;
  for i:=0 to FaceData-1 do begin
    spaces:=0;
    ActualLine:=Data[FD.Data[i]];
    for j:=3 to length(ActualLine)-1 do
      if (ActualLine[j-1]<>' ') and (ActualLine[j]=' ')
       and (ActualLine[j+1]<>' ') then inc(spaces);
    if spaces=2 then
      inc(tris)
    else
      if spaces=3 then
        inc(quads)
      else begin
        MessageDlg('Error','only tris and quads supported '+IntToStr(spaces),
          mtError,[mbCancel],0);
        DebugLn('TRPMesh.LoadMeshFromObjFile ',ActualLine);
        exit;
      end;
  end;
  //writeln('Tris: ',tris);
  //writeln('Quads: ',quads);
  if (tris<>0) and (quads=0) then
    Primitive:=GL_TRIANGLES
  else
    if (tris=0) and (quads<>0) then
      Primitive:=GL_QUADS
    else
      ShowMessage('Mixed primitives not yet supported');


  // second run to actually load the data
  // load verticies
  for i:=0 to VertexData-1 do begin
    ActualLine:=Data[VD.Data[i]];
    start:=3;
    while ActualLine[start]=' ' do inc(start);
    FloatCntInLine:=0;
    j:=start;
    repeat
      if (j>length(ActualLine)) or (ActualLine[j]=' ') then begin
        inc(VDAcnt);
        VDA.Add(StringToGLFloat(ActualLine,start,j-start));
        inc(FloatCntInLine);
        start:=j+1;
        if j>length(ActualLine) then break;
      end;
      inc(j);
    until false;
    if FloatCntInLine<>3 then RaiseError(ActualLine,VD.Data[i]);
  end;

  // load normals
  for i:=0 to NormalData-1 do begin
    ActualLine:=Data[ND.Data[i]];
    start:=4;
    while ActualLine[start]=' ' do inc(start);
    FloatCntInLine:=0;
    j:=start;
    repeat
      if (j>length(ActualLine)) or (ActualLine[j]=' ') then begin
        inc(NDAcnt);
        NDA.Add(StringToGLFloat(ActualLine,start,j-start));
        inc(FloatCntInLine);
        start:=j+1;
        if j>length(ActualLine) then break;
      end;
      inc(j);
    until false;
    if FloatCntInLine<>3 then RaiseError(ActualLine,ND.Data[i]);
  end;

  // load texture coords
  for i:=0 to TextureData-1 do begin
    ActualLine:=Data[TD.Data[i]];
    start:=4;
    while ActualLine[start]=' ' do inc(start);
    FloatCntInLine:=0;
    j:=start;
    repeat
      if (j>length(ActualLine)) or (ActualLine[j]=' ') then begin
        inc(TDAcnt);
        inc(FloatCntInLine);
        TexCoord:=StringToGLFloat(ActualLine,start,j-start);
        if FloatCntInLine=2 then
          TexCoord:=1.0-TexCoord;
        TDA.Add(TexCoord);
        start:=j+1;
        if j>length(ActualLine) then break;
      end;
      inc(j);
    until false;
    if FloatCntInLine<>3 then RaiseError(ActualLine,TD.Data[i]);
  end;

  VDALength3:=VDA.Length div 3;
  NDALength3:=NDA.Length div 3;
  TDALength3:=TDA.Length div 3;

  // create indexed arrays from face information
  //writeln('Creating indexed arrays from face information');
  if VertexData<>0 then begin
    for i:=0 to FaceData-1 do begin
      ActualLine:=Data[FD.Data[i]];
      start:=3;
      repeat
        // read 'vertexid' or 'vertexid/texid' or 'vertexid/texid/normalid'
        while (start<=length(ActualLine)) and (ActualLine[start]=' ') do
          inc(start);
        if start>length(ActualLine) then break;
        inc(Fcount);
        // read vertex id
        j:=start;
        while (j<=length(ActualLine)) and (not (ActualLine[j] in [' ','/']))
        do
          inc(j);
        ActualVIA:=StringToInteger(ActualLine,start,j-start);
        if ActualVIA>VDALength3 then RaiseErrorTooBig(ActualVIA,VDALength3,FD.Data[i]);
        if ActualVIA<=0 then RaiseErrorLessThan1(ActualVIA,FD.Data[i]);
        VIA.Add(ActualVIA);
        if (j<=length(ActualLine)) and (ActualLine[j]='/') then begin
          // read texture id
          inc(j);
          start:=j;
          while (j<=length(ActualLine)) and (not (ActualLine[j] in [' ','/']))
          do
            inc(j);
          if (TextureData>0) then begin
            ActualTIA:=StringToInteger(ActualLine,start,j-start);
            if ActualTIA>TDALength3 then RaiseErrorTooBig(ActualTIA,TDALength3,FD.Data[i]);
            if ActualTIA<=0 then RaiseErrorLessThan1(ActualTIA,FD.Data[i]);
            TIA.Add(ActualTIA);
          end;
          if (j<=length(ActualLine)) and (ActualLine[j]='/') then begin
            // read normal id
            inc(j);
            start:=j;
            while (j<=length(ActualLine))
            and (not (ActualLine[j] in [' ','/'])) do
              inc(j);
            if (NormalData>0) then begin
              ActualNIA:=StringToInteger(ActualLine,start,j-start);
              if ActualNIA>NDALength3 then RaiseErrorTooBig(ActualNIA,NDALength3,FD.Data[i]);
              if ActualNIA<=0 then RaiseErrorLessThan1(ActualNIA,FD.Data[i]);
              NIA.Add(ActualNIA);
            end;
          end else begin
            // missing normal id
            if NormalData>0 then
              RaiseError('missing normal',FD.Data[i]);
          end;
        end else begin
          // missing texture and normal id
          if (NormalData>0) or (TextureData>0) then
            RaiseError('missing texture and/or normal',FD.Data[i]);
        end;
        start:=j+1;
      until false;
    end;
  end;

  // create vbo usable arrays
  //writeln('Creating VBO usable arrays...');
  // vertices only
  if (NormalData=0) and (TextureData=0) then
    for i:=0 to count-1 do begin
      ActualVIA:=(VIA.Data[i]-1)*3;
      if cardinal(ActualVIA)>=VDA.Length then RaiseErrorTooBig(ActualVIA,VDA.Length-1);
      lx:=VDA.Data[ActualVIA];
      ly:=VDA.Data[ActualVIA+1];
      lz:=VDA.Data[ActualVIA+2];
      IndexArray.Add(AddVertex3(lx,ly,lz));
    end;
  // with normals
  if (NormalData<>0) and (TextureData=0) then
    for i:=0 to count-1 do begin
      ActualVIA:=(VIA.Data[i]-1)*3;
      if cardinal(ActualVIA)>=VDA.Length then RaiseErrorTooBig(ActualVIA,VDA.Length-1);
      lx:=VDA.Data[ActualVIA];
      ly:=VDA.Data[ActualVIA+1];
      lz:=VDA.Data[ActualVIA+2];
      ActualNIA:=(NIA.Data[i]-1)*3;
      if cardinal(ActualNIA)>=NDA.Length then RaiseErrorTooBig(ActualNIA,NDA.Length-1);
      l1x:=NDA.Data[ActualNIA];
      l1y:=NDA.Data[ActualNIA+1];
      l1z:=NDA.Data[ActualNIA+2];
      IndexArray.Add(AddVertex3_3(lx,ly,lz,l1x,l1y,l1z,NormalArray));
    end;
  // with texcoords
  if (NormalData=0) and (TextureData<>0) then
    for i:=0 to count-1 do begin
      ActualVIA:=(VIA.Data[i]-1)*3;
      if cardinal(ActualVIA)>=VDA.Length then RaiseErrorTooBig(ActualVIA,VDA.Length-1);
      lx:=VDA.Data[ActualVIA];
      ly:=VDA.Data[ActualVIA+1];
      lz:=VDA.Data[ActualVIA+2];
      ActualTIA:=(TIA.Data[i]-1)*3;
      if cardinal(ActualTIA)>=TDA.Length then RaiseErrorTooBig(ActualTIA,TDA.Length-1);
      l1x:=TDA.Data[ActualTIA];
      l1y:=TDA.Data[ActualTIA+1];
      //l1z:=TDA.Data[(TIA.Data[i]-1)*3+2];
      IndexArray.Add(AddVertex3_2(lx,ly,lz,l1x,l1y,TexCoordArray));
    end;
  // with texcoords and normals
  if (NormalData<>0) and (TextureData<>0) then
    for i:=0 to count-1 do begin
      ActualVIA:=(VIA.Data[i]-1)*3;
      if cardinal(ActualVIA)>=VDA.Length then RaiseErrorTooBig(ActualVIA,VDA.Length-1);
      lx:=VDA.Data[ActualVIA];
      ly:=VDA.Data[ActualVIA+1];
      lz:=VDA.Data[ActualVIA+2];
      ActualNIA:=(NIA.Data[i]-1)*3;
      if cardinal(ActualNIA)>=NDA.Length then RaiseErrorTooBig(ActualNIA,NDA.Length-1);
      l1x:=NDA.Data[ActualNIA];
      l1y:=NDA.Data[ActualNIA+1];
      l1z:=NDA.Data[ActualNIA+2];
      ActualTIA:=(TIA.Data[i]-1)*3;
      if cardinal(ActualTIA)>=TDA.Length then RaiseErrorTooBig(ActualTIA,TDA.Length-1);
      l2x:=TDA.Data[ActualTIA];
      l2y:=TDA.Data[ActualTIA+1];
      IndexArray.Add(AddVertex3_3_2(lx,ly,lz,l1x,l1y,l1z,l2x,l2y,NormalArray,
                                    TexCoordArray));
    end;
  // Calculate bounding sphere
  //writeln('Calculating bounding sphere');
  minp:=VertexArray.Data[IndexArray.Data[0]];
  maxp:=minp;
  for i:=1 to count-1 do begin
    // min
    j:=IndexArray.Data[i];
    if cardinal(j)>=VertexArray.Length then RaiseErrorTooBig(j,VertexArray.Length-1);
    if VertexArray.Data[j].x<minp.x then
      minp.x:=VertexArray.Data[j].x;
    if VertexArray.Data[j].y<minp.y then
      minp.y:=VertexArray.Data[j].y;
    if VertexArray.Data[j].z<minp.z then
      minp.z:=VertexArray.Data[j].z;
    // max
    if VertexArray.Data[j].x>maxp.x then
      maxp.x:=VertexArray.Data[j].x;
    if VertexArray.Data[j].y>maxp.y then
      maxp.y:=VertexArray.Data[j].y;
    if VertexArray.Data[j].z>maxp.z then
      maxp.z:=VertexArray.Data[j].z;
  end;
  BS.Center:=(maxp+minp)/2;
  BoundingBox:=maxp-BS.Center;
  //
  for i:=0 to count-1 do begin
    distance:=vLength(VertexArray.Data[IndexArray.Data[i]]-BS.Center);
    if distance>BS.Radius then begin
      BS.Radius:=distance;
    end;
  end;
  //writeln(VertexArray.Length div 3,' unique vertices created');
  //writeln('BSCenter: ',BS.Center.x,',',BS.Center.y,',',BS.Center.z,',');
  //writeln('BSRadius: ',BS.Radius);
  //writeln('Ready');
  //
  vda.free; nda.free; tda.free;
  via.free; nia.free; tia.free;
  fd.free; vd.free; nd.free; td.free;
  BuildGeometryInfo;
  CreateBuffers;
  Result:=true;
end;

procedure TRPMesh.ClearSource;
begin
  FObjFilename:='';
  FDBPath:='';
end;

{ TRPAsmObject }

function TRPAsmObject.IsMatrixEqual(AnObject: TAsmObject): boolean;
begin
  Result:=CompareMem(@fMatrix[0,0],@AnObject.fMatrix[0,0],SizeOf(TMatrix4x4));
  //DebugLn('TRPAsmObject.IsMatrixEqual ',dbgs(SizeOf(TMatrix4x4)),' ',dbgs(Result));
end;

procedure TRPAsmObject.CopyMatrix(AnObject: TAsmObject);
begin
  System.Move(AnObject.fMatrix[0,0],fMatrix[0,0],SizeOf(TMatrix4x4));
  fPosition:=AnObject.fPosition;
  fScale:=AnObject.fScale;
  fCoR:=AnObject.fCoR;
  fmaxScale:=AnObject.fmaxScale;
  fBS:=AnObject.fBS;
  if (Mesh<>nil) and (AnObject.Mesh<>nil) then begin
    Mesh.BS:=AnObject.Mesh.BS;
    Mesh.BoundingBox:=AnObject.Mesh.BoundingBox;
  end;
end;

procedure TRPAsmObject.ComputeBoundingBox(out Box: TRPAsmBox);
var
  BoxPoint: TVector3;
  BoxPoint1: TVector3;
  BoxPoint2: TVector3;
  BoxPoint3: TVector3;
begin
  FillChar(Box,SizeOf(Box),0);
  if Mesh=nil then exit;
  BoxPoint:=Mesh.BS.Center-Mesh.BoundingBox;
  BoxPoint1:=BoxPoint;  BoxPoint1.X+=Mesh.BoundingBox.X*2;
  BoxPoint2:=BoxPoint;  BoxPoint2.Y+=Mesh.BoundingBox.Y*2;
  BoxPoint3:=BoxPoint;  BoxPoint3.Z+=Mesh.BoundingBox.Z*2;
  BoxPoint:=iTransformVector(BoxPoint,fMatrix);
  BoxPoint1:=iTransformVector(BoxPoint1,fMatrix);
  BoxPoint2:=iTransformVector(BoxPoint2,fMatrix);
  BoxPoint3:=iTransformVector(BoxPoint3,fMatrix);

  Box.Point:=BoxPoint+Position;
  Box.Side1:=BoxPoint1-BoxPoint;
  Box.Side2:=BoxPoint2-BoxPoint;
  Box.Side3:=BoxPoint3-BoxPoint;
  {DebugLn('TRPAsmObject.ComputeBoundingBox Position=',dbgs(Position,4),
    ' Center=',dbgs(Mesh.BS.Center,4),
    ' Box=(Point=',dbgs(Box.Point,4),
    ' Side1=',dbgs(Box.Side1,4),' Side2=',dbgs(Box.Side2,4),' Side3=',dbgs(Box.Side3,4));}
end;

destructor TRPAsmObject.Destroy;
begin
  if Texture is TRPTexture then begin
    // Do not free it. It is reference counted by the lists.
    Texture:=nil;
  end;
  inherited Destroy;
end;

{ TRPMeshes }

function TRPMeshes.GetCount: integer;
begin
  Result:=FItems.Count;
end;

procedure TRPMeshes.ChangingName(Item: TRPMesh);
begin
  FItems.Remove(Item);
end;

procedure TRPMeshes.ChangedName(Item: TRPMesh);
begin
  FItems.Add(Item);
end;

constructor TRPMeshes.Create;
begin
  FItems:=TAVLTree.Create(@CompareRPMeshesByRPNames);
end;

destructor TRPMeshes.Destroy;
begin
  Clear;
  FItems.Free;
  inherited Destroy;
end;

procedure TRPMeshes.Clear;
var
  ANode: TAVLTreeNode;
  Mesh: TRPMesh;
begin
  ANode:=FItems.FindLowest;
  while ANode<>nil do begin
    Mesh:=TRPMesh(ANode.Data);
    Mesh.FContainers.Remove(Self);
    Mesh.Release;
    ANode:=FItems.FindSuccessor(ANode);
  end;
  FItems.Clear;
end;

function TRPMeshes.FindByRPName(const RPName: string): TRPMesh;
var
  ANode: TAVLTreeNode;
begin
  ANode:=FindNodeByRPName(RPName);
  if ANode<>nil then
    Result:=TRPMesh(ANode.Data)
  else
    Result:=nil;
end;

function TRPMeshes.GetMeshFromDB(const DBPath: string): TRPMesh;
begin
  Result:=FindByRPName(DBPath);
  if Result=nil then begin
    Result:=TRPMesh.Create;
    Result.RPName:=DBPath;
    Result.LoadMeshFromDB(DBPath);
    Add(Result);
  end else begin
    Result.Reference;
  end;
end;

function TRPMeshes.GetMeshFromObjFile(const RPName, Filename: string): TRPMesh;
begin
  Result:=FindByRPName(RPName);
  //DebugLn('TRPMeshes.GetMeshFromObjFile Name=',Name,' ',dbgs(Result));
  if Result=nil then begin
    Result:=TRPMesh.Create;
    Result.RPName:=RPName;
    Result.LoadMeshFromObjFile(Filename);
    Add(Result);
    //DebugLn('TRPMeshes.GetMeshFromObjFile Name=',Name,' Result.Name=',Result.RPName,' ',dbgs(FindByRPName(Result.RPName)));
  end else begin
    Result.Reference;
  end;
end;

function TRPMeshes.FindNodeByRPName(const RPName: string): TAVLTreeNode;
begin
  Result:=FItems.FindKey(PChar(RPName),@CompareNameWithRPMeshRPName);
end;

procedure TRPMeshes.Add(Mesh: TRPMesh);
begin
  Mesh.Reference;
  Mesh.RPName:=NewRPName(Mesh.RPName);
  Mesh.FContainers.Add(Self);
  FItems.Add(Mesh);
end;

function TRPMeshes.NewRPName(const StartName: string): string;
begin
  Result:=StartName;
  while FindNodeByRPName(Result)<>nil do
    Result:=CreateNextIdentifier(Result);
end;

{ TRPTexture }

function TRPTexture.GetContainerCount: integer;
begin
  Result:=FContainers.Count;
end;

function TRPTexture.GetContainers(Index: integer): TRPTextures;
begin
  Result:=TRPTextures(FContainers[Index]);
end;

procedure TRPTexture.SetRPName(const AValue: string);
var
  i: Integer;
begin
  if FRPName=AValue then exit;
  for i:=0 to ContainerCount-1 do
    Containers[i].ChangingName(Self);
  FRPName:=AValue;
  for i:=0 to ContainerCount-1 do
    Containers[i].ChangedName(Self);
end;

constructor TRPTexture.Create(AWidth, AHeight: integer);
begin
  inherited Create(AWidth,AHeight);
  FRefCount:=1;
  FContainers:=TFPList.Create;
end;

destructor TRPTexture.Destroy;
begin
  FContainers.Free;
  inherited Destroy;
end;

procedure TRPTexture.Release;
begin
  dec(FRefCount);
  if FRefCount=0 then Free;
end;

procedure TRPTexture.Reference;
begin
  inc(FRefCount);
end;

function TRPTexture.LoadFromFile(const Filename: string): TModalResult;
var
  ReaderClass: TFPCustomImageReaderClass;
  FileStream: TFileStream;
  MemStream: TMemoryStream;
  Reader: TFPCustomImageReader;
begin
  Result:=mrCancel;
  ClearSource;
  
  // open file
  FTextureFilename:=Filename;
  try
    ReaderClass:=GetFPImageReaderForFileExtension(ExtractFileExt(Filename));
    if ReaderClass=nil then begin
      DebugLn('TRPTexture.LoadFromFile unknown file extension ',Filename);
      exit;
    end;
    FileStream:=TFileStream.Create(Filename,fmOpenRead);
  except
    on E: Exception do begin
      DebugLn('TRPTexture.LoadFromFile Error reading file: ',E.Message);
      exit;
    end;
  end;
  MemStream:=nil;
  try
    // read file into mem
    MemStream:=TMemoryStream.Create;
    MemStream.CopyFrom(FileStream,FileStream.Size);
    // convert png stream to texture
    MemStream.Position:=0;
    Reader:=ReaderClass.Create;
    try
      LoadFromStream(MemStream,Reader);
      //DebugLn('TRPTexture.LoadFromFile ',dbgs(Width),' ',dbgs(Height));
      Bind;
    except
      on E: Exception do begin
        DebugLn('TRPTexture.LoadFromFile Error loading texture: ',E.Message);
        exit;
      end;
    end;
  finally
    FileStream.Free;
    MemStream.Free;
    Reader.Free;
  end;
  Result:=mrOk;
end;

function TRPTexture.LoadFromDB(const DBPath: string): TModalResult;
var
  ReaderClass: TFPCustomImageReaderClass;
  AStream: TStream;
  MemStream: TMemoryStream;
  Reader: TFPCustomImageReader;
begin
  Result:=mrCancel;
  ClearSource;
  
  // open DB file
  FTextureDBPath:=DBPath;
  try
    ReaderClass:=GetFPImageReaderForFileExtension(ExtractFileExt(DBPath));
    if ReaderClass=nil then begin
      DebugLn('TRPTexture.LoadFromDB unknown file extension ',DBPath);
      exit;
    end;
    AStream:=RPGLFilesDB.GetStream(DBPath);
  except
    on E: Exception do begin
      DebugLn('TRPTexture.LoadFromDB Error reading file: ',E.Message);
      exit;
    end;
  end;
  MemStream:=nil;
  try
    // read file into mem
    MemStream:=TMemoryStream.Create;
    MemStream.CopyFrom(AStream,AStream.Size);
    // convert png stream to texture
    MemStream.Position:=0;
    Reader:=ReaderClass.Create;
    try
      LoadFromStream(MemStream,Reader);
      //DebugLn('TRPTexture.LoadFromDB ',dbgs(Width),' ',dbgs(Height));
      Bind;
    except
      on E: Exception do begin
        DebugLn('TRPTexture.LoadFromDB Error loading texture: ',E.Message);
        exit;
      end;
    end;
  finally
    AStream.Free;
    MemStream.Free;
    Reader.Free;
  end;
  Result:=mrOk;
end;

procedure TRPTexture.ClearSource;
begin
  FTextureDBPath:='';
  FTextureFilename:='';
end;

{ TRPTextures }

function TRPTextures.GetCount: integer;
begin
  Result:=FItems.Count;
end;

procedure TRPTextures.ChangingName(Item: TRPTexture);
begin
  FItems.Remove(Item);
end;

procedure TRPTextures.ChangedName(Item: TRPTexture);
begin
  FItems.Add(Item);
end;

constructor TRPTextures.Create;
begin
  FItems:=TAVLTree.Create(@CompareRPTexturesByRPNames);
end;

destructor TRPTextures.Destroy;
begin
  Clear;
  FItems.Free;
  inherited Destroy;
end;

procedure TRPTextures.Clear;
var
  ANode: TAVLTreeNode;
  Texture: TRPTexture;
begin
  ANode:=FItems.FindLowest;
  while ANode<>nil do begin
    Texture:=TRPTexture(ANode.Data);
    Texture.FContainers.Remove(Self);
    Texture.Release;
    ANode:=FItems.FindSuccessor(ANode);
  end;
  FItems.Clear;
end;

function TRPTextures.FindByRPName(const RPName: string): TRPTexture;
var
  ANode: TAVLTreeNode;
begin
  ANode:=FindNodeByRPName(RPName);
  if ANode<>nil then
    Result:=TRPTexture(ANode.Data)
  else
    Result:=nil;
end;

function TRPTextures.GetTextureFromFile(const RPName, Filename: string
  ): TRPTexture;
begin
  Result:=FindByRPName(RPName);
  if Result=nil then begin
    Result:=TRPTexture.Create(0,0);
    Result.RPName:=RPName;
    Result.LoadFromFile(Filename);
    Add(Result);
  end else
    Result.Reference;
end;

function TRPTextures.GetTextureFromDB(const DBPath: string): TRPTexture;
begin
  Result:=FindByRPName(DBPath);
  if Result=nil then begin
    Result:=TRPTexture.Create(0,0);
    Result.RPName:=DBPath;
    Result.LoadFromDB(DBPath);
    Add(Result);
  end else
    Result.Reference;
end;

function TRPTextures.FindNodeByRPName(const RPName: string): TAVLTreeNode;
begin
  Result:=FItems.FindKey(PChar(RPName),@CompareNameWithRPTextureRPName);
end;

procedure TRPTextures.Add(Texture: TRPTexture);
begin
  Texture.Reference;
  Texture.RPName:=NewRPName(Texture.RPName);
  Texture.FContainers.Add(Self);
  FItems.Add(Texture);
  //DebugLn('TRPTextures.Add ',dbgs(FindByRPName(Texture.RPName)));
end;

function TRPTextures.NewRPName(const StartName: string): string;
begin
  Result:=StartName;
  while FindNodeByRPName(Result)<>nil do
    Result:=CreateNextIdentifier(Result);
end;

initialization
  RPTextures:=TRPTextures.Create;
  RPMeshes:=TRPMeshes.Create;
  
finalization
  RPMeshes.Free;
  RPMeshes:=nil;
  RPTextures.Free;
  RPTextures:=nil;

end.

