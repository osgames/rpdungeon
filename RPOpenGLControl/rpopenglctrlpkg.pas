{ This file was automatically created by Lazarus. Do not edit!
This source is only used to compile and install the package.
 }

unit RPOpenGLCtrlPkg; 

interface

uses
  RPOpenGLControl, RPGLUtils, RPGLObjects, RPGLStdObstacles, RPGLStdCombatants, 
    RPGLMesh, RPGLObjDB, LazarusPackageIntf; 

implementation

procedure Register; 
begin
  RegisterUnit('RPOpenGLControl', @RPOpenGLControl.Register); 
end; 

initialization
  RegisterPackage('RPOpenGLCtrlPkg', @Register); 
end.
