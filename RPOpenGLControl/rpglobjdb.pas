{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit RPGLObjDB;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil;

type

  { TCustomRPGLFileDB }

  TCustomRPGLFileDB = class(TPersistent)
  private
    FName: string;
  public
    constructor Create(const AName: string);
    function GetStream(const Path: string): TStream; virtual;
    function GetString(const Path: string): string; virtual;
  public
    property Name: string read FName write FName;
  end;
  
  { TRPGLFileDB }

  TRPGLFileDB = class(TCustomRPGLFileDB)
  private
    FBaseDirectory: string;
    procedure SetBaseDirectory(const AValue: string);
  public
    function GetStream(const Path: string): TStream; override;
  public
    property BaseDirectory: string read FBaseDirectory write SetBaseDirectory;
  end;

  { TRPGLFilesDB }

  TRPGLFilesDB = class
  private
    FItems: TFPList;
    function GetCount: Integer;
    function GetItems(Index: integer): TCustomRPGLFileDB;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Add(NewDB: TRPGLFileDB);
    function IndexOfName(const DBName: string): Integer;
    function ItemByName(const DBName: string): TCustomRPGLFileDB;
    function GetStream(const Path: string): TStream;
    function GetString(const Path: string): string;
    function GetDBPath(const Path: string; out SubPath: string): TCustomRPGLFileDB;
    property Count: Integer read GetCount;
    property Items[Index: integer]: TCustomRPGLFileDB read GetItems; default;
  end;
  
var
  RPGLFilesDB: TRPGLFilesDB;

implementation

{ TCustomRPGLFileDB }

constructor TCustomRPGLFileDB.Create(const AName: string);
begin
  FName:=AName;
end;

function TCustomRPGLFileDB.GetStream(const Path: string): TStream;
begin
  Result:=nil;
  if Path='' then ;
end;

function TCustomRPGLFileDB.GetString(const Path: string): string;
var
  AStream: TStream;
begin
  AStream:=GetStream(Path);
  if AStream<>nil then begin
    try
      SetLength(Result,AStream.Size);
      if Result<>'' then
        AStream.Read(Result[1],length(Result));
    finally
      AStream.Free;
    end;
  end else
    Result:='';
end;

{ TRPGLFilesDB }

function TRPGLFilesDB.GetCount: Integer;
begin
  Result:=FItems.Count;
end;

function TRPGLFilesDB.GetItems(Index: integer): TCustomRPGLFileDB;
begin
  Result:=TCustomRPGLFileDB(FItems[Index]);
end;

constructor TRPGLFilesDB.Create;
begin
  FItems:=TFPList.Create;
end;

destructor TRPGLFilesDB.Destroy;
begin
  Clear;
  FItems.Free;
  inherited Destroy;
end;

procedure TRPGLFilesDB.Clear;
var
  i: Integer;
begin
  for i:=0 to Count-1 do TObject(Items[i]).Free;
end;

procedure TRPGLFilesDB.Add(NewDB: TRPGLFileDB);
begin
  FItems.Add(NewDB);
end;

function TRPGLFilesDB.IndexOfName(const DBName: string): Integer;
begin
  for Result:=0 to Count-1 do
    if CompareText(Items[Result].Name,DBName)=0 then exit;
  Result:=-1;
end;

function TRPGLFilesDB.ItemByName(const DBName: string): TCustomRPGLFileDB;
var
  i: LongInt;
begin
  i:=IndexOfName(DBName);
  if i>=0 then
    Result:=Items[i]
  else
    Result:=nil;
end;

function TRPGLFilesDB.GetStream(const Path: string): TStream;
var
  SubPath: string;
  FileDB: TCustomRPGLFileDB;
begin
  FileDB:=GetDBPath(Path,SubPath);
  Result:=FileDB.GetStream(SubPath);
end;

function TRPGLFilesDB.GetString(const Path: string): string;
var
  SubPath: string;
  FileDB: TCustomRPGLFileDB;
begin
  FileDB:=GetDBPath(Path,SubPath);
  Result:=FileDB.GetString(SubPath);
end;

function TRPGLFilesDB.GetDBPath(const Path: string; out SubPath: string
  ): TCustomRPGLFileDB;
var
  p: LongInt;
  DBName: String;
begin
  p:=System.Pos('/',Path);
  DBName:=copy(Path,1,p-1);
  SubPath:=copy(Path,p+1,length(Path));
  Result:=ItemByName(DBName);
  if Result=nil then
    raise Exception.Create('FileDB "'+DBName+'" not found');
end;

{ TRPGLFileDB }

procedure TRPGLFileDB.SetBaseDirectory(const AValue: string);
var
  NewBaseDirectory: String;
begin
  NewBaseDirectory:=CleanAndExpandDirectory(AValue);
  if FBaseDirectory=NewBaseDirectory then exit;
  FBaseDirectory:=NewBaseDirectory;
end;

function TRPGLFileDB.GetStream(const Path: string): TStream;
var
  AFilename: String;
begin
  AFilename:=TrimFilename(AppendPathDelim(BaseDirectory)+SetDirSeparators(Path));
  Result:=TFileStream.Create(AFilename,fmOpenRead);
end;

initialization
  RPGLFilesDB:=TRPGLFilesDB.Create;

finalization
  RPGLFilesDB.Free;
  RPGLFilesDB:=nil;

end.

