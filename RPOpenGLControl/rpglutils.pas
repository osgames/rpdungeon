{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit RPGLUtils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, Forms, Controls, FPImage, Graphics,
  Vectors, AsmTypes, GL, AsmUtils;

type
  TRPAsmBox = record
    Point: TVector3;
    Side1, Side2, Side3: TVector3;
  end;
  
  TMatrix4xV3 = array[0..3] of TVector3;

procedure SetAsmObjectColor(AsmObject: TAsmObject; AColor: TColor);
function Max(const a, b: GLfloat): GLFloat; inline;
function Min(const a, b: GLfloat): GLFloat; inline;
function SolveMatrix(var Matrix: TMatrix4xV3; out Solution: TVector3): boolean;
function InverseMatrix(Matrix, hv: PGLfloat; RowPermutations: PInteger;
  n: integer): boolean;

function RayBoxIntersection(const RayOrigin, Ray: TVector3;
                            const Box: TRPAsmBox): boolean;
function RayBoxIntersection(const RayOrigin, Ray: TVector3;
                            const Box: TRPAsmBox;
                            out NearIntersectionPoint,
                            FarIntersectionPoint: TVector3): boolean;
function RayFaceIntersection(const RayOrigin, Ray: TVector3;
                             const FaceStart, FaceSide1, FaceSide2: TVector3;
                             out FaceFactor1, FaceFactor2, RayFactor: GLfloat
                             ): boolean;
function RayFaceIntersection(const RayOrigin, Ray: TVector3;
                             const FaceStart, FaceSide1, FaceSide2: TVector3
                             ): boolean;
procedure RayFaceIntersection(const RayOrigin, Ray: TVector3;
                              const FaceStart, FaceSide1, FaceSide2: TVector3;
                              var RayFactorsValid: boolean;
                              var NearRayFactor, FarRayFactor: GLfloat);

function CreateTextureFromFile(const Filename: string;
                               out Texture: TAsmTexture): TModalResult;


function DbgS(const v: TVector3; MaxDecimals: integer = 100): string; overload;
function DbgS(const v: TMatrix4xV3): string; overload;

function Middle(const a, b: TVector3): TVector3;

Operator =(const a, b: TSphere): boolean;

function StringToGLFloat(const Source: string; Start, Size: integer): GLfloat;
function StringToInteger(const Source: string; Start, Size: integer): integer;


implementation

var
  DecimalDividers: array[0..30] of GLFloat;

//todo: move to asmutils
function StringToGLFloat(const Source: string; Start, Size: integer): GLfloat;
var
  Negated: Boolean;
  p: PChar;
  EndPos: PChar;
  PointPos: PChar;
begin
  Result:=0;
  p:=@Source[Start];
  // read '-'
  if p^='-' then begin
    inc(p);
    Negated:=true;
  end else
    Negated:=false;
  // read numbers
  PointPos:=nil;
  EndPos:=@Source[Start+Size];
  while (p<EndPos) do begin
    case p^ of
    '0'..'9':
      Result:=Result*10+ord(p^)-ord('0');
    '.': begin
        PointPos:=p; // save '.' position
      end;
    else
      break;
    end;
    inc(p);
  end;
  // decimal point
  if PointPos<>nil then begin
    Result:=Result*DecimalDividers[PtrInt(p-PointPos-1)];
  end;
  // apply '-'
  if Negated then
    Result:=-Result;
end;

function StringToInteger(const Source: string; Start, Size: integer): integer;
var
  Negated: Boolean;
  p: PChar;
  EndPos: PChar;
begin
  Result:=0;
  p:=@Source[Start];
  // read '-'
  if p^='-' then begin
    inc(p);
    Negated:=true;
  end else
    Negated:=false;
  // read number
  EndPos:=@Source[Start+Size];
  while (p<EndPos) do begin
    case p^ of
    '0'..'9':
      Result:=Result*10+ord(p^)-ord('0');
    else
      break;
    end;
    inc(p);
  end;
  // apply '-'
  if Negated then
    Result:=-Result;
end;

function Max(const a, b: GLfloat): GLFloat; inline;
begin
  if a>b then Result:=a else Result:=b;
end;

function Min(const a, b: GLfloat): GLFloat; inline;
begin
  if a<b then Result:=a else Result:=b;
end;

function SolveMatrix(var Matrix: TMatrix4xV3; out Solution: TVector3
  ): boolean;
// First 3 columns of Matrix is A, last column is b
// Solve A * x = b
var
  MaxValue: GLfloat;
  MaxColumn: Integer;
  i: Integer;
  CurMax: GLfloat;
  Factor1, Factor2: GLfloat;
  SolutionArray: array[0..2] of GLfloat;
begin
  //writeln('Step START');
  //writeln(dbgs(Matrix));
  // find pivot element in first row
  MaxValue:=abs(Matrix[0].X);
  MaxColumn:=0;
  for i:=1 to 2 do begin
    CurMax:=abs(Matrix[i].X);
    if CurMax>MaxValue then begin
      MaxValue:=CurMax;
      MaxColumn:=i;
    end;
  end;
  if MaxValue=0 then exit(false); // A is singular
  // add first row to second and third row
  Factor1:=Matrix[MaxColumn].Y/Matrix[MaxColumn].X;
  Factor2:=Matrix[MaxColumn].Z/Matrix[MaxColumn].X;
  for i:=0 to 3 do
    if i<>MaxColumn then begin
      Matrix[i].Y -= Factor1*Matrix[i].X;
      Matrix[i].Z -= Factor2*Matrix[i].X;
    end else begin
      Matrix[i].Y := 0;
      Matrix[i].Z := 0;
    end;
  //writeln('Step Added first row');
  //writeln(dbgs(Matrix));
  // find pivot element in second row
  MaxValue:=abs(Matrix[0].Y);
  MaxColumn:=0;
  for i:=1 to 2 do begin
    CurMax:=abs(Matrix[i].Y);
    if CurMax>MaxValue then begin
      MaxValue:=CurMax;
      MaxColumn:=i;
    end;
  end;
  if MaxValue=0 then exit(false); // A is singular
  // add second row to third row
  Factor2:=Matrix[MaxColumn].Z/Matrix[MaxColumn].Y;
  for i:=0 to 3 do
    if i<>MaxColumn then begin
      Matrix[i].Z -= Factor2*Matrix[i].Y;
    end else begin
      Matrix[i].Z := 0;
    end;
  //writeln('Step Added second row');
  //writeln(dbgs(Matrix));
  // find pivot element in third row
  MaxValue:=abs(Matrix[0].Z);
  MaxColumn:=0;
  for i:=1 to 2 do begin
    CurMax:=abs(Matrix[i].Z);
    if CurMax>MaxValue then begin
      MaxValue:=CurMax;
      MaxColumn:=i;
    end;
  end;
  if MaxValue=0 then exit(false); // A is singular
  // scale pivot element to 1
  Matrix[3].Z /= Matrix[MaxColumn].Z;
  Matrix[MaxColumn].Z := 1;
  SolutionArray[MaxColumn]:=Matrix[3].Z;
  // add third row to first and second row
  Factor1:=Matrix[MaxColumn].X/Matrix[MaxColumn].Z;
  Factor2:=Matrix[MaxColumn].Y/Matrix[MaxColumn].Z;
  for i:=0 to 3 do
    if i<>MaxColumn then begin
      Matrix[i].X -= Factor1*Matrix[i].Z;
      Matrix[i].Y -= Factor2*Matrix[i].Z;
    end else begin
      Matrix[i].X := 0;
      Matrix[i].Y := 0;
    end;
  //writeln('Step Added third row');
  //writeln(dbgs(Matrix));
  // find pivot element in second row
  MaxValue:=abs(Matrix[0].Y);
  MaxColumn:=0;
  for i:=1 to 2 do begin
    CurMax:=abs(Matrix[i].Y);
    if CurMax>MaxValue then begin
      MaxValue:=CurMax;
      MaxColumn:=i;
    end;
  end;
  if MaxValue=0 then exit(false); // A is singular
  // scale pivot element to 1
  Matrix[3].Y /= Matrix[MaxColumn].Y;
  Matrix[MaxColumn].Y := 1;
  SolutionArray[MaxColumn]:=Matrix[3].Y;
  // add second row to first row
  Factor1:=Matrix[MaxColumn].X/Matrix[MaxColumn].Y;
  for i:=0 to 3 do
    if i<>MaxColumn then begin
      Matrix[i].X -= Factor1*Matrix[i].Y;
    end else begin
      Matrix[i].X := 0;
    end;
  //writeln('Step Added second row');
  //writeln(dbgs(Matrix));
  // find pivot element in first row
  MaxValue:=abs(Matrix[0].X);
  MaxColumn:=0;
  for i:=1 to 2 do begin
    CurMax:=abs(Matrix[i].X);
    if CurMax>MaxValue then begin
      MaxValue:=CurMax;
      MaxColumn:=i;
    end;
  end;
  if MaxValue=0 then exit(false); // A is singular
  // scale pivot element to 1
  Matrix[3].X /= Matrix[MaxColumn].X;
  Matrix[MaxColumn].X := 1;
  //writeln('Step Complete');
  //writeln(dbgs(Matrix));
  SolutionArray[MaxColumn]:=Matrix[3].X;
  // create solution
  Result:=true;
  Solution.X:=SolutionArray[0];
  Solution.Y:=SolutionArray[1];
  Solution.Z:=SolutionArray[2];
end;

function InverseMatrix(Matrix, hv: PGLfloat; RowPermutations: PInteger;
  n: integer): boolean;
var
  j: Integer;
  i: Integer;
  k: Integer;
  hi: Integer;
  hr: GLfloat;
  MaxValue: GLfloat;
  CurMax: GLfloat;
  r: LongInt;
begin
  for j:=0 to n-1 do RowPermutations[j]:=j;
  for j:=0 to n-1 do begin
    // search pivot
    MaxValue := abs(Matrix[j*n+j]);
    r := j;
    for i:=j+1 to n-1 do begin
      CurMax:=abs(Matrix[i*n+j]);
      if CurMax>MaxValue then begin
        MaxValue:=CurMax;
        r:=i;
      end;
    end;
    if MaxValue=0 then exit(false); // Matrix is singular
    // exchange rows
    if r>j then begin
      for k:=0 to n-1 do begin
        hr:=Matrix[j*n+k];
        Matrix[j*n+k]:=Matrix[r*n+k];
        Matrix[r*n+k]:=hr;
      end;
      hi:=RowPermutations[j];
      RowPermutations[j]:=RowPermutations[r];
      RowPermutations[r]:=hi;
    end;
    // transformation
    hr:=1/Matrix[j*n+j];
    for i:=0 to n-1 do Matrix[i*n+j]:=hr*Matrix[i*n+j];
    Matrix[j*n+j]:=hr;
    for k:=0 to n-1 do begin
      if k<>j then begin
        for i:=0 to n-1 do begin
          if i<>j then begin
            Matrix[i*n+k]:=Matrix[i*n+k]-Matrix[i*n+j]*Matrix[j*n+k];
          end;
        end;
        Matrix[j*n+k]:=-hr*Matrix[j*n+k];
      end;
    end;
  end;
  // exchange columns
  for i:=0 to n-1 do begin
    for k:=0 to n-1 do hv[RowPermutations[k]]:=Matrix[i*n+k];
    for k:=0 to n-1 do Matrix[i*n+k]:=hv[k];
  end;
end;

function RayBoxIntersection(const RayOrigin, Ray: TVector3; const Box: TRPAsmBox
  ): boolean;
begin
  Result:=
     RayFaceIntersection(RayOrigin,Ray,Box.Point,Box.Side1,Box.Side2)
  or RayFaceIntersection(RayOrigin,Ray,Box.Point,Box.Side1,Box.Side3)
  or RayFaceIntersection(RayOrigin,Ray,Box.Point,Box.Side2,Box.Side3)
  or RayFaceIntersection(RayOrigin,Ray,Box.Point+Box.Side3,Box.Side1,Box.Side2)
  or RayFaceIntersection(RayOrigin,Ray,Box.Point+Box.Side2,Box.Side1,Box.Side3)
  or RayFaceIntersection(RayOrigin,Ray,Box.Point+Box.Side1,Box.Side2,Box.Side3);
end;

function RayBoxIntersection(const RayOrigin, Ray: TVector3;
  const Box: TRPAsmBox; out NearIntersectionPoint,
  FarIntersectionPoint: TVector3): boolean;
var
  NearRayFactor, FarRayFactor: GLfloat;
begin
  Result:=false;
  NearRayFactor:=0;
  FarRayFactor:=0;
  RayFaceIntersection(RayOrigin,Ray,Box.Point,Box.Side1,Box.Side2,
                      Result,NearRayFactor,FarRayFactor);
  RayFaceIntersection(RayOrigin,Ray,Box.Point,Box.Side1,Box.Side3,
                      Result,NearRayFactor,FarRayFactor);
  RayFaceIntersection(RayOrigin,Ray,Box.Point,Box.Side2,Box.Side3,
                      Result,NearRayFactor,FarRayFactor);
  RayFaceIntersection(RayOrigin,Ray,Box.Point+Box.Side3,Box.Side1,Box.Side2,
                      Result,NearRayFactor,FarRayFactor);
  RayFaceIntersection(RayOrigin,Ray,Box.Point+Box.Side2,Box.Side1,Box.Side3,
                      Result,NearRayFactor,FarRayFactor);
  RayFaceIntersection(RayOrigin,Ray,Box.Point+Box.Side1,Box.Side2,Box.Side3,
                      Result,NearRayFactor,FarRayFactor);
  NearIntersectionPoint:=RayOrigin+Ray*NearRayFactor;
  FarIntersectionPoint:=RayOrigin+Ray*FarRayFactor;
end;

function RayFaceIntersection(const RayOrigin, Ray: TVector3;
  const FaceStart, FaceSide1, FaceSide2: TVector3;
  out FaceFactor1, FaceFactor2, RayFactor: GLfloat): boolean;
// Solve:  FaceStart + FaceSide1 * x + FaceSide2 * y = RayOrigin + Ray * z
// <=> FaceSide1 * x + FaceSide2 * y - Ray * z = RayOrigin - FaceStart
var
  Matrix: TMatrix4xV3;
  Solution: TVector3;
begin
  Matrix[0]:=FaceSide1;
  Matrix[1]:=FaceSide2;
  Matrix[2]:=-Ray;
  Matrix[3]:=RayOrigin-FaceStart;
  if SolveMatrix(Matrix,Solution) then begin
    Result:=true;
    FaceFactor1:=Solution.X;
    FaceFactor2:=Solution.Y;
    RayFactor:=Solution.Z;
  end else begin
    Result:=false;
  end;
end;

function RayFaceIntersection(const RayOrigin, Ray: TVector3; const FaceStart,
  FaceSide1, FaceSide2: TVector3): boolean;
var
  FaceFactor1, FaceFactor2, RayFactor: GLfloat;
begin
  if RayFaceIntersection(RayOrigin,Ray,FaceStart,FaceSide1,FaceSide2,
                         FaceFactor1,FaceFactor2,RayFactor)
  and (FaceFactor1>=0) and (FaceFactor1<=1)
  and (FaceFactor2>=0) and (FaceFactor2<=1) then
    exit(true)
  else
    exit(false);
  if RayFactor>0 then ;
end;

procedure RayFaceIntersection(const RayOrigin, Ray: TVector3; const FaceStart,
  FaceSide1, FaceSide2: TVector3; var RayFactorsValid: boolean;
  var NearRayFactor, FarRayFactor: GLfloat);
var
  FaceFactor1, FaceFactor2, RayFactor: GLfloat;
begin
  if RayFaceIntersection(RayOrigin,Ray,FaceStart,FaceSide1,FaceSide2,
                         FaceFactor1,FaceFactor2,RayFactor)
  and (FaceFactor1>=0) and (FaceFactor1<=1)
  and (FaceFactor2>=0) and (FaceFactor2<=1) then begin
    if (not RayFactorsValid) or (NearRayFactor>RayFactor) then
      NearRayFactor:=RayFactor;
    if (not RayFactorsValid) or (FarRayFactor<RayFactor) then
      FarRayFactor:=RayFactor;
    RayFactorsValid:=true;
  end;
  {DebugLn('RayFaceIntersection Face:(Start=',dbgs(FaceStart,6),
          ',Side1=',dbgs(FaceSide1,6)+
          ',Side2=',dbgs(FaceSide2,6)+
          ',Factor1='+dbgs(FaceFactor1,6)+
          ',Factor2='+dbgs(FaceFactor2,6)+')'+
          ' RayFactor='+dbgs(RayFactor,6)+
          ' Intersection='+dbgs(RayOrigin+Ray*RayFactor,5));}
end;

function CreateTextureFromFile(const Filename: string; out Texture: TAsmTexture
  ): TModalResult;
var
  ReaderClass: TFPCustomImageReaderClass;
  FileStream: TFileStream;
  MemStream: TMemoryStream;
  Reader: TFPCustomImageReader;
begin
  Result:=mrCancel;
  //DebugLn('CreateTextureFromFile Filename="',Filename,'"');
  // open file
  try
    ReaderClass:=GetFPImageReaderForFileExtension(ExtractFileExt(Filename));
    if ReaderClass=nil then begin
      DebugLn('CreateTextureFromFile unknown file extension ',Filename);
      exit;
    end;
    FileStream:=TFileStream.Create(Filename,fmOpenRead);
  except
    on E: Exception do begin
      DebugLn('CreateTextureFromFile Error reading file: ',E.Message);
      exit;
    end;
  end;
  MemStream:=nil;
  try
    // read file into mem
    MemStream:=TMemoryStream.Create;
    MemStream.CopyFrom(FileStream,FileStream.Size);
    // convert png stream to texture
    MemStream.Position:=0;
    Texture:=TAsmTexture.Create(0,0);
    Reader:=ReaderClass.Create;
    try
      Texture.LoadFromStream(MemStream,Reader);
      DebugLn('CreateTextureFromFile ',dbgs(Texture.Width),' ',dbgs(Texture.Height));
      Texture.Bind;
    except
      on E: Exception do begin
        Texture.Free;
        DebugLn('CreateTextureFromFile Error loading texture: ',E.Message);
        exit;
      end;
    end;
  finally
    FileStream.Free;
    MemStream.Free;
    Reader.Free;
  end;
  Result:=mrOk;
end;

function DbgS(const v: TVector3; MaxDecimals: integer = 100): string;
begin
  Result:='('+dbgs(v.x,MaxDecimals)+','+dbgs(v.y,MaxDecimals)+','+dbgs(v.z,MaxDecimals)+')';
end;

function DbgS(const v: TMatrix4xV3): string;

  function f(const p: GLfloat): string;
  begin
    Result:=copy(FloatToStr(p),1,8);
    while length(Result)<8 do Result:=' '+Result;
  end;

begin
  Result:=f(v[0].X)+' '+f(v[1].X)+' '+f(v[2].X)+' '+f(v[3].X)+LineEnding
         +f(v[0].Y)+' '+f(v[1].Y)+' '+f(v[2].Y)+' '+f(v[3].Y)+LineEnding
         +f(v[0].Z)+' '+f(v[1].Z)+' '+f(v[2].Z)+' '+f(v[3].Z)+LineEnding;
end;

function Middle(const a, b: TVector3): TVector3;
begin
  Result.X:=(a.X+b.X)/2;
  Result.Y:=(a.Y+b.Y)/2;
  Result.Z:=(a.Z+b.Z)/2;
end;

operator =(const a, b: TSphere): boolean;
begin
  Result:=(a.Center=b.Center) and (a.Radius=b.Radius);
end;

procedure SetAsmObjectColor(AsmObject: TAsmObject; AColor: TColor);
begin
  AsmObject.SetColor(GLfloat(Red(AColor))/256,
                     GLfloat(Green(AColor))/256,
                     GLfloat(Blue(AColor))/256,
                     1);
end;

procedure Init;
var
  i: Integer;
begin
  DecimalDividers[0]:=1;  // use constants for minimal rounding errors
  DecimalDividers[1]:=0.1;
  DecimalDividers[2]:=0.01;
  DecimalDividers[3]:=0.001;
  DecimalDividers[4]:=0.0001;
  DecimalDividers[5]:=0.00001;
  DecimalDividers[6]:=0.000001;
  DecimalDividers[7]:=0.0000001;
  DecimalDividers[8]:=0.00000001;
  DecimalDividers[9]:=0.000000001;
  for i:=10 to High(DecimalDividers) do
    DecimalDividers[i]:=DecimalDividers[i-1]/10;
    
end;

initialization
  Init;

end.

