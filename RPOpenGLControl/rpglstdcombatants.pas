{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit RPGLStdCombatants;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, Controls, LCLProc,
  RPGLObjects, RPGLUtils, RPGLMesh,
  GL, Vectors, Asmoday, AsmTypes;

type
  { TRPCombatantCreation }

  TRPCombatantCreation = class(TRPCreationItem)
  public
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
                        const Position: TVector3); override;
    function CreateCombatant(Shift: TShiftState): TRPCombatant; virtual;
    function CreateCombatantFromDB(const ObjPath, TexturePath: string
                                   ): TRPCombatant; virtual;
    function CreateCombatantFromFiles(const ObjFilename, TextureFilename: string
                                      ): TRPCombatant; virtual;
  end;

  { TRPBeholderCreation }

  TRPBeholderCreation = class(TRPCombatantCreation)
  public
    function CreateCombatant(Shift: TShiftState): TRPCombatant; override;
  end;

  { TRPMageCreation }

  TRPMageCreation = class(TRPCombatantCreation)
  public
    function CreateCombatant(Shift: TShiftState): TRPCombatant; override;
  end;

  { TRPFighterCreation }

  TRPFighterCreation = class(TRPCombatantCreation)
  public
    function CreateCombatant(Shift: TShiftState): TRPCombatant; override;
  end;

implementation

{ TRPCombatantCreation }

procedure TRPCombatantCreation.MouseDown(Button: TMouseButton; Shift: TShiftState;
  const Position: TVector3);
var
  NewObject: TRPCombatant;
begin
  if Button<>mbLeft then exit;
  NewObject:=CreateCombatant(Shift);
  if Shift=[] then ;
  // create Player
  AddObject(NewObject);
  NewObject.AsmObject.SetPosition(Position);
  SetAsmObjectColor(NewObject.AsmObject,NewObject.Color);
  AddObjectToScene(NewObject.AsmObject);
  NewObject.Release;
end;

function TRPCombatantCreation.CreateCombatant(Shift: TShiftState
  ): TRPCombatant;
begin
  if Shift=[] then ;
  Result:=TRPCombatant.Create(Name,rpocfCreateDefaults);
  Result.Color:=clBlue;
  with Result.AsmObject do begin
    (Mesh as TRPMesh).CreateCohen(0.7,0.2,1.8,20);
    BS.Radius:=Mesh.BS.Radius;
    Shader:=Lighting;
    Visible:=true;
    RotateAboutLocalX(-90);
  end;
  Result.CopyBoundingToCollision;
  Result.CollisionType:=rpbtCylinder;
end;

function TRPCombatantCreation.CreateCombatantFromFiles(const ObjFilename,
  TextureFilename: string): TRPCombatant;
begin
  Result:=TRPCombatant.Create(Name,Name,ObjFilename,Name,TextureFilename);
  Result.AsmObject.Visible:=true;
  Result.CopyBoundingToCollision;
  Result.CollisionType:=rpbtCylinder;
end;

function TRPCombatantCreation.CreateCombatantFromDB(const ObjPath,
  TexturePath: string): TRPCombatant;
begin
  Result:=TRPCombatant.Create(Name,ObjPath,TexturePath);
  Result.AsmObject.Visible:=true;
  Result.CopyBoundingToCollision;
  Result.CollisionType:=rpbtCylinder;
end;

{ TRPBeholderCreation }

function TRPBeholderCreation.CreateCombatant(Shift: TShiftState): TRPCombatant;
begin
  if Shift=[] then ;
  Result:=CreateCombatantFromDB('objects/monsters/beholder1.obj',
                                'objects/monsters/beholder1.png');
end;

{ TRPMageCreation }

function TRPMageCreation.CreateCombatant(Shift: TShiftState): TRPCombatant;
begin
  if Shift=[] then ;
  Result:=CreateCombatantFromDB('objects/mages/mage2.obj',
                                'objects/mages/mage2.png');
end;

{ TRPFighterCreation }

function TRPFighterCreation.CreateCombatant(Shift: TShiftState): TRPCombatant;
begin
  if Shift=[] then ;
  Result:=CreateCombatantFromDB('objects/fighter/fighter1.obj',
                                'objects/fighter/fighter1.png');
end;

initialization
  RPCreationItems.Add(TRPBeholderCreation.Create('Beholder'));
  RPCreationItems.Add(TRPMageCreation.Create('Mage'));
  RPCreationItems.Add(TRPFighterCreation.Create('Fighter'));

end.

