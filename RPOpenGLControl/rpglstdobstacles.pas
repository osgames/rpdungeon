{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit RPGLStdObstacles;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, Graphics, Controls, LCLProc,
  RPGLObjects, RPGLUtils, RPGLMesh,
  GL, Vectors, Asmoday, AsmUtils, AsmTypes;
  
type

  { TRPSingleClickObstacleCreation }

  TRPSingleClickObstacleCreation = class(TRPCreationItem)
  public
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
                        const Position: TVector3); override;
    function CreateObstacle(Shift: TShiftState; const Position: TVector3
                            ): TRPObstacle; virtual;
    function CreateObstacleFromDB(const ObjDBPath, TextureDBPath: string
                                  ): TRPObstacle; virtual;
    function CreateObstacleFromFiles(const ObjFilename, TextureFilename: string
                                     ): TRPObstacle; virtual;
  end;
  
  { TRPWallCreation }

  TRPWallCreation = class(TRPCreationItem)
  private
    FSettingWallEnd: boolean;
    FWallStart: TVector3;
    procedure SetSettingWallEnd(const AValue: boolean);
    procedure SetWallStart(const AValue: TVector3);
  protected
    procedure CreateMouseMarker; override;
  public
    procedure CreateWall(const StartPos, EndPos: TVector3);
    procedure MoveMarker(Shift: TShiftState; const Position: TVector3); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
                        const Position: TVector3); override;
    property SettingWallEnd: boolean read FSettingWallEnd write SetSettingWallEnd;
    property WallStart: TVector3 read FWallStart write SetWallStart;
  end;

  { TRPChestCreation }

  TRPChestCreation = class(TRPSingleClickObstacleCreation)
  public
    function CreateObstacle(Shift: TShiftState; const Position: TVector3
                            ): TRPObstacle; override;
  end;

  { TRPDoorCreation }

  TRPDoorCreation = class(TRPSingleClickObstacleCreation)
  public
    function CreateObstacle(Shift: TShiftState; const Position: TVector3
                            ): TRPObstacle; override;
  end;

implementation

{ TRPSingleClickObstacleCreation }

procedure TRPSingleClickObstacleCreation.MouseDown(Button: TMouseButton;
  Shift: TShiftState; const Position: TVector3);
var
  NewObject: TRPObstacle;
begin
  if Button<>mbLeft then exit;
  NewObject:=CreateObstacle(Shift,Position);
  AddObject(NewObject);
  SetAsmObjectColor(NewObject.AsmObject,NewObject.Color);
  AddObjectToScene(NewObject.AsmObject);
  NewObject.Release;
end;

function TRPSingleClickObstacleCreation.CreateObstacle(Shift: TShiftState;
  const Position: TVector3): TRPObstacle;
begin
  if Shift=[] then ;
  // create Tree obstacle
  Result:=TRPObstacle.Create(Name,rpocfCreateDefaults);
  Result.Color:=clGreen;
  with Result.AsmObject do begin
    (Mesh as TRPMesh).CreateCohen(1,0.1,5,10);
    BS.Radius:=Mesh.BS.Radius;
    Shader:=Lighting;
    Visible:=true;
    RotateAboutLocalX(-90);
  end;
  Result.AsmObject.SetPosition(Position+Vector(0,2.5,0));
end;

function TRPSingleClickObstacleCreation.CreateObstacleFromDB(const ObjDBPath,
  TextureDBPath: string): TRPObstacle;
begin
  Result:=TRPObstacle.Create(Name,ObjDBPath,TextureDBPath);
  Result.AsmObject.Visible:=true;
  Result.CopyBoundingToCollision;
  Result.CollisionType:=rpbtCylinder;
end;

function TRPSingleClickObstacleCreation.CreateObstacleFromFiles(
  const ObjFilename, TextureFilename: string): TRPObstacle;
begin
  Result:=TRPObstacle.Create(Name,Name,ObjFilename,Name,TextureFilename);
  Result.AsmObject.Visible:=true;
  Result.CopyBoundingToCollision;
  Result.CollisionType:=rpbtCylinder;
end;

{ TRPWallCreation }

procedure TRPWallCreation.SetWallStart(const AValue: TVector3);
begin
  if FWallStart=AValue then exit;
  FWallStart:=AValue;
end;

procedure TRPWallCreation.CreateMouseMarker;
begin
  if SettingWallEnd then begin
    FreeMouseMarker;
    MouseMarker:=TRPAsmObject.Create(nil);
    with MouseMarker do begin
      Mesh:=TRPMesh.Create;
      (Mesh as TRPMesh).CreateBox(1,2,0.1);
      BS.Radius:=Mesh.BS.Radius;
      Shader:=Lighting;
      Visible:=true;
    end;
    SetAsmObjectColor(MouseMarker,clGray);
    AddObjectToScene(MouseMarker);
  end else begin
    inherited CreateMouseMarker;
  end;
end;

procedure TRPWallCreation.CreateWall(const StartPos, EndPos: TVector3);
var
  RotationAngle: Single;
  WallLength: Single;
  NewObject: TRPObstacle;
begin
  // create Wall obstacle
  NewObject:=TRPObstacle.Create('Wall',rpocfCreateDefaults);
  AddObject(NewObject);

  WallLength:=vLength(StartPos-EndPos);

  with NewObject.AsmObject do begin
    (Mesh as TRPMesh).CreateWall(WallLength,2,0.5,ceil(WallLength/2),1,1);
    //(Mesh as TRPMesh).CreateBox(WallLength,2,0.5);
    BS.Radius:=Mesh.BS.Radius;
    Shader:=LightingTexture;
    Visible:=true;
  end;
  NewObject.AsmObject.Texture:=
                     RPTextures.GetTextureFromDB('objects/textures/stone2.png');
  NewObject.FreeTexture:=true;

  if WallLength>0.1 then begin
    // rotate wall
    RotationAngle:=Angle(Vector(1,0,0),EndPos-StartPos);
    if EndPos.Z>StartPos.Z then
      RotationAngle:=-RotationAngle;
    NewObject.AsmObject.Rotate(RotationAngle,0,1,0);
  end;
  // place center between Start and End
  NewObject.AsmObject.SetPosition((EndPos+StartPos)/2+Vector(0,1,0));

  SetAsmObjectColor(NewObject.AsmObject,NewObject.Color);
  AddObjectToScene(NewObject.AsmObject);
  NewObject.Release;
end;

procedure TRPWallCreation.MoveMarker(Shift: TShiftState;
  const Position: TVector3);
var
  RotationAngle: GLfloat;
  WallLength: Single;
begin
  if SettingWallEnd then begin
    // span wall between WallStart and Position
    MouseMarker.ResetRotation;
    MouseMarker.ResetScale;
    WallLength:=vLength(WallStart-Position);
    if WallLength>0.1 then begin
      // scale wall from WallStart to Position
      MouseMarker.SetScale(WallLength,1,1);
      // rotate wall from WallStart to Position
      RotationAngle:=Angle(Vector(1,0,0),Position-WallStart);
      if Position.Z>WallStart.Z then
        RotationAngle:=-RotationAngle;
      //DebugLn('TRPWallCreation.MoveMarker WallLength=',dbgs(WallLength),' RotationAngle=',dbgs(RotationAngle));
      MouseMarker.Rotate(RotationAngle,0,1,0);
    end;
    // place center between WallStart and Position
    MouseMarker.SetPosition(((WallStart+Position)/2)+Vector(0,1,0));
    Invalidate;
  end else begin
    inherited MoveMarker(Shift, Position);
  end;
end;

procedure TRPWallCreation.SetSettingWallEnd(const AValue: boolean);
begin
  if FSettingWallEnd=AValue then exit;
  FSettingWallEnd:=AValue;
  CreateMouseMarker;
  Invalidate;
end;

procedure TRPWallCreation.MouseDown(Button: TMouseButton; Shift: TShiftState;
  const Position: TVector3);
begin
  if Button<>mbLeft then begin
    // cancel
    SettingWallEnd:=false;
  end else if SettingWallEnd then begin
    // create wall
    CreateWall(WallStart,Position);
    if ssShift in Shift then begin
      // start the next wall immediately
      WallStart:=Position;
    end else begin
      // end wall
      SettingWallEnd:=false;
    end;
  end else begin
    // set WallStart. Next the user should choose the WallEnd position
    WallStart:=Position;
    SettingWallEnd:=true;
  end;
  MoveMarker(Shift,Position);
end;

{ TRPChestCreation }

function TRPChestCreation.CreateObstacle(Shift: TShiftState;
  const Position: TVector3): TRPObstacle;
begin
  if Shift=[] then ;
  Result:=CreateObstacleFromDB('objects/chests/chest1.obj',
                               'objects/chests/chest1.png');
  Result.AsmObject.SetPosition(Position);
end;

{ TRPDoorCreation }

function TRPDoorCreation.CreateObstacle(Shift: TShiftState;
  const Position: TVector3): TRPObstacle;
begin
  if Shift=[] then ;
  Result:=CreateObstacleFromDB('objects/doors/door1.obj',
                               'objects/doors/door1.png');
  Result.AsmObject.SetPosition(Position);
end;

initialization
  RPCreationItems.Add(TRPWallCreation.Create('Wall'));
  RPCreationItems.Add(TRPChestCreation.Create('Chest'));
  RPCreationItems.Add(TRPDoorCreation.Create('Door'));

end.

