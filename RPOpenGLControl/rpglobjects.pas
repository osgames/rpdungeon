{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit RPGLObjects;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, Controls, LCLProc, GL, AsmTypes, AVL_Tree,
  Vectors, Asmoday, AsmUtils, RPGLUtils, RPGLMesh, ComCtrls;

type
  TRPObjectCreationFlag = (
    rpocfCreateAsmObject,
    rpocfCreateMesh
    );
  TRPObjectCreationFlags = set of TRPObjectCreationFlag;

const
  rpocfCreateDefaults = [rpocfCreateAsmObject,rpocfCreateMesh];


type
  TRPBoundingType = (
    rpbtSphere,
    rpbtBox,
    rpbtCylinder  // Height = BoundingBox.Y, Radius = BoundingShere.Radius
    );

  TRPObjects = class;

  { TRPObject }

  TRPObject = class(TPersistent)
  private
    FAsmObject: TRPAsmObject;
    FBoundingType: TRPBoundingType;
    FCollisionBox: TVector3;
    FCollisionSphere: TSphere;
    FCollisionType: TRPBoundingType;
    FColor: TColor;
    FContainers: TFPList;
    FFreeAsmObject: Boolean;
    FFreeMesh: Boolean;
    FFreeTexture: Boolean;
    FName: string;
    FRefCount: Integer;
    function GetContainerCount: integer;
    function GetContainers(Index: Integer): TRPObjects;
    procedure SetAsmObject(const AValue: TRPAsmObject);
    procedure SetBoundingType(const AValue: TRPBoundingType);
    procedure SetCollisionBox(const AValue: TVector3);
    procedure SetCollisionSphere(const AValue: TSphere);
    procedure SetCollisionType(const AValue: TRPBoundingType);
    procedure SetColor(const AValue: TColor);
    procedure SetName(const AValue: string);
  public
    constructor Create(const AName: string; Flags: TRPObjectCreationFlags = [];
                       Mesh: TRPMesh = nil);
    constructor Create(const AName, MeshDBPath, TextureDBPath: string);
    constructor Create(const AName, MeshName, ObjFilename,
                       TextureName, TextureFilename: string);
    destructor Destroy; override;
  public
    property Color: TColor read FColor write SetColor;
    property AsmObject: TRPAsmObject read FAsmObject write SetAsmObject;
    property FreeAsmObject: Boolean read FFreeAsmObject write FFreeAsmObject;
    property FreeMesh: Boolean read FFreeMesh write FFreeMesh;
    property FreeTexture: Boolean read FFreeTexture write FFreeTexture;
    procedure Reference;
    procedure Release;
    procedure LeaveContainers;
    procedure CopyBoundingToCollision;
  public
    property RefCount: Integer read FRefCount;
    property BoundingType: TRPBoundingType read FBoundingType write SetBoundingType;
    property ContainerCount: integer read GetContainerCount;
    property Containers[Index: Integer]: TRPObjects read GetContainers;
    property CollisionType: TRPBoundingType read FCollisionType write SetCollisionType;
    property CollisionSphere: TSphere read FCollisionSphere write SetCollisionSphere;
    property CollisionBox: TVector3 read FCollisionBox write SetCollisionBox; // distance from CollisionSphere.Center
  published
    property Name: string read FName write SetName;
  end;
  TRPObjectClass = class of TRPObject;

  { TRPObjects }

  TRPObjects = class(TPersistent)
  private
    function GetCount: Integer;
  protected
    FObjectClass: TRPObjectClass;
    function GetDefaultObjectClass: TRPObjectClass; virtual;
    procedure ChangingName(Item: TRPObject);
    procedure ChangedName(Item: TRPObject);
    procedure ObjectLeaves(Item: TRPObject);
  public
    Objects: TAVLTree;// tree of TRPObject sorted for name
    constructor Create; virtual;
    destructor Destroy; override;
    function FindNodeByName(const Name: string): TAVLTreeNode;
    function FindByName(const Name: string): TRPObject;
    procedure Add(AnObject: TRPObject);
    function FindAtRay(const RayOrigin, RayDirection: TVector3): TRPObject;
    function NewName(const StartName: string): string;
    property ObjectClass: TRPObjectClass read FObjectClass;
  public
    property Count: Integer read GetCount;
  end;

  { TRPCombatant }

  TRPCombatant = class(TRPObject)
  end;

  { TRPCombatants }

  TRPCombatants = class(TRPObjects)
  public
    function GetDefaultObjectClass: TRPObjectClass; override;
  end;

  { TRPObstacle }

  TRPObstacle = class(TRPObject)
  end;

  { TRPObstacles }

  TRPObstacles = class(TRPObjects)
  public
    function GetDefaultObjectClass: TRPObjectClass; override;
  end;
  
  TRPCreationItems = class;

  { TRPCreationItem }

  TRPCreationItem = class(TPersistent)
  private
    FMouseMarker: TRPAsmObject;
    FName: string;
    FOwner: TRPCreationItems;
    FSelected: boolean;
    procedure SetMouseMarker(const AValue: TRPAsmObject);
    procedure SetName(const AValue: string);
    procedure SetOwner(const AValue: TRPCreationItems);
    procedure SetSelected(const AValue: boolean);
  protected
    procedure FreeMouseMarker; virtual;
    procedure CreateMouseMarker; virtual;
    procedure AddObject(AnObject: TRPObject);
    procedure AddObjectToScene(AnObject: TRPAsmObject);
    procedure RemoveObjectFromScene(AnObject: TRPAsmObject);
  public
    constructor Create(const AName: string); virtual;
    destructor Destroy; override;
    function GetCaption: string; virtual;
    procedure MoveMarker(Shift: TShiftState; const Position: TVector3); virtual;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
                        const Position: TVector3); virtual; abstract;
    procedure Invalidate;
  public
    property Name: string read FName write SetName;
    property MouseMarker: TRPAsmObject read FMouseMarker write SetMouseMarker;
    property Selected: boolean read FSelected write SetSelected;
    property Owner: TRPCreationItems read FOwner write SetOwner;
  end;
  
  TRPOnInvalidate = procedure of object;
  TRPOnAddRemoveObjectToScene = procedure(AnObject: TRPAsmObject) of object;
  TRPOnAddRemoveRPObject = procedure(AnObject: TRPObject) of object;

  { TRPCreationItems }

  TRPCreationItems = class(TPersistent)
  private
    FItems: TFPList;
    FOnAddObject: TRPOnAddRemoveRPObject;
    FOnAddObjectToScene: TRPOnAddRemoveObjectToScene;
    FOnInvalidate: TRPOnInvalidate;
    FOnRemoveObjectFromScene: TRPOnAddRemoveObjectToScene;
    function GetCount: Integer;
    function GetItems(Index: integer): TRPCreationItem;
  protected
    procedure DoAdd(Item: TRPCreationItem);
    procedure DoRemove(Item: TRPCreationItem);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Add(Item: TRPCreationItem);
    procedure FillTreeView(ATreeView: TTreeView);
    function FindByName(const AName: string): TRPCreationItem;
    function NewName(const StartName: string): string;
    procedure Invalidate;
    procedure AddObjectToScene(AnObject: TRPAsmObject);
    procedure RemoveObjectFromScene(AnObject: TRPAsmObject);
    procedure AddObject(AnObject: TRPObject);
    procedure RemoveHooks(HookOwner: TObject);
  public
    property Count: Integer read GetCount;
    property Items[Index: integer]: TRPCreationItem read GetItems;
    property OnInvalidate: TRPOnInvalidate read FOnInvalidate write FOnInvalidate;
    property OnAddObject: TRPOnAddRemoveRPObject read FOnAddObject write FOnAddObject;
    property OnAddObjectToScene: TRPOnAddRemoveObjectToScene
                             read FOnAddObjectToScene write FOnAddObjectToScene;
    property OnRemoveObjectFromScene: TRPOnAddRemoveObjectToScene
                   read FOnRemoveObjectFromScene write FOnRemoveObjectFromScene;
  end;
  
var
  RPCreationItems: TRPCreationItems;

function CompareRPObjectNames(Data1, Data2: Pointer): Integer;
function CompareNameWithRPObject(Key, Data: Pointer): Integer;

function FindTreePRObjectAtRay(TreeOfCombatants: TAVLTree;
  const RayOrigin, RayDirection: TVector3): TRPObject;

implementation

function FindTreePRObjectAtRay(TreeOfCombatants: TAVLTree; const RayOrigin,
  RayDirection: TVector3): TRPObject;
var
  ANode: TAVLTreeNode;
  RPObject: TRPObject;
  AsmObject: TRPAsmObject;
  NearIntersectionPoint, FarIntersectionPoint: TVector3;
  BestDistance: Single;
  BestRPObject: TRPObject;
  CurDistance: Single;
  Intersecting: Boolean;
  Box: TRPAsmBox;
begin
  BestRPObject:=nil;
  BestDistance:=-1;
  ANode:=TreeOfCombatants.FindLowest;
  while ANode<>nil do begin
    RPObject:=TRPObject(ANode.Data);
    AsmObject:=RPObject.AsmObject;
    NearIntersectionPoint:=Vector(0,0,0);
    FarIntersectionPoint:=Vector(0,0,0);
    case RPObject.BoundingType of
    rpbtSphere:
      Intersecting:=RaySphereIntersection(RayOrigin, RayDirection,
        AsmObject.Mesh.BS.Center+AsmObject.Position,
        AsmObject.Mesh.BS.Radius,NearIntersectionPoint,FarIntersectionPoint);
    rpbtBox:
      begin
        // quick test with sphere
        Intersecting:=RaySphereIntersection(RayOrigin, RayDirection,
          AsmObject.Mesh.BS.Center+AsmObject.Position,
          AsmObject.Mesh.BS.Radius,NearIntersectionPoint,FarIntersectionPoint);
        if Intersecting then begin
          // slow test with box
          AsmObject.ComputeBoundingBox(Box);
          //DebugLn('FindTreePRObjectAtRay Point=',dbgs(Box.Point,6),' Side1=',dbgs(Box.Side1,6),' Side2=',dbgs(Box.Side2,6),' Side3=',dbgs(Box.Side3,6),' RayOrigin=',dbgs(RayOrigin,6),' Ray=',dbgs(RayDirection,6));
          Intersecting:=RayBoxIntersection(RayOrigin, RayDirection,
                                Box,NearIntersectionPoint,FarIntersectionPoint);
        end;
      end;
    else
      Intersecting:=false;
    end;
    if Intersecting then begin
      CurDistance:=vLength(NearIntersectionPoint-RayOrigin);
      if CurDistance>0 then begin
        if (BestDistance<0) or (CurDistance<BestDistance) then begin
          BestDistance:=CurDistance;
          BestRPObject:=RPObject;
        end;
      end;
    end;
    ANode:=TreeOfCombatants.FindSuccessor(ANode);
  end;
  Result:=BestRPObject;
end;

function CompareRPObjectNames(Data1, Data2: Pointer): Integer;
begin
  Result:=CompareText(TRPCombatant(Data1).Name,TRPCombatant(Data2).Name);
end;

function CompareNameWithRPObject(Key, Data: Pointer): Integer;
var
  Name: String;
begin
  Name:='';
  Pointer(Name):=Key;
  Result:=CompareText(Name,TRPCombatant(Data).Name);
  Pointer(Name):=nil;
end;

{ TRPCombatants }

function TRPCombatants.GetDefaultObjectClass: TRPObjectClass;
begin
  Result:=TRPCombatant;
end;

{ TRPObject }

procedure TRPObject.SetAsmObject(const AValue: TRPAsmObject);
begin
  if FAsmObject=AValue then exit;
  FAsmObject:=AValue;
end;

function TRPObject.GetContainers(Index: Integer): TRPObjects;
begin
  Result:=TRPObjects(FContainers[Index]);
end;

function TRPObject.GetContainerCount: integer;
begin
  Result:=FContainers.Count;
end;

procedure TRPObject.SetBoundingType(const AValue: TRPBoundingType);
begin
  if FBoundingType=AValue then exit;
  FBoundingType:=AValue;
end;

procedure TRPObject.SetCollisionBox(const AValue: TVector3);
begin
  if FCollisionBox=AValue then exit;
  FCollisionBox:=AValue;
end;

procedure TRPObject.SetCollisionSphere(const AValue: TSphere);
begin
  if FCollisionSphere=AValue then exit;
  FCollisionSphere:=AValue;
end;

procedure TRPObject.SetCollisionType(const AValue: TRPBoundingType);
begin
  if FCollisionType=AValue then exit;
  FCollisionType:=AValue;
end;

procedure TRPObject.SetColor(const AValue: TColor);
begin
  if FColor=AValue then exit;
  FColor:=AValue;
end;

procedure TRPObject.SetName(const AValue: string);
var
  i: Integer;
begin
  if FName=AValue then exit;
  for i:=0 to FContainers.Count-1 do
    TRPObjects(FContainers[i]).ChangingName(Self);
  FName:=AValue;
  for i:=0 to FContainers.Count-1 do
    TRPObjects(FContainers[i]).ChangedName(Self);
end;

constructor TRPObject.Create(const AName: string; Flags: TRPObjectCreationFlags;
  Mesh: TRPMesh = nil);
begin
  FRefCount:=1;
  FName:=AName;
  FContainers:=TFPList.Create;
  FBoundingType:=rpbtBox;
  FCollisionType:=rpbtBox;
  FColor:=clWhite;
  if rpocfCreateAsmObject in Flags then begin
    AsmObject:=TRPAsmObject.Create(nil);
    FreeAsmObject:=true;
    if (rpocfCreateMesh in Flags) and (Mesh=nil) then begin
      Mesh:=TRPMesh.Create;
    end else if Mesh<>nil then begin
      Mesh.Reference;
    end;
    AsmObject.Mesh:=Mesh;
    if Mesh<>nil then begin
      AsmObject.BS:=Mesh.BS;
    end;
  end;
end;

constructor TRPObject.Create(const AName, MeshDBPath, TextureDBPath: string);
begin
  Create(AName,[rpocfCreateAsmObject]);
  AsmObject.Mesh:=RPMeshes.GetMeshFromDB(MeshDBPath);
  AsmObject.Texture:=RPTextures.GetTextureFromDB(TextureDBPath);
  FFreeTexture:=true;
  AsmObject.Shader:=LightingTexture;
end;

constructor TRPObject.Create(const AName, MeshName, ObjFilename,
  TextureName, TextureFilename: string);
begin
  Create(AName,[rpocfCreateAsmObject]);
  AsmObject.Mesh:=RPMeshes.GetMeshFromObjFile(MeshName,ObjFilename);
  AsmObject.Texture:=RPTextures.GetTextureFromFile(TextureName,TextureFilename);
  FFreeTexture:=true;
  AsmObject.Shader:=LightingTexture;
end;

destructor TRPObject.Destroy;
var
  Mesh: TAsmMesh;
  Texture: TAsmTexture;
begin
  if FContainers.Count>0 then RaiseGDBException('');
  if FreeMesh and (AsmObject<>nil) then begin
    Mesh:=AsmObject.Mesh;
    AsmObject.Mesh:=nil;
    if Mesh is TRPMesh then
      TRPMesh(Mesh).Release
    else
      Mesh.Free;
  end;
  if FreeTexture and (AsmObject<>nil) then begin
    Texture:=AsmObject.Texture;
    AsmObject.Texture:=nil;
    if Texture is TRPTexture then
      TRPTexture(Texture).Release
    else
      Texture.Free;
  end;
  if FreeAsmObject then FreeAndNil(FAsmObject);
  FreeThenNil(FContainers);
  inherited Destroy;
end;

procedure TRPObject.Reference;
begin
  inc(FRefCount);
end;

procedure TRPObject.Release;
begin
  dec(FRefCount);
  if FRefCount=0 then Free;
end;

procedure TRPObject.LeaveContainers;
var
  i: Integer;
begin
  for i:=0 to FContainers.Count-1 do
    TRPObjects(FContainers[i]).ObjectLeaves(Self);
  FContainers.Clear;
end;

procedure TRPObject.CopyBoundingToCollision;
begin
  CollisionType:=BoundingType;
  if AsmObject<>nil then begin
    if AsmObject.Mesh<>nil then begin
      CollisionSphere:=AsmObject.Mesh.BS;
      CollisionBox:=AsmObject.Mesh.BoundingBox;
    end;
  end;
end;

{ TRPObjects }

function TRPObjects.GetCount: Integer;
begin
  Result:=Objects.Count
end;

function TRPObjects.GetDefaultObjectClass: TRPObjectClass;
begin
  Result:=TRPObject;
end;

procedure TRPObjects.ChangingName(Item: TRPObject);
begin
  Objects.Remove(Item);
end;

procedure TRPObjects.ChangedName(Item: TRPObject);
begin
  Objects.Add(Item);
end;

procedure TRPObjects.ObjectLeaves(Item: TRPObject);
begin
  Objects.Remove(Item);
  Item.Release;
end;

constructor TRPObjects.Create;
begin
  if FObjectClass=nil then
    FObjectClass:=GetDefaultObjectClass;
  Objects:=TAVLTree.Create(@CompareRPObjectNames);
end;

destructor TRPObjects.Destroy;
var
  ANode: TAVLTreeNode;
  CurObject: TRPObject;
begin
  ANode:=Objects.FindLowest;
  while ANode<>nil do begin
    CurObject:=TRPObject(ANode.Data);
    CurObject.FContainers.Remove(Self);
    CurObject.Release;
    ANode:=Objects.FindSuccessor(ANode);
  end;
  Objects.Free;
  inherited Destroy;
end;

function TRPObjects.FindNodeByName(const Name: string): TAVLTreeNode;
begin
  Result:=Objects.FindKey(PChar(Name),@CompareNameWithRPObject);
end;

function TRPObjects.FindByName(const Name: string): TRPObject;
var
  ANode: TAVLTreeNode;
begin
  ANode:=FindNodeByName(Name);
  if ANode<>nil then
    Result:=TRPObject(ANode.Data)
  else
    Result:=nil;
end;

procedure TRPObjects.Add(AnObject: TRPObject);

  procedure RaiseInvalidClass;
  begin
    if AnObject=nil then
      raise Exception.Create('AnObject=nil')
    else
      raise Exception.Create(AnObject.ClassName+' is not a '+FObjectClass.ClassName);
  end;

begin
  if Self=nil then raise Exception.Create('Self=nil');
  if not (AnObject is FObjectClass) then RaiseInvalidClass;
  AnObject.Reference;
  AnObject.Name:=NewName(AnObject.Name);
  Objects.Add(AnObject);
  AnObject.FContainers.Add(Self);
end;

function TRPObjects.FindAtRay(const RayOrigin, RayDirection: TVector3
  ): TRPObject;
begin
  Result:=FindTreePRObjectAtRay(Objects,RayOrigin,RayDirection);
end;

function TRPObjects.NewName(const StartName: string): string;
begin
  Result:=StartName;
  while FindByName(Result)<>nil do
    Result:=CreateNextIdentifier(Result);
end;

{ TRPObstacles }

function TRPObstacles.GetDefaultObjectClass: TRPObjectClass;
begin
  Result:=TRPObstacle;
end;

{ TRPCreationItem }

procedure TRPCreationItem.SetMouseMarker(const AValue: TRPAsmObject);
begin
  if FMouseMarker=AValue then exit;
  FMouseMarker:=AValue;
end;

procedure TRPCreationItem.SetName(const AValue: string);
begin
  if FName=AValue then exit;
  FName:=AValue;
end;

procedure TRPCreationItem.SetOwner(const AValue: TRPCreationItems);
begin
  if FOwner=AValue then exit;
  if FOwner<>nil then
    FOwner.DoRemove(Self);
  FOwner:=AValue;
  if FOwner<>nil then
    FOwner.DoAdd(Self);
end;

procedure TRPCreationItem.SetSelected(const AValue: boolean);
begin
  if FSelected=AValue then exit;
  FSelected:=AValue;
  if FSelected then begin
    CreateMouseMarker;
  end else begin
    FreeMouseMarker;
  end;
  Invalidate;
end;

procedure TRPCreationItem.FreeMouseMarker;
begin
  if FMouseMarker<>nil then begin
    RemoveObjectFromScene(FMouseMarker);
    FMouseMarker.Mesh.Free;
    FMouseMarker.Free;
    FMouseMarker:=nil;
  end;
end;

procedure TRPCreationItem.CreateMouseMarker;
begin
  FreeMouseMarker;
  FMouseMarker:=TRPAsmObject.Create(nil);
  with FMouseMarker do begin
    Mesh:=TRPMesh.Create;
    (Mesh as TRPMesh).CreateCohen(0.2,0.2,4,10);
    BS.Radius:=Mesh.BS.Radius;
    Shader:=Lighting;
    Visible:=true;
    RotateAboutLocalX(-90);
  end;
  SetAsmObjectColor(FMouseMarker,clYellow);
  AddObjectToScene(FMouseMarker);
end;

procedure TRPCreationItem.AddObject(AnObject: TRPObject);
begin
  if Owner<>nil then Owner.AddObject(AnObject);
end;

procedure TRPCreationItem.AddObjectToScene(AnObject: TRPAsmObject);
begin
  if Owner<>nil then Owner.AddObjectToScene(AnObject);
end;

procedure TRPCreationItem.RemoveObjectFromScene(AnObject: TRPAsmObject);
begin
  if Owner<>nil then Owner.RemoveObjectFromScene(AnObject);
end;

constructor TRPCreationItem.Create(const AName: string);
begin
  FName:=AName;
end;

destructor TRPCreationItem.Destroy;
begin
  FreeMouseMarker;
  inherited Destroy;
end;

function TRPCreationItem.GetCaption: string;
begin
  Result:=Name;
end;

procedure TRPCreationItem.MoveMarker(Shift: TShiftState; const Position: TVector3);
var
  NewPos: TVector3;
begin
  if MouseMarker=nil then exit;
  if Shift=[] then ;
  NewPos:=Position+Vector(0,2,0);
  if (MouseMarker.Position=NewPos) and MouseMarker.Visible then exit;
  MouseMarker.SetPosition(NewPos);
  MouseMarker.Visible:=true;
  Invalidate;
end;

procedure TRPCreationItem.Invalidate;
begin
  if Owner<>nil then Owner.Invalidate;
end;

{ TRPCreationItems }

function TRPCreationItems.GetCount: Integer;
begin
  Result:=FItems.Count;
end;

function TRPCreationItems.GetItems(Index: integer): TRPCreationItem;
begin
  Result:=TRPCreationItem(FItems[Index]);
end;

procedure TRPCreationItems.DoAdd(Item: TRPCreationItem);
begin
  Item.Name:=NewName(Item.Name);
  FItems.Add(Item);
end;

procedure TRPCreationItems.DoRemove(Item: TRPCreationItem);
begin
  FItems.Remove(Item);
end;

constructor TRPCreationItems.Create;
begin
  FItems:=TFPList.Create;
end;

destructor TRPCreationItems.Destroy;
begin
  Clear;
  FItems.Free;
  inherited Destroy;
end;

procedure TRPCreationItems.Clear;
var
  i: Integer;
begin
  for i:=0 to FItems.Count-1 do TObject(FItems[i]).Free;
  FItems.Clear;
end;

procedure TRPCreationItems.Add(Item: TRPCreationItem);
begin
  Item.Owner:=Self;
end;

procedure TRPCreationItems.FillTreeView(ATreeView: TTreeView);
var
  i: Integer;
begin
  ATreeView.BeginUpdate;
  ATreeView.Items.Clear;
  for i:=0 to Count-1 do
    ATreeView.Items.AddChildObject(nil,Items[i].GetCaption,Items[i]);
  ATreeView.EndUpdate;
end;

function TRPCreationItems.FindByName(const AName: string): TRPCreationItem;
var
  i: Integer;
begin
  for i:=0 to FItems.Count-1 do begin
    Result:=Items[i];
    if CompareText(AName,Result.Name)=0 then
      exit;
  end;
  Result:=nil;
end;

function TRPCreationItems.NewName(const StartName: string): string;
begin
  Result:=StartName;
  while FindByName(Result)<>nil do
    Result:=CreateNextIdentifier(Result);
end;

procedure TRPCreationItems.Invalidate;
begin
  if Assigned(OnInvalidate) then OnInvalidate;
end;

procedure TRPCreationItems.AddObjectToScene(AnObject: TRPAsmObject);
begin
  if Assigned(OnAddObjectToScene) then OnAddObjectToScene(AnObject);
end;

procedure TRPCreationItems.RemoveObjectFromScene(AnObject: TRPAsmObject);
begin
  if Assigned(OnRemoveObjectFromScene) then OnRemoveObjectFromScene(AnObject);
end;

procedure TRPCreationItems.AddObject(AnObject: TRPObject);
begin
  if Assigned(OnAddObject) then OnAddObject(AnObject);
end;

procedure TRPCreationItems.RemoveHooks(HookOwner: TObject);

  procedure RemoveHook(var AMethod: TMethod);
  begin
    if TObject(AMethod.Data)=HookOwner then begin
      AMethod.Code:=nil;
      AMethod.Data:=nil;
    end;
  end;

begin
  RemoveHook(TMethod(FOnInvalidate));
  RemoveHook(TMethod(FOnAddObject));
  RemoveHook(TMethod(FOnAddObjectToScene));
  RemoveHook(TMethod(FOnRemoveObjectFromScene));
end;

initialization
  RPCreationItems:=TRPCreationItems.Create;

finalization
  RPCreationItems.Free;

end.

