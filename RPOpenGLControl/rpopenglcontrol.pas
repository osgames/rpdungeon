{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit RPOpenGLControl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLContext, ComCtrls, ExtCtrls,
  Asmoday, AsmTypes, AsmShaders, AsmUtils,
  GL, Vectors, GLU, RPGLMesh, RPGLObjects, RPGLObjDB;

type
  TRPEditMode = (
    emSelectObject,
    emCreateObject
    );

const
  RPEditModeNames: array[TRPEditMode] of string = (
    'Select Object',
    'Create Object'
    );

type
  TCustomRPOpenGLControl = class;

  TRPSceneOptions = class
  public
  end;

  { TRPScene }

  TRPScene = class(TComponent)
  private
    FEditMode: TRPEditMode;
    FGameView: TCustomRPOpenGLControl;
    FOnEditModeChanged: TNotifyEvent;
    FOnSelectionChanged: TNotifyEvent;
    FOnSelectionChanging: TNotifyEvent;
    FOptions: TRPSceneOptions;
    FOptionsFilename: string;
    FSelectedRPObject: TRPObject;
    FSelectedCreationItem: TRPCreationItem;
    FWorldInitialized: Boolean;
    procedure SetGameView(const AValue: TCustomRPOpenGLControl);
    procedure OnGameViewChangeBounds(Sender: TObject);
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure InitWorld;
    procedure CreateRPObjects;
    procedure CreateCombatants;
    procedure CreateObstacles;
    procedure RenderWorld;
    procedure SelectRPObject(const ScreenPos: TPoint);
    function FindRPObjectAtScreen(const ScreenPos: TPoint): TRPObject;
    procedure SetEditMode(const AValue: TRPEditMode);
    procedure SetSelectedRPObject(const AValue: TRPObject);
    procedure MoveSelection(const Diff: TVector3);
    procedure SetSelectedCreationItem(const AValue: TRPCreationItem);
    procedure MoveCreationMarker(Shift: TShiftState; const P: TVector3);
    procedure MoveCamera(DiffX, DiffY, DiffZ: GLfloat);
    procedure MoveCameraAroundCenterOfView(const Alpha, Beta: GLfloat);
    procedure DeleteSelection;
    procedure UpdatePointingObject(const ScreenPos: TPoint);
    procedure UpdatePointingBox;
    procedure HidePointingBox;
    procedure UpdateSelectionBox;
    procedure HideSelectionBox;
    // utility functions
    procedure CreateRayFromXY(const ScreenPos: TPoint;
                              out RayOrigin, Ray: TVector3);
    function ScreenToFloorPosition(const ScreenPos: TPoint): TVector3;
    procedure FillClassTreeView(ClassTreeView: TTreeView);
    procedure FillEditModeRadioGroup(ModeRadioGroup: TRadioGroup);
  protected
    procedure RPCreationItemsInvalidate;
    procedure RPCreationItemsAddObject(AnObject: TRPObject);
    procedure RPCreationItemsAddObjectToScene(AnObject: TRPAsmObject);
    procedure RPCreationItemsRemoveObjectFromScene(AnObject: TRPAsmObject);
  public
    property GameView: TCustomRPOpenGLControl read FGameView write SetGameView;
    Scene: TAsmScene;
    Camera: TAsmRotationCamera;
    Floor: TRPObstacle;
    Combatants: TRPCombatants;
    Obstacles: TRPObstacles;
    RPObjects: TRPObjects;
    NewObject: TRPObject;
    PointingBox: TRPAsmObject;
    PointingObject: TRPObject;
    SelectionBox: TRPAsmObject;
    procedure SaveOptions;
    property SelectedRPObject: TRPObject read FSelectedRPObject write SetSelectedRPObject;
    property EditMode: TRPEditMode read FEditMode write SetEditMode;
    property SelectedCreationItem: TRPCreationItem read FSelectedCreationItem write SetSelectedCreationItem;
    property Options: TRPSceneOptions read FOptions;
    property OptionsFilename: string read FOptionsFilename;
    property OnEditModeChanged: TNotifyEvent read FOnEditModeChanged write FOnEditModeChanged;
    property OnSelectionChanging: TNotifyEvent read FOnSelectionChanging write FOnSelectionChanging;
    property OnSelectionChanged: TNotifyEvent read FOnSelectionChanged write FOnSelectionChanged;
    property WorldInitialized: Boolean read FWorldInitialized;
  end;

  { TCustomRPOpenGLControl }

  TCustomRPOpenGLControl = class(TOpenGLControl)
  private
  protected
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
  end;
  
  { TRPOpenGLControl }

  TRPOpenGLControl = class(TCustomRPOpenGLControl)
  published
    property Align;
    property Anchors;
    property AutoResizeViewport;
    property BorderSpacing;
    property Enabled;
    property OnClick;
    property OnConstrainedResize;
    property OnDblClick;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property OnPaint;
    property OnResize;
    property OnShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
  end;


procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('RolePlaying',[TRPOpenGLControl]);
end;

{ TCustomRPOpenGLControl }

constructor TCustomRPOpenGLControl.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
end;

destructor TCustomRPOpenGLControl.Destroy;
begin
  inherited Destroy;
end;

{ TRPScene }

procedure TRPScene.SetGameView(const AValue: TCustomRPOpenGLControl);
begin
  if FGameView=AValue then exit;
  if FGameView<>nil then begin
    FGameView.RemoveAllHandlersOfObject(Self);
  end;
  
  FGameView:=AValue;

  FreeThenNil(Scene);
  if GameView<>nil then begin
    GameView.MakeCurrent(false);
    Scene:=TAsmScene.Create(GameView,nil);
    GameView.AddHandlerOnChangeBounds(@OnGameViewChangeBounds,true);
  end;
end;

procedure TRPScene.OnGameViewChangeBounds(Sender: TObject);
begin
  if Sender=nil then ;
  if Scene<>nil then Scene.UpdateViewport;
  GameView.Invalidate;
end;

constructor TRPScene.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  FOptions:=TRPSceneOptions.Create;
end;

destructor TRPScene.Destroy;
begin
  RPCreationItems.RemoveHooks(Self);
  FreeThenNil(Combatants);
  FreeThenNil(Obstacles);
  FreeThenNil(RPObjects);
  if PointingBox<>nil then begin
    PointingBox.Mesh.Free;
    FreeThenNil(PointingBox);
  end;
  if SelectionBox<>nil then begin
    SelectionBox.Mesh.Free;
    FreeThenNil(SelectionBox);
  end;
  FreeThenNil(Floor);
  FreeThenNil(NewObject);
  inherited Destroy;
end;

procedure TRPScene.InitWorld;
var
  Light: TAsmPositionalLight;
  FileDB: TRPGLFileDB;
begin
  Scene.Init;
  Scene.SetBackgroundColor(0,0,0);

  FileDB:=TRPGLFileDB.Create('Objects');
  FileDB.BaseDirectory:='..'+PathDelim+'objects';
  RPGLFilesDB.Add(FileDB);

  // Light
  Light:=TAsmPositionalLight.Create(Self);
  with Light do begin
    SetPosition(0,600,800);
    SetAmbientColor(0.1,0.1,0.1,1);
    SetDiffuseColor(0.6,0.6,0.6,1);
    Enabled:=true;
  end;
  Scene.Lightlist.Add(Light);

  // Light
  Light:=TAsmPositionalLight.Create(Self);
  with Light do begin
    SetPosition(0,600,-800);
    SetAmbientColor(0.1,0.1,0.1,1);
    SetDiffuseColor(0.6,0.6,0.6,1);
    Enabled:=true;
  end;
  Scene.Lightlist.Add(Light);

  // Floor
  Floor:=TRPObstacle.Create('Floor',rpocfCreateDefaults);
  with Floor.AsmObject do begin
    Mesh.CreateSquare(100,66);
    BS.Radius:=Mesh.BS.Radius;
    Shader:=LightingTexture;
    Visible:=true;
    SetColor(1,1,1,1);
    RotateAboutLocalX(-90);
    Texture:=RPTextures.GetTextureFromDB('objects/textures/stone1.png');
  end;
  Scene.ObjectList.Add(Floor.AsmObject);

  CreateRPObjects;

  // Camera
  Camera:=TAsmRotationCamera.Create(20,10,0,0,0,45,1,1000,Self);
  Scene.ActiveCamera:=Camera;

  fWorldInitialized:=true;
end;

procedure TRPScene.CreateRPObjects;
begin
  RPObjects:=TRPObjects.Create;
  CreateCombatants;
  CreateObstacles;
end;

procedure TRPScene.CreateCombatants;
begin
  Combatants:=TRPCombatants.Create;

  RPCreationItems.FindByName('Mage').MouseDown(mbLeft,[],Vector(0,0,0));
  RPCreationItems.FindByName('Beholder').MouseDown(mbLeft,[],Vector(-2,0,0));
  RPCreationItems.FindByName('Fighter').MouseDown(mbLeft,[],Vector(-4,0,0));
end;

procedure TRPScene.CreateObstacles;
begin
  Obstacles:=TRPObstacles.Create;

  RPCreationItems.FindByName('Chest').MouseDown(mbLeft,[],Vector(2,0,0));
end;

procedure TRPScene.RenderWorld;
begin
  if not WorldInitialized then InitWorld;
  Scene.RenderScene;
end;

procedure TRPScene.SelectRPObject(const ScreenPos: TPoint);
var
  RayOrigin, RayDirection: TVector3;
begin
  CreateRayFromXY(ScreenPos,RayOrigin,RayDirection);
  SelectedRPObject:=RPObjects.FindAtRay(RayOrigin,RayDirection);
end;

function TRPScene.FindRPObjectAtScreen(const ScreenPos: TPoint): TRPObject;
var
  RayOrigin, RayDirection: TVector3;
begin
  CreateRayFromXY(ScreenPos,RayOrigin, RayDirection);
  Result:=RPObjects.FindAtRay(RayOrigin,RayDirection);
end;

procedure TRPScene.SetEditMode(const AValue: TRPEditMode);
begin
  if FEditMode=AValue then exit;
  case FEditMode of
  emCreateObject:
    if SelectedCreationItem<>nil then SelectedCreationItem.Selected:=false;
  end;

  FEditMode:=AValue;

  if Assigned(OnEditModeChanged) then OnEditModeChanged(Self);
  case FEditMode of
  emCreateObject:
    if SelectedCreationItem<>nil then SelectedCreationItem.Selected:=true;
  end;
end;

procedure TRPScene.CreateRayFromXY(const ScreenPos: TPoint; out RayOrigin,
  Ray: TVector3);
var
  UnProj1X, UnProj1Y, UnProj1Z, UnProj2X, UnProj2Y, UnProj2Z: GLdouble;
begin
  gluUnProject(ScreenPos.X, GameView.ClientHeight-ScreenPos.Y,
               0, Scene.mm,Scene.pm,Scene.viewport,
               @UnProj1X, @UnProj1Y, @UnProj1Z);
  gluUnProject(ScreenPos.X, GameView.ClientHeight-ScreenPos.Y,
               1, Scene.mm,Scene.pm,Scene.viewport,
               @UnProj2X, @UnProj2Y, @UnProj2Z);
  RayOrigin.X:=GLfloat(UnProj1X);
  RayOrigin.Y:=GLfloat(UnProj1Y);
  RayOrigin.Z:=GLfloat(UnProj1Z);
  Ray.X:=GLfloat(UnProj2X-UnProj1X);
  Ray.Y:=GLfloat(UnProj2Y-UnProj1Y);
  Ray.Z:=GLfloat(UnProj2Z-UnProj1Z);
  Normalize(Ray);
end;

procedure TRPScene.SetSelectedRPObject(const AValue: TRPObject);
begin
  if FSelectedRPObject=AValue then exit;
  if Assigned(OnSelectionChanging) then OnSelectionChanging(Self);
  FSelectedRPObject:=AValue;
  if Assigned(OnSelectionChanged) then OnSelectionChanged(Self);
  GameView.Invalidate;
end;

procedure TRPScene.RPCreationItemsInvalidate;
begin
  if GameView<>nil then GameView.Invalidate;
end;

procedure TRPScene.RPCreationItemsAddObject(AnObject: TRPObject);
begin
  if RPObjects<>nil then
    RPObjects.Add(AnObject);
  if (Combatants<>nil) and (AnObject is Combatants.ObjectClass) then
    Combatants.Add(AnObject);
  if (Obstacles<>nil) and (AnObject is Obstacles.ObjectClass) then
    Obstacles.Add(AnObject);
end;

procedure TRPScene.RPCreationItemsAddObjectToScene(AnObject: TRPAsmObject);
begin
  if Scene<>nil then Scene.Objectlist.Add(AnObject);
end;

procedure TRPScene.RPCreationItemsRemoveObjectFromScene(AnObject: TRPAsmObject);
begin
  if Scene<>nil then Scene.Objectlist.Remove(AnObject);
end;

procedure TRPScene.SaveOptions;
begin

end;

function TRPScene.ScreenToFloorPosition(const ScreenPos: TPoint): TVector3;
var
  RayOrigin, Ray: TVector3;
begin
  CreateRayFromXY(ScreenPos,RayOrigin,Ray);
  if not RayPlaneIntersection(RayOrigin,Ray,Vector(0,1,0),0,Result) then
    Result:=Vector(0,0,0);
end;

procedure TRPScene.FillClassTreeView(ClassTreeView: TTreeView);
begin
  RPCreationItems.OnInvalidate:=@RPCreationItemsInvalidate;
  RPCreationItems.OnAddObject:=@RPCreationItemsAddObject;
  RPCreationItems.OnAddObjectToScene:=@RPCreationItemsAddObjectToScene;
  RPCreationItems.OnRemoveObjectFromScene:=@RPCreationItemsRemoveObjectFromScene;

  RPCreationItems.FillTreeView(ClassTreeView);
  ClassTreeView.Selected:=ClassTreeView.Items[0];
end;

procedure TRPScene.FillEditModeRadioGroup(ModeRadioGroup: TRadioGroup);
var
  m: TRPEditMode;
begin
  ModeRadioGroup.Items.BeginUpdate;
  ModeRadioGroup.Items.Clear;
  for m:=Low(TRPEditMode) to High(TRPEditMode) do
    ModeRadioGroup.Items.Add(RPEditModeNames[m]);
  ModeRadioGroup.Items.EndUpdate;
  ModeRadioGroup.ItemIndex:=ord(EditMode);
end;

procedure TRPScene.MoveSelection(const Diff: TVector3);
var
  NewPosition: TVector3;
begin
  if vLength(Diff)=0 then exit;
  if SelectedRPObject<>nil then begin
    NewPosition:=SelectedRPObject.AsmObject.Position+Diff;
    if (NewPosition.X>100) or (NewPosition.X<-100)
    or (NewPosition.Y>100) or (NewPosition.Y<-100)
    or (NewPosition.Z>100) or (NewPosition.Z<-100) then exit;
    //DebugLn('TRPScene.MoveSelection ',dbgs(NewPosition));
    SelectedRPObject.AsmObject.SetPosition(NewPosition);
    UpdateSelectionBox;
    UpdatePointingBox;
  end;
end;

procedure TRPScene.SetSelectedCreationItem(const AValue: TRPCreationItem);
begin
  if FSelectedCreationItem=AValue then exit;
  if FSelectedCreationItem<>nil then begin
    FSelectedCreationItem.Selected:=false;
  end;
  FSelectedCreationItem:=AValue;
  if FSelectedCreationItem<>nil then begin
    FSelectedCreationItem.Selected:=EditMode=emCreateObject;
  end;
end;

procedure TRPScene.MoveCreationMarker(Shift: TShiftState; const P: TVector3);
begin
  if SelectedCreationItem=nil then exit;
  SelectedCreationItem.MoveMarker(Shift,P);
end;

procedure TRPScene.MoveCamera(DiffX, DiffY, DiffZ: GLfloat);
// move camera relative to view, keeping radius
var
  CurRad: GLfloat;
  CurSin: GLfloat;
  CurCos: GLfloat;
  Diff: TVector3;
begin
  CurRad:=Camera.Alpha*(pi/180);
  CurSin:=sin(CurRad);
  CurCos:=cos(CurRad);
  Diff.x:=CurCos*DiffX-CurSin*DiffZ;
  Diff.y:=DiffY;
  Diff.z:= -CurSin*DiffX-CurCos*DiffZ;
  //DebugLn('TRPScene.MoveCamera Alpha=',dbgs(Camera.Alpha),' DiffX=',dbgs(DiffX),' DiffY=',dbgs(DiffY),' DiffZ=',dbgs(DiffZ),' Diff=',dbgs(Diff));
  Camera.SetCoV(Camera.CoV+Diff);
  GameView.Invalidate;
end;

procedure TRPScene.MoveCameraAroundCenterOfView(const Alpha, Beta: GLfloat);
begin
  Camera.RotateAlphaAboutCoV(Alpha);
  Camera.RotateBetaAboutCov(Beta);
  UpdatePointingBox;
  GameView.Invalidate;
end;

procedure TRPScene.DeleteSelection;
var
  AnObject: TRPObject;
begin
  if SelectedRPObject=nil then exit;
  AnObject:=SelectedRPObject;
  AnObject.Reference;
  SelectedRPObject:=nil;
  Scene.Objectlist.Remove(AnObject.AsmObject);
  if PointingObject=AnObject then begin
    HidePointingBox;
    PointingObject:=nil;
  end;
  AnObject.LeaveContainers;
  AnObject.Release;
  GameView.Invalidate;
end;

procedure TRPScene.UpdatePointingObject(const ScreenPos: TPoint);
begin
  PointingObject:=FindRPObjectAtScreen(ScreenPos);
  UpdatePointingBox;
end;

procedure TRPScene.UpdatePointingBox;
begin
  if PointingObject<>nil then begin
    // create PointingBox
    if PointingBox=nil then begin
      PointingBox:=TRPAsmObject.Create(nil);
      PointingBox.Mesh:=TRPMesh.Create;
      Scene.Objectlist.Add(PointingBox);
      PointingBox.LineWidth:=2;
      PointingBox.SetColor(0,1,0,0.8);
    end;
    // check if something changed
    if PointingBox.Visible
    and PointingBox.IsMatrixEqual(PointingObject.AsmObject) then exit;
    // position pointing box
    PointingBox.CopyMatrix(PointingObject.AsmObject);
    TRPMesh(PointingBox.Mesh).CreateSelection;
    PointingBox.Visible:=true;
    GameView.Invalidate;
  end else begin
    HidePointingBox;
  end;
end;

procedure TRPScene.HidePointingBox;
begin
  if (PointingBox<>nil) and (PointingBox.Visible) then begin
    PointingBox.Visible:=false;
    GameView.Invalidate;
  end;
end;

procedure TRPScene.UpdateSelectionBox;
begin
  if SelectedRPObject<>nil then begin
    // create SelectionBox
    if SelectionBox=nil then begin
      SelectionBox:=TRPAsmObject.Create(nil);
      SelectionBox.Mesh:=TRPMesh.Create;
      Scene.Objectlist.Add(SelectionBox);
      SelectionBox.LineWidth:=2;
      SelectionBox.SetColor(1,1,0,0.8);
    end;
    // check if something changed
    if SelectionBox.Visible
    and SelectionBox.IsMatrixEqual(SelectedRPObject.AsmObject) then exit;
    // position SelectionBox
    SelectionBox.CopyMatrix(SelectedRPObject.AsmObject);
    TRPMesh(SelectionBox.Mesh).CreateSelection;
    SelectionBox.Visible:=true;
    GameView.Invalidate;
  end else begin
    HideSelectionBox;
  end;
end;

procedure TRPScene.HideSelectionBox;
begin
  if (SelectionBox<>nil) and (SelectionBox.Visible) then begin
    SelectionBox.Visible:=false;
    GameView.Invalidate;
  end;
end;

initialization
{$I rpopenglcontrol.lrs}

end.
