{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  Abstract:
    Defines the base classes for client-server connections.
}
unit RPConnector;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, FileUtil, DynQueue;
  
type
  TConnectionError = class(Exception);

  { TCustomComponentConnection }

  TCustomComponentConnection = class(TComponent)
  private
    FComponentReceived: TNotifyEvent;
    FSentBytes: int64;
    FReceiveQueue: TLazComponentQueue;
    FSendQueue: TStrings;
    FSendQueueTopStart: PtrInt;
    function GetOnFindComponentClass: TFindComponentClassEvent;
    procedure SetOnFindComponentClass(const AValue: TFindComponentClassEvent);
  protected
    function GetConnected: Boolean; virtual; abstract;
    procedure SetConnected(const AValue: Boolean); virtual; abstract;
    function SendBuffer(var Buffer; Count: integer): integer; virtual; abstract;// used to sent data
    function ReceiveBuffer(const Buffer; Count: integer): integer;// called when data received
    function ReceiveStream(AStream: TStream; Count: integer): integer;// called when data received
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure SendComponent(AComponent: TComponent);
    function HasComponent: Boolean;
    function ReadComponent(var AComponent: TComponent;
                           NewOwner: TComponent = nil): Boolean;
    procedure ClearSendQueue;
    function NeedsPolling: Boolean; virtual; // true = application must call Poll from time to time
    procedure Poll; virtual;
    function FlushSendQueue: int64;
    procedure CheckConnected;
  public
    property Connected: Boolean read GetConnected write SetConnected;
    property ComponentReceived: TNotifyEvent read FComponentReceived write FComponentReceived;
    property SentBytes: int64 read FSentBytes write FSentBytes;
    property OnFindComponentClass: TFindComponentClassEvent
                     read GetOnFindComponentClass write SetOnFindComponentClass;
  end;
  
  { TCustomComponentTCPConnection }

  TCustomComponentTCPConnection = class(TCustomComponentConnection)
  private
    FHost: string;
    FPort: string;
    procedure SetHost(const AValue: string);
    procedure SetPort(const AValue: string);
  public
    property Host: string read FHost write SetHost;
    property Port: string read FPort write SetPort;
  end;
  
  { TCustomComponentFileConnection }

  TCustomComponentFileConnection = class(TCustomComponentConnection)
  private
    FReceiveFilename: string;
    FReceiveFilePosition: int64;
    FSendFilename: string;
    FSendFilePosition: int64;
    fConnected: Boolean;
  protected
    function GetConnected: Boolean; override;
    procedure SetConnected(const AValue: Boolean); override;
    function SendBuffer(const Buffer; Count: integer): integer; override;
    procedure SetReceiveFilename(const AValue: string); virtual;
    procedure SetReceiveFilePosition(const AValue: int64); virtual;
    procedure SetSendFilename(const AValue: string); virtual;
    procedure SetSendFilePosition(const AValue: int64); virtual;
  public
    function NeedsPolling: Boolean; override;
    procedure Poll; override;
  public
    property ReceiveFilename: string read FReceiveFilename write SetReceiveFilename;
    property ReceiveFilePosition: int64 read FReceiveFilePosition write SetReceiveFilePosition;
    property SendFilename: string read FSendFilename write SetSendFilename;
    property SendFilePosition: int64 read FSendFilePosition write SetSendFilePosition;
  end;

implementation

{ TCustomComponentConnection }

function TCustomComponentConnection.GetOnFindComponentClass: TFindComponentClassEvent;
begin
  Result:=FReceiveQueue.OnFindComponentClass;
end;

procedure TCustomComponentConnection.SetOnFindComponentClass(
  const AValue: TFindComponentClassEvent);
begin
  FReceiveQueue.OnFindComponentClass:=AValue;
end;

function TCustomComponentConnection.ReceiveBuffer(const Buffer;
  Count: integer): integer;
begin
  Result:=FReceiveQueue.Write(Buffer,Count);
end;

function TCustomComponentConnection.ReceiveStream(AStream: TStream;
  Count: integer): integer;
begin
  Result:=FReceiveQueue.CopyFrom(AStream,Count);
end;

constructor TCustomComponentConnection.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  FReceiveQueue:=TLazComponentQueue.Create(nil);
  FSendQueue:=TStringList.Create;
end;

destructor TCustomComponentConnection.Destroy;
begin
  FreeAndNil(FSendQueue);
  FreeAndNil(FReceiveQueue);
  inherited Destroy;
end;

procedure TCustomComponentConnection.SendComponent(AComponent: TComponent);
var
  s: String;
begin
  s:=FReceiveQueue.ConvertComponentAsString(AComponent);
  FSendQueue.Add(s);
  FlushSendQueue;
end;

function TCustomComponentConnection.HasComponent: Boolean;
begin
  Result:=FReceiveQueue.HasComponent;
end;

function TCustomComponentConnection.ReadComponent(var AComponent: TComponent;
  NewOwner: TComponent): Boolean;
begin
  Result:=FReceiveQueue.ReadComponent(AComponent,NewOwner);
end;

procedure TCustomComponentConnection.ClearSendQueue;
// clear all, except the partially sent component, so the other site does not
// get corrupted data
var
  i: Integer;
begin
  if FSendQueueTopStart=0 then
    FSendQueue.Clear
  else
    for i:=FSendQueue.Count-1 downto 1 do FSendQueue.Delete(i);
end;

function TCustomComponentConnection.NeedsPolling: Boolean;
begin
  Result:=true;
end;

procedure TCustomComponentConnection.Poll;
begin
  FlushSendQueue;
end;

function TCustomComponentConnection.FlushSendQueue: int64;
var
  Buf: array[1..1024] of byte;
  Count: LongInt;
  SentCount: LongInt;
begin
  Result:=0;
  while FSendQueue.Size>0 do begin
    // get some bytes to send
    Count:=FSendQueue.Top(Buf[1],1024);
    if Count=0 then exit;
    // try to send the bytes
    SentCount:=SendBuffer(Buf[1],Count);
    if SentCount>Count then
      raise Exception.Create('TCustomComponentConnection.FlushSendQueue sent too many bytes');
    if SentCount=0 then exit;
    inc(Result,SentCount); // increase sent bytes
    // remove sent bytes from queue
    FSendQueue.Pop(Buf[1],SentBytes);
    if SentBytes<Count then exit;
  end;
end;

procedure TCustomComponentConnection.CheckConnected;
begin
  if not fConnected then
    raise EInOutError.Create('not connected');
end;

{ TCustomComponentTCPConnection }

procedure TCustomComponentTCPConnection.SetHost(const AValue: string);
begin
  if FHost=AValue then exit;
  FHost:=AValue;
end;

procedure TCustomComponentTCPConnection.SetPort(const AValue: string);
begin
  if FPort=AValue then exit;
  FPort:=AValue;
end;

{ TCustomComponentFileConnection }

function TCustomComponentFileConnection.GetConnected: Boolean;
begin
  Result:=fConnected;
end;

procedure TCustomComponentFileConnection.SetConnected(const AValue: Boolean);
begin
  if fConnected=AValue then exit;
  fConnected:=AValue;
  if fConnected then begin
    Poll;
  end;
end;

function TCustomComponentFileConnection.SendBuffer(const Buffer; Count: integer
  ): integer;
var
  fs: TFileStream;
begin
  CheckConnected;
  fs:=TFileStream.Create(SendFilename,fmCreate);
  try
    fs.Position:=FSendFilePosition;
    Result:=fs.Write(Buffer,Count);
    inc(FSendFilePosition,Result);
  finally
    fs.Free;
  end;
end;

procedure TCustomComponentFileConnection.SetReceiveFilename(const AValue: string
  );
begin
  if FReceiveFilename=AValue then exit;
  FReceiveFilename:=AValue;
end;

procedure TCustomComponentFileConnection.SetReceiveFilePosition(
  const AValue: int64);
begin
  if FReceiveFilePosition=AValue then exit;
  FReceiveFilePosition:=AValue;
end;

procedure TCustomComponentFileConnection.SetSendFilename(const AValue: string);
begin
  if FSendFilename=AValue then exit;
  FSendFilename:=AValue;
end;

procedure TCustomComponentFileConnection.SetSendFilePosition(const AValue: int64
  );
begin
  if FSendFilePosition=AValue then exit;
  FSendFilePosition:=AValue;
end;

function TCustomComponentFileConnection.NeedsPolling: Boolean;
begin
  Result:=true;
end;

procedure TCustomComponentFileConnection.Poll;
var
  fs: TFileStream;
begin
  if not FileExists(ReceiveFilename) then exit;
  if FileSize(ReceiveFilename)<=ReceiveFilePosition then exit;
  fs:=TFileStream.Create(ReceiveFilename,fmOpenRead);
  try
    fs.Position:=ReceiveFilePosition;
    ReceiveFilePosition:=fs.Position+ReceiveFromStream(fs,fs.Size-fs.Position);
  finally
    fs.Free;
  end;
end;

end.

