{ This file was automatically created by Lazarus. Do not edit!
This source is only used to compile and install the package.
 }

unit RPProtocolPkg; 

interface

uses
  RPProtocol, RPServerMsg, RPClientMsg, RPMsg, RPNetClient, RPConnector, 
    LazarusPackageIntf; 

implementation

procedure Register; 
begin
end; 

initialization
  RegisterPackage('RPProtocolPkg', @Register); 
end.
