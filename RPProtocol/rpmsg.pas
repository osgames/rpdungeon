{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  Abstract:
    Classes and functions for the network protocol of client/server.
}
unit RPMsg;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, GL, LResources;

type

  { TRPMessage }

  TRPMessage = class(TComponent)
  end;
  TRPMessageClass = class of TRPMessage;

  { TRPErrorMessage }

  TRPErrorMessage = class(TRPMessage)
  private
    FError: string;
  published
    property Error: string read FError write FError;
  end;


  { TRPMsgPartVector }

  TRPMsgPartVector = class(TPersistent)
  private
    FX: GLFloat;
    FY: GLFloat;
    FZ: GLFloat;
  published
    property X: GLFloat read FX write FX;
    property Y: GLFloat read FY write FY;
    property Z: GLFloat read FZ write FZ;
  end;

  { TRPMsgPartFloor }

  TRPMsgPartFloor = class(TPersistent)
  private
    FSizeX: integer;
    FSizeY: integer;
    FTextureDBPath: string;
  published
    property SizeX: integer read FSizeX write FSizeX;
    property SizeY: integer read FSizeY write FSizeY;
    property TextureDBPath: string read FTextureDBPath write FTextureDBPath;
  end;

  { TRPMsgPartCamera }

  TRPMsgPartCamera = class(TPersistent)
  private
    FCenterOfView: TRPMsgPartVector;
    FPosition: TRPMsgPartVector;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property CenterOfView: TRPMsgPartVector read FCenterOfView write FCenterOfView;
    property Position: TRPMsgPartVector read FPosition write FPosition;
  end;

  { TRPWorldPropertiesMessage }

  TRPWorldPropertiesMessage = class(TRPMessage)
  private
    FCamera: TRPMsgPartCamera;
    FFloor: TRPMsgPartFloor;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Floor: TRPMsgPartFloor read FFloor write FFloor;
    property Camera: TRPMsgPartCamera read FCamera write FCamera;
  end;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
type

  { TRPRegisteredMessages }

  TRPRegisteredMessages = class
  private
    FItems: TFPList;
    function GetCount: integer;
    function GetItems(Index: integer): TRPMessageClass;
    procedure SetItems(Index: integer; const AValue: TRPMessageClass);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Add(MsgClass: TRPMessageClass);
    procedure Add(MsgClasses: array of TRPMessageClass);
    function FindByClassName(const AClassName: string;
                             ErrorIfNotFound: Boolean): TRPMessageClass;
    procedure WriteMessageToStream(AStream: TStream; AMessage: TRPMessage);
    procedure ReadMessageFromStream(AStream: TStream; Owner: TComponent;
                                    out AMessage: TRPMessage);
    procedure FindComponentClass(Reader: TReader; const AClassName: string;
                                 var ComponentClass: TComponentClass);
    property Items[Index: integer]: TRPMessageClass read GetItems write SetItems;
    property Count: integer read GetCount;
  end;

var
  RegisteredRPMessages: TRPRegisteredMessages = nil;
  
procedure WriteMessageToStream(AStream: TStream; AMessage: TRPMessage);
procedure ReadMessageFromStream(AStream: TStream; Owner: TComponent;
                                out AMessage: TRPMessage);

procedure RegisterMessages;

implementation

procedure WriteMessageToStream(AStream: TStream; AMessage: TRPMessage);
begin
  RegisteredRPMessages.WriteMessageToStream(AStream,AMessage);
end;

procedure ReadMessageFromStream(AStream: TStream; Owner: TComponent;
  out AMessage: TRPMessage);
begin
  RegisteredRPMessages.ReadMessageFromStream(AStream,Owner,AMessage);
end;

procedure RegisterMessages;
begin
  RegisteredRPMessages.Add([TRPErrorMessage,TRPWorldPropertiesMessage]);
end;

{ TRPWorldPropertiesMessage }

constructor TRPWorldPropertiesMessage.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  FFloor:=TRPMsgPartFloor.Create;
  FCamera:=TRPMsgPartCamera.Create;
end;

destructor TRPWorldPropertiesMessage.Destroy;
begin
  FreeAndNil(FFloor);
  FreeAndNil(FCamera);
  inherited Destroy;
end;

{ TRPMsgPartCamera }

constructor TRPMsgPartCamera.Create;
begin
  FCenterOfView:=TRPMsgPartVector.Create;
  FPosition:=TRPMsgPartVector.Create;
end;

destructor TRPMsgPartCamera.Destroy;
begin
  FreeAndNil(FCenterOfView);
  FreeAndNil(FPosition);
  inherited Destroy;
end;

{ TRPRegisteredMessages }

function TRPRegisteredMessages.GetCount: integer;
begin
  Result:=FItems.Count;
end;

function TRPRegisteredMessages.GetItems(Index: integer): TRPMessageClass;
begin
  Result:=TRPMessageClass(FItems[Index]);
end;

procedure TRPRegisteredMessages.SetItems(Index: integer;
  const AValue: TRPMessageClass);
begin
  FItems[Index]:=AValue;
end;

constructor TRPRegisteredMessages.Create;
begin
  FItems:=TFPList.Create;
end;

destructor TRPRegisteredMessages.Destroy;
begin
  Clear;
  FreeAndNil(FItems);
  inherited Destroy;
end;

procedure TRPRegisteredMessages.Clear;
begin
  FItems.Clear;
end;

procedure TRPRegisteredMessages.Add(MsgClass: TRPMessageClass);
begin
  FItems.Add(MsgClass);
end;

procedure TRPRegisteredMessages.Add(MsgClasses: array of TRPMessageClass);
var
  i: Integer;
begin
  for i:=Low(MsgClasses) to High(MsgClasses) do
    Add(MsgClasses[i]);
end;

function TRPRegisteredMessages.FindByClassName(const AClassName: string;
  ErrorIfNotFound: Boolean): TRPMessageClass;
var
  i: Integer;
begin
  for i:=0 to FItems.Count-1 do begin
    Result:=Items[i];
    if CompareText(Result.ClassName,AClassName)=0 then exit;
  end;
  Result:=nil;
  if ErrorIfNotFound then
    raise EClassNotFound.CreateFmt('Class "%s" not found', [AClassName]);
end;

procedure TRPRegisteredMessages.WriteMessageToStream(AStream: TStream;
  AMessage: TRPMessage);
begin
  WriteComponentAsBinaryToStream(AStream,AMessage);
end;

procedure TRPRegisteredMessages.ReadMessageFromStream(AStream: TStream;
  Owner: TComponent; out AMessage: TRPMessage);
begin
  AMessage:=nil;
  ReadComponentFromBinaryStream(AStream,AMessage,@FindComponentClass,Owner);
end;

procedure TRPRegisteredMessages.FindComponentClass(Reader: TReader;
  const AClassName: string; var ComponentClass: TComponentClass);
begin
  if Reader=nil then ;
  ComponentClass:=FindByClassName(AClassName,true);
end;

end.

