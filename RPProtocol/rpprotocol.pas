{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  Abstract:
    Classes and functions for the network protocol of client/server.
}
unit RPProtocol;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, RPMsg, RPServerMsg, RPClientMsg;
  
procedure RegisterRPMessages;
procedure UnregisterRPMessages;
  
implementation

procedure RegisterRPMessages;
begin
  RegisteredRPMessages:=TRPRegisteredMessages.Create;
  RPMsg.RegisterMessages;
  RPServerMsg.RegisterMessages;
  RPClientMsg.RegisterMessages;
end;

procedure UnregisterRPMessages;
begin
  FreeAndNil(RegisteredRPMessages);
end;

end.

