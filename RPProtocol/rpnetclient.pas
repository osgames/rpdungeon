{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  Abstract:
    The network part of the clients.
}
unit RPNetClient;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BlckSock, RPLog, LResources;
  
type
  TComponentStreamClient = class;

  { TCSCSendThread }

  TCSCSendThread = class(TThread)
  private
    FConnector: TComponentStreamClient;
  public
    constructor Create(TheConnector: TComponentStreamClient;
                       CreateSuspended: Boolean);
    destructor Destroy; override;
    procedure Execute; override;
    property Connector: TComponentStreamClient read FConnector;
  end;

  { TCSCReceiveThread }

  TCSCReceiveThread = class(TThread)
  private
    FConnector: TComponentStreamClient;
  public
    constructor Create(TheConnector: TComponentStreamClient;
                       CreateSuspended: Boolean);
    destructor Destroy; override;
    procedure Execute; override;
    property Connector: TComponentStreamClient read FConnector;
  end;
  
  { TQueueItem }

  TQueueItem = class
  public
    Data: Pointer;
    Count: PtrInt;
    constructor Create(AStream: TStream);
    destructor Destroy; override;
  end;
  PQueueItem = ^TQueueItem;
  
  { TComponentStreamClient }

  TComponentStreamClient = class(TComponent)
  private
    FReceiveThread: TCSCReceiveThread;
    FSendThread: TCSCSendThread;
    FServerHost: string;
    FServerPort: string;
    FClientSocket: TTCPBlockSocket;
    FSendQueue: TThreadList; // list of TQueueItem
  protected
    procedure SetServerHost(const AValue: string); virtual;
    procedure SetServerPort(const AValue: string); virtual;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure Connect; virtual;
    procedure Send(AComponent: TComponent);
  published
    property ServerPort: string read FServerPort write SetServerPort;
    property ServerHost: string read FServerHost write SetServerHost;
    property SendThread: TCSCSendThread read FSendThread;
    property ReceiveThread: TCSCReceiveThread read FReceiveThread;
  end;
  
  { TRPClientConnector }

  TRPClientConnector = class(TComponentStreamClient)
  end;

implementation

{ TComponentStreamClient }

procedure TComponentStreamClient.SetServerHost(const AValue: string);
begin
  if FServerHost=AValue then exit;
  FServerHost:=AValue;
end;

procedure TComponentStreamClient.SetServerPort(const AValue: string);
begin
  if FServerPort=AValue then exit;
  FServerPort:=AValue;
end;

constructor TComponentStreamClient.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  FServerHost:='';
  FServerPort:='17000';
  FSendThread:=TCSCSendThread.Create(Self,true);
  FSendQueue:=TThreadList.Create;
  FReceiveThread:=TCSCReceiveThread.Create(Self,true);
end;

destructor TComponentStreamClient.Destroy;
begin
  if FSendThread<>nil then begin
    FSendThread.FConnector:=nil;
    FSendThread.Terminate;
  end;
  FreeAndNil(FSendQueue);

  if FReceiveThread<>nil then begin
    FReceiveThread.FConnector:=nil;
    FReceiveThread.Terminate;
  end;

  inherited Destroy;
end;

procedure TComponentStreamClient.Connect;
begin
  Log('Connecting to server '+ServerHost+':'+ServerPort);
  FClientSocket:=TTCPBlockSocket.Create;
  FClientSocket.Connect(ServerHost,ServerPort);

  SendThread.Resume;
  ReceiveThread.Resume;
end;

procedure TComponentStreamClient.Send(AComponent: TComponent);
var
  AStream: TMemoryStream;
  Item: TQueueItem;
begin
  AStream:=TMemoryStream.Create;
  try
    WriteComponentAsBinaryToStream(AStream,AComponent);
    AComponent.Free;
    Item:=TQueueItem.Create(AStream);
  finally
    AStream.Free;
  end;
  // put stream on
  FSendQueue.LockList.Add(Item);
  FSendQueue.UnlockList;
  // wake send thread if not already done
  FSendThread.Resume;
end;

{ TCSCSendThread }

constructor TCSCSendThread.Create(TheConnector: TComponentStreamClient;
  CreateSuspended: Boolean);
begin
  FConnector:=TheConnector;
  Priority:=tpNormal;
  inherited Create(CreateSuspended);
end;

destructor TCSCSendThread.Destroy;
begin
  if FConnector<>nil then FConnector.FSendThread:=nil;
  inherited Destroy;
end;

procedure TCSCSendThread.Execute;
var
  List: TList;
  Item: TQueueItem;
begin
  while not Terminated do begin
    if FConnector=nil then exit;
    Item:=nil;
    List:=FConnector.FSendQueue.LockList;
    if List.Count>0 then begin
      Item:=TQueueItem(List[0]);
      List.Delete(0);
    end;
    List.UnlockList;
    if Item=nil then begin
      // nothing to send -> sleep
      Suspend;
    end else begin
      // send item
      if Item.Count>0 then
        FConnector.FClientSocket.SendBuffer(Item.Data,Item.Count);
      Item.Free;
    end;
  end;
end;

{ TCSCReceiveThread }

constructor TCSCReceiveThread.Create(TheConnector: TComponentStreamClient;
  CreateSuspended: Boolean);
begin
  FConnector:=TheConnector;
  Priority:=tpNormal;
  inherited Create(CreateSuspended);
end;

destructor TCSCReceiveThread.Destroy;
begin
  inherited Destroy;
end;

procedure TCSCReceiveThread.Execute;
begin

end;

{ TQueueItem }

constructor TQueueItem.Create(AStream: TStream);
begin
  AStream.Position:=0;
  Count:=AStream.Size;
  GetMem(Data,Count);
  if Count>0 then
    AStream.Read(Data^,Count);
end;

destructor TQueueItem.Destroy;
begin
  FreeMem(Data);
  Data:=nil;
  inherited Destroy;
end;

end.

