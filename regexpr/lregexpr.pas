{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit LRegExpr;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SynRegExpr;

  { Regular expressions

    This is a simple interface to regular expressions. The syntax is similar
    to Perl regular expressions. An illegal pattern will raise an Exception.

    REMatches - function to test a regular expression.
    REVar - function to read the bracket values, found in the last call
            of REMatches.
    The ModifierStr sets the default values of r.e.syntax modifiers. Modifiers
    in r.e. (?ismx-ismx) will replace this default values.
    If you try to set unsupported modifier, Error will be called

     Modifier /i - caseinsensitive, initialized from RegExprModifierI
     Modifier /s - '.' works as any char (else as [^\n]),
     Modifier /g - Turns all operators to non-greedy. e.g. '*' works as '*?',
                   all '+' as '+?' and so on.
     Modifier /m - Treat string as multiple lines. That is, change `^' and `$'
                   from matching at only the very start or end of the string to
                   the start or end of any line anywhere within the string.

    Examples:
      if REMatches('Lazarus','aza') then ...

      if REMatches('Lazarus','a(.)a') then
        s:=REVar(1); // this will be the 'z'
  }

type

  { TLazRegExpression }

  TLazRegExpression = class(TRegExpr)
  public
    function Matches(const TheText, TheRegExpr: string;
                       const TheModifierStr: string = ''): boolean;
    function Replace(const TheText, FindRegExpr, ReplaceRegExpr: string;
                        UseSubstutition: boolean;
                        const TheModifierStr: string = ''): string;
    function Split(const TheText, SeparatorRegExpr: string;
                     const TheModifierStr: string = ''): TStrings;
    procedure Split(const TheText, SeparatorRegExpr: string; Pieces: TStrings;
                      const TheModifierStr: string = '');

  end;


function REMatches(const TheText, RegExpr: string;
                   const ModifierStr: string = ''): boolean;
function REVar(Index: Integer): string; // 1 is the first
procedure REVarPos(Index: Integer; var MatchStart, MatchLength: integer);
function REVarCount: Integer;
function REReplace(const TheText, FindRegExpr, ReplaceRegExpr: string;
                    UseSubstutition: boolean;
                    const ModifierStr: string = ''): string;
function RESplit(const TheText, SeparatorRegExpr: string;
                 const ModifierStr: string = ''): TStrings;
procedure RESplit(const TheText, SeparatorRegExpr: string; Pieces: TStrings;
                  const ModifierStr: string = '');
                  
implementation

var
  SynREEngine: TRegExpr;

procedure InitSynREEngine;
begin
  if SynREEngine=nil then
    SynREEngine:=TRegExpr.Create;
end;

function REMatches(const TheText, RegExpr: string;
  const ModifierStr: string): boolean;
begin
  InitSynREEngine;
  SynREEngine.ModifierStr:=ModifierStr;
  SynREEngine.Expression:=RegExpr;
  Result:=SynREEngine.Exec(TheText);
end;

function REVar(Index: Integer): string;
begin
  if SynREEngine<>nil then
    Result:=SynREEngine.Match[Index]
  else
    Result:='';
end;

procedure REVarPos(Index: Integer; var MatchStart, MatchLength: integer);
begin
  if SynREEngine<>nil then begin
    MatchStart:=SynREEngine.MatchPos[Index];
    MatchLength:=SynREEngine.MatchLen[Index];
  end else begin
    MatchStart:=-1;
    MatchLength:=-1;
  end;
end;

function REVarCount: Integer;
begin
  if SynREEngine<>nil then
    Result:=SynREEngine.SubExprMatchCount
  else
    Result:=0;
end;

function REReplace(const TheText, FindRegExpr, ReplaceRegExpr: string;
  UseSubstutition: boolean; const ModifierStr: string): string;
begin
  InitSynREEngine;
  SynREEngine.ModifierStr:=ModifierStr;
  SynREEngine.Expression:=FindRegExpr;
  Result:=SynREEngine.Replace(TheText,ReplaceRegExpr,UseSubstutition);
end;

function RESplit(const TheText, SeparatorRegExpr: string;
  const ModifierStr: string): TStrings;
begin
  Result:=TStringList.Create;
  RESplit(TheText,SeparatorRegExpr,Result,ModifierStr);
end;

procedure RESplit(const TheText, SeparatorRegExpr: string; Pieces: TStrings;
  const ModifierStr: string);
begin
  InitSynREEngine;
  SynREEngine.ModifierStr:=ModifierStr;
  SynREEngine.Expression:=SeparatorRegExpr;
  SynREEngine.Split(TheText,Pieces);
end;

{ TLazRegExpression }

function TLazRegExpression.Matches(const TheText, TheRegExpr: string;
  const TheModifierStr: string): boolean;
begin
  ModifierStr:=TheModifierStr;
  Expression:=TheRegExpr;
  Result:=Exec(TheText);
end;

function TLazRegExpression.Replace(const TheText, FindRegExpr,
  ReplaceRegExpr: string; UseSubstutition: boolean; const TheModifierStr: string
  ): string;
begin
  ModifierStr:=TheModifierStr;
  Expression:=FindRegExpr;
  Result:=inherited Replace(TheText,ReplaceRegExpr,UseSubstutition);
end;

function TLazRegExpression.Split(const TheText, SeparatorRegExpr: string;
  const TheModifierStr: string): TStrings;
begin
  Result:=TStringList.Create;
  Split(TheText,SeparatorRegExpr,Result,TheModifierStr);
end;

procedure TLazRegExpression.Split(const TheText, SeparatorRegExpr: string;
  Pieces: TStrings; const TheModifierStr: string);
begin
  ModifierStr:=TheModifierStr;
  Expression:=SeparatorRegExpr;
  inherited Split(TheText,Pieces);
end;

finalization
  FreeAndNil(SynREEngine);

end.

