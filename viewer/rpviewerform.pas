{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  Abstract:
    Main form of the RPViewer.
}
unit RPViewerForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, RPLog,
  RPNetClient, RPClientMsg;

type

  { TRPViewerWindow }

  TRPViewerWindow = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  RPViewerWindow: TRPViewerWindow;

implementation

{ TRPViewerWindow }

procedure TRPViewerWindow.FormCreate(Sender: TObject);
var
  Connector: TRPClientConnector;
  AMessage: TRPConnectMessage;
begin
  if Sender=nil then exit;
  MainRPLog:=TRPLog.Create(Self);
  Connector:=TRPClientConnector.Create(Self);
  if Application.HasOption('p','port') then
    Connector.ServerPort:=Application.GetOptionValue('p','port');
  if Application.HasOption('a','address') then
    Connector.ServerHost:=Application.GetOptionValue('a','address');
  Connector.Connect;
  
  AMessage:=TRPConnectMessage.Create;
  AMessage.PlayerName:='Player1';
  Connector.Send(AMessage);
end;

initialization
  {$I rpviewerform.lrs}

end.

