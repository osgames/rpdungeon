program RPViewer;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms
  { add your units here }, RPViewerForm, RPOpenGLCtrlPkg, RPProtocolPkg;

begin
  Application.Initialize;
  Application.CreateForm(TRPViewerWindow, RPViewerWindow);
  Application.Run;
end.

