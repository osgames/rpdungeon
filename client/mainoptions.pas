{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit MainOptions;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, Laz_XMLCfg, Dialogs, Forms, Controls, Graphics,
  RPProcs;

type
  { TMainOptions }

  TMainOptions = class
  private
    FFileDlgHeight: integer;
    FFileDlgHistoryList: TStringList;
    FFileDlgInitialDir: string;
    FFileDlgMaxHistory: integer;
    FFileDlgWidth: integer;
    FLoadedCharacters: TStrings;
    FMainFormBounds: TRect;
    FMaxRecentCharacterFilenames: integer;
    FRecentCharacterFilenames: TStringList;
    FModified: boolean;
    function GetModified: boolean;
    procedure SetFileDlgHeight(const AValue: integer);
    procedure SetFileDlgHistoryList(const AValue: TStringList);
    procedure SetFileDlgInitialDir(const AValue: string);
    procedure SetFileDlgMaxHistory(const AValue: integer);
    procedure SetFileDlgWidth(const AValue: integer);
    procedure SetLoadedCharacters(const AValue: TStrings);
    procedure SetMainFormBounds(const AValue: TRect);
    procedure SetMaxRecentCharacterFilenames(const AValue: integer);
    procedure SetModified(const AValue: boolean);
    procedure SetRecentCharacterFilenames(const AValue: TStringList);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure LoadFromFile(const Filename: string);
    procedure SaveToFile(const Filename: string);
    procedure InitializeFileDialog(FileDialog: TFileDialog);
    procedure SaveFileDialogSettings(FileDialog: TFileDialog);
    procedure AddToRecentCharacterFilenames(const Filename: string);
    procedure RemoveFromRecentCharacterFilenames(const Filename: string);
    procedure ReplaceLoadedCharacter(const OldFilename, NewFilename: string);
    function MainFormBoundsValid: boolean;
  public
    property FileDlgInitialDir: string read FFileDlgInitialDir write SetFileDlgInitialDir;
    property FileDlgWidth: integer read FFileDlgWidth write SetFileDlgWidth;
    property FileDlgHeight: integer read FFileDlgHeight write SetFileDlgHeight;
    property FileDlgHistoryList: TStringList read FFileDlgHistoryList write SetFileDlgHistoryList;
    property FileDlgMaxHistory: integer read FFileDlgMaxHistory write SetFileDlgMaxHistory;
    property Modified: boolean read GetModified write SetModified;
    property RecentCharacterFilenames: TStringList read FRecentCharacterFilenames write SetRecentCharacterFilenames;
    property MaxRecentCharacterFilenames: integer read FMaxRecentCharacterFilenames write SetMaxRecentCharacterFilenames;
    property MainFormBounds: TRect read FMainFormBounds write SetMainFormBounds;
    property LoadedCharacters: TStrings read FLoadedCharacters write SetLoadedCharacters;
  end;
  
implementation

{ TMainOptions }

function TMainOptions.GetModified: boolean;
begin
  Result:=FModified;
end;

procedure TMainOptions.SetFileDlgHeight(const AValue: integer);
begin
  if FFileDlgHeight=AValue then exit;
  FFileDlgHeight:=AValue;
end;

procedure TMainOptions.SetFileDlgHistoryList(const AValue: TStringList);
begin
  if FFileDlgHistoryList=AValue then exit;
  FFileDlgHistoryList.Assign(AValue);
  Modified:=true;
end;

procedure TMainOptions.SetFileDlgInitialDir(const AValue: string);
begin
  if FFileDlgInitialDir=AValue then exit;
  FFileDlgInitialDir:=AValue;
end;

procedure TMainOptions.SetFileDlgMaxHistory(const AValue: integer);
begin
  if FFileDlgMaxHistory=AValue then exit;
  FFileDlgMaxHistory:=AValue;
end;

procedure TMainOptions.SetFileDlgWidth(const AValue: integer);
begin
  if FFileDlgWidth=AValue then exit;
  FFileDlgWidth:=AValue;
end;

procedure TMainOptions.SetLoadedCharacters(const AValue: TStrings);
begin
  if FLoadedCharacters=AValue then exit;
  FLoadedCharacters.Assign(AValue);
end;

procedure TMainOptions.SetMainFormBounds(const AValue: TRect);
begin
  if CompareRect(@FMainFormBounds,@AValue) then exit;
  FMainFormBounds:=AValue;
end;

procedure TMainOptions.SetMaxRecentCharacterFilenames(
  const AValue: integer);
begin
  if FMaxRecentCharacterFilenames=AValue then exit;
  FMaxRecentCharacterFilenames:=AValue;
end;

procedure TMainOptions.SetModified(const AValue: boolean);
begin
  FModified:=AValue;
end;

procedure TMainOptions.SetRecentCharacterFilenames(
  const AValue: TStringList);
begin
  if FRecentCharacterFilenames=AValue then exit;
  FRecentCharacterFilenames.Assign(AValue);
  Modified:=true;
end;

constructor TMainOptions.Create;
begin
  FFileDlgHistoryList:=TStringList.Create;
  FRecentCharacterFilenames:=TStringList.Create;
  FLoadedCharacters:=TStringList.Create;
  Clear;
end;

destructor TMainOptions.Destroy;
begin
  FreeAndNil(FRecentCharacterFilenames);
  FreeAndNil(FFileDlgHistoryList);
  FreeAndNil(FLoadedCharacters);
  inherited Destroy;
end;

procedure TMainOptions.Clear;
begin
  FFileDlgInitialDir:='';
  FFileDlgWidth:=400;
  FFileDlgHeight:=300;
  FFileDlgMaxHistory:=15;
  FFileDlgHistoryList.Clear;
  FRecentCharacterFilenames.Clear;
  FMaxRecentCharacterFilenames:=20;
  FLoadedCharacters.Clear;
end;

procedure TMainOptions.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  FFileDlgInitialDir:=XMLConfig.GetValue(Path+'FileDlg/InitialDir/Value','');
  FFileDlgWidth:=XMLConfig.GetValue(Path+'FileDlg/Width/Value',400);
  FFileDlgHeight:=XMLConfig.GetValue(Path+'FileDlg/Height/Value',300);
  FFileDlgMaxHistory:=XMLConfig.GetValue(Path+'FileDlg/MaxHistory/Value',15);
  LoadRecentList(XMLConfig,FFileDlgHistoryList,Path+'FileDlg/History/');
  FMaxRecentCharacterFilenames:=XMLConfig.GetValue(
                                        Path+'RecentCharacterFilenames/Max',20);
  LoadRecentList(XMLConfig,FRecentCharacterFilenames,
                 Path+'RecentCharacterFilenames/Files/');
  LoadRectFromXMLConfig(XMLConfig,Path+'MainFormBounds/',FMainFormBounds);
  LoadStringList(XMLConfig,LoadedCharacters,Path+'LoadedCharacters/');
  RemoveDoubles(LoadedCharacters);

  Modified:=false;
end;

procedure TMainOptions.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  XMLConfig.SetDeleteValue(Path+'FileDlg/InitialDir/Value',FFileDlgInitialDir,'');
  XMLConfig.SetDeleteValue(Path+'FileDlg/Width/Value',FFileDlgWidth,400);
  XMLConfig.SetDeleteValue(Path+'FileDlg/Height/Value',FFileDlgHeight,300);
  XMLConfig.SetDeleteValue(Path+'FileDlg/MaxHistory/Value',FFileDlgMaxHistory,15);
  SaveRecentList(XMLConfig,FFileDlgHistoryList,Path+'FileDlg/History/');
  XMLConfig.SetDeleteValue(Path+'RecentCharacterFilenames/Max',
                           FMaxRecentCharacterFilenames,20);
  SaveRecentList(XMLConfig,FRecentCharacterFilenames,
                 Path+'RecentCharacterFilenames/Files/');
  SaveRectToXMLConfig(XMLConfig,Path+'MainFormBounds/',FMainFormBounds);
  SaveStringList(XMLConfig,LoadedCharacters,Path+'LoadedCharacters/');

  Modified:=false;
end;

procedure TMainOptions.LoadFromFile(const Filename: string);
var
  XMLConfig: TXMLConfig;
begin
  Clear;
  if not FileExists(Filename) then exit;
  XMLConfig:=TXMLConfig.Create(Filename);
  try
    LoadFromXMLConfig(XMLConfig,'Battlefield/');
  finally
    XMLConfig.Free;
  end;
end;

procedure TMainOptions.SaveToFile(const Filename: string);
var
  XMLConfig: TXMLConfig;
begin
  if FileExists(Filename) and (not Modified) then exit;
  XMLConfig:=TXMLConfig.CreateClean(Filename);
  try
    SaveToXMLConfig(XMLConfig,'Battlefield/');
    XMLConfig.Flush;
  finally
    XMLConfig.Free;
  end;
end;

procedure TMainOptions.InitializeFileDialog(FileDialog: TFileDialog);
begin
  FileDialog.InitialDir:=FileDlgInitialDir;
  FileDialog.Width:=FileDlgWidth;
  FileDialog.Height:=FileDlgHeight;
  FileDialog.HistoryList:=FFileDlgHistoryList;
end;

procedure TMainOptions.SaveFileDialogSettings(FileDialog: TFileDialog);
var s: string;
begin
  FFileDlgInitialDir:=FileDialog.InitialDir;
  FFileDlgWidth:=FileDialog.Width;
  FFileDlgHeight:=FileDialog.Height;
  s:=ExtractFilePath(FFileDlgInitialDir);
  if s<>'' then
    AddToRecentList(s,FFileDlgHistoryList,FFileDlgMaxHistory);
end;

procedure TMainOptions.AddToRecentCharacterFilenames(
  const Filename: string);
begin
  if AddToRecentList(Filename,RecentCharacterFilenames,
    MaxRecentCharacterFilenames)
  then
    Modified:=true;
end;

procedure TMainOptions.RemoveFromRecentCharacterFilenames(
  const Filename: string);
begin
  if RemoveFromRecentList(Filename,RecentCharacterFilenames) then
    Modified:=true;
end;

procedure TMainOptions.ReplaceLoadedCharacter(const OldFilename,
  NewFilename: string);
var
  i: LongInt;
begin
  repeat
    i:=LoadedCharacters.IndexOf(OldFilename);
    if i>=0 then LoadedCharacters.Delete(i);
  until i<0;
  LoadedCharacters.Add(NewFilename);
end;

function TMainOptions.MainFormBoundsValid: boolean;
begin
  Result:=(FMainFormBounds.Left+200<FMainFormBounds.Right)
           and (FMainFormBounds.Top+200<FMainFormBounds.Bottom);
  DebugLn('TMainOptions.MainFormBoundsValid ',dbgs(FMainFormBounds),' ',dbgs(Result));
end;

end.

