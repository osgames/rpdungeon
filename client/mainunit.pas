{ Copyright (C) 2006 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, LCLType, LResources, Forms, Controls, Graphics,
  Dialogs, RPOpenGLControl, FPImage, AsmTypes, AsmShaders, AsmUtils,
  ExtCtrls, GL, Vectors, GLU, StdCtrls, RTTICtrls, ComCtrls, RPGLUtils,
  RPGLMesh, RPGLObjects, RPGLObjDB, MainOptions, Buttons;

type

  { TMainForm }

  TMainForm = class(TForm)
    SaveAllButton: TButton;
    GameView: TRPOpenGLControl;
    RPObjectGroupBox: TGroupBox;
    RPObjectNameLabel: TLabel;
    RPObjectNameTIEdit: TTIEdit;
    ClassGroupBox: TGroupBox;
    ModeRadioGroup: TRadioGroup;
    ClassTreeView: TTreeView;
    procedure ClassTreeViewSelectionChanged(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GameViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
      );
    procedure GameViewMouseDown(Sender: TOBject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GameViewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure GameViewMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure GameViewPaint(Sender: TObject);
    procedure ModeRadioGroupClick(Sender: TObject);
    procedure SaveAllButtonClick(Sender: TObject);
    procedure SceneEditModeChanged(Sender: TObject);
    procedure SceneSelectionChanged(Sender: TObject);
    procedure SceneSelectionChanging(Sender: TObject);
  private
    FOptions: TMainOptions;
    FOptionsFilename: string;
    LastMouseCoord: TPoint;
    procedure DoSaveAll;
  public
    Scene: TRPScene;
    procedure SaveOptions;
    property Options: TMainOptions read FOptions;
    property OptionsFilename: string read FOptionsFilename;
  end;

var
  MainForm: TMainForm;
  
implementation

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);

  procedure TestRayFaceIntersection;
  var
    RayOrigin, Ray: TVector3;
    FaceStart, FaceSide1, FaceSide2: TVector3;
    FaceFactor1, FaceFactor2, RayFactor: GLfloat;
  begin
    RayOrigin:=Vector(0,5,28.5);
    Ray:=Vector(-0.022,-0.118,-0.992);
    FaceStart:=Vector(1.251,1,1);
    FaceSide1:=Vector(10,0.1,0.1);
    FaceSide2:=Vector(0.1,10,0.1);
    if RayFaceIntersection(RayOrigin,Ray,
                           FaceStart,FaceSide1,FaceSide2,
                           FaceFactor1,FaceFactor2,RayFactor) then
    begin
      DebugLn('TMainForm.FormCreate FaceFactor1=',dbgs(FaceFactor1),' FaceFactor2=',dbgs(FaceFactor2),' RayFactor=',dbgs(RayFactor));
      Halt;
    end;
  end;
  
begin

  
  Caption:='RPDungeon 0.1a';
  FOptionsFilename:='mainconfig.xml';
  FOptions:=TMainOptions.Create;

  Scene:=TRPScene.Create(Self);
  Scene.GameView:=GameView;
  Scene.OnEditModeChanged:=@SceneEditModeChanged;
  Scene.OnSelectionChanging:=@SceneSelectionChanging;
  Scene.OnSelectionChanged:=@SceneSelectionChanged;

  Scene.FillClassTreeView(ClassTreeView);
  Scene.FillEditModeRadioGroup(ModeRadioGroup);
  
  FOptions.LoadFromFile(OptionsFilename);
  if FOptions.MainFormBoundsValid then
    BoundsRect:=FOptions.MainFormBounds;
end;

procedure TMainForm.ClassTreeViewSelectionChanged(Sender: TObject);
begin
  if Sender=nil then ;
  if (ClassTreeView.Selected<>nil)
  and (TObject(ClassTreeView.Selected.Data) is TRPCreationItem) then
    Scene.SelectedCreationItem:=TRPCreationItem(ClassTreeView.Selected.Data)
  else
    Scene.SelectedCreationItem:=nil;
end;

procedure TMainForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if Sender=nil then ;
  if CloseAction=caNone then ;
  SaveOptions;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  if Sender=nil then ;
  FreeThenNil(Scene);
  FreeThenNil(FOptions);
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  GameViewKeyDown(Sender,Key,Shift);
end;

procedure TMainForm.GameViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Speed: GLfloat;
  Handled: Boolean;
begin
  if Sender=nil then ;
  if ssShift in Shift then Speed:=5 else Speed:=1;
  Handled:=true;
  case Key of
  VK_LEFT:  Scene.MoveCamera(Speed*(-1),0,0);
  VK_RIGHT: Scene.MoveCamera(Speed*1,0,0);
  VK_UP:    Scene.MoveCamera(0,0,Speed*1);
  VK_DOWN:  Scene.MoveCamera(0,0,Speed*(-1));
  VK_DELETE: Scene.DeleteSelection;
  else
    Handled:=false;
  end;
  if Handled then Key:=VK_UNKNOWN;
end;

procedure TMainForm.GameViewMouseDown(Sender: TOBject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  MousePos: TPoint;
begin
  if Sender=nil then ;
  if Shift=[] then ;
  MousePos:=Point(X,Y);
  LastMouseCoord:=MousePos;

  case Scene.EditMode of
  emSelectObject:
    if Button=mbLeft then
      Scene.SelectRPObject(MousePos);
  emCreateObject:
    if Scene.SelectedCreationItem<>nil then
      Scene.SelectedCreationItem.MouseDown(Button,Shift,
                                     Scene.ScreenToFloorPosition(MousePos));
  end;
  Scene.UpdatePointingObject(MousePos);

  GameView.SetFocus;
  //DebugLn('TMainForm.GameViewMouseDown CanFocus=',dbgs(GameView.CanFocus));
end;

procedure TMainForm.GameViewMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  MouseDiff: TPoint;
  MousePos: TPoint;
  LastFloorCoord: TVector3;
  CurFloorCoord: TVector3;
  DiffFloorCoord: TVector3;
begin
  if Sender=nil then ;
  MousePos:=Point(X,Y);
  MouseDiff:=Point(X-LastMouseCoord.X,Y-LastMouseCoord.Y);
  case Scene.EditMode of
  emSelectObject:
    begin
      if ssLeft in Shift then begin
        if ssShift in Shift then begin
          // move camera around center of view
          Scene.MoveCameraAroundCenterOfView(-MouseDiff.X,-MouseDiff.Y);
        end else begin
          // move selection
          LastFloorCoord:=Scene.ScreenToFloorPosition(LastMouseCoord);
          CurFloorCoord:=Scene.ScreenToFloorPosition(MousePos);
          DiffFloorCoord:=CurFloorCoord-LastFloorCoord;
          Scene.MoveSelection(DiffFloorCoord);
        end;
      end else if ssMiddle in Shift then begin
        // rotate selection
        if Scene.SelectedRPObject<>nil then begin
          Scene.SelectedRPObject.AsmObject.RotateAboutLocalOrigin(MouseDiff.X*5,0,1,0);
          Scene.GameView.Invalidate;
        end;
      end else begin
        Scene.UpdatePointingObject(MousePos);
      end;
    end;
  emCreateObject:
    begin
      Scene.HidePointingBox;
      CurFloorCoord:=Scene.ScreenToFloorPosition(MousePos);
      Scene.MoveCreationMarker(Shift,CurFloorCoord);
    end;
  else
    Scene.HidePointingBox;
  end;
  LastMouseCoord:=Point(X,Y);
end;

procedure TMainForm.GameViewMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  CurFloorCoord: TVector3;
begin
  if Sender=nil then ;
  if (Shift=[]) or (MousePos.X=0) or Handled then ;
  Scene.Camera.Radius:=Max(1.0,Scene.Camera.Radius-WheelDelta);
  case Scene.EditMode of
  emSelectObject:
    ;
  emCreateObject:
    begin
      CurFloorCoord:=Scene.ScreenToFloorPosition(MousePos);
      Scene.MoveCreationMarker(Shift,CurFloorCoord);
    end;
  end;
  GameView.Invalidate;
end;

procedure TMainForm.GameViewPaint(Sender: TObject);
begin
  if Sender=nil then ;
  Scene.RenderWorld;
end;

procedure TMainForm.ModeRadioGroupClick(Sender: TObject);
var
  s: string;
  m: TRPEditMode;
begin
  if Sender=nil then ;
  if ModeRadioGroup.ItemIndex>=0 then begin
    s:=ModeRadioGroup.Items[ModeRadioGroup.ItemIndex];
    for m:=Low(TRPEditMode) to High(TRPEditMode) do begin
      if s=RPEditModeNames[m] then
        Scene.EditMode:=m;
    end;
  end;
end;

procedure TMainForm.SaveAllButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  DoSaveAll;
end;

procedure TMainForm.SceneEditModeChanged(Sender: TObject);
begin
  if Sender=nil then ;
  ModeRadioGroup.ItemIndex:=Ord(Scene.EditMode);
end;

procedure TMainForm.SceneSelectionChanged(Sender: TObject);
begin
  if Sender=nil then ;
  if Scene.SelectedRPObject<>nil then begin
    RPObjectNameTIEdit.Link.SetObjectAndProperty(Scene.SelectedRPObject,'Name');
    RPObjectNameTIEdit.Enabled:=true;
  end else begin
    RPObjectNameTIEdit.Enabled:=false;
  end;
end;

procedure TMainForm.SceneSelectionChanging(Sender: TObject);
begin
  if Sender=nil then ;
  RPObjectNameTIEdit.Link.SetObjectAndProperty(nil,'Name');
end;

procedure TMainForm.SaveOptions;
begin
  FOptions.MainFormBounds:=BoundsRect;
  FOptions.SaveToFile(OptionsFilename);
end;

procedure TMainForm.DoSaveAll;
begin

  SaveOptions;
end;

initialization
  {$I mainunit.lrs}

end.

