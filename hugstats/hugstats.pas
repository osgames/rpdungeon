{ This file was automatically created by Lazarus. Do not edit!
This source is only used to compile and install the package.
 }

unit HUGStats; 

interface

uses
  NewStats, RMStats, StatGainLevel, LazarusPackageIntf; 

implementation

procedure Register; 
begin
end; 

initialization
  RegisterPackage('HUGStats', @Register); 
end.
