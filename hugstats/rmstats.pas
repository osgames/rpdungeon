{ Copyright (C) 2004 Mattias Gaertner

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit RMStats;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Laz_XMLCfg;

const
  StatMinID = 1;
  StatMaxID = 10;

type
  { TCharakterTePotStats }

  TCharakterTePotStats = class(TObject)
  public
    Temp, Pot: array[StatMinID..StatMaxID] of integer;
    constructor Create;
    procedure Clear; virtual;
    function LoadFromStream(var s:TStream):boolean; virtual;
    function SaveToStream(var s:TStream):boolean; virtual;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string); virtual;
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string); virtual;
    procedure ConvertOldToUTF8;
  end;
  
  { TCharakterStats }

  TCharakterStats = class(TCharakterTePotStats)
  public
    MiscStatBoni: array[1..10] of integer;
    ItemBoni: array[1..10] of integer;
    DevPtsAdder: array[1..10] of single;
    DevPtsMultiplier: array[1..10] of single;
    Notes: array[1..10] of string;
    procedure Clear; override;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string); override;
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string); override;
    function GetDefaultDevPtsMultiplier(i: integer): single;
  end;

const
  StatAbbrevations: array[StatMinID..StatMaxID] of string = (
    'Co',
    'Ag',
    'sD',
    'Me',
    'Re',
    'St',
    'Qu',
    'Pr',
    'Em',
    'In'
    );

function Bonus2Stat(a: integer): integer;
function Stat2Bonus(a: integer): integer;
function Pot2DevPts(Pot: integer): single;
function Wuerfe2Potential(a,b:integer):integer;
function Potential2Wuerfe(p:integer):integer;
procedure GainStatsDueToLevel(Stats: TCharakterTePotStats; AnzLvl: integer;
  NoStatLossDueToLvl: boolean);

var
  BonusMedian: integer;

implementation

var
  Potential: array[1..10,1..42] of integer;
  HochWurfTab: array[0..9,0..19] of integer;

{ Liefert zum temporary Bonus einen Stat }
function Bonus2Stat(a:integer):integer;
begin
  case a of
  -25..-16:Result:=(a div 2)+14;
  -15..-11:Result:=a+21;
  -10:Result:=11;
  -9:Result:=13;
  -8:Result:=16;
  -7:Result:=19;
  -6:Result:=22;
  -5:Result:=25;
  -4:Result:=29;
  -3:Result:=32;
  -2:Result:=35;
  -1:Result:=38;
  0:Result:=50;
  1:Result:=61;
  2:Result:=65;
  3:Result:=69;
  4:Result:=73;
  5:Result:=76;
  6:Result:=79;
  7:Result:=82;
  8:Result:=85;
  9:Result:=88;
  10..15:Result:=a+80;
  16..24:Result:=(a div 2)+88;
  25..45:Result:=(a div 5)+95;
  46..65:Result:=((a-2) div 4)+94;
  66..80:Result:=(a div 3)+88;
  81..90:Result:=(a div 2)+74;
  91..999:Result:=a+29;
  else Result:=0;
  end;
end;

{ Liefert zum temporary Stat den Bonus }
function Stat2Bonus(a:integer):integer;
begin
  case a of
    1..5:Result:=a*2-27;
    6..11:Result:=a-21;
    12..23:Result:=a div 3-13;
    24:Result:=-5;
    25..39:Result:=(a-1) div 3-13;
    40:Result:=-1;
    41..59:Result:=0;
    60:Result:=1;
    61..89:Result:=a div 3-20;
    90..95:Result:=a-80;
    96..100:Result:=a*2-175;
    101..104:Result:=a*5-475;
    105..109:Result:=a*4-371;
    110..114:Result:=a*3-262;
    115..119:Result:=a*2-238;
  else Result:=a-29;
  end;
end;

{ Liefert zum Pot die Dev.Pts }
function Pot2DevPts(Pot:integer):single;
begin
  case Pot of
    1:Result:=1.0;
    2:Result:=1.3;
    3:Result:=1.6;
    4:Result:=1.8;
    5:Result:=2.0;
    6:Result:=2.2;
    7:Result:=2.4;
    8:Result:=2.6;
    9:Result:=2.8;
    10:Result:=3.0;
    11:Result:=3.2;
    12..14:Result:=3.4;
    15..17:Result:=3.6;
    18..20:Result:=3.8;
    21..23:Result:=4.0;
    24..27:Result:=4.2;
    28..30:Result:=4.4;
    31..33:Result:=4.6;
    34..36:Result:=4.8;
    37..40:Result:=5.0;
    41..59:Result:=5.5;
    60..63:Result:=6.0;
    64..66:Result:=6.3;
    67..71:Result:=6.6;
    72..74:Result:=6.8;
    75..77:Result:=7.0;
    78..80:Result:=7.4;
    81..83:Result:=7.7;
    84..86:Result:=8.0;
    87..89:Result:=8.2;
    90:Result:=8.4;
    91:Result:=8.6;
    92:Result:=8.7;
    93:Result:=8.8;
    94:Result:=8.9;
    95:Result:=9.0;
    96:Result:=9.2;
    97:Result:=9.4;
    98:Result:=9.6;
    99:Result:=9.8;
    100:Result:=10.0;
    101:Result:=10.5;
    102:Result:=11.0;
    103:Result:=11.5;
    104:Result:=12.0;
    105:Result:=12.4;
    106:Result:=12.8;
    107:Result:=13.2;
    108:Result:=13.6;
    109:Result:=14.0;
    110:Result:=14.3;
    111:Result:=14.6;
    112:Result:=14.9;
    113:Result:=15.2;
    114:Result:=15.5;
    115:Result:=15.7;
    116:Result:=15.9;
    117:Result:=16.1;
    118:Result:=16.3;
    119:Result:=16.5;
  else
    Result:=(pot div 10)+4.6;
  end;
end;

{ Stat-Generierung: aus 1.Wurf und 2.Wurf den Potential berechnen }
function Wuerfe2Potential(a,b:integer):integer;
var c:integer;
begin
  case a of
    0..24:c:=1;
    25..39:c:=2;
    40..59:c:=3;
    60..74:c:=4;
    75..84:c:=5;
    85..89:c:=6;
    90..94:c:=7;
    95..97:c:=8;
    98..99:c:=9;
  else c:=10;
  end;
  case b of
    1..10:b:=2;
    11..20:b:=3;
    21..30:b:=4;
    31..35:b:=5;
    36..40:b:=6;
    41..45:b:=7;
    46..49:b:=8;
    50..89:b:=b div 2-16;
  else b:=b-61;
  end;
  c:=Potential[c,b];
  if c<a then c:=a;
  Result:=c;
end;

{ 1.temp + 2.temp = pot, diese Tabelle wird hier generiert }
procedure BauPotentials;
var a,b:integer;
begin
  Potential[1,1]:=1; Potential[1,2]:=25;
  Potential[2,1]:=21;Potential[2,2]:=36;
  Potential[3,1]:=31;Potential[3,2]:=56;
  Potential[4,1]:=56;Potential[4,2]:=73;
  Potential[5,1]:=56;Potential[5,2]:=83;
  Potential[6,1]:=66;Potential[6,2]:=88;
  Potential[7,1]:=84;Potential[7,2]:=93;
  Potential[8,1]:=90;Potential[8,2]:=96;
  Potential[9,1]:=94;Potential[9,2]:=98;
  Potential[10,1]:=100;Potential[10,2]:=100;Potential[10,39]:=101;
  for a:=0 to 2 do Potential[1,2+a]:=25+a*5;
  for a:=0 to 26 do Potential[1,5+a]:=38+a*2;
  for a:=0 to 7 do Potential[1,32+a]:=91+a;
  for a:=0 to 3 do Potential[2,2+a]:=33+a*3;
  for a:=0 to 20 do Potential[2,6+a]:=45+a*2;
  for a:=0 to 12 do Potential[2,a+27]:=86+a;
  for a:=0 to 3 do Potential[3,2+a]:=50+a*3;
  for a:=0 to 4 do Potential[3,6+a]:=62+a*2;
  for a:=0 to 28 do Potential[3,11+a]:=71+a;
  for a:=0 to 9 do Potential[4,2+a]:=54+a*2;
  for a:=0 to 27 do Potential[4,12+a]:=74+a;
  Potential[4,36]:=97;Potential[4,37]:=98;Potential[4,38]:=98;Potential[4,39]:=99;
  for a:=0 to 9 do Potential[5,2+a]:=64+a*2;
  for a:=0 to 3 do Potential[5,a+12]:=84+a;
  for a:=0 to 23 do Potential[5,a+16]:=88+a div 2;
  for a:=0 to 14 do Potential[6,2+a]:=44+a*3;
  for a:=0 to 22 do Potential[6,a+17]:=89+a div 2;
  for a:=0 to 23 do Potential[7,2+a]:=22+a*3;
  for a:=0 to 13 do Potential[7,a+26]:=94+a div 2;
  for a:=0 to 10 do Potential[8,a+29]:=97+a div 3;
  for a:=0 to 6 do Potential[9,a+33]:=99+a div 4;

  for a:=1 to 10 do begin
    for b:=39 to 41 do Potential[a,b+1]:=Potential[a,b]+1;
  end;
end;

function Potential2Wuerfe(p: integer): integer;
begin
  for Result:=20 to 100 do
    if Wuerfe2Potential(Result,Result)>p then exit;
end;

procedure GainStatsDueToLevel(Stats: TCharakterTePotStats; AnzLvl: integer;
  NoStatLossDueToLvl: boolean);
var
  l: Integer;
  s: Integer;
  r: Integer;
  diff: Integer;
begin
  for l:=1 to AnzLvl do begin // jedes lvl hochwürfeln
    for s:=1 to 10 do begin   // jeden Stat hochwürfeln
      r:=random(100)+1;
      if (r<5) or (r=100) then begin  // Chance auf 00
        if r<5 then r:=random(3)*4;   // bei 1-4 drehen, bei Lost wird verdoppelt
        if (r=0) or (r=100) then begin // 00 = 100
          if Stats.Pot[s]-Stats.Temp[s]<15 then begin // bis Dif 15 kann 00: +1 Pot und Temp=Pot
            inc(Stats.Pot[s]);
            Stats.Temp[s]:=Stats.Pot[s];
          end;
        end
        else if not NoStatLossDueToLvl then
          dec(Stats.Temp[s],r); //1-4
      end
      else begin
        diff:=Stats.Pot[s]-Stats.Temp[s];
        if diff=0 then begin // 96-99: Pot +1
          if r>95 then inc(Stats.Pot[s]);
        end
        else if diff>0 then begin // Dif zwischen Pot und Temp ...
          case diff of
            1..4  :diff:=diff;
            5     :diff:=4;
            6..7  :diff:=5;
            8..9  :diff:=6;
            10..11:diff:=7;
            12..14:diff:=8;
          else     diff:=9;
          end;
          inc(Stats.Temp[s],HochWurfTab[diff,(r-4) div 5]);
        end;
      end;
    end;
  end;
end;

{ Hochwürfel-Tabelle }
procedure BauHochWurf;
var a:integer;
begin
  HochWurfTab[0,0]:=5;
  for a:=1 to 18 do HochWurfTab[0,a]:=a*5+6;
  HochWurfTab[0,19]:=100;
  for a:=0 to 18 do begin
    HochWurfTab[1,a]:=HochWurfTab[0,a] div 51;
    HochWurfTab[2,a]:=(HochWurfTab[0,a]+2) div 34;
    HochWurfTab[3,a]:=(HochWurfTab[0,a]+1) div 25;
    HochWurfTab[4,a]:=(HochWurfTab[0,a]+1) div 20;
    HochWurfTab[5,a]:=(HochWurfTab[0,a]+4) div 15;
    HochWurfTab[6,a]:=(HochWurfTab[0,a]+2) div 12;
    HochWurfTab[7,a]:=(HochWurfTab[0,a]+5) div 10;
  end;
  HochWurfTab[8,0]:=1; HochWurfTab[8,1]:=2;  HochWurfTab[8,2]:=3;
  for a:=3 to 18 do HochWurfTab[8,a]:=(HochWurfTab[0,a]+24) div 10;
  for a:=0 to 9 do HochWurfTab[9,a]:=a+1;
  for a:=10 to 18 do HochWurfTab[9,a]:=(HochWurfTab[0,a]+54) div 10;
end;

{ TCharakterTePotStats }

constructor TCharakterTePotStats.Create;
begin
  Clear;
end;

procedure TCharakterTePotStats.Clear;
var a:integer;
begin
  for a:=1 to 10 do begin
    temp[a]:=90;
    pot[a]:=90;
  end;
end;

function TCharakterTePotStats.LoadFromStream(var s: TStream): boolean;
var a:integer;
begin
  try
    for a:=1 to 10 do begin
      s.ReadBuffer(temp[a],sizeof(temp[a]));
      s.ReadBuffer(pot[a],sizeof(pot[a]));
    end;
    Result:=true;
  except
    Result:=false;
  end;
end;

function TCharakterTePotStats.SaveToStream(var s: TStream): boolean;
var a:integer;
begin
  try
    for a:=1 to 10 do begin
      s.WriteBuffer(temp[a],sizeof(temp[a]));
      s.WriteBuffer(pot[a],sizeof(pot[a]));
    end;
    Result:=true;
  except
    Result:=false;
  end;
end;

procedure TCharakterTePotStats.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  i: Integer;
begin
  for i:=1 to 10 do begin
    temp[i]:=XMLConfig.GetValue(Path+'Stat'+IntToStr(i)+'/Temporary',90);
    pot[i]:=XMLConfig.GetValue(Path+'Stat'+IntToStr(i)+'/Potential',90);
  end;
end;

procedure TCharakterTePotStats.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  i: Integer;
begin
  for i:=Low(Temp) to High(Temp) do begin
    XMLConfig.SetDeleteValue(Path+'Stat'+IntToStr(i)+'/Temporary',temp[i],90);
    XMLConfig.SetDeleteValue(Path+'Stat'+IntToStr(i)+'/Potential',pot[i],90);
  end;
end;

procedure TCharakterTePotStats.ConvertOldToUTF8;
begin

end;

{ TCharakterStats }

procedure TCharakterStats.Clear;
var
  i: Integer;
begin
  inherited Clear;
  for i:=Low(MiscStatBoni) to High(MiscStatBoni) do begin
    MiscStatBoni[i]:=0;
    ItemBoni[i]:=0;
    DevPtsAdder[i]:=0;
    DevPtsMultiplier[i]:=GetDefaultDevPtsMultiplier(i);
    Notes[i]:='';
  end;
end;

procedure TCharakterStats.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  i: Integer;
begin
  inherited LoadFromXMLConfig(XMLConfig, Path);
  for i:=Low(Temp) to High(Temp) do begin
    MiscStatBoni[i]:=XMLConfig.GetValue(Path+'Stat'+IntToStr(i)+'/MiscBonus',0);
    ItemBoni[i]:=XMLConfig.GetValue(Path+'Stat'+IntToStr(i)+'/ItemBoni',0);
    DevPtsAdder[i]:=Single(XMLConfig.GetExtendedValue(
                                    Path+'Stat'+IntToStr(i)+'/DevPtsAdder',0.0));
    DevPtsMultiplier[i]:=Single(XMLConfig.GetExtendedValue(
    Path+'Stat'+IntToStr(i)+'/DevPtsMultiplier',GetDefaultDevPtsMultiplier(i)));
    Notes[i]:=XMLConfig.GetValue(Path+'Stat'+IntToStr(i)+'/Notes','');
  end;
end;

procedure TCharakterStats.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  i: Integer;
begin
  inherited SaveToXMLConfig(XMLConfig, Path);
  for i:=Low(Temp) to High(Temp) do begin
    XMLConfig.SetDeleteValue(Path+'Stat'+IntToStr(i)+'/MiscBonus',
                             MiscStatBoni[i],0);
    XMLConfig.SetDeleteValue(Path+'Stat'+IntToStr(i)+'/ItemBoni',
                             ItemBoni[i],0);
    XMLConfig.SetDeleteExtendedValue(Path+'Stat'+IntToStr(i)+'/DevPtsAdder',
                             DevPtsAdder[i],0);
    XMLConfig.SetDeleteExtendedValue(Path+'Stat'+IntToStr(i)+'/DevPtsMultiplier',
                             DevPtsMultiplier[i],GetDefaultDevPtsMultiplier(i));
    XMLConfig.SetDeleteValue(Path+'Stat'+IntToStr(i)+'/Notes',Notes[i],'');
  end;
end;

function TCharakterStats.GetDefaultDevPtsMultiplier(i: integer): single;
begin
  if (i>=1) and (i<=5) then
    Result:=1.0
  else
    Result:=0.0;
end;

initialization
  BauPotentials;
  BauHochWurf;
  BonusMedian:=110; // dieser Durchschnittswert wird angepeilt

end.

