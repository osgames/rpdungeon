{ Copyright (C) 2004 Mattias Gaertner

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
  Place - Suite 330, Boston, MA 02111-1307, USA.
}
unit NewStats;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, Buttons,
  StdCtrls, Grids, RMStats, StatGainLevel;

type
  TNewStatsDialog = class(TForm)
    SortFinalButton: TButton;
    GainButton: TButton;
    ComboABButton: TButton;
    ComboBAButton: TButton;
    ComboACButton: TButton;
    ComboCAButton: TButton;
    ComboBCButton: TButton;
    ComboCBButton: TButton;
    CombinationGroupBox: TGroupBox;
    ComboShowBonusRadioButton: TRadioButton;
    ComboShowStatRadioButton: TRadioButton;
    FinalGroupBox: TGroupBox;
    RollRow1Button: TButton;
    RollRow2Button: TButton;
    RollRow3Button: TButton;
    RollsGroupBox: TGroupBox;
    OkButton: TButton;
    CancelButton: TButton;
    RollStringGrid: TStringGrid;
    ComboStringGrid: TStringGrid;
    FinalStringGrid: TStringGrid;
    procedure CombinationGroupBoxResize(Sender: TObject);
    procedure ComboABButtonClick(Sender: TObject);
    procedure ComboACButtonClick(Sender: TObject);
    procedure ComboBAButtonClick(Sender: TObject);
    procedure ComboBCButtonClick(Sender: TObject);
    procedure ComboCAButtonClick(Sender: TObject);
    procedure ComboCBButtonClick(Sender: TObject);
    procedure ComboShowBonusRadioButtonChange(Sender: TObject);
    procedure ComboShowStatRadioButtonChange(Sender: TObject);
    procedure FinalGroupBoxResize(Sender: TObject);
    procedure GainButtonClick(Sender: TObject);
    procedure NewStatsDialogCreate(Sender: TObject);
    procedure RollRow1ButtonClick(Sender: TObject);
    procedure RollRow2ButtonClick(Sender: TObject);
    procedure RollRow3ButtonClick(Sender: TObject);
    procedure RollStringGridKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RollsGroupBoxResize(Sender: TObject);
    procedure SortFinalButtonClick(Sender: TObject);
  private
    procedure ResizeColums(Grid: TStringGrid);
    procedure ReRollRow(Column: integer);
    procedure UseRolls(ComboColumn: integer);
    procedure SortFinals;
  public
    function CreateStats: TCharakterTePotStats;
    procedure UseFinalStats(Stats: TCharakterTePotStats);
    procedure UpdateFinalSums;
    procedure UpdateRollSums;
    procedure UpdateComboSums;
    procedure UpdateCombos;
    procedure UpdateRolls;
  end;

var
  NewStatsDialog: TNewStatsDialog;

function NeueStatsGenerieren: TCharakterTePotStats;

implementation

const
  ComboTemp:array[0..5] of integer=(0,1,0,2,1,2);
  ComboPot :array[0..5] of integer=(1,0,2,0,2,1);

function NeueStatsGenerieren: TCharakterTePotStats;
begin
  Result:=nil;
  if NewStatsDialog=nil then
    NewStatsDialog:=TNewStatsDialog.Create(nil);
  try
    if NewStatsDialog.ShowModal=mrOk then begin
      Result:=NewStatsDialog.CreateStats;
    end;
  finally
    if NewStatsDialog.Owner=nil then
      NewStatsDialog.Free;
  end;
end;

{ TNewStatsDialog }

procedure TNewStatsDialog.NewStatsDialogCreate(Sender: TObject);
var
  c: Integer;
  ColCnt: LongInt;
  RowCnt: LongInt;
  r: Integer;
  Median: LongInt;
begin
  if Sender=nil then ;

  ColCnt:=RollStringGrid.ColCount;
  RowCnt:=RollStringGrid.RowCount;
  Median:=Potential2Wuerfe(Bonus2Stat(BonusMedian div 10));
  writeln('TNewStatsDialog.NewStatsDialogCreate ',Median,' ',BonusMedian,' ',RowCnt);
  for c:=0 to ColCnt-1 do begin
    RollStringGrid.Cells[c,0]:=chr(c+ord('A'));
    for r:=1 to RowCnt-1 do begin
      RollStringGrid.Cells[c,r]:=IntToStr(Median);
    end;
  end;
  
  ComboStringGrid.Cells[0,0]:='AB';
  ComboStringGrid.Cells[1,0]:='BA';
  ComboStringGrid.Cells[2,0]:='AC';
  ComboStringGrid.Cells[3,0]:='CA';
  ComboStringGrid.Cells[4,0]:='BC';
  ComboStringGrid.Cells[5,0]:='CB';
  
  UpdateRolls;
end;

procedure TNewStatsDialog.RollRow1ButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  ReRollRow(1);
end;

procedure TNewStatsDialog.RollRow2ButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  ReRollRow(2);
end;

procedure TNewStatsDialog.RollRow3ButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  ReRollRow(3);
end;

procedure TNewStatsDialog.RollStringGridKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Sender=nil then ;
  if Key=0 then ;
  if Shift=[] then ;
  UpdateRolls;
end;

procedure TNewStatsDialog.CombinationGroupBoxResize(Sender: TObject);
begin
  if Sender=nil then ;
  ResizeColums(ComboStringGrid);
end;

procedure TNewStatsDialog.ComboABButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  UseRolls(0);
end;

procedure TNewStatsDialog.ComboACButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  UseRolls(2);
end;

procedure TNewStatsDialog.ComboBAButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  UseRolls(1);
end;

procedure TNewStatsDialog.ComboBCButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  UseRolls(4);
end;

procedure TNewStatsDialog.ComboCAButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  UseRolls(3);
end;

procedure TNewStatsDialog.ComboCBButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  UseRolls(5);
end;

procedure TNewStatsDialog.ComboShowBonusRadioButtonChange(Sender: TObject);
begin
  if Sender=nil then ;
  UpdateCombos;
end;

procedure TNewStatsDialog.ComboShowStatRadioButtonChange(Sender: TObject);
begin
  if Sender=nil then ;
  UpdateCombos;
end;

procedure TNewStatsDialog.FinalGroupBoxResize(Sender: TObject);
begin
  if Sender=nil then ;
  ResizeColums(FinalStringGrid);
end;

procedure TNewStatsDialog.GainButtonClick(Sender: TObject);
var
  Stats: TCharakterTePotStats;
begin
  if Sender=nil then ;
  Stats:=CreateStats;
  try
    if ShowStatsGainLevelDialog(Stats) then
      UseFinalStats(Stats);
  finally
    Stats.Free;
  end;
end;

procedure TNewStatsDialog.RollsGroupBoxResize(Sender: TObject);
begin
  if Sender=nil then ;
  ResizeColums(RollStringGrid);
end;

procedure TNewStatsDialog.SortFinalButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  SortFinals;
end;

procedure TNewStatsDialog.ResizeColums(Grid: TStringGrid);
var
  i: Integer;
  ColW: LongInt;
  ColCnt: LongInt;
  W: Integer;
begin
  ColCnt:=Grid.ColCount;
  W:=(Grid.ClientWidth-5);
  ColW:=W div ColCnt;
  for i:=0 to ColCnt-1 do begin
    if i<ColCnt-1 then
      Grid.ColWidths[i]:=ColW
    else
      Grid.ColWidths[i]:=W-(ColW*(ColCnt-1));
  end;
end;

procedure TNewStatsDialog.ReRollRow(Column: integer);
var
  Rolls: array[0..2,1..10] of integer;
  c: Integer;
  r: Integer;
  v: Integer;
  PotSum: array[0..5] of integer;
  Roll1: LongInt;
  Roll2: LongInt;
  PotStat: LongInt;
begin
  if MessageDlg('Reroll?','Reroll row '+IntToStr(Column)+'?',
    mtConfirmation,[mbYes,mbNo],0)<>mrYes
  then exit;
  
  // get rolls
  for c:=0 to 2 do begin
    for r:=1 to 10 do begin
      Rolls[c,r]:=StrToIntDef(RollStringGrid.Cells[c,r],0);
    end;
  end;
  
  // Normales Wuerfeln einer 10er Reihe
  for r:=1 to 10 do begin
    repeat
      v:=random(100)+1;
      if (v<10) and (random(5)=0) then v:=100;
      Rolls[Column-1,r]:=v;
    until v>25;
  end;
  
  // Ausgleichen der Stats
  repeat
    // Summen errechnen
    for c:=0 to 5 do begin
      if (ComboTemp[c]=Column-1) or (ComboPot[c]=Column-1) then begin
        // this row depends on the rerolling row
        PotSum[c]:=0;
        for r:=1 to 10 do begin
          Roll1:=Rolls[ComboTemp[c],r];
          Roll2:=Rolls[ComboPot[c],r];
          PotStat:=Wuerfe2Potential(Roll1,Roll2);
          v:=Stat2Bonus(PotStat);
          inc(PotSum[c],v);
        end;
      end else
        PotSum[c]:=BonusMedian;
    end;
    //Durchschnitts-Abweichung berechnen
    v:=0;
    for c:=0 to 5 do begin
      inc(v,sqr(PotSum[c]-BonusMedian));
    end;
    v:=integer(round(sqrt(v div 6)));
    //writeln(v);
    if v>10 then begin
      //Bei Abweichung: Einen Wert aendern
      Rolls[Column-1,random(10)+1]:=random(75)+26;
    end;
  until v<=10;
  
  // show new rolls
  for r:=1 to 10 do begin
    RollStringGrid.Cells[Column-1,r]:=IntToStr(Rolls[Column-1,r]);
  end;
  UpdateRolls;
end;

procedure TNewStatsDialog.UseRolls(ComboColumn: integer);
var
  r: Integer;
  Roll1: LongInt;
  Roll2: LongInt;
  PotStat: LongInt;
begin
  for r:=1 to 10 do begin
    Roll1:=StrToIntDef(RollStringGrid.Cells[ComboTemp[ComboColumn],r],0);
    Roll2:=StrToIntDef(RollStringGrid.Cells[ComboPot[ComboColumn],r],0);
    FinalStringGrid.Cells[0,r]:=IntToStr(Roll1);
    PotStat:=Wuerfe2Potential(Roll1,Roll2);
    FinalStringGrid.Cells[1,r]:=IntToStr(PotStat);
  end;
  UpdateFinalSums;
end;

procedure TNewStatsDialog.SortFinals;
var
  TempStat, PotStat: array[1..10] of integer;
  r: Integer;
  i: Integer;
  j: Integer;
  h: LongInt;
begin
  // read
  for r:=1 to 10 do begin
    TempStat[r]:=StrToIntDef(FinalStringGrid.Cells[0,r],0);
    PotStat[r]:=StrToIntDef(FinalStringGrid.Cells[1,r],0);
  end;
  // bubble sort
  for i:=1 to 9 do
    for j:=i+1 to 10 do
      if (PotStat[i]<PotStat[j])
      or ((PotStat[i]=PotStat[j]) and (TempStat[i]<TempStat[j])) then begin
        h:=PotStat[i];
        PotStat[i]:=PotStat[j];
        PotStat[j]:=h;
        h:=TempStat[i];
        TempStat[i]:=TempStat[j];
        TempStat[j]:=h;
      end;
  // put back
  for r:=1 to 10 do begin
    FinalStringGrid.Cells[0,r]:=IntToStr(TempStat[r]);
    FinalStringGrid.Cells[1,r]:=IntToStr(PotStat[r]);
  end;
end;

function TNewStatsDialog.CreateStats: TCharakterTePotStats;
var
  r: Integer;
begin
  Result:=TCharakterTePotStats.Create;
  for r:=1 to 10 do begin
    Result.Temp[r]:=StrToIntDef(FinalStringGrid.Cells[0,r],0);
    Result.Pot[r]:=StrToIntDef(FinalStringGrid.Cells[1,r],0);
  end;
end;

procedure TNewStatsDialog.UseFinalStats(Stats: TCharakterTePotStats);
var
  r: Integer;
begin
  for r:=1 to 10 do begin
    FinalStringGrid.Cells[0,r]:=IntToStr(Stats.Temp[r]);
    FinalStringGrid.Cells[1,r]:=IntToStr(Stats.Pot[r]);
  end;
  UpdateFinalSums;
end;

procedure TNewStatsDialog.UpdateFinalSums;
var
  c: Integer;
  r: Integer;
  Sum: Integer;
begin
  for c:=0 to FinalStringGrid.ColCount-1 do begin
    Sum:=0;
    for r:=1 to FinalStringGrid.RowCount-2 do begin
      inc(Sum,StrToIntDef(FinalStringGrid.Cells[c,r],0));
    end;
    FinalStringGrid.Cells[c,FinalStringGrid.RowCount-1]:=IntToStr(Sum);
  end;
end;

procedure TNewStatsDialog.UpdateRollSums;
var
  c: Integer;
  r: Integer;
  Sum: Integer;
begin
  for c:=0 to RollStringGrid.ColCount-1 do begin
    Sum:=0;
    for r:=1 to RollStringGrid.RowCount-2 do begin
      inc(Sum,StrToIntDef(RollStringGrid.Cells[c,r],0));
    end;
    RollStringGrid.Cells[c,RollStringGrid.RowCount-1]:=IntToStr(Sum);
  end;
end;

procedure TNewStatsDialog.UpdateComboSums;
var
  c: Integer;
  r: Integer;
  Sum: Integer;
begin
  for c:=0 to ComboStringGrid.ColCount-1 do begin
    Sum:=0;
    for r:=1 to ComboStringGrid.RowCount-2 do begin
      inc(Sum,StrToIntDef(ComboStringGrid.Cells[c,r],0));
    end;
    ComboStringGrid.Cells[c,ComboStringGrid.RowCount-1]:=IntToStr(Sum);
  end;
end;

procedure TNewStatsDialog.UpdateCombos;

  procedure CalculatePot(TargetCol, TempCol, PotCol: integer);
  var
    r: Integer;
    Roll1: LongInt;
    Roll2: LongInt;
    PotStat: LongInt;
    PotBonus: LongInt;
  begin
    for r:=1 to ComboStringGrid.RowCount-2 do begin
      Roll1:=StrToIntDef(RollStringGrid.Cells[TempCol,r],0);
      Roll2:=StrToIntDef(RollStringGrid.Cells[PotCol,r],0);
      PotStat:=Wuerfe2Potential(Roll1,Roll2);
      PotBonus:=Stat2Bonus(PotStat);
      if ComboShowBonusRadioButton.Checked then
        ComboStringGrid.Cells[TargetCol,r]:=IntToStr(PotBonus)
      else
        ComboStringGrid.Cells[TargetCol,r]:=IntToStr(PotStat);
    end;
  end;

var
  c: Integer;
begin
  for c:=0 to 5 do
    CalculatePot(c,ComboTemp[c],ComboPot[c]);
  UpdateComboSums;
end;

procedure TNewStatsDialog.UpdateRolls;
begin
  UpdateRollSums;
  UpdateCombos;
end;

initialization
  {$I newstats.lrs}

end.

