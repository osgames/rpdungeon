{
 *****************************************************************************
 *                                                                           *
 *  This source is free software. See the file COPYING.modifiedLGPL,         *
 *  included in this distribution, for details about the copyright.          *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************

 asmutils.pp
 Author: Reimar Grabowski (2002-2005)
}

unit asmutils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, nvgl, sgiglu, Vectors, dyn_arrays, graphics, math;

type
  glRGBAColor = array [0..3] of GLfloat;
  glRGBColor = array [0..2] of GLfloat;
  glVector3 = array [0..2] of GLfLoat;
  glVector4 = array [0..3] of GLfLoat;
  //glMatrix = array [0..15] of GLfloat;
  TMatrix4x4 = array [0..3, 0..3] of GLfloat;
  
  //
  TCone = object
  private
    fAngle: GLfloat;
    fCosAngle: GLfloat;
    frecSinAngle: GLfloat;
    fSinAngle: GLfloat;
    fsqrCosAngle: GLfloat;
    fsqrSinAngle: GLfloat;
    procedure SetAngle(const AValue: GLfloat);
  public
    property Angle: GLfloat read fAngle write SetAngle;
    property CosAngle: GLfloat read fCosAngle;
    property SinAngle: GLfloat read fSinAngle;
    property recSinAngle: GLfloat read frecSinAngle;
    property sqrCosAngle: GLfloat read fsqrCosAngle;
    property sqrSinAngle: GLfloat read fsqrSinAngle;
    Axis: TVector3;
    Vertex: TVector3;
  end;
  
  TEdge = record
    Vertex: array [1..2] of GLuint;
    Triangle: array [1..2] of integer;
  end;
  
  TPlane = record
    Normal: TVector3;
    Distance: GLfloat;
  end;
  
  TSphere = record
    Center: TVector3;
    Radius: GLfloat;
  end;
  
  TSilhouetteEdge = record
    v1, v2: GLuint;
  end;
  
  TTriangle = record
    p1, p2, p3: GLuint;
    Plane: TPlane;
  end;
  
  TArraySortCompare = function (Item1, Item2: GLuint; const IndArray: ArrayOfGLuint; const refArray: ArrayOfTVector3): Integer;

// color conversion
function glRGBToTColor(red, green, blue: GLfloat): TColor;
procedure TColorToglRGB(color: TColor; var red, green, blue: GLfloat);

// sort
function Compare3(Data1, Data2: pointer): integer;
function Compare3_2(Data1, Data2: pointer): integer;
function Compare3_3(Data1, Data2: pointer): integer;
function Compare3_3_2(Data1, Data2: pointer): integer;
function CompareVectorDesc(Index1, Index2: GLuint; const IndArray: ArrayOfGLuint; const VertArray: ArrayOfTVector3): Integer;
procedure ListMergeSort(l: TFPList; StartIndex, EndIndex: Integer;
  Compare : TListSortCompare);
procedure IndexArrayMergeSort(var a: ArrayOfGLuint; const IndArray: ArrayOfGLuint;
 const refArray: ArrayOfTVector3; Compare: TArraySortCompare);
procedure EdgeListMergeSort(var a: array of TEdge);

// vector transformations
function iTransformVector(const Vector: TVector3; const Matrix: TMatrix4x4): TVector3;
function iTransformVector4(const Vector: TVector4; const Matrix: TMatrix4x4): TVector4;
function TransformVector(const Vector: TVector3; const Matrix: TMatrix4x4): TVector3;
function TransformVector(const Vector: TVector3; const Matrix: TGLUMatrixD): TVector3;
function TransVec(const Vector: TVector3; const Matrix: TMatrix4x4): TVector3;
function TransVec3(const Vector: TVector3; const Matrix: Matrix3X3): TVector3;

// Matrix functions
function MultglMatrix(const ma1,ma2: TMatrix4x4): TMatrix4x4;

// array to vector - vector to array
function glArray(const Vector: TVector3): glVector3;
function Vector(const glArray: glVector3): TVector3;
function Vector(const a, b, c: GLFloat): TVector3; overload;

// intersections
function RayPlaneIntersection(const RayOrigin, Ray: TVector3; PlaneNormal: TVector3;
 PlaneDistance: GLfloat; var IntersectionPoint: TVector3): boolean;
function RaySphereIntersection(RayOrigin, Ray, SphereOrigin: TVector3; Radius:
 GLfloat; var NearIntersectionPoint, FarIntersectionPoint: TVector3): boolean;
function RayTriangleIntersection(const RayOrigin,Ray,Vert0,Vert1,Vert2: TVector3;
 var ti: GLfloat): boolean;
function SphereConeIntersection(Sphere: TSphere; Cone: TCone): boolean;
function SphereSphereIntersection(TestSphere, RefSphere: TSphere): boolean;

// misc
function CreateNormalisationCubeMap: GLuint;
function CreatePlaneFromTriangle(const Vertex1, Vertex2, Vertex3: TVector3): TPlane;

implementation

// color conversion functions
function glRGBToTColor(red, green, blue: GLfloat): TColor;
begin
  Result:=TColor((round(red*255))+(round(green*255) shl 8)
   +(round(blue*255) shl 16));
end;

procedure TColorToglRGB(color: TColor; var red, green, blue: GLfloat);
begin
  red:=GLfloat(Color and $FF)/255;
  green:=GLfloat((Color shr 8) and $FF)/255;
  blue:=GLfloat((Color shr 16) and $FF)/255;
end;

// AVL Tree compare functions
function Compare3(Data1, Data2: pointer): integer;
var r1, r2: PV3Rec;
begin
 r1:=PV3Rec(Data1);
 r2:=PV3Rec(Data2);
 if r1^.x>r2^.x then exit(1)
  else if r1^.x<r2^.x then exit(-1)
  else if r1^.y>r2^.y then exit(1)
  else if r1^.y<r2^.y then exit(-1)
  else if r1^.z>r2^.z then exit(1)
  else if r1^.z<r2^.z then exit(-1)
  else exit(0);
end;

function Compare3_2(Data1, Data2: pointer): integer;
var r1, r2: PV3_3Rec;
begin
  r1:=PV3_3Rec(Data1);
  r2:=PV3_3Rec(Data2);
  if r1^.x>r2^.x then exit(1)
  else if r1^.x<r2^.x then exit(-1)
  else if r1^.y>r2^.y then exit(1)
  else if r1^.y<r2^.y then exit(-1)
  else if r1^.z>r2^.z then exit(1)
  else if r1^.z<r2^.z then exit(-1)
  else if r1^.x2>r2^.x2 then exit(1)
  else if r1^.x2<r2^.x2 then exit(-1)
  else if r1^.y2>r2^.y2 then exit(1)
  else if r1^.y2<r2^.y2 then exit(-1)
  else exit(0);
end;

function Compare3_3(Data1, Data2: pointer): integer;
var r1, r2: PV3_3Rec;
begin
  r1:=PV3_3Rec(Data1);
  r2:=PV3_3Rec(Data2);
  if r1^.x>r2^.x then exit(1)
  else if r1^.x<r2^.x then exit(-1)
  else if r1^.y>r2^.y then exit(1)
  else if r1^.y<r2^.y then exit(-1)
  else if r1^.z>r2^.z then exit(1)
  else if r1^.z<r2^.z then exit(-1)
  else if r1^.x2>r2^.x2 then exit(1)
  else if r1^.x2<r2^.x2 then exit(-1)
  else if r1^.y2>r2^.y2 then exit(1)
  else if r1^.y2<r2^.y2 then exit(-1)
  else if r1^.z2>r2^.z2 then exit(1)
  else if r1^.z2<r2^.z2 then exit(-1)
  else exit(0);
end;

function Compare3_3_2(Data1, Data2: pointer): integer;
var r1, r2: PV3_3_2Rec;
begin
  r1:=PV3_3_2Rec(Data1);
  r2:=PV3_3_2Rec(Data2);
  if r1^.x>r2^.x then exit(1)
  else if r1^.x<r2^.x then exit(-1)
  else if r1^.y>r2^.y then exit(1)
  else if r1^.y<r2^.y then exit(-1)
  else if r1^.z>r2^.z then exit(1)
  else if r1^.z<r2^.z then exit(-1)
  else if r1^.x2>r2^.x2 then exit(1)
  else if r1^.x2<r2^.x2 then exit(-1)
  else if r1^.y2>r2^.y2 then exit(1)
  else if r1^.y2<r2^.y2 then exit(-1)
  else if r1^.z2>r2^.z2 then exit(1)
  else if r1^.z2<r2^.z2 then exit(-1)
  else if r1^.x3>r2^.x3 then exit(1)
  else if r1^.x3<r2^.x3 then exit(-1)
  else if r1^.y3>r2^.y3 then exit(1)
  else if r1^.y3<r2^.y3 then exit(-1)
  else exit(0);
end;

function CompareVectorDesc(Index1, Index2: GLuint; const IndArray: ArrayOfGLuint;
  const VertArray: ArrayOfTVector3): Integer;
var v1, v2: PVector3;
begin
 v1:=@VertArray.Data[IndArray.Data[Index1]];
 v2:=@VertArray.Data[IndArray.Data[Index2]];
 if v1^.x>v2^.x then exit(1)
  else if v1^.x<v2^.x then exit(-1)
  else if v1^.y>v2^.y then exit(1)
  else if v1^.y<v2^.y then exit(-1)
  else if v1^.z>v2^.z then exit(1)
  else if v1^.z<v2^.z then exit(-1)
  else exit(0);
end;

// Sort part of a TFPList with the MergeSort algorithm
procedure ListMergeSort(l: TFPList; StartIndex, EndIndex: Integer;
  Compare : TListSortCompare);

  procedure Copy(Src, Dest: PPointer; StartPos, EndPos: Integer);
  var i: Integer;
  begin
    for i:=StartPos to EndPos do Dest[i]:=Src[i];
  end;

  procedure Merge(Src, Dest: PPointer; StartPos, MidPos, EndPos: Integer);
  // merge two sorted list parts
  // the first part ranges from StartPos to MidPos,
  // the second from MidPos+1 to EndPos
  var DestPos, Src1Pos, Src2Pos, cmp: Integer;
  begin
    DestPos:=StartPos;
    Src1Pos:=StartPos;
    Src2Pos:=MidPos+1;
    repeat
      if Src1Pos<=MidPos then begin
        if Src2Pos<=EndPos then begin
          cmp:=Compare(Src[Src1Pos],Src[Src2Pos]);
          if cmp<0 then begin
            Dest[DestPos]:=Src[Src2Pos];
            inc(Src2Pos);
          end else if cmp>0 then begin
            Dest[DestPos]:=Src[Src1Pos];
            inc(Src1Pos);
          end else begin
            Dest[DestPos]:=Src[Src1Pos];
            inc(DestPos);
            Dest[DestPos]:=Src[Src2Pos];
            inc(Src1Pos);
            inc(Src2Pos);
          end;
        end else begin
          Dest[DestPos]:=Src[Src1Pos];
          inc(Src1Pos);
        end;
      end else begin
        if Src2Pos<=EndPos then begin
          Dest[DestPos]:=Src[Src2Pos];
          inc(Src2Pos);
        end else begin
          break;
        end;
      end;
      inc(DestPos);
    until false;
  end;

  // sort part of List1
  // returns true if sorted list is in List1 and false if in list2
  function Sort(List1, List2: PPointer; StartPos, EndPos: Integer): Boolean;
  var
    MidPos: Integer;
    First, Second: Boolean;
  begin
    case EndPos-StartPos of

    0:// only one element => already sorted
      begin
        Result:=true;
      end;

    1:// two elements -> sort them
      if Compare(List1[StartPos],List1[EndPos])>0 then begin
        // already sorted
        Result:=true;
      end else begin
        // not sorted -> exchange
        List2[StartPos]:=List1[EndPos];
        List2[EndPos]:=List1[StartPos];
        Result:=false;
      end;

    else
      // more than two elements -> sort recursively
      MidPos:=(StartPos+EndPos) shr 1;
      First:=Sort(List1,List2,StartPos,MidPos);
      Second:=Sort(List1,List2,MidPos+1,EndPos);
      if First and Second then begin
        // both parts are sorted in List1
        // -> merge them in List2
        Merge(List1,List2,StartPos,MidPos,EndPos);
        Result:=false;
      end else begin
        // first or second part is sorted in List2
        if First then begin
          // first part is sorted in List1 -> copy it to List2
          Copy(List1,List2,StartPos,MidPos);
        end else if Second then begin
          // second part is sorted in List1 -> copy it to List2
          Copy(List1,List2,MidPos+1,EndPos);
        end;
        // merge them in List1
        Merge(List2,List1,StartPos,MidPos,EndPos);
        Result:=true;
      end;
    end;
  end;

var
  l1,l2,SortedList: PPointer;
  Count, i: Integer;
begin
  if EndIndex<=StartIndex then exit;
  Count:=EndIndex-StartIndex+1;
  // to avoid fpc hint: variable not initialized
  l1:=nil;
  GetMem(l1,SizeOf(Pointer)*Count*2);
  try
    for i:=0 to Count-1 do
      l1[i]:=l[StartIndex+i];
    l2:=@l1[Count];
    if Sort(l1,l2,0,Count-1) then
      SortedList:=l1
    else
      SortedList:=l2;
    for i:=0 to Count-1 do
      l[StartIndex+i]:=SortedList[i];
  finally
    FreeMem(l1);
  end;
end;

// Sort an index array with the MergeSort algorithm
procedure IndexArrayMergeSort(var a: ArrayOfGLuint; const IndArray: ArrayOfGLuint;
 const refArray: ArrayOfTVector3; Compare: TArraySortCompare);

  procedure Copy(var Src, Dest: ArrayOfGLuint; StartPos, EndPos: Integer);
  var i: Integer;
  begin
    for i:=StartPos to EndPos do Dest.Data[i]:=Src.Data[i];
  end;

  procedure Merge(var Src, Dest: ArrayOfGLuint; StartPos, MidPos, EndPos: Integer);
  // merge two sorted array parts
  // the first part ranges from StartPos to MidPos,
  // the second from MidPos+1 to EndPos
  var DestPos, Src1Pos, Src2Pos, cmp: Integer;
  begin
    DestPos:=StartPos;
    Src1Pos:=StartPos;
    Src2Pos:=MidPos+1;
    repeat
      if Src1Pos<=MidPos then begin
        if Src2Pos<=EndPos then begin
          cmp:=Compare(Src.Data[Src1Pos],Src.Data[Src2Pos],IndArray,refArray);
          if cmp<0 then begin
            Dest.Data[DestPos]:=Src.Data[Src2Pos];
            inc(Src2Pos);
          end else if cmp>0 then begin
            Dest.Data[DestPos]:=Src.Data[Src1Pos];
            inc(Src1Pos);
          end else begin
            Dest.Data[DestPos]:=Src.Data[Src1Pos];
            inc(DestPos);
            Dest.Data[DestPos]:=Src.Data[Src2Pos];
            inc(Src1Pos);
            inc(Src2Pos);
          end;
        end else begin
          Dest.Data[DestPos]:=Src.Data[Src1Pos];
          inc(Src1Pos);
        end;
      end else begin
        if Src2Pos<=EndPos then begin
          Dest.Data[DestPos]:=Src.Data[Src2Pos];
          inc(Src2Pos);
        end else begin
          break;
        end;
      end;
      inc(DestPos);
    until false;
  end;

  // sort part of Array1
  // returns true if sorted array is in Array1 and false if in Array2
  function Sort(var Array1, Array2: ArrayOfGLuint; StartPos, EndPos: Integer): Boolean;
  var
    MidPos: Integer;
    First, Second: Boolean;
  begin
    case EndPos-StartPos of

    0:// only one element => already sorted
      begin
        Result:=true;
      end;

    1:// two elements -> sort them
      if Compare(Array1.Data[StartPos],Array1.Data[EndPos], IndArray, refArray)>0 then begin
        // already sorted
        Result:=true;
      end else begin
        // not sorted -> exchange
        Array2.Data[StartPos]:=Array1.Data[EndPos];
        Array2.Data[EndPos]:=Array1.Data[StartPos];
        Result:=false;
      end;

    else
      // more than two elements -> sort recursively
      MidPos:=(StartPos+EndPos) shr 1;
      First:=Sort(array1,array2,StartPos,MidPos);
      Second:=Sort(array1,array2,MidPos+1,EndPos);
      if First and Second then begin
        // both parts are sorted in Array1
        // -> merge them in Array2
        Merge(Array1,Array2,StartPos,MidPos,EndPos);
        Result:=false;
      end else begin
        // first or second part is sorted in Array2
        if First then begin
          // first part is sorted in Array1 -> copy it to Array2
          Copy(Array1,Array2,StartPos,MidPos);
        end else if Second then begin
          // second part is sorted in Array1 -> copy it to Array2
          Copy(Array1,Array2,MidPos+1,EndPos);
        end;
        // merge them in Array1
        Merge(Array2,Array1,StartPos,MidPos,EndPos);
        Result:=true;
      end;
    end;
  end;

var
  a1,a2,SortedArray: ArrayOfGLuint;
  Count, i: Integer;
begin
  if a.Length<=0 then exit;
  Count:=a.Length;
  try
    a1.Create;
    a2.Create;
    for i:=0 to Count-1 do begin
      a1.Add(a.Data[i]);
      a2.Add(0);
    end;
    SortedArray.Create;
    if Sort(a1,a2,0,Count-1) then
      SortedArray:=a1
    else
      SortedArray:=a2;
    for i:=0 to Count-1 do
      a.Data[i]:=SortedArray.Data[i];
  finally
    a1.Free;
    a2.Free;
  end;
end;

// Sorts an array of TEdge using merge sort.
procedure EdgeListMergeSort(var a: array of TEdge);

  function Compare(const Edge1, Edge2: TEdge): integer;
  var
    min1: GLuint;
    min2: GLuint;
    max1: GLuint;
    max2: GLuint;
  begin
    min1:=min(Edge1.Vertex[1],Edge1.Vertex[2]);
    min2:=min(Edge2.Vertex[1],Edge2.Vertex[2]);
    max1:=max(Edge1.Vertex[1],Edge1.Vertex[2]);
    max2:=max(Edge2.Vertex[1],Edge2.Vertex[2]);
    if min1<min2 then exit(1)
      else if min1>min2 then exit(-1)
      else if max1<max2 then exit(1)
      else if max1>max2 then exit(-1)
      else exit(0);
  end;

  procedure Copy(var Src, Dest: array of TEdge; StartPos, EndPos: Integer);
  var i: Integer;
  begin
    for i:=StartPos to EndPos do Dest[i]:=Src[i];
  end;

  procedure Merge(var Src, Dest: array of TEdge; StartPos, MidPos, EndPos: Integer);
  // merge two sorted array parts
  // the first part ranges from StartPos to MidPos,
  // the second from MidPos+1 to EndPos
  var DestPos, Src1Pos, Src2Pos, cmp: Integer;
  begin
    DestPos:=StartPos;
    Src1Pos:=StartPos;
    Src2Pos:=MidPos+1;
    repeat
      if Src1Pos<=MidPos then begin
        if Src2Pos<=EndPos then begin
          cmp:=Compare(Src[Src1Pos],Src[Src2Pos]);
          if cmp<0 then begin
            Dest[DestPos]:=Src[Src2Pos];
            inc(Src2Pos);
          end else if cmp>0 then begin
            Dest[DestPos]:=Src[Src1Pos];
            inc(Src1Pos);
          end else begin
            Dest[DestPos]:=Src[Src1Pos];
            inc(DestPos);
            Dest[DestPos]:=Src[Src2Pos];
            inc(Src1Pos);
            inc(Src2Pos);
          end;
        end else begin
          Dest[DestPos]:=Src[Src1Pos];
          inc(Src1Pos);
        end;
      end else begin
        if Src2Pos<=EndPos then begin
          Dest[DestPos]:=Src[Src2Pos];
          inc(Src2Pos);
        end else begin
          break;
        end;
      end;
      inc(DestPos);
    until false;
  end;

  // sort part of Array1
  // returns true if sorted array is in Array1 and false if in Array2
  function Sort(var Array1, Array2: array of TEdge; StartPos, EndPos: Integer): Boolean;
  var
    MidPos: Integer;
    First, Second: Boolean;
  begin
    case EndPos-StartPos of

    0:// only one element => already sorted
      begin
        Result:=true;
      end;

    1:// two elements -> sort them
      if Compare(Array1[StartPos],Array1[EndPos])>0 then begin
        // already sorted
        Result:=true;
      end else begin
        // not sorted -> exchange
        Array2[StartPos]:=Array1[EndPos];
        Array2[EndPos]:=Array1[StartPos];
        Result:=false;
      end;

    else
      // more than two elements -> sort recursively
      MidPos:=(StartPos+EndPos) shr 1;
      First:=Sort(array1,array2,StartPos,MidPos);
      Second:=Sort(array1,array2,MidPos+1,EndPos);
      if First and Second then begin
        // both parts are sorted in Array1
        // -> merge them in Array2
        Merge(Array1,Array2,StartPos,MidPos,EndPos);
        Result:=false;
      end else begin
        // first or second part is sorted in Array2
        if First then begin
          // first part is sorted in Array1 -> copy it to Array2
          Copy(Array1,Array2,StartPos,MidPos);
        end else if Second then begin
          // second part is sorted in Array1 -> copy it to Array2
          Copy(Array1,Array2,MidPos+1,EndPos);
        end;
        // merge them in Array1
        Merge(Array2,Array1,StartPos,MidPos,EndPos);
        Result:=true;
      end;
    end;
  end;

var
  a1,a2,SortedArray: array of TEdge;
  Count, i: Integer;
begin
  if Length(a)<=0 then exit;
  Count:=Length(a);
  try
    SetLength(a1,Count);
    SetLength(a2,Count);
    for i:=0 to Count-1 do begin
      a1[i]:=a[i];
    end;
    SetLength(SortedArray, Count);
    if Sort(a1,a2,0,Count-1) then
      SortedArray:=a1
    else
      SortedArray:=a2;
    for i:=0 to Count-1 do
      a[i]:=SortedArray[i];
  finally
    SetLength(a1,0);
    SetLength(a2,0);
  end;
end;

// vector transformations
function iTransformVector(const Vector: TVector3; const Matrix: TMatrix4x4): TVector3;
var Res: TVector3; m: TMatrix4x4 absolute Matrix;
begin
  //m:=Matrix;
  Res.x:=m[0,0]*Vector.x+m[1,0]*Vector.y+m[2,0]*Vector.z;
  Res.y:=m[0,1]*Vector.x+m[1,1]*Vector.y+m[2,1]*Vector.z;
  Res.z:=m[0,2]*Vector.x+m[1,2]*Vector.y+m[2,2]*Vector.z;
  Result:=Res;
end;

function iTransformVector4(const Vector: TVector4; const Matrix: TMatrix4x4
  ): TVector4;
var Res: TVector4; M: TMatrix4x4;
begin
  M:=Matrix;
  Res.x:=m[0,0]*Vector.x+m[1,0]*Vector.y+m[2,0]*Vector.z+m[3,0]*Vector.w;
  Res.y:=m[0,1]*Vector.x+m[1,1]*Vector.y+m[2,1]*Vector.z+m[3,1]*Vector.w;
  Res.z:=m[0,2]*Vector.x+m[1,2]*Vector.y+m[2,2]*Vector.z+m[3,2]*Vector.w;
  Res.w:=m[0,3]*Vector.x+m[1,3]*Vector.y+m[2,3]*Vector.z+m[3,3]*Vector.w;
  Result:=Res;
end;

function TransformVector(const Vector: TVector3; const Matrix: TMatrix4x4): TVector3;
var Res: TVector3; m: TMatrix4x4;
begin
  m:=Matrix;
  Res.x:=m[0,0]*Vector.x+m[0,1]*Vector.y+m[0,2]*Vector.z;
  Res.y:=m[1,0]*Vector.x+m[1,1]*Vector.y+m[1,2]*Vector.z;
  Res.z:=m[2,0]*Vector.x+m[2,1]*Vector.y+m[2,2]*Vector.z;
  Result:=Res;
end;

function TransformVector(const Vector: TVector3; const Matrix: TGLUMatrixD): TVector3;
var Res: TVector3;
begin
  Res.x:=Matrix[0]*Vector.x+Matrix[1]*Vector.y+Matrix[2]*Vector.z;
  Res.y:=Matrix[4]*Vector.x+Matrix[5]*Vector.y+Matrix[6]*Vector.z;
  Res.z:=Matrix[8]*Vector.x+Matrix[9]*Vector.y+Matrix[10]*Vector.z;
  Result:=Res;
end;

function TransVec(const Vector: TVector3; const Matrix: TMatrix4x4): TVector3;
var Res: TVector3;
begin
  Res.x:=Matrix[0,0]*Vector.x+Matrix[1,0]*Vector.y+Matrix[2,0]*Vector.z+Matrix[3,0];
  Res.y:=Matrix[0,1]*Vector.x+Matrix[1,1]*Vector.y+Matrix[2,1]*Vector.z+Matrix[3,1];
  Res.z:=Matrix[0,2]*Vector.x+Matrix[1,2]*Vector.y+Matrix[2,2]*Vector.z+Matrix[3,2];
  Result:=Res;
end;

function TransVec3(const Vector: TVector3; const Matrix: Matrix3X3): TVector3;
var Res: TVector3;
begin
  Res.x:=Matrix[0]*Vector.x+Matrix[1]*Vector.y+Matrix[2]*Vector.z;
  Res.y:=Matrix[3]*Vector.x+Matrix[4]*Vector.y+Matrix[5]*Vector.z;
  Res.z:=Matrix[6]*Vector.x+Matrix[7]*Vector.y+Matrix[8]*Vector.z;
  Result:=Res;
end;

// Matrix functions ////////////////////////////////////////////////////////////
function MultglMatrix(const ma1,ma2: TMatrix4x4): TMatrix4x4;
var Res,m1,m2: TMatrix4x4;
begin
  m1:=ma1;
  m2:=ma2;
  Res[0,0]:= m1[0,0]*m2[0,0]+ m1[1,0]*m2[0,1]+ m1[2,0]*m2[0,2]+ m1[3,0]*m2[0,3];
  Res[1,0]:= m1[0,0]*m2[1,0]+ m1[1,0]*m2[1,1]+ m1[2,0]*m2[1,2]+ m1[3,0]*m2[1,3];
  Res[2,0]:= m1[0,0]*m2[2,0]+ m1[1,0]*m2[2,1]+ m1[2,0]*m2[2,2]+m1[3,0]*m2[2,3];
  Res[3,0]:=m1[0,0]*m2[3,0]+m1[1,0]*m2[3,1]+m1[2,0]*m2[3,2]+m1[3,0]*m2[3,3];
  Res[0,1]:= m1[0,1]*m2[0,0]+ m1[1,1]*m2[0,1]+ m1[2,1]*m2[0,2]+ m1[3,1]*m2[0,3];
  Res[1,1]:= m1[0,1]*m2[1,0]+ m1[1,1]*m2[1,1]+ m1[2,1]*m2[1,2]+ m1[3,1]*m2[1,3];
  Res[2,1]:= m1[0,1]*m2[2,0]+ m1[1,1]*m2[2,1]+ m1[2,1]*m2[2,2]+m1[3,1]*m2[2,3];
  Res[3,1]:=m1[0,1]*m2[3,0]+m1[1,1]*m2[3,1]+m1[2,1]*m2[3,2]+m1[3,1]*m2[3,3];
  Res[0,2]:= m1[0,2]*m2[0,0]+ m1[1,2]*m2[0,1]+ m1[2,2]*m2[0,2]+ m1[3,2]*m2[0,3];
  Res[1,2]:= m1[0,2]*m2[1,0]+ m1[1,2]*m2[1,1]+ m1[2,2]*m2[1,2]+ m1[3,2]*m2[1,3];
  Res[2,2]:=m1[0,2]*m2[2,0]+ m1[1,2]*m2[2,1]+ m1[2,2]*m2[2,2]+m1[3,2]*m2[2,3];
  Res[3,2]:=m1[0,2]*m2[3,0]+m1[1,2]*m2[3,1]+m1[2,2]*m2[3,2]+m1[3,2]*m2[3,3];
  Res[0,3]:= m1[0,3]*m2[0,0]+ m1[1,3]*m2[0,1]+ m1[2,3]*m2[0,2]+ m1[3,3]*m2[0,3];
  Res[1,3]:= m1[0,3]*m2[1,0]+ m1[1,3]*m2[1,1]+ m1[2,3]*m2[1,2]+ m1[3,3]*m2[1,3];
  Res[2,3]:=m1[0,3]*m2[2,0]+ m1[1,3]*m2[2,1]+ m1[2,3]*m2[2,2]+m1[3,3]*m2[2,3];
  Res[3,3]:=m1[0,3]*m2[3,0]+m1[1,3]*m2[3,1]+m1[2,3]*m2[3,2]+m1[3,3]*m2[3,3];
  MultglMatrix:=TMatrix4x4(Res);
end;

// Vector to glArray Conversion
function glArray(const Vector: TVector3): glVector3;
begin
  glArray[0]:=Vector.x;
  glArray[1]:=Vector.y;
  glArray[2]:=Vector.z;
end;

// glArray to Vector Conversion
function Vector(const glArray: glVector3): TVector3;
begin
  Vector.x:=glArray[0];
  Vector.y:=glArray[1];
  Vector.z:=glArray[2];
end;

function Vector(const a, b, c: GLFloat): TVector3;
begin
  Vector.x:=a;
  Vector.y:=b;
  Vector.z:=c;
end;

// intersection
function RayPlaneIntersection(const RayOrigin, Ray: TVector3; PlaneNormal: TVector3;
 PlaneDistance: GLfloat; var IntersectionPoint: TVector3): boolean;
var Vd, V0, t: GLfloat; Res: boolean;
begin
  Res:=false;
  Vd:=DotProduct(PlaneNormal,Ray);
  if Vd<>0 then begin
    if Vd>0 then begin Vd:=-Vd; PlaneDistance:=-PlaneDistance; PlaneNormal:=-PlaneNormal; end;
    V0:=PlaneDistance-DotProduct(PlaneNormal,RayOrigin);
    t:=V0/Vd;
    if t>=0 then begin
      IntersectionPoint:=RayOrigin+Ray*t;
      Res:=true;
    end;
  end;
  RayPlaneIntersection:=Res;
end;

function RaySphereIntersection(RayOrigin, Ray, SphereOrigin: TVector3; Radius:
 GLfloat; var NearIntersectionPoint, FarIntersectionPoint: TVector3): boolean;
var p,q,d,t1,t2: GLfloat; Res: boolean;
begin
  Res:=false;
  //Ray.Normalize;
  p:=2*DotProduct((RayOrigin-SphereOrigin),Ray);
  if p<=0 then begin
    q:=DotProduct((RayOrigin-SphereOrigin),(RayOrigin-SphereOrigin))-Radius*Radius;
    if q<sqr(p*0.5) then begin
      d:=sqr(p*0.5)-q;
      t1:=-p*0.5+sqrt(d);
      t2:=-p*0.5-sqrt(d);
      Res:=true;
      if t1<t2 then begin
        NearIntersectionPoint:=RayOrigin+Ray*t1;
        FarIntersectionPoint:=RayOrigin+Ray*t2;
      end else begin
        NearIntersectionPoint:=RayOrigin+Ray*t2;
        FarIntersectionPoint:=RayOrigin+Ray*t1;
      end;
    end;
  end;
  RaySphereIntersection:=Res;
end;

function RayTriangleIntersection(const RayOrigin,Ray,Vert0,Vert1,Vert2: TVector3;
 var ti: GLfloat): boolean;
var Edge1,Edge2,p,t,q: TVector3; det,inv_det,u,v: GLfloat;
begin
  RayTriangleIntersection:=false;
  Edge1:=Vert1-Vert0;
  Edge2:=Vert2-Vert0;
  p:=CrossProduct(Ray,Edge2);
  det:=DotProduct(Edge1,p);
  if (det>-0.00001) and (det<0.00001) then exit;
  inv_det:=1/det;
  t:=RayOrigin-Vert0;
  u:=(DotProduct(t,p))*inv_det;
  if (u<0) or (u>1) then exit;
  q:=CrossProduct(t,Edge1);
  v:=DotProduct(Ray,q)*inv_det;
  if (v<0) or (u+v>1) then exit;
  ti:=DotProduct(edge2,q)*inv_det;
  RayTriangleIntersection:=true;
end;

function SphereConeIntersection(Sphere: TSphere; Cone: TCone): boolean;
var d, u: TVector3; dsqr, e: GLFloat;
begin
  u:=Cone.Vertex-(Sphere.Radius*Cone.recSinAngle)*Cone.Axis;
  d:=Sphere.Center-u;
  dsqr:=DotProduct(d,d);
  e:=DotProduct(Cone.Axis,d);
  if (e>0) and (e*e>=dsqr*Cone.sqrCosAngle) then begin
    d:=Sphere.Center-Cone.Vertex;
    dsqr:=DotProduct(d,d);
    e:=-DotProduct(Cone.Axis,d);
    if (e>0) and (e*e>=dsqr*Cone.sqrSinAngle) then begin
      SphereConeIntersection:=dsqr<=Sphere.Radius*Sphere.Radius;
      exit;
    end else begin
      SphereConeIntersection:=true;
      exit;
    end;
  end;
  SphereConeIntersection:=false;
end;

function SphereSphereIntersection(TestSphere, RefSphere: TSphere): boolean;
var distance: TVector3; radiisum: GLFloat;
begin
  distance:=TestSphere.Center-RefSphere.Center;
  radiisum:=TestSphere.Radius+RefSphere.Radius;
  if vLength(distance)<=radiisum then SphereSphereIntersection:=true
    else SphereSphereIntersection:=false;
end;


// misc
function CreateNormalisationCubeMap: GLuint;
var CubeMap: array[0..(32*32*3)-1] of GLfloat; CubeMapID: GLuint;
    tempVector: TVector3; count, i,j,size: integer; offset, halfSize: GLfloat;
begin
  size:=32;
  offset:=0.5;
  halfSize:=size div 2;
  //
  glGenTextures(1, @CubeMapID);
  glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, CubeMapID);
  //positive x
  count:=0;
  for j:=0 to size-1 do
    for i:=0 to size-1 do begin
      tempVector.X:=halfSize;
      tempVector.Y:=-(j+offset-halfSize);
      tempVector.Z:=-(i+offset-halfSize);
      Normalize(tempVector);
      CubeMap[count+0]:=(tempVector.X+1)*0.5;
      CubeMap[count+1]:=(tempVector.Y+1)*0.5;
      CubeMap[count+2]:=(tempVector.Z+1)*0.5;
      count:=count+3;
    end;
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB,0,GL_RGBA8,size,size,0,GL_RGB,
   GL_FLOAT, @CubeMap);
  //negative x
  count:=0;
  for j:=0 to size-1 do
    for i:=0 to size-1 do begin
      tempVector.X:=-halfSize;
      tempVector.Y:=-(j+offset-halfSize);
      tempVector.Z:=(i+offset-halfSize);
      Normalize(tempVector);
      CubeMap[count+0]:=(tempVector.X+1)*0.5;
      CubeMap[count+1]:=(tempVector.Y+1)*0.5;
      CubeMap[count+2]:=(tempVector.Z+1)*0.5;
      count:=count+3;
    end;
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB,0,GL_RGBA8,size,size,0,GL_RGB,
   GL_FLOAT, @CubeMap);
  //positive y
  count:=0;
  for j:=0 to size-1 do
    for i:=0 to size-1 do begin
      tempVector.X:=(i+offset-halfSize);
      tempVector.Y:=halfSize;
      tempVector.Z:=(j+offset-halfSize);
      Normalize(tempVector);
      CubeMap[count+0]:=(tempVector.X+1)*0.5;
      CubeMap[count+1]:=(tempVector.Y+1)*0.5;
      CubeMap[count+2]:=(tempVector.Z+1)*0.5;
      count:=count+3;
    end;
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB,0,GL_RGBA8,size,size,0,GL_RGB,
   GL_FLOAT, @CubeMap);
  //negative y
  count:=0;
  for j:=0 to size-1 do
    for i:=0 to size-1 do begin
      tempVector.X:=i+offset-halfSize;
      tempVector.Y:=-halfSize;
      tempVector.Z:=-(j+offset-halfSize);
      Normalize(tempVector);
      CubeMap[count+0]:=(tempVector.X+1)*0.5;
      CubeMap[count+1]:=(tempVector.Y+1)*0.5;
      CubeMap[count+2]:=(tempVector.Z+1)*0.5;
      count:=count+3;
    end;
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB,0,GL_RGBA8,size,size,0,GL_RGB,
   GL_FLOAT, @CubeMap);
  //positive z
  count:=0;
  for j:=0 to size-1 do
    for i:=0 to size-1 do begin
      tempVector.X:=i+offset-halfSize;
      tempVector.Y:=-(j+offset-halfSize);
      tempVector.Z:=halfSize;
      Normalize(tempVector);
      CubeMap[count+0]:=(tempVector.X+1)*0.5;
      CubeMap[count+1]:=(tempVector.Y+1)*0.5;
      CubeMap[count+2]:=(tempVector.Z+1)*0.5;
      count:=count+3;
    end;
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB,0,GL_RGBA8,size,size,0,GL_RGB,
   GL_FLOAT, @CubeMap);
  //negative z
  count:=0;
  for j:=0 to size-1 do
    for i:=0 to size-1 do begin
      tempVector.X:=-(i+offset-halfSize);
      tempVector.Y:=-(j+offset-halfSize);
      tempVector.Z:=-halfSize;
      Normalize(tempVector);
      CubeMap[count+0]:=(tempVector.X+1)*0.5;
      CubeMap[count+1]:=(tempVector.Y+1)*0.5;
      CubeMap[count+2]:=(tempVector.Z+1)*0.5;
      count:=count+3;
    end;
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB,0,GL_RGBA8,size,size,0,GL_RGB,
   GL_FLOAT, @CubeMap);
  //
  glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  //
  Result:=CubeMapID;
end;

function CreatePlaneFromTriangle(const Vertex1, Vertex2, Vertex3: TVector3): TPlane;
var Normal: TVector3;
begin
  Normal:=Normalized(CrossProduct(Vertex2-Vertex1, Vertex3-Vertex1));
  Result.Distance:=-DotProduct(Vertex1, Normal);
  Result.Normal:=Normal;
end;

{ TCone }

procedure TCone.SetAngle(const AValue: GLFloat);
begin
  if fAngle=AValue then exit;
  fAngle:=AValue;
  fCosAngle:=cos(Angle);
  fsqrCosAngle:=fCosAngle*fCosAngle;
  fSinAngle:=sin(Angle);
  fsqrSinAngle:=fSinAngle*fSinAngle;
  frecSinAngle:=1/fSinAngle;
end;

end.

