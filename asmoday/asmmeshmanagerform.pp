{
 ***************************************************************************
 *                                                                         *
 *   This source is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This code is distributed in the hope that it will be useful, but      *
 *   WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   General Public License for more details.                              *
 *                                                                         *
 *   A copy of the GNU General Public License is available on the World    *
 *   Wide Web at <http://www.gnu.org/copyleft/gpl.html>. You can also      *
 *   obtain it by writing to the Free Software Foundation,                 *
 *   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.        *
 *                                                                         *
 ***************************************************************************

 asmmeshmanagerform.pp
 Author: Reimar Grabowski (2004-2005)
}

unit asmmeshmanagerform;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs,
  Asmoday, AsmTypes, AsmShaders, Menus, nvgl, lcltype,
  StdCtrls, ExtCtrls, Buttons, RTTICtrls, OpenGLContext;

type

  { TAsmMeshManager }

  TAsmMeshManager = class(TForm)
    AssignMeshButton: TButton;
    BoxWidthEdit: TEdit;
    BoxHeightEdit: TEdit;
    BoxDepthEdit: TEdit;
    CohenTopRadiusEdit: TEdit;
    CohenBottomRadiusEdit: TEdit;
    CohenLengthEdit: TEdit;
    CohenFacesEdit: TEdit;
    GroupBox1: TGroupBox;
    DeleteMenuItem: TMenuItem;
    OpenGLControl1: TOpenGLControl;
    SphereRadiusEdit: TEdit;
    SphereSegmentsEdit: TEdit;
    SphereRingsEdit: TEdit;
    GridPage: TPage;
    BoxWidthLabel: TLabel;
    HeightLabel: TLabel;
    DepthLabel: TLabel;
    BoxPage: TPage;
    CohenTopRadiusLabel: TLabel;
    CohenBottomRadiusLabel: TLabel;
    CohenLengthLabel: TLabel;
    CohenFacesLabel: TLabel;
    CohenPage: TPage;
    SphereRadiusLabel: TLabel;
    SphereSegmentsLabel: TLabel;
    SphereRingsLabel: TLabel;
    SpherePage: TPage;
    SquareSizeEdit: TEdit;
    SquareSubdivisionEdit: TEdit;
    SquareSizeLabel: TLabel;
    SquareSubdivisionLabel: TLabel;
    NameLabel: TLabel;
    NameEdit: TEdit;
    Notebook1: TNotebook;
    MeshPage: TPage;
    SquarePage: TPage;
    Panel1: TPanel;
    SupportedShadersCheckGroup: TCheckGroup;
    MeshListBox: TListBox;
    Mainmenu1: TMAINMENU;
    CreateMeshMenuitem: TMENUITEM;
    BoxMenuitem: TMENUITEM;
    CohenMenuitem: TMENUITEM;
    GridMenuitem: TMENUITEM;
    Opendialog1: TOPENDIALOG;
    SphereMenuitem: TMENUITEM;
    SquareMenuitem: TMENUITEM;
    ObjectMenuitem: TMENUITEM;
    LoadMeshMenuitem: TMENUITEM;
    TIEdit1: TTIEdit;
    procedure AsmMeshManagerClose(Sender: TObject;
      var CloseAction: TCloseAction);
    procedure AsmMeshManagerCreate(Sender: TObject);
    procedure AsmMeshManagerKeyPress(Sender: TObject; var Key: Char);
    procedure AssignMeshButtonClick(Sender: TObject);
    procedure BoxMenuitemClick(Sender: TObject);
    procedure BoxWidthEditExit(Sender: TObject);
    procedure CohenBottomRadiusEditExit(Sender: TObject);
    procedure CohenFacesEditExit(Sender: TObject);
    procedure CohenLengthEditExit(Sender: TObject);
    procedure CohenMenuitemClick(Sender: TObject);
    procedure CohenTopRadiusEditExit(Sender: TObject);
    procedure DeleteMenuItemClick(Sender: TObject);
    procedure DepthEditExit(Sender: TObject);
    procedure GridMenuitemClick(Sender: TObject);
    procedure BoxHeightEditExit(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure LoadMeshMenuitemClick(Sender: TObject);
    procedure MeshListBoxClick(Sender: TObject);
    procedure MeshListBoxDblClick(Sender: TObject);
    procedure NameEditChange(Sender: TObject);
    procedure NameEditExit(Sender: TObject);
    procedure NameEditKeyPress(Sender: TObject; var Key: Char);
    procedure OpenGLControl1Paint(Sender: TObject);
    procedure OpenGLControl1Resize(Sender: TObject);
    procedure SphereMenuitemClick(Sender: TObject);
    procedure SphereRadiusEditExit(Sender: TObject);
    procedure SphereRingsEditExit(Sender: TObject);
    procedure SphereSegmentsEditExit(Sender: TObject);
    procedure SquareMenuitemClick(Sender: TObject);
    procedure SquareSizeEditChange(Sender: TObject);
    procedure SquareSizeEditExit(Sender: TObject);
    procedure SquareSubdivisionEditExit(Sender: TObject);
    procedure SupportedShadersCheckGroupItemClick(Sender: TObject;
      Index: integer);
  private
    { private declarations }
    Camera: TAsmRotationCamera;
    Light: TAsmDirectionalLight;
    //Mesh: TAsmMesh;
    MeshList: TFPList;
    MeshObject: TAsmObject;
    //newNearPlane: GLfloat;
    Scene: TAsmScene;
    SceneInitialized: Boolean;
    procedure AdjustCamera;
    procedure DisableControls;
    procedure EnableControls;
    procedure InitScene;
    procedure GetArrayStatus;
    procedure ClearArrayStatus;
  public
    { public declarations }
    OnAssignMesh: procedure(AssignedMesh: TAsmMesh) of object;
  end; 

var
  AsmMeshManager: TAsmMeshManager;

implementation

{ TAsmMeshManager }

procedure TAsmMeshManager.AsmMeshManagerCreate(Sender: TObject);
begin
  if Sender=nil then;
  Scene:=TAsmScene.Create(OpenGLControl1, Self);
  MeshList:=TFPList.Create;
  MeshListBox.Items.Append('none');
  MeshListBox.Selected[0]:=true;
  SupportedShadersCheckGroup.CheckEnabled[1]:=false;
end;

procedure TAsmMeshManager.AsmMeshManagerClose(Sender: TObject;
  var CloseAction: TCloseAction);
var
  i: Integer;
begin
  if Sender=nil then;
  if CloseAction=caNone then;
  Light.Free;
  Camera.Free;
  for i:=0 to MeshList.Count-1 do
    TAsmMesh(MeshList.Items[i]).Free;
  MeshList.Free;
  MeshObject.Free;
end;

procedure TAsmMeshManager.AsmMeshManagerKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Sender=nil then;
  if Key='' then;
end;

procedure TAsmMeshManager.AssignMeshButtonClick(Sender: TObject);
begin
  MeshListBoxDblClick(Sender);
end;

procedure TAsmMeshManager.BoxMenuitemClick(Sender: TObject);
var Mesh: TAsmMesh;
begin
  if Sender=nil then;
  OpenGLControl1.MakeCurrent;
  Mesh:=TAsmMesh.Create;
  Mesh.CreateBox(1,1,1);
  MeshObject.Mesh:=Mesh;
  MeshObject.SetColor(1,1,1,1);
  if ShaderSupported(Lighting,MeshObject.Mesh) then
    MeshObject.Shader:=Lighting
  else
    MeshObject.Shader:=NoLightingWireframe;
  MeshObject.visible:=true;
  GetArrayStatus;
  Mesh.Name:='Box'+IntToStr(MeshList.Count+1);
  MeshList.Add(Mesh);
  MeshListBox.Items.Append(Mesh.Name);
  MeshListBox.Selected[MeshList.Count]:=true;
  NameEdit.Enabled:=true;
  NameEdit.Text:=Mesh.Name;
  Mesh:=nil;
  AdjustCamera;
end;

procedure TAsmMeshManager.BoxWidthEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateBox(StrToFloat(BoxWidthEdit.Text),StrToFloat(
   BoxHeightEdit.Text),StrToFloat(BoxDepthEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.CohenBottomRadiusEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateCohen(StrToFloat(CohenTopRadiusEdit.Text),StrToFloat(
   CohenBottomRadiusEdit.Text),StrToFloat(CohenLengthEdit.Text),StrToInt(
   CohenFacesEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.CohenFacesEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateCohen(StrToFloat(CohenTopRadiusEdit.Text),StrToFloat(
   CohenBottomRadiusEdit.Text),StrToFloat(CohenLengthEdit.Text),StrToInt(
   CohenFacesEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.CohenLengthEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateCohen(StrToFloat(CohenTopRadiusEdit.Text),StrToFloat(
   CohenBottomRadiusEdit.Text),StrToFloat(CohenLengthEdit.Text),StrToInt(
   CohenFacesEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.CohenMenuitemClick(Sender: TObject);
var
  Mesh: TAsmMesh;
begin
  if Sender=nil then;
  OpenGLControl1.MakeCurrent;
  Mesh:=TAsmMesh.Create;
  Mesh.CreateCohen(0,1,1,4);
  MeshObject.Mesh:=Mesh;
  MeshObject.SetColor(1,1,1,1);
  if ShaderSupported(Lighting,MeshObject.Mesh) then
    MeshObject.Shader:=Lighting
  else
    MeshObject.Shader:=NoLightingWireframe;
  MeshObject.visible:=true;
  //Mesh:=nil;
  GetArrayStatus;
  Mesh.Name:='Cohen'+IntToStr(MeshList.Count+1);
  MeshList.Add(Mesh);
  MeshListBox.Items.Append(Mesh.Name);
  MeshListBox.Selected[MeshList.Count]:=true;
  NameEdit.Enabled:=true;
  NameEdit.Text:=Mesh.Name;
  Mesh:=nil;
  AdjustCamera;
end;

procedure TAsmMeshManager.CohenTopRadiusEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateCohen(StrToFloat(CohenTopRadiusEdit.Text),StrToFloat(
   CohenBottomRadiusEdit.Text),StrToFloat(CohenLengthEdit.Text),StrToInt(
   CohenFacesEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.DeleteMenuItemClick(Sender: TObject);
begin
  writeln('delete clicked');
  // delete Stuff

  // disable controls if necessary
end;

procedure TAsmMeshManager.DepthEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateBox(StrToFloat(BoxWidthEdit.Text),StrToFloat(
   BoxHeightEdit.Text),StrToFloat(BoxDepthEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.GridMenuitemClick(Sender: TObject);
var Mesh: TAsmMesh;
begin
  if Sender=nil then;
  OpenGLControl1.MakeCurrent;
  Mesh:=TAsmMesh.Create;
  Mesh.CreateGrid(100,5);
  MeshObject.Mesh:=Mesh;
  MeshObject.SetColor(1,1,1,1);
  if ShaderSupported(Lighting,MeshObject.Mesh) then
    MeshObject.Shader:=Lighting
  else
    MeshObject.Shader:=NoLightingWireframe;
  MeshObject.visible:=true;
  //Mesh:=nil;
  GetArrayStatus;
  Mesh.Name:='Mesh'+IntToStr(MeshList.Count+1);
  MeshList.Add(Mesh);
  MeshListBox.Items.Append(Mesh.Name);
  MeshListBox.Selected[MeshList.Count]:=true;
  NameEdit.Enabled:=true;
  NameEdit.Text:=Mesh.Name;
  Mesh:=nil;
  AdjustCamera;
end;

procedure TAsmMeshManager.BoxHeightEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateBox(StrToFloat(BoxWidthEdit.Text),StrToFloat(
   BoxHeightEdit.Text),StrToFloat(BoxDepthEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.Label1Click(Sender: TObject);
begin

end;

procedure TAsmMeshManager.LoadMeshMenuitemClick(Sender: TObject);
var Mesh: TAsmMesh;
begin
  if Sender=nil then;
  OpenGLControl1.MakeCurrent;
  // debugging
  //try
    if OpenDialog1.Execute then begin
      Mesh:=TAsmMesh.Create;
      Mesh.LoadMeshFromObjFile(Opendialog1.FileName);
      MeshObject.Mesh:=Mesh;
      //MeshObject.Mesh.LoadMeshFromObjFile(OpenDialog1.FileName);
      //MeshObject.BSCenter:=MeshObject.Mesh.BSCenter;
      //MeshObject.BSRadius:=MeshObject.Mesh.BSRadius;
      if ShaderSupported(Lighting,MeshObject.Mesh) then
        MeshObject.Shader:=Lighting
      else
        MeshObject.Shader:=NoLightingWireframe;
      MeshObject.SetColor(1,1,1,1);
      MeshObject.visible:=true;
      //MeshObject.Name:='Object'+IntToStr(ObjectList.Count+1);
      OpenDialog1.InitialDir:='';
      // call new model event
      //MeshObject:=nil;
    end;
  //except
  //  ShowMessage('Error while loading file');
  //end;
  GetArrayStatus;
  Mesh.Name:='Mesh'+IntToStr(MeshList.Count+1);
  MeshList.Add(Mesh);
  MeshListBox.Items.Append(Mesh.Name);
  MeshListBox.Selected[MeshList.Count]:=true;
  NameEdit.Enabled:=true;
  NameEdit.Text:=Mesh.Name;
  Mesh:=nil;
  AdjustCamera;
  // workaround
  ShowOnTop;
  // debugging
  writeln('Loading complete');
end;

procedure TAsmMeshManager.MeshListBoxClick(Sender: TObject);
var
  i: Integer;
begin
  if Sender=nil then;
  OpenGLControl1.MakeCurrent;
  i:=0;
  while not MeshListBox.Selected[i] do inc(i);
  if i>0 then begin
    MeshObject.Mesh:=TAsmMesh(MeshList[i-1]);
    if ShaderSupported(Lighting,MeshObject.Mesh) then begin
      MeshObject.Shader:=Lighting;
    end else
      MeshObject.Shader:=NoLightingWireframe;
    MeshObject.visible:=true;
    GetArrayStatus;
    NameEdit.Text:=TAsmMesh(MeshList[i-1]).Name;
    if not NameEdit.Enabled then NameEdit.Enabled:=true;
    AdjustCamera;
  end else begin
    MeshObject.visible:=false;
    ClearArrayStatus;
    NameEdit.Text:='';
    NameEdit.Enabled:=false;
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmMeshManager.MeshListBoxDblClick(Sender: TObject);
var
  i: Integer;
begin
  if Sender=nil then;
  i:=0;
  while not MeshListBox.Selected[i] do inc(i);
  if i>0 then OnAssignMesh(TAsmMesh(MeshList[i-1]));
end;

procedure TAsmMeshManager.NameEditChange(Sender: TObject);
begin
  if MeshListBox.Items.Count>1 then EnableControls;
end;

procedure TAsmMeshManager.NameEditExit(Sender: TObject);
begin
  if sender=nil then;
  MeshObject.Mesh.Name:=NameEdit.Text;
  MeshListBox.Items[MeshListBox.ItemIndex]:=MeshObject.Mesh.Name;
end;

procedure TAsmMeshManager.NameEditKeyPress(Sender: TObject; var Key: Char);
begin
  if Sender=nil then;
  if Key=#13 then NameEditExit(Sender);
end;

procedure TAsmMeshManager.OpenGLControl1Paint(Sender: TObject);
begin
  if Sender=nil then;
  if not SceneInitialized then InitScene;
  Scene.RenderScene;
end;

procedure TAsmMeshManager.OpenGLControl1Resize(Sender: TObject);
begin
  if Sender=nil then;
  Scene.UpdateViewport;
end;

procedure TAsmMeshManager.SphereMenuitemClick(Sender: TObject);
var Mesh: TAsmMesh;
begin
  if Sender=nil then;
  OpenGLControl1.MakeCurrent;
  Mesh:=TAsmMesh.Create;
  Mesh.CreateSphere(1,16,16);
  MeshObject.Mesh:=Mesh;
  MeshObject.SetColor(1,1,1,1);
  {if ShaderSupported(Lighting,MeshObject.Mesh) then begin
    MeshObject.Shader:=Lighting;
  end else}
    MeshObject.Shader:=NoLightingWireframe;
  MeshObject.visible:=true;
  GetArrayStatus;
  Mesh.Name:='Mesh'+IntToStr(MeshList.Count+1);
  MeshList.Add(Mesh);
  MeshListBox.Items.Append(Mesh.Name);
  MeshListBox.Selected[MeshList.Count]:=true;
  NameEdit.Enabled:=true;
  NameEdit.Text:=Mesh.Name;
  Mesh:=nil;
  AdjustCamera;
end;

procedure TAsmMeshManager.SphereRadiusEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateSphere(StrToFloat(SphereRadiusEdit.Text),
   StrToInt(SphereRingsEdit.Text),StrToInt(SphereSegmentsEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.SphereRingsEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateSphere(StrToFloat(SphereRadiusEdit.Text),
   StrToInt(SphereRingsEdit.Text),StrToInt(SphereSegmentsEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.SphereSegmentsEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateSphere(StrToFloat(SphereRadiusEdit.Text),
   StrToInt(SphereRingsEdit.Text),StrToInt(SphereSegmentsEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.SquareMenuitemClick(Sender: TObject);
var
  Mesh: TAsmMesh;
begin
  if Sender=nil then;
  OpenGLControl1.MakeCurrent;
  Mesh:=TAsmMesh.Create;
  Mesh.CreateSquare(1,1);
  MeshObject.Mesh:=Mesh;
  MeshObject.SetColor(1,1,1,1);
  if ShaderSupported(Lighting,MeshObject.Mesh) then begin
    MeshObject.Shader:=Lighting;
  end else
    MeshObject.Shader:=NoLightingWireframe;
  MeshObject.visible:=true;
  GetArrayStatus;
  Mesh.Name:='Square'+IntToStr(MeshList.Count+1);
  MeshList.Add(Mesh);
  MeshListBox.Items.Append(Mesh.Name);
  MeshListBox.Selected[MeshList.Count]:=true;
  NameEdit.Enabled:=true;
  NameEdit.Text:=Mesh.Name;
  Mesh:=nil;
  AdjustCamera;
end;

procedure TAsmMeshManager.SquareSizeEditChange(Sender: TObject);
begin

end;

procedure TAsmMeshManager.SquareSizeEditExit(Sender: TObject);
var
  i: LongInt;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateSquare(StrToFloat(SquareSizeEdit.Text),StrToFloat(
   SquareSubdivisionEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.SquareSubdivisionEditExit(Sender: TObject);
var
  i: Integer;
begin
  i:=MeshListBox.ItemIndex-1;
  TAsmMesh(MeshList[i]).CreateSquare(StrToFloat(SquareSizeEdit.Text),StrToFloat(
   SquareSubdivisionEdit.Text));
  MeshObject.BS.Radius:=TAsmMesh(MeshList[i]).BS.Radius*MeshObject.maxScale;
  AdjustCamera;
end;

procedure TAsmMeshManager.SupportedShadersCheckGroupItemClick(Sender: TObject;
  Index: integer);
var
  i: Integer;
begin
  if Sender=nil then;
  if SupportedShadersCheckGroup.Checked[index]=true then
    case index of
      0: begin end;
      1: begin end;
      2: with MeshObject.Mesh do begin
           writeln('Generating Bumpmapping information');
           if ColorArray.length>0 then
             writeln('Color information found')
           else begin
             // workaround - fill color array with nothing (setsize) / empty buffer would be better
             for i:=0 to VertexArray.length-1 do ColorArray.Add(0,0,0);
             CreateTBNMatrixArray;
           end;
         end;
    end;
end;

procedure TAsmMeshManager.AdjustCamera;
var newRadius: GLfloat;
begin
  // AdjustCamera
  newRadius:=MeshObject.BS.Radius*3;
  Camera.Radius:=newRadius;
  Camera.FarPlane:=newRadius+MeshObject.BS.Radius;
  Camera.NearPlane:=newRadius-MeshObject.BS.Radius;
  Camera.SetCoV(MeshObject.BS.Center);
  Scene.UpdateViewport;
  OpenGLControl1.Invalidate;
end;

procedure TAsmMeshManager.DisableControls;
begin
  AssignMeshButton.Enabled:=true;
  NameEdit.Enabled:=false;
  Notebook1.Enabled:=false;
  SupportedShadersCheckGroup.Enabled:=false;
end;

procedure TAsmMeshManager.EnableControls;
begin
  AssignMeshButton.Enabled:=true;
  NameEdit.Enabled:=true;
  Notebook1.Enabled:=true;
  SupportedShadersCheckGroup.Enabled:=true;
end;

procedure TAsmMeshManager.InitScene;
begin
  Scene.Init;
  Scene.SetBackgroundColor(0,0,0);
  Camera:=TAsmRotationCamera.Create(15,0,0,0,0,45,1,100000,self);
  Scene.ActiveCamera:=Camera;
  MeshObject:=TAsmObject.Create(self);
  Scene.Objectlist.Add(MeshObject);
  Light:=TAsmDirectionalLight.Create(self);
  Light.SetPosition(0,0,100);
  Light.SetAmbientColor(0.1,0.1,0.1,1);
  Light.SetDiffuseColor(0.9,0.9,0.9,1);
  Light.SetSpecularColor(1,1,1,1);
  Light.Enabled:=true;
  Scene.LightList.Add(Light);
  SceneInitialized:=true;
  OpenGLControl1.Invalidate;
end;

procedure TAsmMeshManager.GetArrayStatus;
var i: integer;
begin
  for i:=0 to 2 do begin
    SupportedShadersCheckGroup.Checked[i]:=false;
    SupportedShadersCheckGroup.CheckEnabled[i]:=false;
  end;
  if MeshObject.Mesh.NormalArray.length>0 then begin
    SupportedShadersCheckGroup.Checked[0]:=true;
    SupportedShadersCheckGroup.CheckEnabled[0]:=true;
  end;
  if MeshObject.Mesh.TexCoordArray.length>0 then begin
    SupportedShadersCheckGroup.Checked[1]:=true;
    if MeshObject.Mesh.TBNMatrixArray.length>0 then
      SupportedShadersCheckGroup.Checked[2]:=true;
    if MeshObject.Mesh.NormalArray.length>0 then
      SupportedShadersCheckGroup.CheckEnabled[2]:=true;
  end else
    SupportedShadersCheckGroup.CheckEnabled[2]:=false;
  SupportedShadersCheckGroup.Enabled:=true;
end;

procedure TAsmMeshManager.ClearArrayStatus;
var
  i: Integer;
begin
  for i:=0 to 2 do SupportedShadersCheckGroup.Checked[i]:=false;
  SupportedShadersCheckGroup.Enabled:=false;
end;

initialization
  {$I asmmeshmanagerform.lrs}

end.

