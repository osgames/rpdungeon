{
 *****************************************************************************
 *                                                                           *
 *  This source is free software. See the file COPYING.modifiedLGPL,         *
 *  included in this distribution, for details about the copyright.          *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************

 asmodayeditor.lpr
 Author: Reimar Grabowski (2002-2004)
}

program asmodayeditor;

{$mode objfpc}{$H+}

uses
  Interfaces,
  Forms, asmodayeditorform, asmmeshmanagerform, asmoday, LazOpenGLContext, nvGL,
  nvGLX;

begin
  Application.Initialize;
  Application.CreateForm(TAsmEditorMain, AsmEditorMain);
  Application.CreateForm(TAsmMeshManager, AsmMeshManager);
  Application.Run;
end.

