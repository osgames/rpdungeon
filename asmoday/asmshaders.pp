{
 *****************************************************************************
 *                                                                           *
 *  This source is free software. See the file COPYING.modifiedLGPL,         *
 *  included in this distribution, for details about the copyright.          *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************

 asmshaders.pp
 Author: Reimar Grabowski (2003)
}

unit asmshaders;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, nvgl, Vectors, asmtypes, asmutils;

type
  TAsmShaderNoLighting = class(TAsmShader)
  public
    constructor Create;
    procedure Draw(AsmObject: TAsmObject); override;
  end;

  TAsmShaderLighting = class(TAsmShader)
  public
    constructor Create;
    procedure Draw(AsmObject: TAsmObject); override;
  end;

  TAsmShaderNoLightingWireframe = class(TAsmShaderNoLighting)
  public
    constructor Create;
  end;

  TAsmShaderLightingWireframe = class(TAsmShaderLighting)
  public
    constructor Create;
  end;

  TAsmShaderNoLightingTexture = class(TAsmShader)
  public
    constructor Create;
    procedure Draw(AsmObject: TAsmObject); override;
  end;

  TAsmShaderLightingTexture = class(TAsmShader)
  public
    constructor Create;
    procedure Draw(AsmObject: TAsmObject); override;
  end;

  TAsmShaderLightingNormal = class(TAsmShader)
  public
    constructor Create;
    procedure Draw(AsmObject: TAsmObject); override;
  end;

  TAsmShaderNoLightingColor = class(TAsmShader)
  public
    constructor Create;
    procedure Draw(AsmObject: TAsmObject); override;
  end;

  TAsmShaderLightingColor = class(TAsmShader)
  public
    constructor Create;
    procedure Draw(AsmObject: TAsmObject); override;
  end;

  function ShaderSupported(Shader: GLuint; Mesh: TAsmMesh): boolean;

var
  ShaderLighting: TAsmShaderLighting;
  ShaderNoLighting: TAsmShaderNoLighting;
  ShaderLightingWireframe: TAsmShaderLightingWireframe;
  ShaderNoLightingWireframe: TAsmShaderNoLightingWireframe;
  ShaderLightingTexture: TAsmShaderLightingTexture;
  ShaderNoLightingTexture: TAsmShaderNoLightingTexture;
  ShaderNoLightingColor: TAsmShaderNoLightingColor;
  ShaderLightingNormal: TAsmShaderLightingNormal;

implementation

function ShaderSupported(Shader: GLuint; Mesh: TAsmMesh): boolean;
var Res: boolean;
begin
  Res:=false;
  with Mesh do
    case Shader of
      0,1: Res:=true;
      2: if TexCoordArray.Length>0 then Res:=true;
      3: if ColorArray.Length>0 then Res:=true;
      4,5,7: if NormalArray.Length>0 then Res:=true;
      6: if (NormalArray.Length>0) and (TexCoordArray.Length>0) then Res:=true;
    end;
  ShaderSupported:=Res;
end;

{ TAsmShaderNoLighting }

constructor TAsmShaderNoLighting.Create;
begin
  fStates:=[];
end;

procedure TAsmShaderNoLighting.Draw(AsmObject: TAsmObject);
var MeshData: TAsmMesh; //debugging BSCenter: TVector;
begin
  glPushMatrix;
  with AsmObject do begin
    gltranslatef(CoR.x, CoR.y, CoR.z);
    glMultMatrixf(@Matrix);
    if (Scale.x<>1) or (Scale.y<>1) or (Scale.z<>1) then glScalef(Scale.x,Scale.y,Scale.z);
    MeshData:=AsmObject.Mesh;
  end;
  with MeshData do begin
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, VertexBuffer);
    glVertexPointer(3, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, IndexBuffer);
    // debugging
    //writeln('Rendering VBO');
    //writeln('glGetError: ',glGetError);
    //
    glDrawRangeElements(Primitive,0,count-1,count,GL_UNSIGNED_INT,nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,0);
     // debugging - render without vbo
    {glVertexPointer(3, GL_FLOAT, 0, VertexArray.Data);
    glDrawRangeElements(Primitive,0,count-1,count,GL_UNSIGNED_INT,IndexArray.Data);}
  end;
  glPopMatrix;
  // debugging - draw bounding sphere
  {glPushMatrix;
  BSCenter:=iTransformVector(AsmObject.BS.Center,AsmObject.Matrix)+AsmObject.Position;
  glTranslatef(BSCenter.X,BSCenter.Y,BSCenter.Z);
  glutwiresphere(AsmObject.BS.Radius,20,20);
  glPopMatrix;}
end;

{ TAsmShaderLighting }

constructor TAsmShaderLighting.Create;
begin
  fStates:=[lit];
end;

procedure TAsmShaderLighting.Draw(AsmObject: TAsmObject);
var MeshData: TAsmMesh;
begin
  glPushMatrix;
  with AsmObject do begin
    gltranslatef(CoR.x, CoR.y, CoR.z);
    glMultMatrixf(@Matrix);
    if (Scale.x<>1) or (Scale.y<>1) or (Scale.z<>1) then glScalef(Scale.x,Scale.y,Scale.z);
    MeshData:=AsmObject.Mesh;
  end;
  with MeshData do begin
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, VertexBuffer);
    glVertexPointer(3, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, NormalBuffer);
    glNormalPointer(GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, IndexBuffer);
    glDrawRangeElements(Primitive,0,count-1,count,GL_UNSIGNED_INT,nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,0);
  end;
  glPopMatrix;
end;

{ TAsmShaderNoLightingWireframe }

constructor TAsmShaderNoLightingWireframe.Create;
begin
  fStates:=[wireframed]
end;

{ TAsmShaderLightingWireframe }

constructor TAsmShaderLightingWireframe.Create;
begin
  fStates:=[lit, wireframed]
end;

{ TAsmShaderNoLightingTexture }

constructor TAsmShaderNoLightingTexture.Create;
begin
  fStates:=[textured];
end;

procedure TAsmShaderNoLightingTexture.Draw(AsmObject: TAsmObject);
var MeshData: TAsmMesh;
begin
  glPushMatrix;
  with AsmObject do begin
    gltranslatef(CoR.x, CoR.y, CoR.z);
    glMultMatrixf(@Matrix);
    if (Scale.x<>1) or (Scale.y<>1) or (Scale.z<>1) then glScalef(Scale.x,Scale.y,Scale.z);
    MeshData:=AsmObject.Mesh;
    glBindTexture(GL_TEXTURE_2D,Texture.id);
  end;
  with MeshData do begin
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, VertexBuffer);
    glVertexPointer(3, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, TexCoordBuffer);
    glTexCoordPointer(2, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, IndexBuffer);
    glDrawRangeElements(Primitive,0,count-1,count,GL_UNSIGNED_INT,nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,0);
  end;
  glPopMatrix;
end;

{ TAsmShaderLightingTexture }

constructor TAsmShaderLightingTexture.Create;
begin
  fStates:=[lit, textured]
end;

procedure TAsmShaderLightingTexture.Draw(AsmObject: TAsmObject);
var MeshData: TAsmMesh;
begin
  glPushMatrix;
  with AsmObject do begin
    gltranslatef(CoR.x, CoR.y, CoR.z);
    glMultMatrixf(@Matrix);
    if (Scale.x<>1) or (Scale.y<>1) or (Scale.z<>1) then glScalef(Scale.x,Scale.y,Scale.z);
    MeshData:=AsmObject.Mesh;
    glBindTexture(GL_TEXTURE_2D,Texture.id);
  end;
  with MeshData do begin
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, VertexBuffer);
    glVertexPointer(3, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, TexCoordBuffer);
    glTexCoordPointer(2, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, NormalBuffer);
    glNormalPointer(GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, IndexBuffer);
    glDrawRangeElements(Primitive,0,count-1,count,GL_UNSIGNED_INT,nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,0);
  end;
  glPopMatrix;
end;

{ TAsmShaderLightingNormal }

constructor TAsmShaderLightingNormal.Create;
begin
  fStates:=[lit]
end;

procedure TAsmShaderLightingNormal.Draw(AsmObject: TAsmObject);
var MeshData: TAsmMesh; vertex, normal: TVector3; i: integer;
begin
  glPushMatrix;
  with AsmObject do begin
    gltranslatef(CoR.x, CoR.y, CoR.z);
    glMultMatrixf(@Matrix);
    if (Scale.x<>1) or (Scale.y<>1) or (Scale.z<>1) then glScalef(Scale.x,Scale.y,Scale.z);
    MeshData:=AsmObject.Mesh;
  end;
  with MeshData do begin
    glVertexPointer(3, GL_FLOAT, 0, VertexArray.Data);
    glNormalPointer(GL_FLOAT, 0, NormalArray.Data);
    glDrawRangeElements(Primitive,0,count-1,count,GL_UNSIGNED_INT,IndexArray.Data);
    // draw normals
    glDisable(GL_LIGHTING);
    glScalef(1,1,1);
    glColor3f(0,0,0);
    glPointSize(10);
    glBegin(GL_LINES);
    for i:=0 to count-1 do begin
      Vertex:=VertexArray.Data[IndexArray.Data[i]];
      Normal:=2*NormalArray.Data[IndexArray.Data[i]];
      glVertex3fv(glArray(Vertex));
      glVertex3fv(glArray(Vertex+Normal));
    end;
    glEnd;
    glEnable(GL_LIGHTING);
  end;
  glPopMatrix;
end;

{ TAsmShaderNoLightingColor }

constructor TAsmShaderNoLightingColor.Create;
begin
  fStates:=[multicolored];
end;

procedure TAsmShaderNoLightingColor.Draw(AsmObject: TAsmObject
  );
var MeshData: TAsmMesh;
begin
  glPushMatrix;
  with AsmObject do begin
    gltranslatef(CoR.x, CoR.y, CoR.z);
    glMultMatrixf(@Matrix);
    if (Scale.x<>1) or (Scale.y<>1) or (Scale.z<>1) then glScalef(Scale.x,Scale.y,Scale.z);
    MeshData:=AsmObject.Mesh;
  end;
  with MeshData do begin
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, VertexBuffer);
    glVertexPointer(3, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, ColorBuffer);
    glColorPointer(3, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, IndexBuffer);
    glDrawRangeElements(Primitive,0,count-1,count,GL_UNSIGNED_INT,nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,0);
  end;
  glPopMatrix;
end;

{ TAsmShaderLightingColor }

constructor TAsmShaderLightingColor.Create;
begin
  fStates:=[lit, multicolored];
end;

procedure TAsmShaderLightingColor.Draw(AsmObject: TAsmObject);
var MeshData: TAsmMesh;
begin
  glPushMatrix;
  with AsmObject do begin
    gltranslatef(CoR.x, CoR.y, CoR.z);
    glMultMatrixf(@Matrix);
    if (Scale.x<>1) or (Scale.y<>1) or (Scale.z<>1) then glScalef(Scale.x,Scale.y,Scale.z);
    MeshData:=AsmObject.Mesh;
  end;
  with MeshData do begin
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, VertexBuffer);
    glVertexPointer(3, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, NormalBuffer);
    glNormalPointer(GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, ColorBuffer);
    glColorPointer(3, GL_FLOAT, 0, nil);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, IndexBuffer);
    glDrawRangeElements(Primitive,0,count-1,count,GL_UNSIGNED_INT,nil);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,0);
    //
  end;
  glPopMatrix;
end;

var count: integer;

initialization

// Setting up shaders
ShaderList:=TFPList.Create;
ShaderNoLighting:=TAsmShaderNoLighting.Create;
ShaderNoLightingWireframe:=TAsmShaderNoLightingWireframe.Create;
ShaderNoLightingTexture:=TAsmShaderNoLightingTexture.Create;
ShaderNoLightingColor:=TAsmShaderNoLightingColor.Create;
ShaderLighting:=TAsmShaderLighting.Create;
ShaderLightingWireframe:=TAsmShaderLightingWireframe.Create;
ShaderLightingTexture:=TAsmShaderLightingTexture.Create;
ShaderLightingNormal:=TAsmShaderLightingNormal.Create;
ShaderList.Add(ShaderNoLighting);
ShaderList.Add(ShaderNoLightingWireframe);
ShaderList.Add(ShaderNoLightingTexture);
ShaderList.Add(ShaderNoLightingColor);
ShaderList.Add(ShaderLighting);
ShaderList.Add(ShaderLightingWireframe);
ShaderList.Add(ShaderLightingTexture);
ShaderList.Add(ShaderLightingNormal);

finalization

for count:=0 to ShaderList.Count-1 do
  TAsmShader(ShaderList.Items[count]).Free;
ShaderList.Free;

end.

