{
 *****************************************************************************
 *                                                                           *
 *  This source is free software. See the file COPYING.modifiedLGPL,         *
 *  included in this distribution, for details about the copyright.          *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************

 asmtypes.pp
 Author: Reimar Grabowski (2001-2005)
}

unit asmtypes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, nvgl, sgiglu, Vectors, dyn_arrays, avl_tree, math, asmutils,
  Dialogs, fpImage;

type
  TFrustum = array [0..5,0..3] of GLFloat;
  
  AsmStates = (static, lit, multicolored, textured, multitextured, transparent,
               wireframed, notDepthTested, Line);
  AsmState = Set of AsmStates;
  
  // StateMachine
  GLStatus = object
  protected
    Blending,
    ColorMaterial,
    noDepthTest,
    Lighting,
    Multicolor,
    Texture2D,
    TextureUnit1,
    TextureUnit2,
    Wireframe: Boolean;
    currentState: AsmState;
    LineWidth: GLfloat;
  public
    procedure SetState(const State: AsmState);
  end;
  
  // Light

  { TAsmLight }

  TAsmLight = class(TComponent)
  private
    function GetPosition: TVector3;
    function GetPosition4: TVector4;
  protected
    fAmbientColor: glRGBAColor;
    fDiffuseColor: glRGBAColor;
    fSpecularColor: glRGBAColor;
    fAliasPosition: PVector3;
    fPosition: TVector4;
    procedure PlaceLight(glLight: GLenum);
  public
    constructor Create(AnOwner: TComponent); override;
    property AmbientColor: glRGBAColor read fAmbientColor;
    property DiffuseColor: glRGBAColor read fDiffuseColor;
    property SpecularColor: glRGBAColor read fSpecularColor;
    property Position: TVector3 read GetPosition;
    property Position4: TVector4 read GetPosition4;
    procedure SetAmbientColor(red, green, blue, alpha: GLFloat);
    procedure SetAmbientColor(const newColor: glRGBAColor);
    procedure SetDiffuseColor(red, green, blue, alpha: GLFloat);
    procedure SetDiffuseColor(const newColor: glRGBAColor);
    procedure SetSpecularColor(red, green, blue, alpha: GLFloat);
    procedure SetSpecularColor(const newColor: glRGBAColor);
    procedure SetPosition(const Pos: TVector3); virtual; abstract;
    procedure SetPosition(x, y, z: GLFloat); virtual; abstract;
    procedure SetPosition(Pos: PVector3); virtual; abstract;
    Enabled: Boolean;
  end;
  
  // Mesh

  { TAsmMesh }

  TAsmMesh = class
  protected
    FCount: GLuint;
    AVLTree: TAVLTree;
    Vert3: ArrayOfV3Rec;
    Vert3_3: ArrayOfV3_3Rec;
    Vert3_2: ArrayOfV3_2Rec;
    Vert3_3_2: ArrayOfV3_3_2Rec;
    function AddVertex3(x, y, z: GLfloat): GLuint;
    function AddVertex3_2(x, y, z, x2, y2: GLfloat; var iArray:
     ArrayOfTVector2): GLuint;
    function AddVertex3_3(x, y, z, x2, y2, z2: GLfloat; var iArray:
     ArrayOfTVector3): GLuint;
    function AddVertex3_3_2(x, y, z, x2, y2, z2, x3, y3: GLfloat; var iArray:
     ArrayOfTVector3; var iArray2: ArrayOfTVector2): GLuint;
    //
    procedure BuildGeometryInfo;
    procedure ClearArrays;
    procedure CreateBuffers;
    procedure CreateEdgeList;
    procedure CreateGeoIndexArray;
    procedure CreateTriangleList;
  public
    // used OpenGL primitive (GL_TRIANGLES and GL_QUADS) supported but GL_QUADS
    // support will be dropped in the future
    Primitive: GLenum;
    // debugging
    //SortedToUnsortedIndex: ArrayOfGLuint;
    // OpenGL arrays
    VertexArray,
    NormalArray,
    ColorArray: ArrayOfTVector3;
    TexCoordArray: ArrayOfTVector2;
    IndexArray, GeoIndexArray: ArrayOfGLuint;
    TBNMatrixArray: ArrayOfMatrix3X3;
    // OpenGL buffers for use of VBO
    VertexBuffer,
    NormalBuffer,
    ColorBuffer,
    TexCoordBuffer,
    IndexBuffer: GLuint;
    // should become a (read only?) property
    TriangleList: array of TTriangle;
    EdgeList: array of TEdge;
    // Bounding geometry
    BS: TSphere;
    BoundingBox: TVector3;
    //
    Name: string;
    constructor Create;
    destructor Destroy; override;
    //
    property Count: Gluint read FCount;
    //
    procedure CreateTBNMatrixArray;
    // Mesh construction or loading routines
    procedure CreateSquare(Size, Subdivisions: GLFloat);
    procedure CreateGrid(Size, StepSize: GLfloat);
    procedure CreateBox(BoxWidth, BoxHeight, BoxDepth: GLfloat);
    procedure CreateSphere(Radius: GLFloat; u, v: integer);
    procedure CreateCohen(Radius1, Radius2, Length: GLfloat; n: integer);
    procedure CreateCoordinateSystem(Size: GLfloat; full: boolean);
    function LoadMeshFromObjFile(const Filename: string): boolean;
  end;
  
  // Texture

  { TAsmTexture }

  TAsmTexture = class(TFPCustomImage)
  private
    procedure SetInternalColor(x, y: integer;
      const Value: TFPColor); override;
    function GetInternalColor(
      x, y: integer): TFPColor; override;
    procedure SetInternalPixel (x,y:integer; Value:integer); override;
    function GetInternalPixel (x,y:integer) : integer; override;
  public
    Data: Pbyte;
    id: GLuint;
    constructor Create(AWidth, AHeight: integer); override;
    destructor Destroy; override;
    procedure SetSize(AWidth, AHeight: integer); override;
    procedure Bind;
  end;
  
  // Asmoday Basic Object Types
  TAsmDrawable = class(TComponent)
  protected
    fColor: glRGBAColor;
    fPosition: TVector3;
    fSpecColor: glRGBAColor;
    DisplayList: Integer;
    FShader: Byte;
    NearPlaneDistance: GLfloat;
  public
    CameraIndependent: Boolean;
    LineWidth: GLfloat;
    Shader: Byte;
    Shininess: GLint;
    Visible: Boolean;
    //
    property Position: TVector3 read fPosition;
    property Color: glRGBAColor read fColor;
    property SpecularColor: glRGBAColor read fSpecColor;
    procedure SetColor(red, green, blue, alpha: GLfloat);
    procedure SetColor(iColor: glRGBAColor);
    procedure SetSpecularColor(red, green, blue: GLfloat);
    procedure SetSpecularColor(iColor: glRGBColor);
  end;

  { TAsmObject }

  TAsmObject = class(TAsmDrawable)
  private
    //function GetInvGLMatrix: TMatrix4x4;
  protected
    fMesh: TAsmMesh;
    procedure SetMesh(const AValue: TAsmMesh);
    fMatrix: TMatrix4x4;
    fCoR: TVector3;
    fScale: TVector3;
    fYaw: GLfloat;
    fmaxScale: GLfloat;
    fBS: TSphere;
    procedure UpdateMatrix;
    function GetinvMatrix: TMatrix4x4;
    //
    procedure BuildSilhouette(const Light: TAsmLight);
    procedure BuildShadowVolume(const Light: TAsmLight);
    procedure DrawShadowVolume;
  public
    Texture: TAsmTexture;
    transparent: boolean;
    constructor Create(AnOwner: TComponent); override;
    destructor Destroy; override;
    //
    property Matrix: TMatrix4x4 read fMatrix write fMatrix;
    property invMatrix: TMatrix4x4 read GetinvMatrix;
    //property invGLMatrix: TMatrix4x4 read GetInvGLMatrix;
    property maxScale: GLfloat read fmaxScale;
    property CoR: TVector3 read fCoR;
    property Scale: TVector3 read fScale;
    property Mesh:  TAsmMesh read fMesh write SetMesh;
    Silhouette: array of TSilhouetteEdge;
    ShadowVolumeIA: array of GLuint; // index array
    ShadowVolumeVA: array of TVector4; // vertex array
    // workaround
    property BS: TSphere read fBS write fBS;
    //
    function GetEulerAngles: TVector3;
    procedure SetEulerAngles(Angle: TVector3);
    // Transformation
    procedure SetScale(const iScale: TVector3);
    procedure SetScale(ScaleX, ScaleY, ScaleZ: GLfloat);
    procedure ResetMatrix;
    procedure ResetRotation;
    procedure ResetScale;
    procedure ResetTranslation;
    procedure SetCoR(const Pos: TVector3);
    procedure SetCoR(x, y, z: GLFloat);
    procedure SetPosition(const Pos: TVector3);
    procedure SetPosition(x, y, z: GLFloat);
    procedure Translate(const Pos: TVector3);
    procedure Translate(ax, ay, az: GLFloat);
    procedure TranslateCoR(ax, ay, az: GLFloat);
    procedure RotateAboutLocalX(angle: GLFloat);
    procedure RotateAboutLocalY(angle: GLFloat);
    procedure RotateAboutLocalZ(angle: GLFloat);
    procedure Rotate(angle: GLFLoat; axis: TVector3);
    procedure Rotate(angle: GLFLoat; ax, ay, az: GLFloat);
    procedure RotateAboutLocalOrigin(angle: GLFloat; const axis: TVector3);
    procedure RotateAboutLocalOrigin(angle: GLFloat; ax, ay, az: GLFloat);
  end;
  
  // Shader
  TAsmShader = class
  protected
    fStates: AsmState;
  public
    property States: AsmState read fStates;
    procedure Draw(AsmObject: TAsmObject); virtual; abstract;
  end;
  
  // Base class for all cameras

  { TAsmCamera }

  TAsmCamera = class(TComponent)
  private
    fHeight: integer;
    fWidth: integer;
    procedure SetHeight(const AValue: integer);
    procedure SetWidth(const AValue: integer);
  protected
    fFarPlane: GLFloat;
    fFoV: GLFloat;
    fNearPlane: GLFLoat;
    fProjMatrix: TMatrix4x4;
    fModelMatrix: TMatrix4x4;
    fUp, fCoV: TVector3;
    Frustum: TFrustum;
    FrustumCone: TCone;
    FrustumBoundingSphere: TSphere;
    procedure Rotate(var Vector: TVector3; const Axis: TVector3; angle: GLFloat);
    procedure SetFarPlane(const AValue: GLFloat);
    procedure SetFoV(const AValue: GLFloat);
    procedure SetNearPlane(const AValue: GLFLoat);
  public
    Pos: TVector3;
    constructor Create(iFoV, iNearPlane, iFarPlane: GLFloat; AnOwner:
     TComponent); overload;
    property CoV: TVector3 read fCoV;
    property Up: TVector3 read fUp;
    property FoV: GLFloat read fFoV write SetFoV;
    property NearPlane: GLFLoat read fNearPlane write SetNearPlane;
    property FarPlane: GLFloat read fFarPlane write SetFarPlane;
    property ProjMatrix: TMatrix4x4 read fProjMatrix;
    property ModelMatrix: TMatrix4x4 read fModelMatrix;
    property Height: integer read fHeight write SetHeight;
    property Width: integer read fWidth write SetWidth;
    procedure GetFrustum;
    procedure GetFrustumBoundingSphere;
    procedure GetFrustumCone;
    procedure RenderView(ObjectList, Lightlist: TFPList; var GLStateMachine:
     GLStatus); virtual;
    procedure RenderToTexture;
  end;
  
function CompareDistanceDes(p1, p2: pointer): Integer;
function CompareDistanceAsc(p1, p2: pointer): Integer;
function CompareShaderAsc(p1, p2: pointer): Integer;
function CompareShaderDes(p1, p2: pointer): Integer;

var
  // Shader
  ShaderList: TFPList;
  glLights: array [0..7] of GLenum;
  //GLStateMachine : GLStatus;
  

implementation

// Merge Sort Compare Functions

function CompareDistanceDes(p1, p2: pointer): Integer;
var Distance1, Distance2: GLfloat;
begin
  Distance1:=TAsmObject(p1).NearPlaneDistance;
  Distance2:=TAsmObject(p2).NearPlaneDistance;
  if Distance1>Distance2 then Result:=1
  else if Distance1<Distance2 then Result:=-1
  else Result:=0;
end;

function CompareDistanceAsc(p1, p2: pointer): Integer;
var Distance1, Distance2: GLfloat;
begin
  Distance1:=TAsmObject(p1).NearPlaneDistance;
  Distance2:=TAsmObject(p2).NearPlaneDistance;
  if Distance1>Distance2 then Result:=-1
  else if Distance1<Distance2 then Result:=1
  else Result:=0;
end;

function CompareShaderAsc(p1, p2: pointer): Integer;
var Shader1, Shader2: Byte;
begin
  Shader1:=TAsmObject(p1).Shader;
  Shader2:=TAsmObject(p2).Shader;
  if Shader1>Shader2 then Result:=1
  else if Shader1<Shader2 then Result:=-1
  else Result:=0;
end;

function CompareShaderDes(p1, p2: pointer): Integer;
var Shader1, Shader2: Byte;
begin
  Shader1:=TAsmObject(p1).Shader;
  Shader2:=TAsmObject(p2).Shader;
  if Shader1>Shader2 then Result:=-1
  else if Shader1<Shader2 then Result:=1
  else Result:=0;
end;

{ TAsmMesh }

{ All AddVertex function use an AVL-tree to build OpenGL usable arrays and
  ensure that only unique information is stored. They are of the form
  AddVertexA_B_C (A, B, C integer) where A is the number of components in the
  VertexArray and B, C the number of components of the arrays given to the
  function (for example normal or texture coordinate arrays) }
function TAsmMesh.AddVertex3(x, y, z: GLfloat): GLuint;
var c: cardinal; r: V3Rec; OldData: PV3Rec; Diff: integer; AVLTreeNode: TAVLTreeNode;
begin
  // if it is the first vertex set everything up
  if VertexArray.Length=0 then begin
    AVLTree:=TAVLTree.Create(@Compare3);
    r.x:=x; r.y:=y; r.z:=z;
    r.Position:=0;
    Vert3.Add(r);
    AVLTree.Add(@Vert3.Data[0]);
    VertexArray.Add(x,y,z);
    AddVertex3:=0;
  end else begin
    c:=Vert3.Length;
    r.x:=x; r.y:=y; r.z:=z;
    r.Position:=c;
    AVLTreeNode:=AVLTree.Find(@r);
    // if the vertex already exits just return its position
    if AVLTreeNode<>nil then begin
      AddVertex3:=PV3Rec(AVLTreeNode.Data)^.Position;
    end else begin
      // else add the vertex
      OldData:=Vert3.Data;
      Vert3.Add(r);
      Diff:=integer(Vert3.Data)-integer(OldData);
      // if array memory has been reallocated then rellocate pointers in tree
      if Diff<>0 then begin
        AVLTreeNode:=AVLTree.FindLowest;
        while AVLTreeNode<>nil do begin
          inc(integer(AVLTreeNode.Data),Diff);
          AVLTreeNode:=AVLTree.FindSuccessor(AVLTreeNode);
        end;
      end;
      VertexArray.Add(x,y,z);
      AddVertex3:=c;
      AVLTree.Add(@Vert3.Data[c]);
    end;
  end;
end;

function TAsmMesh.AddVertex3_2(x, y, z, x2, y2: GLfloat; var iArray:
 ArrayOfTVector2): GLuint;
var c: cardinal; r: V3_2Rec; OldData: PV3_2Rec; Diff: integer;
  AVLTreeNode: TAVLTreeNode;
begin
  // if it is the first vertex set everything up
  if VertexArray.Length=0 then begin
    AVLTree:=TAVLTree.Create(@Compare3_2);
    r.x:=x; r.y:=y; r.z:=z;
    r.x2:=x2; r.y2:=y2;
    r.Position:=0;
    Vert3_2.Add(r);
    AVLTree.Add(@Vert3_2.Data[0]);
    VertexArray.Add(x,y,z);
    iArray.Add(x2,y2);
    AddVertex3_2:=0;
  end else begin
    c:=Vert3_2.Length;
    r.x:=x; r.y:=y; r.z:=z;
    r.x2:=x2; r.y2:=y2;
    r.Position:=c;
    AVLTreeNode:=AVLTree.Find(@r);
    // if the vertex already exits just return its position
    if AVLTreeNode<>nil then begin
      AddVertex3_2:=PV3_2Rec(AVLTreeNode.Data)^.Position;
    end else begin
      // else add the vertex
      OldData:=Vert3_2.Data;
      Vert3_2.Add(r);
      // if array memory has been reallocated then rellocate pointers in tree
      Diff:=PtrInt(Vert3_2.Data)-PtrInt(OldData);
      if Diff<>0 then begin
        AVLTreeNode:=AVLTree.FindLowest;
        while AVLTreeNode<>nil do begin
          inc(PtrInt(AVLTreeNode.Data),Diff);
          AVLTreeNode:=AVLTree.FindSuccessor(AVLTreeNode);
        end;
      end;
      VertexArray.Add(x,y,z);
      iArray.Add(x2,y2);
      AddVertex3_2:=c;
      AVLTree.Add(@Vert3_2.Data[c]);
    end;
  end;
end;

function TAsmMesh.AddVertex3_3(x, y, z, x2, y2, z2: GLfloat;
  var iArray: ArrayOfTVector3): GLuint;
var c: cardinal; r: V3_3Rec; OldData: PV3_3Rec; Diff: integer;
  AVLTreeNode: TAVLTreeNode;
begin
  // if it is the first vertex set everything up
  if VertexArray.Length=0 then begin
    AVLTree:=TAVLTree.Create(@Compare3_3);
    r.x:=x; r.y:=y; r.z:=z;
    r.x2:=x2; r.y2:=y2; r.z2:=z2;
    r.Position:=0;
    Vert3_3.Add(r);
    AVLTree.Add(@Vert3_3.Data[0]);
    VertexArray.Add(x,y,z);
    iArray.Add(x2,y2,z2);
    AddVertex3_3:=0;
  end else begin
    c:=Vert3_3.Length;
    r.x:=x; r.y:=y; r.z:=z;
    r.x2:=x2; r.y2:=y2; r.z2:=z2;
    r.Position:=c;
    AVLTreeNode:=AVLTree.Find(@r);
    // if the vertex already exits just return its position
    if AVLTreeNode<>nil then begin
      AddVertex3_3:=PV3_3Rec(AVLTreeNode.Data)^.Position;
    end else begin
      // else add the vertex
      OldData:=Vert3_3.Data;
      Vert3_3.Add(r);
      Diff:=PtrInt(Vert3_3.Data)-PtrInt(OldData);
      // if array memory has been reallocated then rellocate pointers in tree
      if Diff<>0 then begin
        AVLTreeNode:=AVLTree.FindLowest;
        while AVLTreeNode<>nil do begin
          inc(PtrInt(AVLTreeNode.Data),Diff);
          AVLTreeNode:=AVLTree.FindSuccessor(AVLTreeNode);
        end;
      end;
      VertexArray.Add(x,y,z);
      iArray.Add(x2,y2,z2);
      AddVertex3_3:=c;
      AVLTree.Add(@Vert3_3.Data[c]);
    end;
  end;
end;

function TAsmMesh.AddVertex3_3_2(x, y, z, x2, y2, z2, x3, y3: GLfloat;
  var iArray: ArrayOfTVector3; var iArray2: ArrayOfTVector2): GLuint;
var c: cardinal; r: V3_3_2Rec; OldData: PV3_3_2Rec; Diff: integer;
  AVLTreeNode: TAVLTreeNode;
begin
  // if it is the first vertex set everything up
  if VertexArray.Length=0 then begin
    AVLTree:=TAVLTree.Create(@Compare3_3_2);
    r.x:=x; r.y:=y; r.z:=z;
    r.x2:=x2; r.y2:=y2; r.z2:=z2;
    r.x3:=x3; r.y3:=y3;
    r.Position:=0;
    Vert3_3_2.Add(r);
    AVLTree.Add(@Vert3_3_2.Data[0]);
    VertexArray.Add(x,y,z);
    iArray.Add(x2,y2,z2);
    iArray2.Add(x3,y3);
    AddVertex3_3_2:=0;
  end else begin
    c:=Vert3_3_2.Length;
    r.x:=x; r.y:=y; r.z:=z;
    r.x2:=x2; r.y2:=y2; r.z2:=z2;
    r.x3:=x3; r.y3:=y3;
    r.Position:=c;
    AVLTreeNode:=AVLTree.Find(@r);
    // if the vertex already exits just return its position
    if AVLTreeNode<>nil then begin
      AddVertex3_3_2:=PV3_3_2Rec(AVLTreeNode.Data)^.Position;
    end else begin
      // else add the vertex
      OldData:=Vert3_3_2.Data;
      Vert3_3_2.Add(r);
      // if array memory has been reallocated then rellocate pointers in tree
      Diff:=PtrInt(Vert3_3_2.Data)-PtrInt(OldData);
      if Diff<>0 then begin
        AVLTreeNode:=AVLTree.FindLowest;
        while AVLTreeNode<>nil do begin
          inc(AVLTreeNode.Data,Diff);
          AVLTreeNode:=AVLTree.FindSuccessor(AVLTreeNode);
        end;
      end;
      VertexArray.Add(x,y,z);
      iArray.Add(x2,y2,z2);
      iArray2.Add(x3,y3);
      AddVertex3_3_2:=c;
      AVLTree.Add(@Vert3_3_2.Data[c]);
    end;
  end;
end;

procedure TAsmMesh.BuildGeometryInfo;
begin
  CreateGeoIndexArray;
  CreateTriangleList;
  CreateEdgeList;
end;

procedure TAsmMesh.ClearArrays;
begin
  FreeAndNil(AVLTree); // mattias
  VertexArray.Free;
  NormalArray.Free;
  ColorArray.Free;
  TexCoordArray.Free;
  IndexArray.Free;
  GeoIndexArray.Free;
  SetLength(EdgeList,0);
  SetLength(TriangleList,0);
  Vert3.Free;
  Vert3_3.Free;
  Vert3_3_2.Free;
  FCount:=0;
end;

procedure TAsmMesh.CreateSquare(Size, Subdivisions: GLFloat);
var
  x,y: Integer;
  StepSize: GLfloat;
begin
  if FCount>0 then ClearArrays;
  Primitive:=GL_TRIANGLES;
  BS.Radius:=sqrt(2*(Size/2)*(Size/2));
  BoundingBox.X:=Size/2;
  BoundingBox.Y:=Size/2;
  BoundingBox.Z:=0.01;
  StepSize:=Size/Subdivisions;
  for x:=0 to round(Size/StepSize)-1 do
    for y:=0 to round(Size/StepSize)-1 do begin
      IndexArray.Add(AddVertex3_3_2(-Size/2+x*StepSize,Size/2-y*StepSize,0,0,0,1,0,0,NormalArray,TexCoordArray));
      IndexArray.Add(AddVertex3_3_2(-Size/2+x*StepSize,Size/2-(y+1)*StepSize,0,0,0,1,0,1,NormalArray,TexCoordArray));
      IndexArray.Add(AddVertex3_3_2(-Size/2+(x+1)*StepSize,Size/2-y*StepSize,0,0,0,1,1,0,NormalArray,TexCoordArray));

      IndexArray.Add(AddVertex3_3_2(-Size/2+(x+1)*StepSize,Size/2-y*StepSize,0,0,0,1,1,0,NormalArray,TexCoordArray));
      IndexArray.Add(AddVertex3_3_2(-Size/2+x*StepSize,Size/2-(y+1)*StepSize,0,0,0,1,0,1,NormalArray,TexCoordArray));
      IndexArray.Add(AddVertex3_3_2(-Size/2+(x+1)*StepSize,Size/2-(y+1)*StepSize,0,0,0,1,1,1,NormalArray,TexCoordArray));
    end;
  FCount:=IndexArray.Length;
  BuildGeometryInfo;
  CreateBuffers;
end;

procedure TAsmMesh.CreateGrid(Size, StepSize: GLfloat);
var x: integer;
begin
  if FCount>0 then ClearArrays;
  Primitive:=GL_LINES;
  BS.Radius:=sqrt(2*(Size/2)*(Size/2));
  BoundingBox.X:=Size/2;
  BoundingBox.Y:=Size/2;
  BoundingBox.Z:=0.01;
  for x:=0 to round(Size/StepSize) do begin
    IndexArray.Add(AddVertex3(-Size/2+x*StepSize,Size/2,0));
    IndexArray.Add(AddVertex3(-Size/2+x*StepSize,-Size/2,0));
    IndexArray.Add(AddVertex3(Size/2,-Size/2+x*StepSize,0));
    IndexArray.Add(AddVertex3(-Size/2,-Size/2+x*StepSize,0));
  end;
  FCount:=IndexArray.Length;
  CreateTriangleList;
  CreateEdgeList;
  CreateBuffers;
end;

procedure TAsmMesh.CreateBox(BoxWidth, BoxHeight, BoxDepth: GLfloat);
var xDraw, yDraw, zDraw: GLfloat;
begin
  if FCount>0 then ClearArrays;
  xDraw:= -(BoxWidth/2);
  yDraw:= +(BoxHeight/2);
  zDraw:= +(BoxDepth/2);
  Primitive:=GL_TRIANGLES;
  BS.Radius:=sqrt(xDraw*xDraw+yDraw*yDraw+zDraw*zDraw);
  BS.Center.x:=0; BS.Center.y:=0; BS.Center.z:=0;
  BoundingBox.X:=BoxWidth/2;
  BoundingBox.Y:=BoxHeight/2;
  BoundingBox.Z:=BoxDepth/2;
  // Top Face
  IndexArray.Add(AddVertex3_3(xDraw, yDraw, zDraw-BoxDepth,0,1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw, zDraw,0,1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw, zDraw-BoxDepth,0,1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw, zDraw-BoxDepth,0,1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw, zDraw,0,1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw, zDraw,0,1,0,NormalArray));
  // Front Face
  IndexArray.Add(AddVertex3_3(xDraw, yDraw, zDraw,0,0,1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw-BoxHeight, zDraw,0,0,1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw, zDraw,0,0,1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw, zDraw,0,0,1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw-BoxHeight, zDraw,0,0,1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw-BoxHeight, zDraw,0,0,1,NormalArray));
  // Right Face
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw, zDraw,1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw-BoxHeight, zDraw,1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw, zDraw-BoxDepth,1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw, zDraw-BoxDepth,1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw-BoxHeight, zDraw,1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw-BoxHeight, zDraw-BoxDepth,1,0,0,NormalArray));
  // Back Face
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw, zDraw-BoxDepth,0,0,-1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw-BoxHeight, zDraw-BoxDepth,0,0,-1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw, zDraw-BoxDepth,0,0,-1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw, zDraw-BoxDepth,0,0,-1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw-BoxHeight, zDraw-BoxDepth,0,0,-1,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw-BoxHeight, zDraw-BoxDepth,0,0,-1,NormalArray));
  // Left Face
  IndexArray.Add(AddVertex3_3(xDraw, yDraw, zDraw-BoxDepth,-1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw-BoxHeight, zDraw-BoxDepth,-1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw, zDraw,-1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw, zDraw,-1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw-BoxHeight, zDraw-BoxDepth,-1,0,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw-BoxHeight, zDraw,-1,0,0,NormalArray));
  // Bottom Face
  IndexArray.Add(AddVertex3_3(xDraw, yDraw-BoxHeight, zDraw,0,-1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw-BoxHeight, zDraw-BoxDepth,0,-1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw-BoxHeight, zDraw,0,-1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw-BoxHeight, zDraw,0,-1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw, yDraw-BoxHeight, zDraw-BoxDepth,0,-1,0,NormalArray));
  IndexArray.Add(AddVertex3_3(xDraw+BoxWidth, yDraw-BoxHeight, zDraw-BoxDepth,0,-1,0,NormalArray));
  FCount:=IndexArray.Length;
  BuildGeometryInfo;
  CreateBuffers;
end;


procedure TAsmMesh.CreateSphere(Radius: GLFloat; u, v: integer);
var i, j: integer; s1, s2, alpha, alpha1, beta, beta1: GLfloat;
    e, bottom: TVector3;
begin
  if FCount>0 then ClearArrays;
  s1:=pi/u;
  s2:=2*pi/v;
  //
  bottom.x:=0; bottom.y:=-1; bottom.z:=0;
  // calculating circle
  Primitive:=GL_TRIANGLES;
  BS.Radius:=Radius;
  BS.Center.x:=0; BS.Center.y:=0; BS.Center.z:=0;
  BoundingBox.X:=Radius;
  BoundingBox.Y:=Radius;
  BoundingBox.Z:=Radius;
  for i:=0 to u-1 do begin
    alpha:=s1*i;
    if i=u-1 then alpha1:=pi else alpha1:=s1*(i+1);
    for j:=0 to v-1 do begin
      beta:=s2*j;
      if j=v-1 then beta1:=0 else beta1:=s2*(j+1);
      if i<>u-1 then begin
        e.x:=sin(alpha)*sin(beta1);
        e.y:=cos(alpha);
        e.z:=sin(alpha)*cos(beta1);
        IndexArray.Add(AddVertex3_3(Radius*e.x,Radius*e.y,Radius*e.z,e.x,e.y,e.z,NormalArray));
        e.x:=sin(alpha1)*sin(beta);
        e.y:=cos(alpha1);
        e.z:=sin(alpha1)*cos(beta);
        IndexArray.Add(AddVertex3_3(Radius*e.x,Radius*e.y,Radius*e.z,e.x,e.y,e.z,NormalArray));
        e.x:=sin(alpha1)*sin(beta1);
        e.y:=cos(alpha1);
        e.z:=sin(alpha1)*cos(beta1);
        IndexArray.Add(AddVertex3_3(Radius*e.x,Radius*e.y,Radius*e.z,e.x,e.y,e.z,NormalArray));
      end;
      if i<>0 then begin
        e.x:=sin(alpha)*sin(beta);
        e.y:=cos(alpha);
        e.z:=sin(alpha)*cos(beta);
        IndexArray.Add(AddVertex3_3(Radius*e.x,Radius*e.y,Radius*e.z,e.x,e.y,e.z,NormalArray));
        if i<>u-1 then begin
          e.x:=sin(alpha1)*sin(beta);
          e.y:=cos(alpha1);
          e.z:=sin(alpha1)*cos(beta);
        end else begin
          e:=bottom;
        end;
        IndexArray.Add(AddVertex3_3(Radius*e.x,Radius*e.y,Radius*e.z,e.x,e.y,e.z,NormalArray));
        e.x:=sin(alpha)*sin(beta1);
        e.y:=cos(alpha);
        e.z:=sin(alpha)*cos(beta1);
        IndexArray.Add(AddVertex3_3(Radius*e.x,Radius*e.y,Radius*e.z,e.x,e.y,e.z,NormalArray));
      end;
    end;
  end;
  FCount:=IndexArray.Length;
  BuildGeometryInfo;
  CreateBuffers;
end;

procedure TAsmMesh.CreateCohen(Radius1, Radius2, Length: GLfloat; n: integer);
var
  i: Integer; alpha, rb, rt, s, distance: GLfloat; ApexNormal, Normal,
  CircleNormal, TopCirclePoint, BottomCirclePoint: ArrayOfTVector3;
  Apex, v1, v2, minp, maxp: TVector3;
begin
  if FCount>0 then ClearArrays;
  Primitive:=GL_TRIANGLES;
  TopCirclePoint.Data:=nil;
  BottomCirclePoint.Data:=nil;
  Normal.Data:=nil;
  CircleNormal.Data:=nil;
  ApexNormal.Data:=nil;
  // radius
  rt:=Radius1;
  rb:=Radius2;
  // alpha scaling factor
  s:=2*pi/n;
  // calculating circle
  for i:=0 to n-1 do begin
    alpha:=s*i;
    TopCirclePoint.Add(rt*sin(alpha), rt*cos(alpha), -length/2);
    BottomCirclePoint.Add(rb*sin(alpha), rb*cos(alpha), length/2);
  end;
  TopCirclePoint.Add(TopCirclePoint.Data[0]);
  BottomCirclePoint.Add(BottomCirclePoint.Data[0]);
  //
  if (Radius1>0) and (Radius2>0) then begin
    for i:=0 to n-1 do begin
      v1:=TopCirclePoint.Data[i]-BottomCirclePoint.Data[i];
      v2:=TopCirclePoint.Data[i]-BottomCirclePoint.Data[i+1];
      Normal.Add(CrossProduct(v1,v2));
      Normalize(Normal.Data[i]);
    end;
    Normal.Add(Normal.Data[0]);
    CircleNormal.Add(0,0,0); // dummy
    for i:=1 to n do begin
      CircleNormal.Add(Normal.Data[i]+Normal.Data[i-1]);
      Normalize(CircleNormal.Data[i]);
    end;
    CircleNormal.Data[0]:=CircleNormal.Data[n];
    for i:=0 to n-1 do begin
      IndexArray.Add(AddVertex3_3(TopCirclePoint.Data[i].x,TopCirclePoint.Data[i].y,TopCirclePoint.Data[i].z,
       CircleNormal.Data[i].x,CircleNormal.Data[i].y,CircleNormal.Data[i].z,NormalArray));
      IndexArray.Add(AddVertex3_3(BottomCirclePoint.Data[i].x,BottomCirclePoint.Data[i].y,BottomCirclePoint.Data[i].z,
       CircleNormal.Data[i].x,CircleNormal.Data[i].y,CircleNormal.Data[i].z,NormalArray));
      IndexArray.Add(AddVertex3_3(TopCirclePoint.Data[i+1].x,TopCirclePoint.Data[i+1].y,TopCirclePoint.Data[i+1].z,
       CircleNormal.Data[i+1].x,CircleNormal.Data[i+1].y,CircleNormal.Data[i+1].z,NormalArray));
      IndexArray.Add(AddVertex3_3(TopCirclePoint.Data[i+1].x,TopCirclePoint.Data[i+1].y,TopCirclePoint.Data[i+1].z,
       CircleNormal.Data[i+1].x,CircleNormal.Data[i+1].y,CircleNormal.Data[i+1].z,NormalArray));
      IndexArray.Add(AddVertex3_3(BottomCirclePoint.Data[i].x,BottomCirclePoint.Data[i].y,BottomCirclePoint.Data[i].z,
       CircleNormal.Data[i].x,CircleNormal.Data[i].y,CircleNormal.Data[i].z,NormalArray));
      IndexArray.Add(AddVertex3_3(BottomCirclePoint.Data[i+1].x,BottomCirclePoint.Data[i+1].y,BottomCirclePoint.Data[i+1].z,
       CircleNormal.Data[i+1].x,CircleNormal.Data[i+1].y,CircleNormal.Data[i+1].z,NormalArray));
      IndexArray.Add(AddVertex3_3(0,0,-Length/2,0,0,-1,NormalArray));
      IndexArray.Add(AddVertex3_3(TopCirclePoint.Data[i].x,TopCirclePoint.Data[i].y,TopCirclePoint.Data[i].z,
       0,0,-1,NormalArray));
      IndexArray.Add(AddVertex3_3(TopCirclePoint.Data[i+1].x,TopCirclePoint.Data[i+1].y,TopCirclePoint.Data[i+1].z,
       0,0,-1,NormalArray));
      IndexArray.Add(AddVertex3_3(BottomCirclePoint.Data[i].x,BottomCirclePoint.Data[i].y,BottomCirclePoint.Data[i].z,
       0,0,1,NormalArray));
      IndexArray.Add(AddVertex3_3(0,0,length/2,0,0,1,NormalArray));
      IndexArray.Add(AddVertex3_3(BottomCirclePoint.Data[i+1].x,BottomCirclePoint.Data[i+1].y,BottomCirclePoint.Data[i+1].z,
       0,0,1,NormalArray));
    end;
  end;
  //
  if Radius1=0 then begin
    Apex.x:=0;
    Apex.y:=0;
    Apex.z:=-length/2;
    for i:=0 to n-1 do begin
      v1:=Apex-BottomCirclePoint.Data[i];
      v2:=Apex-BottomCirclePoint.Data[i+1];
      Normal.Add(CrossProduct(v1,v2));
      Normalize(Normal.Data[i]);
    end;
    Normal.Add(Normal.Data[0]);
    CircleNormal.Add(0,0,0); //dummy
    for i:=1 to n do begin
      CircleNormal.Add(Normal.Data[i]+Normal.Data[i-1]);
      Normalize(CircleNormal.Data[i]);
    end;
    CircleNormal.Data[0]:=CircleNormal.Data[n];
    for i:=0 to n-1 do begin
      ApexNormal.Add(CircleNormal.Data[i]+CircleNormal.Data[i+1]);
      Normalize(ApexNormal.Data[i]);
    end;
    for i:=0 to n-1 do begin
      IndexArray.Add(AddVertex3_3(Apex.x,Apex.y,Apex.z,
       ApexNormal.Data[i].x,ApexNormal.Data[i].y,ApexNormal.Data[i].z,NormalArray));
      IndexArray.Add(AddVertex3_3(BottomCirclePoint.Data[i].x,BottomCirclePoint.Data[i].y,BottomCirclePoint.Data[i].z,
       CircleNormal.Data[i].x,CircleNormal.Data[i].y,CircleNormal.Data[i].z,NormalArray));
      IndexArray.Add(AddVertex3_3(BottomCirclePoint.Data[i+1].x,BottomCirclePoint.Data[i+1].y,BottomCirclePoint.Data[i+1].z,
       CircleNormal.Data[i+1].x,CircleNormal.Data[i+1].y,CircleNormal.Data[i+1].z,NormalArray));
      IndexArray.Add(AddVertex3_3(BottomCirclePoint.Data[i].x,BottomCirclePoint.Data[i].y,BottomCirclePoint.Data[i].z,
       0,0,1,NormalArray));
      IndexArray.Add(AddVertex3_3(0,0,length/2,0,0,1,NormalArray));
      IndexArray.Add(AddVertex3_3(BottomCirclePoint.Data[i+1].x,BottomCirclePoint.Data[i+1].y,BottomCirclePoint.Data[i+1].z,
       0,0,1,NormalArray));
    end;
  end;
  //
  if Radius2=0 then begin
    Apex.x:=0;
    Apex.y:=0;
    Apex.z:=length/2;
    for i:=0 to n-1 do begin
      v1:=Apex-TopCirclePoint.Data[i+1];
      v2:=Apex-TopCirclePoint.Data[i];
      Normal.Add(CrossProduct(v1,v2));
      Normalize(Normal.Data[i]);
    end;
    Normal.Add(Normal.Data[0]);
    CircleNormal.Add(0,0,0); //dummy
    for i:=1 to n do begin
      CircleNormal.Add(Normal.Data[i]+Normal.Data[i-1]);
      Normalize(CircleNormal.Data[i]);
    end;
    CircleNormal.Data[0]:=CircleNormal.Data[n];
    for i:=0 to n-1 do begin
      ApexNormal.Add(CircleNormal.Data[i]+CircleNormal.Data[i+1]);
      Normalize(ApexNormal.Data[i]);
    end;
    for i:=0 to n-1 do begin
      IndexArray.Add(AddVertex3_3(TopCirclePoint.Data[i].x,TopCirclePoint.Data[i].y,TopCirclePoint.Data[i].z,
       CircleNormal.Data[i].x,CircleNormal.Data[i].y,CircleNormal.Data[i].z,NormalArray));
      IndexArray.Add(AddVertex3_3(Apex.x,Apex.y,Apex.z,
       ApexNormal.Data[i].x,ApexNormal.Data[i].y,ApexNormal.Data[i].z,NormalArray));
      IndexArray.Add(AddVertex3_3(TopCirclePoint.Data[i+1].x,TopCirclePoint.Data[i+1].y,TopCirclePoint.Data[i+1].z,
       CircleNormal.Data[i+1].x,CircleNormal.Data[i+1].y,CircleNormal.Data[i+1].z,NormalArray));
      IndexArray.Add(AddVertex3_3(0,0,-Length/2,0,0,-1,NormalArray));
      IndexArray.Add(AddVertex3_3(TopCirclePoint.Data[i].x,TopCirclePoint.Data[i].y,TopCirclePoint.Data[i].z,
       0,0,-1,NormalArray));
      IndexArray.Add(AddVertex3_3(TopCirclePoint.Data[i+1].x,TopCirclePoint.Data[i+1].y,TopCirclePoint.Data[i+1].z,
       0,0,-1,NormalArray));
    end;
  end;
  //
  //writeln('Calculating bounding sphere');
  minp:=VertexArray.Data[IndexArray.Data[0]];
  maxp:=minp;
  //writeln('Count: ',IndexArray.Length);
  for i:=1 to IndexArray.Length-1 do begin
    // min
    if VertexArray.Data[IndexArray.Data[i]].x<minp.x then
      minp.x:=VertexArray.Data[IndexArray.Data[i]].x;
    if VertexArray.Data[IndexArray.Data[i]].y<minp.y then
      minp.y:=VertexArray.Data[IndexArray.Data[i]].y;
    if VertexArray.Data[IndexArray.Data[i]].z<minp.z then
      minp.z:=VertexArray.Data[IndexArray.Data[i]].z;
    // max
    if VertexArray.Data[IndexArray.Data[i]].x>maxp.x then
      maxp.x:=VertexArray.Data[IndexArray.Data[i]].x;
    if VertexArray.Data[IndexArray.Data[i]].y>maxp.y then
      maxp.y:=VertexArray.Data[IndexArray.Data[i]].y;
    if VertexArray.Data[IndexArray.Data[i]].z>maxp.z then
      maxp.z:=VertexArray.Data[IndexArray.Data[i]].z;
  end;
  BS.Center:=(maxp+minp)/2;
  BoundingBox:=maxp-BS.Center;
  //
  for i:=0 to IndexArray.Length-1 do begin
    distance:=vLength(VertexArray.Data[IndexArray.Data[i]]-BS.Center);
    if distance>BS.Radius then begin
      BS.Radius:=distance;
    end;
  end;
  //writeln(VertexArray.Length div 3,' unique vertices created');
  //writeln('BSCenter: ',BS.Center.x,',',BS.Center.y,',',BS.Center.z,',');
  //writeln('BSRadius: ',BS.Radius);
  TopCirclePoint.Free;
  BottomCirclePoint.Free;
  CircleNormal.Free;
  ApexNormal.Free;
  Normal.Free;
  FCount:=IndexArray.Length;
  BuildGeometryInfo;
  CreateBuffers;
end;


procedure TAsmMesh.CreateCoordinateSystem(Size: GLfloat; full: boolean);
begin
  if FCount>0 then ClearArrays;
  Primitive:=GL_LINES;
  BS.Radius:=sqrt(2*(Size/2)*(Size/2));
  BoundingBox.X:=Size/2;
  BoundingBox.Y:=Size/2;
  BoundingBox.Z:=0.01;
  if full then begin
    IndexArray.Add(AddVertex3_3(-Size/2,0,0,1,0,0,ColorArray));
    IndexArray.Add(AddVertex3_3(Size/2,0,0,1,0,0,ColorArray));
    IndexArray.Add(AddVertex3_3(0,-Size/2,0,0,1,0,ColorArray));
    IndexArray.Add(AddVertex3_3(0,Size/2,0,0,1,0,ColorArray));
    IndexArray.Add(AddVertex3_3(0,0,-Size/2,0,0,1,ColorArray));
    IndexArray.Add(AddVertex3_3(0,0,Size/2,0,0,1,ColorArray));
  end else begin
    IndexArray.Add(AddVertex3_3(0,0,0,1,0,0,ColorArray));
    IndexArray.Add(AddVertex3_3(Size,0,0,1,0,0,ColorArray));
    IndexArray.Add(AddVertex3_3(0,0,0,0,1,0,ColorArray));
    IndexArray.Add(AddVertex3_3(0,Size,0,0,1,0,ColorArray));
    IndexArray.Add(AddVertex3_3(0,0,0,0,0,1,ColorArray));
    IndexArray.Add(AddVertex3_3(0,0,Size,0,0,1,ColorArray));
  end;
  FCount:=IndexArray.Length;
  BuildGeometryInfo;
  CreateBuffers;
end;

// loads a Wavefront .obj file
// not very robust at the moment
function TAsmMesh.LoadMeshFromObjFile(const Filename: string): boolean;
var
  Data: TStringList; ActualLine: string; start,i, j, k, l, VertexData, NormalData,
  TextureData, FaceData, VDAcnt, TDAcnt, NDAcnt, Groups: Integer; distance, lx,
  ly, lz, l1x, l1y, l1z, l2x, l2y: GLFloat; VD, ND, TD, FD, VIA, NIA, TIA:
   ArrayOfInteger; VDA, NDA, TDA: ArrayOfGLFloat; spaces, tris, quads: integer;
  minp, maxp: TVector3;
begin
  if FCount>0 then ClearArrays;
  VertexData:=0; NormalData:=0; TextureData:=0; FaceData:=0;
  VDAcnt:=0; TDAcnt:=0; NDAcnt:=0; Groups:=0;
  VD.Data:=nil; ND.Data:=nil; TD.Data:=nil; FD.Data:=nil;
  VIA.Data:=nil; NIA.Data:=nil; TIA.Data:=nil;
  VDA.Data:=nil; NDA.Data:=nil; TDA.Data:=nil;
  Data:=TStringList.Create;
  writeln('Load from file...');
  Data.LoadFromFile(Filename);
  writeln('loading complete');
  // first run information gatherering
  for i:=0 to Data.Count-1 do if length(Data[i])>0 then
    if Data[i][1] in ['v','f','g'] then
      if Data[i][1]='v' then
        case Data[i][2] of
          ' ': begin
                 inc(VertexData);
                 VD.Add(i);
               end;
          'n': begin
                 inc(NormalData);
                 ND.Add(i);
               end;
          't': begin
                 inc(TextureData);
                 TD.Add(i);
               end;
        end
      else
        case Data[i][1] of
          'f': begin
                 inc(FaceData);
                 FD.Add(i);
               end;
          'g': begin
                 inc(Groups);
               end;
        end;
  //
  writeln(VertexData, ' Vertices');
  writeln(TextureData, ' Texture Coords');
  writeln(NormalData, ' Normals');
  writeln(FaceData, ' Faces');
  writeln(Groups,' Groups');
  if Groups>1 then begin
    writeln('Either this file contains multiple models or the model consists of multiple parts');
    //writeln('Since grouping is not yet supported model will not be loaded');
    writeln('model may not work correctly');
    //exit;
  end;
  writeln('check primitive');
  // check primitive
  tris:=0; quads:=0;
  for i:=0 to FaceData-1 do begin
    spaces:=0;
    ActualLine:=Data[FD.Data[i]];
    for j:=3 to length(ActualLine)-1 do
      if (ActualLine[j-1]<>' ') and (ActualLine[j]=' ')
       and (ActualLine[j+1]<>' ') then inc(spaces);
    if spaces=2 then
      inc(tris)
    else
      if spaces=3 then
        inc(quads)
      else begin
        ShowMessage('only tris and quads supported '+IntToStr(spaces));
        writeln(ActualLine);
        exit;
      end;
  end;
  writeln('Tris: ',tris);
  writeln('Quads: ',quads);
  if (tris<>0) and (quads=0) then
    Primitive:=GL_TRIANGLES
  else
    if (tris=0) and (quads<>0) then
      Primitive:=GL_QUADS
    else
      ShowMessage('Mixed primitives not yet supported');
  // second run to actually load the data
  // load verticies
  for i:=0 to VertexData-1 do begin
    ActualLine:=Data[VD.Data[i]];
    start:=3;
    while ActualLine[start]=' ' do inc(start);
    for j:=start to length(ActualLine) do
      if (ActualLine[j]=' ') or (j=length(ActualLine)) then begin
        inc(VDAcnt);
        if ActualLine[j]=' ' then begin
          VDA.Add(GLfloat(StrToFloat(copy(ActualLine,start,j-start))));
          start:=j+1;
        end else
          VDA.Add(GLfloat(StrToFloat(copy(ActualLine,start,j-start+1))));
      end;
  end;
  // load normals
  for i:=0 to NormalData-1 do begin
    ActualLine:=Data[ND.Data[i]];
    start:=4;
    while ActualLine[start]=' ' do inc(start);
    for j:=start to length(ActualLine) do
      if (ActualLine[j]=' ') or (j=length(ActualLine)) then begin
        inc(NDAcnt);
        if ActualLine[j]=' ' then begin
          NDA.Add(GLfloat(StrToFloat(copy(ActualLine,start,j-start))));
          start:=j+1;
        end else
          NDA.Add(GLfloat(StrToFloat(copy(ActualLine,start,j-start+1))));
      end;
  end;
  // load texture coords
  for i:=0 to TextureData-1 do begin
    ActualLine:=Data[TD.Data[i]];
    start:=4;
    while ActualLine[start]=' ' do inc(start);
    for j:=start to length(ActualLine) do
      if (ActualLine[j]=' ') or (j=length(ActualLine)) then begin
        inc(TDAcnt);
        if ActualLine[j]=' ' then begin
          TDA.Add(GLfloat(StrToFloat(copy(ActualLine,start,j-start))));
          start:=j+1;
        end else if j=length(ActualLine) then begin
          TDA.Add(GLfloat(StrToFloat(copy(ActualLine,start,j-start+1))));
        end;
      end;
  end;
  // create indexed arrays from face information
  writeln('Creating indexed arrays from face information');
  if VertexData<>0 then begin
    for i:=0 to FaceData-1 do begin
      // only vertices
      if (NormalData=0) and (TextureData=0) then begin
        ActualLine:=Data[FD.Data[i]];
        start:=3;
        for j:=3 to length(ActualLine) do
          if (ActualLine[j]=' ') or (j=length(ActualLine)) then begin
            inc(Fcount);
            if (ActualLine[j]=' ') then begin
              VIA.Add(StrToInt(copy(ActualLine,start,j-start)));
              start:=j+1;
            end else
              VIA.Add(StrToInt(copy(ActualLine,start,j-start+1)));
          end;
      end;
      // with normals
      if (NormalData<>0) and (TextureData=0) then begin
        ActualLine:=Data[FD.Data[i]];
        start:=3;
        for j:=3 to length(ActualLine) do
          if (ActualLine[j]=' ') or (j=length(ActualLine)) then begin
            inc(Fcount);
            if (ActualLine[j]=' ') then begin
              k:=start-1;
              while ActualLine[k]<>'/' do inc(k);
              VIA.Add(StrToInt(copy(ActualLine,start,k-start)));
              NIA.Add(StrToInt(copy(ActualLine,k+2,j-(k+2))));
              start:=j+1;
            end else begin
              k:=start-1;
              while ActualLine[k]<>'/' do inc(k);
              VIA.Add(StrToInt(copy(ActualLine,start,k-start)));
              NIA.Add(StrToInt(copy(ActualLine,k+2,j-(k+1))));
            end;
          end;
      end;
      // with texcoords
      if (NormalData=0) and (TextureData<>0) then begin
        ActualLine:=Data[FD.Data[i]];
        start:=3;
        for j:=3 to length(ActualLine) do
          if (ActualLine[j]=' ') or (j=length(ActualLine)) then begin
            inc(Fcount);
            if (ActualLine[j]=' ') then begin
              k:=start-1;
              while ActualLine[k]<>'/' do inc(k);
              VIA.Add(StrToInt(copy(ActualLine,start,k-start)));
              TIA.Add(StrToInt(copy(ActualLine,k+1,j-(k+1))));
              start:=j+1;
            end else begin
              k:=start-1;
              while ActualLine[k]<>'/' do inc(k);
              VIA.Add(StrToInt(copy(ActualLine,start,k-start)));
              TIA.Add(StrToInt(copy(ActualLine,k+1,j-k)));
            end;
          end;
      end;
      // with normals and texcoords
      if (NormalData<>0) and (TextureData<>0) then begin
        //
        ActualLine:=Data[FD.Data[i]];
        start:=3;
        for j:=3 to length(ActualLine) do begin
          if (ActualLine[j]=' ') or (j=length(ActualLine)) then begin
            inc(Fcount);
            if (ActualLine[j]=' ') then begin
              k:=start-1;
              while ActualLine[k]<>'/' do inc(k);
              l:=k+1;
              while ActualLine[l]<>'/' do inc(l);
              VIA.Add(StrToInt(copy(ActualLine,start,k-start)));
              TIA.Add(StrToInt(copy(ActualLine,k+1,l-(k+1))));
              NIA.Add(StrToInt(copy(ActualLine,l+1,j-(l+1))));
              start:=j+1;
            end else begin
              k:=start-1;
              while ActualLine[k]<>'/' do inc(k);
              l:=k+1;
              while ActualLine[l]<>'/' do inc(l);
              VIA.Add(StrToInt(copy(ActualLine,start,k-start)));
              TIA.Add(StrToInt(copy(ActualLine,k+1,l-(k+1))));
              NIA.Add(StrToInt(copy(ActualLine,l+1,j-l)));
            end;
          end;
        end;
      end;
    end;
  end;
  // create vbo usable arrays
  writeln('Creating VBO usable arrays...');
  // vertices only
  if (NormalData=0) and (TextureData=0) then
    for i:=0 to count-1 do begin
      lx:=VDA.Data[(VIA.Data[i]-1)*3];
      ly:=VDA.Data[(VIA.Data[i]-1)*3+1];
      lz:=VDA.Data[(VIA.Data[i]-1)*3+2];
      IndexArray.Add(AddVertex3(lx,ly,lz));
    end;
  // with normals
  if (NormalData<>0) and (TextureData=0) then
    for i:=0 to count-1 do begin
      lx:=VDA.Data[(VIA.Data[i]-1)*3];
      ly:=VDA.Data[(VIA.Data[i]-1)*3+1];
      lz:=VDA.Data[(VIA.Data[i]-1)*3+2];
      l1x:=NDA.Data[(NIA.Data[i]-1)*3];
      l1y:=NDA.Data[(NIA.Data[i]-1)*3+1];
      l1z:=NDA.Data[(NIA.Data[i]-1)*3+2];
      IndexArray.Add(AddVertex3_3(lx,ly,lz,l1x,l1y,l1z,NormalArray));
    end;
  // with texcoords
  if (NormalData=0) and (TextureData<>0) then
    for i:=0 to count-1 do begin
      lx:=VDA.Data[(VIA.Data[i]-1)*3];
      ly:=VDA.Data[(VIA.Data[i]-1)*3+1];
      lz:=VDA.Data[(VIA.Data[i]-1)*3+2];
      l1x:=TDA.Data[(TIA.Data[i]-1)*3];
      l1y:=TDA.Data[(TIA.Data[i]-1)*3+1];
      //l1z:=TDA.Data[(TIA.Data[i]-1)*3+2];
      IndexArray.Add(AddVertex3_2(lx,ly,lz,l1x,l1y,TexCoordArray));
    end;
  // with texcoords and normals
  if (NormalData<>0) and (TextureData<>0) then
    for i:=0 to count-1 do begin
      lx:=VDA.Data[(VIA.Data[i]-1)*3];
      ly:=VDA.Data[(VIA.Data[i]-1)*3+1];
      lz:=VDA.Data[(VIA.Data[i]-1)*3+2];
      l1x:=NDA.Data[(NIA.Data[i]-1)*3];
      l1y:=NDA.Data[(NIA.Data[i]-1)*3+1];
      l1z:=NDA.Data[(NIA.Data[i]-1)*3+2];
      l2x:=TDA.Data[(TIA.Data[i]-1)*2];
      l2y:=TDA.Data[(TIA.Data[i]-1)*2+1];
      IndexArray.Add(AddVertex3_3_2(lx,ly,lz,l1x,l1y,l1z,l2x,l2y,NormalArray,
       TexCoordArray));
    end;
  // Calculate bounding sphere
  writeln('Calculating bounding sphere');
  minp:=VertexArray.Data[IndexArray.Data[0]];
  maxp:=minp;
  for i:=1 to count-1 do begin
    // min
    if VertexArray.Data[IndexArray.Data[i]].x<minp.x then
      minp.x:=VertexArray.Data[IndexArray.Data[i]].x;
    if VertexArray.Data[IndexArray.Data[i]].y<minp.y then
      minp.y:=VertexArray.Data[IndexArray.Data[i]].y;
    if VertexArray.Data[IndexArray.Data[i]].z<minp.z then
      minp.z:=VertexArray.Data[IndexArray.Data[i]].z;
    // max
    if VertexArray.Data[IndexArray.Data[i]].x>maxp.x then
      maxp.x:=VertexArray.Data[IndexArray.Data[i]].x;
    if VertexArray.Data[IndexArray.Data[i]].y>maxp.y then
      maxp.y:=VertexArray.Data[IndexArray.Data[i]].y;
    if VertexArray.Data[IndexArray.Data[i]].z>maxp.z then
      maxp.z:=VertexArray.Data[IndexArray.Data[i]].z;
  end;
  BS.Center:=(maxp+minp)/2;
  BoundingBox:=maxp-BS.Center;
  //
  for i:=0 to count-1 do begin
    distance:=vLength(VertexArray.Data[IndexArray.Data[i]]-BS.Center);
    if distance>BS.Radius then begin
      BS.Radius:=distance;
    end;
  end;
  writeln(VertexArray.Length div 3,' unique vertices created');
  writeln('BSCenter: ',BS.Center.x,',',BS.Center.y,',',BS.Center.z,',');
  writeln('BSRadius: ',BS.Radius);
  writeln('Ready');
  //
  vda.free; nda.free; tda.free;
  via.free; nia.free; tia.free;
  fd.free; vd.free; nd.free; td.free;
  Data.Free;
  BuildGeometryInfo;
  CreateBuffers;
  LoadMeshFromObjFile:=true;
end;

procedure TAsmMesh.CreateBuffers;
begin
  if glIsBufferARB(VertexBuffer)=GL_TRUE then glDeleteBuffersARB(1, @VertexBuffer);
  glGenBuffersARB(1, @VertexBuffer);
  glBindBufferARB(GL_ARRAY_BUFFER_ARB, VertexBuffer);
  glBufferDataARB(GL_ARRAY_BUFFER_ARB, VertexArray.Length*3*SizeOf(GLfloat), VertexArray.Data, GL_STATIC_DRAW_ARB);
  if NormalArray.length>0 then begin
    if glIsBufferARB(NormalBuffer)=GL_TRUE then glDeleteBuffersARB(1, @NormalBuffer);
    glGenBuffersARB(1, @NormalBuffer);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, NormalBuffer);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, NormalArray.Length*3*SizeOf(GLfloat), NormalArray.Data, GL_STATIC_DRAW_ARB);
  end;
  if ColorArray.length>0 then begin
    if glIsBufferARB(ColorBuffer)=GL_TRUE then glDeleteBuffersARB(1, @ColorBuffer);
    glGenBuffersARB(1, @ColorBuffer);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, ColorBuffer);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, ColorArray.Length*3*SizeOf(GLfloat), ColorArray.Data, GL_STATIC_DRAW_ARB);
  end;
  if TexCoordArray.length>0 then begin
    if glIsBufferARB(TexCoordBuffer)=GL_TRUE then glDeleteBuffersARB(1, @TexCoordBuffer);
    glGenBuffersARB(1, @TexCoordBuffer);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, TexCoordBuffer);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, TexCoordArray.Length*2*SizeOf(GLfloat), TexCoordArray.Data, GL_STATIC_DRAW_ARB);
  end;
  if glIsBufferARB(IndexBuffer)=GL_TRUE then glDeleteBuffersARB(1, @IndexBuffer);
  glGenBuffersARB(1, @IndexBuffer);
  glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, IndexBuffer);
  glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, IndexArray.Length*SizeOf(GLuint), IndexArray.Data, GL_STATIC_DRAW_ARB);
  glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
  glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,0);
  //writeln('Buffer Objects Created');
  //writeln('glGetError: ',glGetError);
end;

// The EdgeList is build by looping through the triangle list and first looking
// for edges where vertex1 < vertex2 and then edges where vertex1 > vertex2.
// This ensures that no edge is dublicated and makes it easy to find edges that
// belong to only one triangle. It is assumed that an edge belongs to max 2 and
// min 1 triangle.

procedure TAsmMesh.CreateEdgeList;
var
  i: Integer;
  Edge: Integer;
  EdgeCount: Integer;
  tmpEdgeList: array of TEdge;
begin
  // slow there must be a better way to build the edgelist
  // workaround until Quad support is dropped
  if (Primitive=GL_TRIANGLES) and (length(TriangleList)>0) then begin
    //writeln('Creating Edge List...');
    SetLength(tmpEdgeList, length(TriangleList)*3);
    Edge:=0;
    for i:=0 to length(TriangleList)-1 do with TriangleList[i] do begin
      tmpEdgeList[Edge].Triangle[1]:=i;
      tmpEdgeList[Edge].Triangle[2]:=-1;
      tmpEdgeList[Edge].Vertex[1]:=p1;
      tmpEdgeList[Edge].Vertex[2]:=p2;
      inc(Edge);
      tmpEdgeList[Edge].Triangle[1]:=i;
      tmpEdgeList[Edge].Triangle[2]:=-1;
      tmpEdgeList[Edge].Vertex[1]:=p2;
      tmpEdgeList[Edge].Vertex[2]:=p3;
      inc(Edge);
      tmpEdgeList[Edge].Triangle[1]:=i;
      tmpEdgeList[Edge].Triangle[2]:=-1;
      tmpEdgeList[Edge].Vertex[1]:=p3;
      tmpEdgeList[Edge].Vertex[2]:=p1;
      inc(Edge);
    end;
    // debuggin
    //writeln('tmpEdge: ', length(tmpEdgeList));
    //
    EdgeListMergeSort(tmpEdgeList);
    SetLength(EdgeList, length(tmpEdgeList));
    EdgeCount:=0;
    for i:=1 to length(tmpEdgeList)-1 do begin
      if (tmpEdgeList[i-1].Vertex[1]=tmpEdgeList[i].Vertex[2])
       and (tmpEdgeList[i-1].Vertex[2]=tmpEdgeList[i].Vertex[1]) then begin
        EdgeList[EdgeCount]:=tmpEdgeList[i-1];
        EdgeList[EdgeCount].Triangle[2]:=tmpEdgeList[i].Triangle[1];
        inc(EdgeCount);
      end else begin
      end;
    end;
    SetLength(EdgeList, EdgeCount);
    // debugging
    //for i:=0 to length(tmpEdgeList)-1 do begin
    //  writeln('Verts: ', tmpEdgeList[i].Vertex[1],' - ',tmpEdgeList[i].Vertex[2]);
    //  writeln('Tris: ', EdgeList[i].Triangle[1],' - ',EdgeList[i].Triangle[2]);
    //end;
    //writeln('Edges: ', Length(EdgeList));
  end;
end;

// This procedure builds an 'index array' that is only based on vertex postions
// and no other vertex attributes unlike OpenGL's 'index array'. This is needed
// to build the TriangleList and EdgeList which only need geometry information.
// For performance reasons a sorted version of IndexArrays is created and the
// GeoIndexArray is build from this sorted array.

procedure TAsmMesh.CreateGeoIndexArray;
  function compare(a,b: TVector3): Boolean;
    function rnd(j,w: single): Boolean;
    begin
      if abs(w-j)<0.00001 then Result:=TRUE else Result:=FALSE;
    end;

    begin
      if (rnd(a.x,b.x)) and (rnd(a.y,b.y)) and (rnd(a.z,b.z)) then Result:=true else Result:=false;
    end;

var i,j: integer; SortedToUnsortedIndex: ArrayOfGLuint;
begin
  // workaround until Quad support is dropped
  if Primitive=GL_TRIANGLES then begin
    //writeln('Creating Geometry Index Array...');
    SortedToUnsortedIndex.Create;
    GeoIndexArray.SetCapacity(IndexArray.Length);
    SortedToUnsortedIndex.SetCapacity(IndexArray.Length);
    for i:=0 to IndexArray.Length-1 do begin
      GeoIndexArray.Add(IndexArray.Data[i]);
      SortedToUnsortedIndex.Add(i);
    end;
    IndexArrayMergeSort(SortedToUnsortedIndex,IndexArray,VertexArray,@CompareVectorDesc);
    // debugging
    //for i:=0 to GeoIndexArray.Length-1 do begin
    //  writeln(GeoIndexArray.Data[i],' - ',SortedToUnsortedIndex.Data[i]);
    //end;
    //
    j:=0;
    for i:=0 to GeoIndexArray.Length-1 do begin
      if VertexArray.Data[IndexArray.Data[SortedToUnsortedIndex.Data[i]]]
        <>VertexArray.Data[IndexArray.Data[SortedToUnsortedIndex.Data[j]]]
      then
        j:=i;
      GeoIndexArray.Data[SortedToUnsortedIndex.Data[i]]:=IndexArray.Data[SortedToUnsortedIndex.Data[j]];
    end;
    {for i := 0 to GeoIndexArray.Length-1 do
      for j := i+1 to GeoIndexArray.Length-1 do
        if compare(VertexArray.Data[GeoIndexArray.Data[i]],VertexArray.Data[GeoIndexArray.Data[j]]) and (GeoIndexArray.Data[i]<>
           GeoIndexArray.Data[j]) then writeln(GeoIndexArray.Data[i],' <> ',
           GeoIndexArray.Data[j]);
    }
    // debugging
    //for i:=0 to GeoIndexArray.Length-1 do begin
    //  writeln(GeoIndexArray.Data[i],' - ',SortedToUnsortedIndex.Data[i]);
    //end;
    //
    SortedToUnsortedIndex.Free;
  end;
end;

procedure TAsmMesh.CreateTriangleList;
var i: integer;
begin
  // workaround until Quad support is dropped
  if Primitive=GL_TRIANGLES then begin
    //writeln('Creating Triangle List...');
    SetLength(TriangleList, ((IndexArray.Length-1) div 3)+1);
    for i:=0 to ((GeoIndexArray.Length-1) div 3) do with TriangleList[i] do begin
      p1:=GeoIndexArray.Data[i*3];
      p2:=GeoIndexArray.Data[i*3+1];
      p3:=GeoIndexArray.Data[i*3+2];
      Plane:=CreatePlaneFromTriangle(VertexArray.Data[p1], VertexArray.Data[p2],
       VertexArray.Data[p3]);
    end;
    // debugging
    //for i:=0 to ((GeoIndexArray.Length-1) div 3) do with TriangleList[i] do begin
    //  writeln('Tri ',i,' - ',p1,',',p2,',',p3);
    //end;
    //writeln('Triangles: ',Length(TriangleList));
  end;
end;

procedure TAsmMesh.CreateTBNMatrixArray;
var i: integer; v0, v1, TriVert0, TriVert1, TriVert2,
    Tangent, Normal, Bitangent: TVector3; Tri0uv, Tri1uv, Tri2uv: TVector2;
    du0, dv0, du1, dv1, denom: GLFloat;
begin
  if TexCoordArray.length<1 then begin writeln('No TexCoords specified'); exit; end;
  // Creating tangent space matricies
  TBNMatrixArray.SetSize(NormalArray.Length div 3);
  for i:=0 to (IndexArray.Length div 3)-1 do begin
    TriVert0:=VertexArray.Data[IndexArray.Data[i*3]];
    Tri0uv:=TexCoordArray.Data[IndexArray.Data[i*3]];
    TriVert1:=VertexArray.Data[IndexArray.Data[i*3+1]];
    Tri1uv:=TexCoordArray.Data[IndexArray.Data[i*3+1]];
    TriVert2:=VertexArray.Data[IndexArray.Data[i*3+2]];
    Tri2uv:=TexCoordArray.Data[IndexArray.Data[i*3+2]];
    // first vertex
    v0:=TriVert1-TriVert0;
    du0:=Tri1uv.x-Tri0uv.x;
    dv0:=Tri1uv.y-Tri0uv.y;
    v1:=TriVert2-TriVert0;
    du1:=Tri2uv.x-Tri0uv.x;
    dv1:=Tri2uv.y-Tri0uv.y;
    denom:=(du1*dv0-du0*dv1);
    if denom=0 then begin
      TBNMatrixArray.Data[IndexArray.Data[i*3]][0]:=1;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][1]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][2]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][3]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][4]:=1;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][5]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][6]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][7]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][8]:=1;
    end else begin
      Tangent:=(dv0*v1-dv1*v0)/denom;
      Normalize(Tangent);
      //
      Normal:=NormalArray.Data[IndexArray.Data[i*3]];
      //
      Bitangent:=CrossProduct(Normal, Tangent);
      Normalize(Bitangent);
      //
      TBNMatrixArray.Data[IndexArray.Data[i*3]][0]:=Tangent.x;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][1]:=Tangent.y;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][2]:=Tangent.z;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][3]:=Bitangent.x;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][4]:=Bitangent.y;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][5]:=Bitangent.z;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][6]:=Normal.x;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][7]:=Normal.y;
      TBNMatrixArray.Data[IndexArray.Data[i*3]][8]:=Normal.z;
    end;
    // second vertex
    v0:=TriVert2-TriVert1;
    du0:=Tri2uv.x-Tri1uv.x;
    dv0:=Tri2uv.y-Tri1uv.y;
    v1:=TriVert0-TriVert1;
    du1:=Tri0uv.x-Tri1uv.x;
    dv1:=Tri0uv.y-Tri1uv.y;
    denom:=(du1*dv0-du0*dv1);
    if denom=0 then begin
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][0]:=1;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][1]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][2]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][3]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][4]:=1;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][5]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][6]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][7]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][8]:=1;
    end else begin
      Tangent:=(dv0*v1-dv1*v0)/denom;
      Normalize(Tangent);
      //
      Normal:=NormalArray.Data[IndexArray.Data[i*3+1]];
      //
      Bitangent:=CrossProduct(Normal, Tangent);
      Normalize(Bitangent);
      //
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][0]:=Tangent.x;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][1]:=Tangent.y;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][2]:=Tangent.z;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][3]:=Bitangent.x;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][4]:=Bitangent.y;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][5]:=Bitangent.z;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][6]:=Normal.x;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][7]:=Normal.y;
      TBNMatrixArray.Data[IndexArray.Data[i*3+1]][8]:=Normal.z;
    end;
    // third vertex
    v0:=TriVert0-TriVert2;
    du0:=Tri0uv.x-Tri2uv.x;
    dv0:=Tri0uv.y-Tri2uv.y;
    v1:=TriVert1-TriVert2;
    du1:=Tri1uv.x-Tri2uv.x;
    dv1:=Tri1uv.y-Tri2uv.y;
    denom:=(du1*dv0-du0*dv1);
    if denom=0 then begin
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][0]:=1;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][1]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][2]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][3]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][4]:=1;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][5]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][6]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][7]:=0;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][8]:=1;
    end else begin
      Tangent:=(dv0*v1-dv1*v0)/denom;
      Normalize(Tangent);
      //
      Normal:=NormalArray.Data[IndexArray.Data[i*3+2]];
      //
      Bitangent:=CrossProduct(Normal, Tangent);
      Normalize(Bitangent);
      //
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][0]:=Tangent.x;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][1]:=Tangent.y;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][2]:=Tangent.z;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][3]:=Bitangent.x;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][4]:=Bitangent.y;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][5]:=Bitangent.z;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][6]:=Normal.x;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][7]:=Normal.y;
      TBNMatrixArray.Data[IndexArray.Data[i*3+2]][8]:=Normal.z;
    end;
  end;
end;

constructor TAsmMesh.Create;
begin
  // Init AVL-Tree
  inherited Create;
end;

destructor TAsmMesh.Destroy;
begin
  ClearArrays;
  inherited Destroy;
end;

{ TAsmDrawable }

procedure TAsmDrawable.SetColor(red, green, blue, alpha: GLFloat);
begin
  fColor[0]:=red;
  fColor[1]:=green;
  fColor[2]:=blue;
  fColor[3]:=alpha;
end;

procedure TAsmDrawable.SetColor(iColor: glRGBAColor);
begin
  fColor:=iColor;
end;

procedure TAsmDrawable.SetSpecularColor(red, green, blue: GLfloat);
begin
  fSpecColor[0]:=red;
  fSpecColor[1]:=green;
  fSpecColor[2]:=blue;
  fSpecColor[3]:=1;
end;

procedure TAsmDrawable.SetSpecularColor(iColor: glRGBColor);
var RGBAColor: glRGBAColor;
begin
  RGBAColor[0]:=iColor[0];
  RGBAColor[1]:=iColor[1];
  RGBAColor[2]:=iColor[2];
  RGBAColor[3]:=1;
  fSpecColor:=RGBAColor;
end;

{function TAsmObject.GetInvGLMatrix: TMatrix4x4;
var Res, AsmMatrix, ScaleMatrix: TMatrix4x4;
begin
  ScaleMatrix[0,0]:=fScale.x; ScaleMatrix[4]:=0; ScaleMatrix[8]:=0;  ScaleMatrix[12]:=0;
  ScaleMatrix[0,1]:=0; ScaleMatrix[5]:=fScale.y; ScaleMatrix[9]:=0;  ScaleMatrix[13]:=0;
  ScaleMatrix[0,2]:=0; ScaleMatrix[6]:=0; ScaleMatrix[10]:=fScale.z; ScaleMatrix[14]:=0;
  ScaleMatrix[0,3]:=0; ScaleMatrix[7]:=0; ScaleMatrix[11]:=0; ScaleMatrix[15]:=1;
  AsmMatrix:=MultglMatrix(ScaleMatrix,fMatrix);
  Res[0]:=fMatrix[0]; Res[1]:=AsmMatrix[4]; Res[2]:=AsmMatrix[8]; Res[3]:=0;
  Res[4]:=AsmMatrix[1]; Res[5]:=AsmMatrix[5]; Res[6]:=AsmMatrix[9]; Res[7]:=0;
  Res[8]:=AsmMatrix[2]; Res[9]:=AsmMatrix[6]; Res[10]:=AsmMatrix[10]; Res[11]:=0;
  Res[12]:=-(AsmMatrix[0]*(AsmMatrix[12]+fCoR.x)+AsmMatrix[1]*(AsmMatrix[13]+fCoR.y)+AsmMatrix[2]*(AsmMatrix[14]+fCoR.z));
  Res[13]:=-(AsmMatrix[4]*(AsmMatrix[12]+fCoR.x)+AsmMatrix[5]*(AsmMatrix[13]+fCoR.y)+AsmMatrix[6]*(AsmMatrix[14]+fCoR.z));
  Res[14]:=-(AsmMatrix[8]*(AsmMatrix[12]+fCoR.x)+AsmMatrix[9]*(AsmMatrix[13]+fCoR.y)+AsmMatrix[10]*(AsmMatrix[14]+fCoR.z));
  Res[15]:=1;
  Result:=Res;
end;}

{ TAsmObject }
procedure TAsmObject.SetMesh(const AValue: TAsmMesh);
begin
  if fMesh=AValue then exit;
  fMesh:=AValue;
  if fMesh<>nil then begin // Mattias
    fBS.Radius:=fMesh.BS.Radius*fmaxScale;
    fBS.Center.x:=fMesh.BS.Center.x*fScale.x;
    fBS.Center.y:=fMesh.BS.Center.y*fScale.y;
    fBS.Center.z:=fMesh.BS.Center.z*fScale.z;
  end;
end;

procedure TAsmObject.UpdateMatrix;
begin
  fMatrix[3,0]:=fPosition.x-fCoR.x;
  fMatrix[3,1]:=fPosition.y-fCoR.y;
  fMatrix[3,2]:=fPosition.z-fCoR.z;
end;

function TAsmObject.GetinvMatrix: TMatrix4x4;
var Res, M: TMatrix4x4;
begin
  m:=fMatrix;
  Res[0,0]:=m[0,0]; Res[0,1]:=m[1,0]; Res[0,2]:=m[2,0]; Res[0,3]:=0;
  Res[1,0]:=m[0,1]; Res[1,1]:=m[1,1]; Res[1,2]:=m[2,1]; Res[1,3]:=0;
  Res[2,0]:=m[0,2]; Res[2,1]:=m[1,2]; Res[2,2]:=m[2,2]; Res[2,3]:=0;
  Res[3,0]:=-(m[0,0]*(m[3,0]+fCoR.x)+m[0,1]*(m[3,1]+fCoR.y)+m[0,2]*(m[3,2]+fCoR.z));
  Res[3,1]:=-(m[1,0]*(m[3,0]+fCoR.x)+m[1,1]*(m[3,1]+fCoR.y)+m[1,2]*(m[3,2]+fCoR.z));
  Res[3,2]:=-(m[2,0]*(m[3,0]+fCoR.x)+m[2,1]*(m[3,1]+fCoR.y)+m[2,2]*(m[3,2]+fCoR.z));
  Res[3,3]:=1;
  Result:=Res;
end;

// Finds the possible silhoette edges for a given light.
// If an edge belongs to one frontfacing and one backfacing triangle it is
// a possible silhouette edge.
procedure TAsmObject.BuildSilhouette(const Light: TAsmLight);
var
  l: TVector4;
  i: Integer;
  SilhouetteEdge: TSilhouetteEdge;
  Face0, Face1: GLFloat;
  SilhouetteCount: Integer;
begin
  SilhouetteCount:=0;
  // transform light to object space
  l:=iTransformVector4(Light.Position4, invMatrix);
  //l:=Light.Position4;
  //writeln('S-Edges: ', Length(Mesh.EdgeList));
  for i:=0 to Length(Mesh.EdgeList)-1 do with Mesh.EdgeList[i] do begin
    if (Triangle[1]<>-1) and (Triangle[2]<>-1) then begin
      // Check for backfacing triangle.
      with Mesh.TriangleList[Triangle[1]].Plane do
        Face0:=Normal.x*l.x+Normal.y*l.y+Normal.z*l.z+Distance*l.w;
      with Mesh.TriangleList[Triangle[2]].Plane do
        Face1:=Normal.x*l.x+Normal.y*l.y+Normal.z*l.z+Distance*l.w;
      if (Face0<=0) xor (Face1<=0) then begin
        if Face0>0 then begin
          SilhouetteEdge.v1:=Vertex[1];
          SilhouetteEdge.v2:=Vertex[2];
        end else begin
          SilhouetteEdge.v1:=Vertex[2];
          SilhouetteEdge.v2:=Vertex[1];
        end;
        if SilhouetteCount>=Length(Silhouette) then begin
          SetLength(Silhouette,Length(Silhouette)+50);
        end;
        Silhouette[SilhouetteCount]:=SilhouetteEdge;
        inc(SilhouetteCount);
      end;
    end else begin
      // Model not closed.
      writeln('Model not closed');
    end;
  end;
  SetLength(Silhouette,SilhouetteCount);
  //writeln(SilhouetteCount,' possible silhouette edges');
end;

procedure TAsmObject.BuildShadowVolume(const Light: TAsmLight);
var
  i: Integer;
  index: Integer;
  l: TVector4;
begin
  BuildSilhouette(Light);
  SetLength(ShadowVolumeVA,Length(Silhouette)*4);
  l:=iTransformVector4(Light.Position4, invMatrix);
  // extrude silhouette uncapped
  if Length(Silhouette)>0 then
    for i:=0 to Length(Silhouette)-1 do with Mesh do begin
      index:=i*4;
      ShadowVolumeVA[index].x:=VertexArray.Data[Silhouette[i].v1].x;
      ShadowVolumeVA[index].y:=VertexArray.Data[Silhouette[i].v1].y;
      ShadowVolumeVA[index].z:=VertexArray.Data[Silhouette[i].v1].z;
      ShadowVolumeVA[index].w:=1;
      ShadowVolumeVA[index+1].x:=VertexArray.Data[Silhouette[i].v1].x*l.w-l.x;
      ShadowVolumeVA[index+1].y:=VertexArray.Data[Silhouette[i].v1].y*l.w-l.y;
      ShadowVolumeVA[index+1].z:=VertexArray.Data[Silhouette[i].v1].z*l.w-l.z;
      ShadowVolumeVA[index+1].w:=0;
      ShadowVolumeVA[index+2].x:=VertexArray.Data[Silhouette[i].v2].x*l.w-l.x;
      ShadowVolumeVA[index+2].y:=VertexArray.Data[Silhouette[i].v2].y*l.w-l.y;
      ShadowVolumeVA[index+2].z:=VertexArray.Data[Silhouette[i].v2].z*l.w-l.z;
      ShadowVolumeVA[index+2].w:=0;
      ShadowVolumeVA[index+3].x:=VertexArray.Data[Silhouette[i].v2].x;
      ShadowVolumeVA[index+3].y:=VertexArray.Data[Silhouette[i].v2].y;
      ShadowVolumeVA[index+3].z:=VertexArray.Data[Silhouette[i].v2].z;
      ShadowVolumeVA[index+3].w:=1;
    end;
end;

procedure TAsmObject.DrawShadowVolume;
begin

end;

constructor TAsmObject.Create(AnOwner: TComponent);
begin
  inherited Create(AnOwner);
  ResetScale;
  ResetMatrix;
end;

destructor TAsmObject.Destroy;
begin
  Texture.Free;
  inherited Destroy;
end;

function TAsmObject.GetEulerAngles: TVector3;
var Pitch, Yaw, Roll, c, tx, ty: GLfloat;
begin
  Pitch:=-arcsin(fMatrix[2,0]);
  c:=cos(Pitch);
  Pitch:=GLfloat(RadToDeg(Pitch));
  if abs(c)>0.005 then begin
    tx:=fMatrix[2,2]/c;
    ty:=-fMatrix[2,1]/c;
    Yaw:=GLfloat(RadToDeg(arctan2(ty,tx)));
    tx:=fMatrix[0,0]/c;
    ty:=-fMatrix[1,0]/c;
    Roll:=GLfloat(RadToDeg(arctan2(ty,tx)));
  end else begin
    Yaw:=0;
    tx:=fMatrix[1,1];
    ty:=fMatrix[0,1];
    Roll:=GLfloat(RadToDeg(arctan2(ty,tx)));
  end;
  if Pitch<=-180 then Pitch:=Pitch+360;
  if Yaw<=-180 then Yaw:=Yaw+360;
  if Roll<=-180 then Roll:=Roll+360;
  //
  GetEulerAngles.y:=Pitch;
  GetEulerAngles.x:=Yaw;
  GetEulerAngles.z:=Roll;
end;

procedure TAsmObject.SetEulerAngles(Angle: TVector3);
var A, B, C, D, E, F, AD, BD: GLfloat;
begin
  Angle.x:=GLfloat(DegToRad(Angle.x));
  Angle.y:=GLfloat(DegToRad(Angle.y));
  Angle.z:=GLfloat(DegToRad(Angle.z));
  A:=cos(Angle.x);
  B:=sin(Angle.x);
  C:=cos(Angle.y);
  D:=sin(Angle.y);
  E:=cos(Angle.z);
  F:=sin(Angle.z);
  AD:=A*D;
  BD:=B*D;
  //
  Matrix[0,0]:=C*E;
  Matrix[0,1]:=-BD*E+A*F;
  Matrix[0,2]:=AD*E+B*F;
  Matrix[1,0]:=-C*F;
  Matrix[1,1]:=BD*F+A*E;
  Matrix[1,2]:=-AD*F+B*E;
  Matrix[2,0]:=-D;
  Matrix[2,1]:=-B*C;
  Matrix[2,2]:=A*C;
  Matrix[0,3]:=0; Matrix[1,3]:=0; Matrix[2,3]:=0;
  Matrix[3,0]:=0; Matrix[3,1]:=0; Matrix[3,2]:=0;
  Matrix[3,3]:=1;
  //
  UpdateMatrix;
end;

procedure TAsmObject.SetScale(const iScale: TVector3);
begin
  fScale:=iScale;
  if fScale.x>fScale.y then
    fmaxScale:=fScale.x
  else
    if fScale.y>fScale.z then
      fmaxScale:=fScale.y
    else
      fmaxScale:=fScale.z;
  if fMesh<>nil then begin
    fBS.Radius:=Mesh.BS.Radius*fmaxScale;
    fBS.Center.x:=Mesh.BS.Center.x*fScale.x;
    fBS.Center.y:=Mesh.BS.Center.y*fScale.y;
    fBS.Center.z:=Mesh.BS.Center.z*fScale.z;
  end;
end;

procedure TAsmObject.SetScale(ScaleX, ScaleY, ScaleZ: GLfloat);
begin
  fScale.x:=ScaleX;
  fScale.y:=ScaleY;
  fScale.z:=ScaleZ;
  if ScaleX>ScaleY then
    fmaxScale:=ScaleX
  else
    if ScaleY>ScaleZ then
      fmaxScale:=ScaleY
    else
      fmaxScale:=ScaleZ;
  if fMesh<>nil then begin
    fBS.Radius:=Mesh.BS.Radius*fmaxScale;
    fBS.Center.x:=Mesh.BS.Center.x*fScale.x;
    fBS.Center.y:=Mesh.BS.Center.y*fScale.y;
    fBS.Center.z:=Mesh.BS.Center.z*fScale.z;
  end;
end;

procedure TAsmObject.ResetMatrix;
begin
  fMatrix[0,0]:=1; fMatrix[1,0]:=0; fMatrix[2,0]:=0; fMatrix[3,0]:=0;
  fMatrix[0,1]:=0; fMatrix[1,1]:=1; fMatrix[2,1]:=0; fMatrix[3,1]:=0;
  fMatrix[0,2]:=0; fMatrix[1,2]:=0; fMatrix[2,2]:=1; fMatrix[3,2]:=0;
  fMatrix[0,3]:=0; fMatrix[1,3]:=0; fMatrix[2,3]:=0; fMatrix[3,3]:=1;
end;

procedure TAsmObject.ResetRotation;
begin
  fMatrix[0,0]:=1; fMatrix[1,0]:=0; fMatrix[2,0]:=0;
  fMatrix[0,1]:=0; fMatrix[1,1]:=1; fMatrix[2,1]:=0;
  fMatrix[0,2]:=0; fMatrix[1,2]:=0; fMatrix[2,2]:=1;
  fMatrix[0,3]:=0; fMatrix[1,3]:=0; fMatrix[2,3]:=0; fMatrix[3,3]:=1;
end;

procedure TAsmObject.ResetScale;
begin
  fScale.x:=1;
  fScale.y:=1;
  fScale.z:=1;
  fmaxScale:=1;
end;

procedure TAsmObject.ResetTranslation;
begin
  fPosition.x:=0;
  fPosition.y:=0;
  fPosition.z:=0;
  UpdateMatrix;
end;

procedure TAsmObject.SetCoR(const Pos: TVector3);
begin
  if fCoR<>Pos then begin
    fCoR:=Pos;
    UpdateMatrix;
  end;
end;

procedure TAsmObject.SetCoR(x, y, z: GLFloat);
begin
  if fCoR.x<>x then fCoR.x:=x;
  if fCoR.y<>y then fCoR.y:=y;
  if fCoR.z<>z then fCoR.z:=z;
  UpdateMatrix;
end;

procedure TAsmObject.SetPosition(const Pos: TVector3);
begin
  fPosition:=Pos;
  UpdateMatrix;
end;

procedure TAsmObject.SetPosition(x, y, z: GLFloat);
begin
  fPosition.x:=x;
  fPosition.y:=y;
  fPosition.z:=z;
  UpdateMatrix;
end;

procedure TAsmObject.Translate(const Pos: TVector3);
begin
  fPosition:=fPosition+Pos;
  UpdateMatrix;
end;

procedure TAsmObject.Translate(ax, ay, az: GLFloat);
begin
  fPosition.x:=fPosition.x+ax;
  fPosition.y:=fPosition.y+ay;
  fPosition.z:=fPosition.z+az;
  UpdateMatrix;
end;

procedure TAsmObject.TranslateCoR(ax, ay, az: GLFloat);
begin
  fCoR.x:=fCoR.x+ax;
  fCoR.y:=fCoR.x+ay;
  fCoR.z:=fCoR.x+az;
  UpdateMatrix;
end;

procedure TAsmObject.RotateAboutLocalX(angle: GLFloat);
var c,s: GLFloat; r: TMatrix4x4;
begin
  angle:=GLfloat(degtorad(angle));
  c:=cos(angle);
  s:=sin(angle);
  // create rotation matrix
  r[0,0]:=1; r[1,0]:=0; r[2,0]:= 0; r[3,0]:=0;
  r[0,1]:=0; r[1,1]:=c; r[2,1]:=-s; r[3,1]:=0;
  r[0,2]:=0; r[1,2]:=s; r[2,2]:= c; r[3,2]:=0;
  r[0,3]:=0; r[1,3]:=0; r[2,3]:= 0; r[3,3]:=1;
  fMatrix:=MultglMatrix(fMatrix,r);
end;

procedure TAsmObject.RotateAboutLocalY(angle: GLFloat);
var c,s: GLFloat; r: TMatrix4x4;
begin
  angle:=GLfloat(degtorad(angle));
  c:=cos(angle);
  s:=sin(angle);
  // create rotation matrix
  r[0,0]:= c; r[1,0]:=0; r[2,0]:=s; r[3,0]:=0;
  r[0,1]:= 0; r[1,1]:=1; r[2,1]:=0; r[3,1]:=0;
  r[0,2]:=-s; r[1,2]:=0; r[2,2]:=c; r[3,2]:=0;
  r[0,3]:= 0; r[1,3]:=0; r[2,3]:=0; r[3,3]:=1;
  fMatrix:=MultglMatrix(fMatrix,r);
end;

procedure TAsmObject.RotateAboutLocalZ(angle: GLFloat);
var c,s: GLFloat; r: TMatrix4x4;
begin
  angle:=GLfloat(degtorad(angle));
  c:=cos(angle);
  s:=sin(angle);
  // create rotation matrix
  r[0,0]:=c; r[1,0]:=-s; r[2,0]:=0; r[3,0]:=0;
  r[0,1]:=s; r[1,1]:= c; r[2,1]:=0; r[3,1]:=0;
  r[0,2]:=0; r[1,2]:= 0; r[2,2]:=1; r[3,2]:=0;
  r[0,3]:=0; r[1,3]:= 0; r[2,3]:=0; r[3,3]:=1;
  fMatrix:=MultglMatrix(fMatrix,r);
end;

procedure TAsmObject.Rotate(angle: GLFLoat; axis: TVector3);
var c, s, t, x, y, z: GLFloat; r: TMatrix4x4;
begin
  Normalize(Axis);
  // Create Rotation Matrix
  angle:=GLfloat(degtorad(angle));
  t:=1-cos(angle);
  s:=sin(angle);
  c:=cos(angle);
  x:=Axis.x;
  y:=Axis.y;
  z:=Axis.z;
  //
  r[0,0]:=t*x*x+c;   r[1,0]:=t*x*y-s*z; r[2,0]:=t*x*z+s*y; r[3,0]:=0;
  r[0,1]:=t*x*y+s*z; r[1,1]:=t*y*y+c;   r[2,1]:=t*y*z-s*x; r[3,1]:=0;
  r[0,2]:=t*x*z-s*y; r[1,2]:=t*y*z+s*x; r[2,2]:=t*z*z+c;   r[3,2]:=0;
  r[0,3]:=0;         r[1,3]:=0;         r[2,3]:=0;         r[3,3]:=1;
  // Set new Vector
  fMatrix:=MultglMatrix(TMatrix4x4(r),fMatrix);
  fPosition.x:=fMatrix[3,0]+fCor.x;
  fPosition.y:=fMatrix[3,1]+fCor.y;
  fPosition.z:=fMatrix[3,2]+fCor.z;
end;

procedure TAsmObject.Rotate(angle: GLFLoat; ax, ay, az: GLFloat);
var c, s, t, x, y, z, l: GLFloat; r: TMatrix4x4;
begin
  // Normalize Axis of Rotation
  l:=1/sqrt(ax*ax+ay*ay+az*az);
  ax:=l*ax;
  ay:=l*ay;
  az:=l*az;
  // Create Rotation Matrix
  angle:=GLfloat(degtorad(angle));
  t:=1-cos(angle);
  s:=sin(angle);
  c:=cos(angle);
  x:=ax;
  y:=ay;
  z:=az;
  //
  r[0,0]:=t*x*x+c;   r[1,0]:=t*x*y-s*z; r[2,0]:=t*x*z+s*y; r[3,0]:=0;
  r[0,1]:=t*x*y+s*z; r[1,1]:=t*y*y+c;   r[2,1]:=t*y*z-s*x; r[3,1]:=0;
  r[0,2]:=t*x*z-s*y; r[1,2]:=t*y*z+s*x; r[2,2]:=t*z*z+c;   r[3,2]:=0;
  r[0,3]:=0;         r[1,3]:=0;         r[2,3]:=0;         r[3,3]:=1;
  // Set new Vector
  fMatrix:=MultglMatrix(r,fMatrix);
  fPosition.x:=fMatrix[3,0]+fCor.x;
  fPosition.y:=fMatrix[3,1]+fCor.y;
  fPosition.z:=fMatrix[3,2]+fCor.z;
end;

procedure TAsmObject.RotateAboutLocalOrigin(angle: GLFloat; const axis: TVector3);
var tmpCoR: TVector3;
begin
  tmpCoR:=CoR;
  SetCoR(Position);
  Rotate(angle, axis);
  SetCoR(tmpCoR);
end;

procedure TAsmObject.RotateAboutLocalOrigin(angle: GLFloat; ax, ay, az: GLFloat);
var tmpCoR: TVector3;
begin
  tmpCoR:=CoR;
  SetCoR(Position);
  Rotate(angle, ax, ay, az);
  SetCoR(tmpCoR);
end;

{ TAsmLight }

function TAsmLight.GetPosition: TVector3;
begin
  if fAliasPosition<>nil then
    GetPosition:=fAliasPosition^
  else begin
    GetPosition.x:=fPosition.x;
    GetPosition.y:=fPosition.y;
    GetPosition.z:=fPosition.z;
  end;
end;

function TAsmLight.GetPosition4: TVector4;
begin
  if fAliasPosition<>nil then begin
    GetPosition4.x:=fAliasPosition^.x;
    GetPosition4.y:=fAliasPosition^.y;
    GetPosition4.z:=fAliasPosition^.z;
  end else begin
    GetPosition4.x:=fPosition.x;
    GetPosition4.y:=fPosition.y;
    GetPosition4.z:=fPosition.z;
  end;
  GetPosition4.w:=fPosition.w;
end;

procedure TAsmLight.PlaceLight(glLight: GLenum);
var glPosition: TVector4;
begin
  glPosition:=Position4;
  glLightfv(glLight, GL_POSITION, @glPosition);
  glLightfv(glLight, GL_AMBIENT, fAmbientColor);
  glLightfv(glLight, GL_DIFFUSE, fDiffuseColor);
  glLightfv(glLight, GL_SPECULAR, fSpecularColor);
end;

constructor TAsmLight.Create(AnOwner: TComponent);
begin
  inherited Create(AnOwner);
end;

procedure TAsmLight.SetAmbientColor(red, green, blue, alpha: GLFloat
  );
begin
  fAmbientColor[0]:=red;
  fAmbientColor[1]:=green;
  fAmbientColor[2]:=blue;
  fAmbientColor[3]:=alpha;
end;

procedure TAsmLight.SetAmbientColor(const newColor: glRGBAColor);
begin
  fAmbientColor:=newColor;
end;

procedure TAsmLight.SetDiffuseColor(red, green, blue, alpha: GLFloat
  );
begin
  fDiffuseColor[0]:=red;
  fDiffuseColor[1]:=green;
  fDiffuseColor[2]:=blue;
  fDiffuseColor[3]:=alpha;
end;

procedure TAsmLight.SetDiffuseColor(const newColor: glRGBAColor);
begin
  fDiffuseColor:=newColor;
end;

procedure TAsmLight.SetSpecularColor(red, green, blue, alpha: GLFloat
  );
begin
  fSpecularColor[0]:=red;
  fSpecularColor[1]:=green;
  fSpecularColor[2]:=blue;
  fSpecularColor[3]:=alpha;
end;

procedure TAsmLight.SetSpecularColor(const newColor: glRGBAColor);
begin
  fSpecularColor:=newColor;
end;

{ TAsmCamera }

procedure TAsmCamera.SetWidth(const AValue: integer);
begin
  if fWidth=AValue then exit;
  fWidth:=AValue;
end;

procedure TAsmCamera.SetHeight(const AValue: integer);
begin
  if fHeight=AValue then exit;
  fHeight:=AValue;
end;

procedure TAsmCamera.Rotate(var Vector: TVector3; const Axis: TVector3; angle: GLFloat);
var TmpVector: TVector3; RM: array [0..2,0..2] of GLFloat;
    t, s, c, x, y, z: GLFloat;
begin
  // Create Rotation Matrix
  angle:=GLfloat(degtorad(angle));
  t:=1-cos(angle);
  s:=sin(angle);
  c:=cos(angle);
  x:=Axis.x;
  y:=Axis.y;
  z:=Axis.z;
  //
  RM[0,0]:=t*x*x+c;   RM[0,1]:=t*x*y-s*z; RM[0,2]:=t*x*z+s*y;
  RM[1,0]:=t*x*y+s*z; RM[1,1]:=t*y*y+c;   RM[1,2]:=t*y*z-s*x;
  RM[2,0]:=t*x*z-s*y; RM[2,1]:=t*y*z+s*x; RM[2,2]:=t*z*z+c;
  // Multiply Rotation Matrix by Vector
  TmpVector.x:=RM[0,0]*Vector.x+RM[0,1]*Vector.y+RM[0,2]*Vector.z;
  TmpVector.y:=RM[1,0]*Vector.x+RM[1,1]*Vector.y+RM[1,2]*Vector.z;
  TmpVector.z:=RM[2,0]*Vector.x+RM[2,1]*Vector.y+RM[2,2]*Vector.z;
  // Set new Vector
  Vector:=TmpVector;
end;

procedure TAsmCamera.SetFarPlane(const AValue: GLFloat);
begin
  FFarPlane:=AValue;
end;

procedure TAsmCamera.SetFoV(const AValue: GLFloat);
begin
  FFoV:=AValue;
end;

procedure TAsmCamera.SetNearPlane(const AValue: GLFLoat);
begin
  FNearPlane:=AValue;
end;

constructor TAsmCamera.Create(iFoV, iNearPlane, iFarPlane: GLFloat; AnOwner: TComponent);
begin
  inherited Create(AnOwner);
  FoV:=iFoV;
  NearPlane:=iNearplane;
  FarPlane:=iFarPlane;
end;

procedure TAsmCamera.GetFrustum;
// RIGHT=0; LEFT=1; BOTTOM=2; TOP=3; FAR=4; NEAR=5; A=0; B=1; C=2; D=3;
var clip: TMatrix4x4; magnitude: GLFloat; side: Integer;
begin
  glGetFloatv(GL_PROJECTION_MATRIX, @ProjMatrix);
  glGetFloatv(GL_MODELVIEW_MATRIX, @ModelMatrix);
  clip:=MultglMatrix(ProjMatrix, ModelMatrix);
	// This will extract the RIGHT side of the frustum
	Frustum[0,0]:=clip[0,3]-clip[0,0];
	Frustum[0,1]:=clip[1,3]-clip[1,0];
	Frustum[0,2]:=clip[2,3]-clip[2,0];
	Frustum[0,3]:=clip[3,3]-clip[3,0];
	// This will extract the LEFT side of the frustum
	Frustum[1,0]:=clip[0,3]+clip[0,0];
	Frustum[1,1]:=clip[1,3]+clip[1,0];
	Frustum[1,2]:=clip[2,3]+clip[2,0];
	Frustum[1,3]:=clip[3,3]+clip[3,0];
	// This will extract the BOTTOM side of the frustum
	Frustum[2,0]:=clip[0,3]+clip[0,1];
	Frustum[2,1]:=clip[1,3]+clip[1,1];
	Frustum[2,2]:=clip[2,3]+clip[2,1];
	Frustum[2,3]:=clip[3,3]+clip[3,1];
	// This will extract the TOP side of the frustum
	Frustum[3,0]:=clip[0,3]-clip[0,1];
	Frustum[3,1]:=clip[1,3]-clip[1,1];
	Frustum[3,2]:=clip[2,3]-clip[2,1];
	Frustum[3,3]:=clip[3,3]-clip[3,1];
	// This will extract the FAR side of the frustum
	Frustum[4,0]:=clip[0,3]-clip[0,2];
	Frustum[4,1]:=clip[1,3]-clip[1,2];
	Frustum[4,2]:=clip[2,3]-clip[2,2];
	Frustum[4,3]:=clip[3,3]-clip[3,2];
	// This will extract the NEAR side of the frustum
	Frustum[5,0]:=clip[0,3]+clip[0,2];
	Frustum[5,1]:=clip[1,3]+clip[1,2];
	Frustum[5,2]:=clip[2,3]+clip[2,2];
	Frustum[5,3]:=clip[3,3]+clip[3,2];
  // normalize frustum planes
  for side:=0 to 5 do begin
    magnitude:=1/sqrt(frustum[side,0]*frustum[side,0]+frustum[side,1]*frustum[side,1]+
     frustum[side,2]*frustum[side,2]);
    frustum[side,0]:=frustum[side,0]*magnitude;
    frustum[side,1]:=frustum[side,1]*magnitude;
    frustum[side,2]:=frustum[side,2]*magnitude;
    frustum[side,3]:=frustum[side,3]*magnitude;
  end;

end;

procedure TAsmCamera.GetFrustumBoundingSphere;
var ViewLen, FrustumWidth, FrustumHeight: GLFloat; P, Q, Diff, LookVector: TVector3;
begin
// calculate the radius of the frustum sphere
ViewLen:=FarPlane-NearPlane;
// use some trig to find the height of the frustum at the far plane
FrustumHeight:=ViewLen*tan(degtorad(FoV)*0.5);
// with an aspect ratio of 1, the width will be the same
FrustumWidth:=FrustumHeight;
// halfway point between near/far planes starting at the origin and extending along the z axis
P.x:=0; P.y:=0; P.z:=NearPlane+ViewLen*0.5;
// the calculate far corner of the frustum
Q.x:=FrustumWidth; Q.y:=FrustumHeight; q.z:=ViewLen;
// the vector between P and Q
Diff:=P-Q;
// the radius becomes the length of this vector
FrustumBoundingSphere.Radius:=vLength(Diff);
// get the look vector of the camera from the view matrix
LookVector:=Normalized(CoV-Pos);
// calculate the center of the sphere
FrustumBoundingSphere.Center:=Pos+(LookVector*(ViewLen*0.5+NearPlane));
end;

procedure TAsmCamera.GetFrustumCone;
var Depth, Corner, halfWidth, halfHeight: GLFloat;
begin
  // set the properties of the frustum cone… vLookVector is the look vector
  // from the view matrix in the
  // camera.  Position() returns the position of the camera.
  // fWidth is half the width of the screen (in pixels).
  // fHeight is half the height of the screen in pixels.
  // m_fFovRadians is the FOV of the frustum.

  // calculate the length of the fov triangle
  //float fDepth  = fHeight / tan(m_fFovRadians * 0.5f);
  halfWidth:=fWidth div 2;
  halfHeight:=fHeight div 2;
  Depth:=halfHeight/tan(DegToRad(Fov/2));
  // calculate the corner of the screen
  //float fCorner = sqrt(fWidth * fWidth + fHeight * fHeight);
  Corner:=sqrt(halfWidth*halfWidth+halfHeight*halfHeight);
  // now calculate the new fov
  //float fFov = atan(fCorner / fDepth);
  FrustumCone.Angle:=arctan(Corner/Depth);
  FrustumCone.Axis:=Normalized(CoV-Pos);
  FrustumCone.Vertex:=Pos;
end;

procedure TAsmCamera.RenderToTexture;
begin
end;

procedure TAsmCamera.RenderView(ObjectList, Lightlist: TFPList; var GLStateMachine
 : GLStatus);

  function PointInsideFrustum(Point: TVector3): Boolean;
  var side: Integer;
  begin
    for side:=0 to 5 do
      if (Frustum[side,0]*Point.x
         +Frustum[side,1]*Point.y
         +Frustum[side,2]*Point.z
         +Frustum[side,3]<=0) then begin
         PointInsideFrustum:=false;
         exit;
      end;
    PointInsideFrustum:=true;
  end;

  function SphereInsideFrustum(Point: TVector3; Radius: GLFloat): GLFloat;
  var side: Integer; distance: GLFLoat;
  begin
    for side:=0 to 5 do begin
      distance:=Frustum[side,0]*Point.x+Frustum[side,1]*Point.y
       +Frustum[side,2]*Point.z+Frustum[side,3];
      if distance<=-Radius then begin
         SphereInsideFrustum:=0;
         exit;
      end;
    end;
    SphereInsideFrustum:=distance+Radius;
  end;

  function CubeInsideFrustum(MidPoint: TVector3; Size: GLFloat): Boolean;
  var p: Integer;
  begin
    for p:=0 to 5 do begin
      if Frustum[p][0]*(MidPoint.x-size)+Frustum[p][1]*(MidPoint.y-size)+
       Frustum[p][2]*(MidPoint.z-size)+Frustum[p][3]<=0 then exit;
      if Frustum[p][0]*(MidPoint.x+size)+Frustum[p][1]*(MidPoint.y-size)+
       Frustum[p][2]*(MidPoint.z-size)+Frustum[p][3]<=0 then exit;
      if Frustum[p][0]*(MidPoint.x-size)+Frustum[p][1]*(MidPoint.y+size)+
       Frustum[p][2]*(MidPoint.z-size)+Frustum[p][3]<=0 then exit;
      if Frustum[p][0]*(MidPoint.x+size)+Frustum[p][1]*(MidPoint.y+size)+
       Frustum[p][2]*(MidPoint.z-size)+Frustum[p][3]<=0 then exit;
      if Frustum[p][0]*(MidPoint.x-size)+Frustum[p][1]*(MidPoint.y-size)+
       Frustum[p][2]*(MidPoint.z+size)+Frustum[p][3]<=0 then exit;
      if Frustum[p][0]*(MidPoint.x+size)+Frustum[p][1]*(MidPoint.y-size)+
       Frustum[p][2]*(MidPoint.z+size)+Frustum[p][3]<=0 then exit;
      if Frustum[p][0]*(MidPoint.x-size)+Frustum[p][1]*(MidPoint.y+size)+
       Frustum[p][2]*(MidPoint.z+size)+Frustum[p][3]<=0 then exit;
      if Frustum[p][0]*(MidPoint.x+size)+Frustum[p][1]*(MidPoint.y+size)+
       Frustum[p][2]*(MidPoint.z+size)+Frustum[p][3]<=0 then exit;
     CubeInsideFrustum:=false;
   end;
   CubeInsideFrustum:=true;
  end;

var i: Integer; OpaqueObjects, TransparentObjects: TFPList; ActualMesh:
  TAsmMesh; ActualObject: TAsmObject; ActualShader: TAsmShader;
  CullSphere: TSphere;
begin
  OpaqueObjects:=TFPList.Create;
  TransparentObjects:=TFPList.Create;
  // clearing the viewport and initializing it
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glLoadIdentity;
  gluLookAt(Pos.x, Pos.y, Pos.z, fCoV.x, fCoV.y, fCoV.z, fUp.x, fUp.y, fUp.z);
  // Light
  glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
  for i:=0 to Lightlist.count-1 do
    if TAsmLight(LightList[i]).Enabled then begin
      glEnable(glLights[i]);
      TAsmLight(Lightlist[i]).PlaceLight(glLights[i]);
    end;
  // Frustum culling
  // culling against frustum bounding sphere
  GetFrustumBoundingSphere;
  i:=0;
  while (i<ObjectList.Count) do begin
    ActualObject:=TAsmObject(ObjectList.Items[i]);
    ActualMesh:=TAsmObject(ObjectList.Items[i]).Mesh;
    CullSphere.Center:=iTransformVector(ActualObject.BS.Center,ActualObject
                                        .fMatrix)+ActualObject.fposition;
    CullSphere.Radius:=ActualObject.bs.Radius;
    if not SphereSphereIntersection(CullSphere,FrustumBoundingSphere) then begin
      ObjectList.Delete(i);
    end;
    inc(i);
  end;
  // Culling against frustum cone
  GetFrustumCone;
  i:=0;
  while (i<ObjectList.Count) do begin
    ActualObject:=TAsmObject(ObjectList.Items[i]);
    ActualMesh:=TAsmObject(ObjectList.Items[i]).Mesh;
    CullSphere.Center:=iTransformVector(ActualObject.BS.Center,ActualObject
                                        .fMatrix)+ActualObject.fposition;
    CullSphere.Radius:=ActualObject.bs.Radius;
    if not SphereConeIntersection(CullSphere,FrustumCone) then begin
      ObjectList.Delete(i);
    end;
    inc(i);
  end;
  // culling against frustum
  GetFrustum;
  for i:=0 to ObjectList.Count-1 do begin
    ActualObject:=TAsmObject(ObjectList.Items[i]);
    ActualMesh:=TAsmObject(ObjectList.Items[i]).Mesh;
    if ActualMesh.BS.Radius<=0 then
      ActualObject.NearPlaneDistance:=1
    else
      ActualObject.NearPlaneDistance:=SphereInsideFrustum(iTransformVector(
       ActualObject.BS.Center,ActualObject.fMatrix)+ActualObject.fposition,ActualObject.BS.Radius);
    if ActualObject.NearPlaneDistance>0 then
      if ActualObject.transparent then begin
        ActualObject.NearPlaneDistance:=ActualObject.NearPlaneDistance-2*
         ActualObject.BS.Radius;
        TransparentObjects.Add(ObjectList.Items[i]);
      end else
        OpaqueObjects.Add(ObjectList.Items[i]);
  end;
  // Sorting here
  ListMergeSort(TransparentObjects,0,TransparentObjects.Count-1,@CompareDistanceDes);
  // Speed Test needed to determine if sorting makes any difference
  ListMergeSort(OpaqueObjects,0,OpaqueObjects.Count-1,@CompareShaderDes);
  // Drawing opaque Objects
  if OpaqueObjects.Count>0 then begin
    for i:=0 to OpaqueObjects.Count-1 do begin
      ActualObject:=TAsmObject(OpaqueObjects.Items[i]);
      // shadow test
      {if Lightlist.Count>0 then begin
        ActualObject.BuildShadowVolume(TAsmLight(Lightlist[0]));
        glColor3f(1,0,0);
        GLStateMachine.SetState([]);
        glPushMatrix;
        with ActualObject do begin
          gltranslatef(CoR.x, CoR.y, CoR.z);
          glMultMatrixf(@Matrix);
          if (Scale.x<>1) or (Scale.y<>1) or (Scale.z<>1) then glScalef(Scale.x,Scale.y,Scale.z);
        end;
        glBegin(GL_QUADS);
        for ii:=0 to Length(ActualObject.ShadowVolumeVA)-1 do begin
          glVertex4fv(@ActualObject.ShadowVolumeVA[ii]);
        end;
        glEnd;
        glPopMatrix;
      end;}
      //
      ActualShader:=TAsmShader(ShaderList.Items[ActualObject.Shader]);
      GLStateMachine.SetState(ActualShader.States);
      // workaround
      if GLStateMachine.LineWidth<>ActualObject.LineWidth then
        if (ActualObject.LineWidth=0) or (ActualObject.LineWidth=1) then begin
          GLStateMachine.LineWidth:=1;
          glLineWidth(1);
        end else begin
          GLStateMachine.LineWidth:=ActualObject.LineWidth;
          glLineWidth(GLStateMachine.LineWidth);
        end;
      //
      with ActualObject do begin
        glColor4fv(fColor);
        glMaterialfv(GL_FRONT,GL_SPECULAR,fSpecColor);
        glMateriali(GL_FRONT,GL_SHININESS,Shininess);
      end;
      //if TAsmObject(OpaqueObjects.Items[i]) is TAsmBFHeightField then
      //  TAsmObject(OpaqueObjects.Items[i]).DrawDL
      //else
      ActualShader.Draw(ActualObject);
    end;
  end;
  // Drawing transparent Objects (buggy like hell)
  if TransparentObjects.Count>0 then begin
    //glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    for i:=0 to TransparentObjects.Count-1 do begin
      ActualObject:=TAsmObject(TransparentObjects.Items[i]);
      ActualShader:=TAsmShader(ShaderList.Items[ActualObject.Shader]);
      GLStateMachine.SetState(ActualShader.States);
      glColor4fv(ActualObject.fColor);
      with ActualObject do begin
        glColor4fv(fColor);
        glMaterialfv(GL_FRONT,GL_SPECULAR,fSpecColor);
        glMateriali(GL_FRONT,GL_SHININESS,Shininess);
      end;
      ActualShader.Draw(ActualObject);
    end;
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
  end;
  //
  for i:=0 to Lightlist.count-1 do glDisable(glLights[i]);
  //
  OpaqueObjects.Free;
  TransparentObjects.Free;
end;

{ GLStatus }

procedure GLStatus.SetState(const State: AsmState);
begin
  if currentState<>State then begin
  // lighting
  if lit in State then begin
    if not Lighting then begin
      glEnable(GL_LIGHTING);
      glEnableClientState(GL_NORMAL_ARRAY);
      Lighting:=true;
    end;
  end else
    if Lighting then begin
      glDisable(GL_LIGHTING);
      glDisableClientState(GL_NORMAL_ARRAY);
      Lighting:=false;
    end;
  // wireframe
  if wireframed in State then begin
    if not Wireframe then begin
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      Wireframe:=true;
    end;
  end else
    if Wireframe then begin
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      Wireframe:=false;
    end;
  // multicolor
  if multicolored in State then begin
    if not Multicolor then begin
      glEnableClientState(GL_COLOR_ARRAY);
      Multicolor:=true;
    end;
  end else
    if Multicolor then begin
      glDisableClientState(GL_COLOR_ARRAY);
      MultiColor:=false;
    end;
  // texture2d
  if textured in State then begin
    if not Texture2D then begin
      glEnable(GL_TEXTURE_2D);
      glEnableClientState(GL_TEXTURE_COORD_ARRAY);
      Texture2D:=true;
    end;
  end else
    if Texture2D then begin
      glDisable(GL_TEXTURE_2D);
      glDisableClientState(GL_TEXTURE_COORD_ARRAY);
      Texture2D:=false;
    end;
  // DepthTest
  if notDepthTested in State then begin
    if not noDepthTest then begin
      glDisable(GL_DEPTH_TEST);
      noDepthTest:=true;
    end;
  end else
    if noDepthTest then begin
      glEnable(GL_DEPTH_TEST);
      noDepthTest:=false;
    end;
  // since there is no real transparency support this state is disabled
  {// blending
  if transparent in State then begin
    if not Blending then begin
      glEnable(GL_BLEND);
      Blending:=true;
    end;
  end else
    if Blending then begin
      glDisable(GL_BLEND);
      Blending:=false;
    end;}
  currentState:=State;
  end;
end;

{ TAsmTexture }

procedure TAsmTexture.SetInternalColor(x, y: integer; const Value: TFPColor);
begin
  Data[(X+Y*Width)*3]:=byte(Value.red shr 8);
  Data[(X+Y*Width)*3+1]:=byte(Value.green shr 8);
  Data[(X+Y*Width)*3+2]:=byte(Value.blue shr 8);
end;

function TAsmTexture.GetInternalColor(x, y: integer): TFPColor;
var Pixel: TFPColor;
begin
  Pixel.red:=Data[(X+Y*Width)*3] shl 8+Data[(X+Y*Width)*3];
  Pixel.green:=Data[(X+Y*Width)*3+1] shl 8+Data[(X+Y*Width)*3+1];
  Pixel.blue:=Data[(X+Y*Width)*3+2] shl 8+Data[(X+Y*Width)*3+2];
  Pixel.alpha:=alphaOpaque;
  Result:=Pixel;
end;

procedure TAsmTexture.SetInternalPixel(x, y: integer; Value: integer);
begin
  if (x=0) and (y=0) and (Value=0) then ;
end;

function TAsmTexture.GetInternalPixel(x, y: integer): integer;
begin
  if (x=0) and (y=0) then ;
  Result:=0;
end;

constructor TAsmTexture.Create(AWidth, AHeight: integer);
begin
  inherited Create(AWidth, AHeight);
end;

destructor TAsmTexture.Destroy;
begin
  if Data<>nil then FreeMem(Data);
  inherited Destroy;
end;

procedure TAsmTexture.SetSize(AWidth, AHeight: integer);
begin
  inherited SetSize(AWidth, AHeight);
  ReallocMem(Data, Width*Height*3);
end;

procedure TAsmTexture.Bind;
begin
  glGenTextures(1, @id);
  glBindTexture(GL_TEXTURE_2D, id);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D,0,3,Width,Height,0,GL_RGB,GL_UNSIGNED_BYTE,Data);
  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
end;

initialization

glLights[0]:=GL_LIGHT0;
glLights[1]:=GL_LIGHT1;
glLights[2]:=GL_LIGHT2;
glLights[3]:=GL_LIGHT3;
glLights[4]:=GL_LIGHT4;
glLights[5]:=GL_LIGHT5;
glLights[6]:=GL_LIGHT6;
glLights[7]:=GL_LIGHT7;

end.

