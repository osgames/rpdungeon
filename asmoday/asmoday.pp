{
 *****************************************************************************
 *                                                                           *
 *  This source is free software. See the file COPYING.modifiedLGPL,         *
 *  included in this distribution, for details about the copyright.          *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************

 asmoday.pp
 Author: Reimar Grabowski (2001-2006)
}

unit asmoday;

{$mode objfpc}{$H+}

interface

uses
  classes, SysUtils, nvgl, sgiglu, glib, gdk, Vectors, OpenGLContext, math,
  avl_tree, asmtypes, asmshaders, asmutils
{$IF defined(LCLgtk) or defined(LCLgtk2) or defined(LCLGnome)}
  ,nvglx
{$ENDIF}
  ;

// ShaderConstants

const
  NoLighting = 0;
  NoLightingWireframe = 1;
  NoLightingTexture = 2;
  NoLightingColor = 3;
  Lighting = 4;
  LightingWireframe = 5;
  LightingTexture = 6;
  LightingNormal = 7;

type
  //  LIGHTS ///////////////////////////////////////////////////////////////////

  TAsmDirectionalLight = class(TAsmLight)
  public
    constructor Create(AnOwner: TComponent); override;
    procedure SetPosition(const Pos: TVector3); override;
    procedure SetPosition(x, y, z: GLFloat); override;
    procedure SetPosition(Pos: PVector3); override;
  end;

  TAsmPositionalLight = class(TAsmLight)
  public
    constructor Create(AnOwner: TComponent); override;
    procedure SetPosition(const Pos: TVector3); override;
    procedure SetPosition(x, y, z: GLFloat); override;
    procedure SetPosition(Pos: PVector3); override;
  end;

  {TAsmSpotLight = class(TAsmLight)
  private
    Light: GLenum;
    fAmbientColor: glRGBAColor;
    fDiffuseColor: glRGBAColor;
    fSpecularColor: glRGBAColor;
    fPosition: TVector3;
    fDirection: TVector3;
  public
    procedure SetAmbientColor(red, green, blue, alpha: GLFloat);
    procedure SetDiffuseColor(red, green, blue, alpha: GLFloat);
    procedure SetSpecularColor(red, green, blue, alpha: GLFloat);
    procedure SetPosition(fPosition);
    procedure SetDirection(x, y, z: GLFloat);
    procedure SetCutoff(angle: GLFLoat);
    procedure SetExponent(exponent: GLFloat);
  end;}
  
  // CAMERAS ///////////////////////////////////////////////////////////////////
  
  // Base class for free movable cameras
  TAsmFreeCamera = class(TAsmCamera)
  private
    Right, View: TVector3;
  public
    procedure RotateAboutUp(angle: GLFloat);
    procedure RotateAboutRight(angle: GLFloat); virtual;
    procedure MoveAlongView(speed: GLFloat);
    procedure MoveAlongUp(speed: GLFloat);
    procedure MoveAlongRight(speed: GLFloat);
  end;

  // 6 degree of freedom camera - flight simulator camera
  TAsm6DOFCamera = Class(TAsmFreeCamera)
  private
  public
    constructor Create(xPosition, yPosition, zPosition, iFoV, iNearPlane,
      iFarPlane: GLFloat; AnOwner: TComponent);
    procedure RotateAboutView(angle: GLFloat);
  end;
  
  // First person shooter like camera
  TAsmFirstPersonCamera = Class(TAsmFreeCamera)
  private
    incAngle: GLFloat;
  public
    constructor Create(xPosition, yPosition, zPosition, xCoV, yCoV, zCoV, iFoV,
      iNearPlane, iFarPlane: GLFloat; AnOwner: TComponent);
    procedure RotateAboutRight(angle: GLFloat); override;
    procedure SetCamera(xPosition, yPosition, zPosition, xCoV, yCoV,
      zCoV: GLFloat);
    procedure SetCamera(const Position: TVector3; iCoV: TVector3);
  end;
  
  // Position or object bound camera
  TAsmRotationCamera = class(TAsmCamera)
  private
    fAliasCoV: PVector3;
    fAlpha: GLfloat;
    fBeta: GLfloat;
    fHeight: GLfloat;
    fRadius: GLfloat;
    procedure SetAliasCoV(const AValue: PVector3);
    procedure SetAlpha(const AValue: GLfloat);
    procedure SetBeta(const AValue: GLfloat);
    procedure SetHeight(const AValue: GLfloat);
    procedure SetRadius(const AValue: GLfloat);
    procedure UpdatePosition;
  public
    constructor Create(iRadius, iHeight, xCoV, yCoV, zCoV, iFoV, iNearPlane,
      iFarPlane: GLFloat; AnOwner: TComponent);
    procedure RotateAlphaAboutCoV(angle: GLfloat);
    procedure RotateBetaAboutCov(angle: GLfloat);
    procedure SetCoV(xCoV, yCoV, zCoV: GLfloat);
    procedure SetCoV(const iCoV: TVector3);
    procedure SetCoV(pCoV: PVector3);
    procedure RenderView(ObjectList, Lightlist: TFPList; var GLStateMachine: GLStatus); override;
    property Alpha: GLfloat read fAlpha write SetAlpha;
    property Beta: GLfloat read fBeta write SetBeta;
    property Height: GLfloat read fHeight write SetHeight;
    property Radius: GLfloat read fRadius write SetRadius;
    property AliasCoV: PVector3 read fAliasCoV write SetAliasCoV;
  end;
  
  // Text //////////////////////////////////////////////////////////////////////
  
  TAsmBitmapText = class(TComponent)
  private
    fText: String;
    fColor: glRGBColor;
    Base: GLuint;
    procedure SetText(const AValue: String);
  public
    X, Y, Z: GLDouble;
    CoordinateIs3D: Boolean;
    constructor Create(Fontname: PGChar; AnOwner: TComponent);
    destructor Destroy; override;
    procedure SetColor(red, green, blue: GLfloat);
    property Text: String read fText write SetText;
    procedure Draw(const ModelMatrix, ProjMatrix: TGLUMatrixD; const Viewport: TGLUViewport);
  end;

  // Scene  ////////////////////////////////////////////////////////////////////

  TAsmScene = class(TComponent)
  private
    Area: TOpenGLControl;
    fActiveCamera: TAsmCamera;
    procedure SetActiveCamera(const AValue: TAsmCamera);
  public
    mm, pm: TGLUMatrixD;
    Viewport: array [0..3] of GLint;
    Objectlist: TFPList;
    Lightlist: TFPList;
    Textlist: TFPList;
    Vendor, Renderer, Version, Extensions: String;
    GLStateMachine : GLStatus;
    constructor Create(Context: TOpenGLControl; AnOwner: TComponent);
    destructor Destroy; override;
    procedure Init;
    procedure SetBackgroundColor(red, green, blue: GLfloat);
    procedure SetBackgroundColor(newColor: glRGBColor);
    property ActiveCamera: TAsmCamera read fActiveCamera write SetActiveCamera;
    procedure RenderCameraIndependentObjects(Objects: TFPList);
    procedure UpdateViewport;
    procedure GetFrustum;
    procedure RenderScene;
  end;

implementation

{ TAsmSpotLight }

{constructor TAsmLight.Create(glLight: GLenum);
begin
  Light:=glLight;
  // overriding OpenGL default values
  SetAmbientColor(0,0,0,0);
  SetDiffuseColor(0,0,0,0);
  SetSpecularColor(0,0,0,0);
  SetPosition(0,0,0,0);
  SetSpotDirection(0,0,-1);
  //SetAttenuation(0,0,0);
  //SetCutoff(180);
  //SetExponent(0);
  glDisable(Light);
end;

procedure TAsmLight.Setenabled(const AValue: Boolean);
begin
  fenabled:=AValue;
  if fenabled then
    glEnable(Light)
  else
    glDisable(Light);
end;

procedure TAsmLight.SetPosition(x, y, z, w: GLFloat);
begin
  fPosition[0]:=x;
  fPosition[1]:=y;
  fPosition[2]:=z;
  fPosition[3]:=w;
  glLightfv(Light, GL_POSITION, fPosition);
end;

procedure TAsmLight.SetAmbientColor(red, green, blue, alpha: GLFloat);
begin
  fAmbientColor[0]:=red;
  fAmbientColor[1]:=green;
  fAmbientColor[2]:=blue;
  fAmbientColor[3]:=alpha;
  glLightfv(Light, GL_AMBIENT, fAmbientColor);
end;

procedure TAsmLight.SetDiffuseColor(red, green, blue, alpha: GLFloat);
begin
  fDiffuseColor[0]:=red;
  fDiffuseColor[1]:=green;
  fDiffuseColor[2]:=blue;
  fDiffuseColor[3]:=alpha;
  glLightfv(Light, GL_DIFFUSE, fDiffuseColor);
end;

procedure TAsmLight.SetSpecularColor(red, green, blue, alpha: GLFloat);
begin
  fSpecularColor[0]:=red;
  fSpecularColor[1]:=green;
  fSpecularColor[2]:=blue;
  fSpecularColor[3]:=alpha;
  glLightfv(Light, GL_SPECULAR, fSpecularColor);
end;

procedure TAsmLight.SetSpotDirection(x, y, z:GLFloat);
begin
  fDirection[0]:=x;
  fDirection[1]:=y;
  fDirection[2]:=z;
  glLightfv(Light, GL_SPOT_DIRECTION, fDirection);
end;

procedure TAsmLight.SetCutoff(angle : GLFLoat);
begin
  glLightf(Light, GL_SPOT_CUTOFF, angle);
end;

procedure TAsmLight.SetAttenuation(constant, linear, quadratic: GLFloat);
begin
  glLightf(Light, GL_CONSTANT_ATTENUATION, constant);
  glLightf(Light, GL_LINEAR_ATTENUATION, linear);
  glLightf(Light, GL_QUADRATIC_ATTENUATION, quadratic);
end;

procedure TAsmLight.SetExponent(exponent: GLFloat);
begin
  glLightf(Light, GL_SPOT_EXPONENT, exponent);
end;}

{ TAsmDirectionalLight }

constructor TAsmDirectionalLight.Create(AnOwner: TComponent);
begin
  inherited Create(AnOwner);
  fPosition.w:=0;
end;

procedure TAsmDirectionalLight.SetPosition(const Pos: TVector3);
begin
  fPosition.x:=Pos.x; fPosition.y:=Pos.y; fPosition.z:=Pos.z;
end;

procedure TAsmDirectionalLight.SetPosition(x, y, z: GLFloat);
begin
  fPosition.x:=x; fPosition.y:=y; fPosition.z:=z;
end;

procedure TAsmDirectionalLight.SetPosition(Pos: PVector3);
begin
  fAliasPosition:=Pos;
end;

{ TAsmPositionalLight }

constructor TAsmPositionalLight.Create(AnOwner: TComponent);
begin
  inherited Create(AnOwner);
  fPosition.w:=1;
end;

procedure TAsmPositionalLight.SetPosition(const Pos: TVector3);
begin
  fPosition.x:=Pos.x; fPosition.y:=Pos.y; fPosition.z:=Pos.z;
end;

procedure TAsmPositionalLight.SetPosition(x, y, z: GLFloat);
begin
  fPosition.x:=x; fPosition.y:=y; fPosition.z:=z;
end;

procedure TAsmPositionalLight.SetPosition(Pos: PVector3);
begin
  fAliasPosition:=Pos;
end;

{ TAsm6DOFCamera }

constructor TAsm6DOFCamera.Create(xPosition, yPosition, zPosition, iFoV,
  iNearPlane, iFarPlane: GLFloat; AnOwner: TComponent);
begin
  inherited Create(iFoV, iNearPlane, iFarPlane, AnOwner);
  Pos.x:=xPosition;
  Pos.y:=yPosition;
  Pos.z:=zPosition;
  fUp.x:=0;
  fUp.y:=1;
  fUp.z:=0;
  View.x:=0;
  View.y:=0;
  View.z:=-1;
  Right.x:=1;
  Right.y:=0;
  Right.z:=0;
  fCoV:=Pos+View;
end;

procedure TAsm6DOFCamera.RotateAboutView(angle: GLFloat);
begin
  Normalize(View);
  Rotate(Right,View,angle);
  Normalize(Right);
  // create orthogonal Up
  fUp:=CrossProduct(Right,View);
  Normalize(fUp);
end;

{ TAsmFirstPersonCamera }

constructor TAsmFirstPersonCamera.Create(xPosition, yPosition, zPosition,
  xCoV, yCoV, zCoV, iFoV, iNearPlane, iFarPlane: GLFLoat; AnOwner: TComponent);
begin
  inherited Create(iFoV, iNearPlane, iFarPlane, AnOwner);
  fUp.x:=0;
  fUp.y:=1;
  fUp.z:=0;
  SetCamera(xPosition, yPosition, zPosition, xCoV, yCoV, zCoV);
end;

procedure TAsmFirstPersonCamera.RotateAboutRight(angle: GLFloat);
begin
  if (incAngle+angle<90) and (incangle+angle>-90) then begin
    incAngle:=incAngle+angle;
    Normalize(Right);
    Rotate(fUp,Right,incAngle);
    // create orthogonal right
    View:=CrossProduct(fUp,Right);
    Normalize(View);
    fCoV:=Pos+View;
    fUp.x:=0;
    fUp.y:=1;
    fUp.z:=0;
  end;
end;

procedure TAsmFirstPersonCamera.SetCamera(xPosition, yPosition, zPosition,
  xCoV, yCoV, zCoV: GLFloat);
var tmp: TVector3;
begin
  Pos.x:=xPosition;
  Pos.y:=yPosition;
  Pos.z:=zPosition;
  tmp.x:=xCoV;
  tmp.y:=yCoV;
  tmp.z:=zCoV;
  View:=tmp-Pos;
  Normalize(View);
  fCoV:=Pos+View;
  // create orthogonal right
  Right:=CrossProduct(View,fUp);
  Normalize(Right);
  incAngle:=-((arccos(DotProduct(fUp,View))*(180/pi))-90);
end;

procedure TAsmFirstPersonCamera.SetCamera(const Position: TVector3; iCoV: TVector3);
begin
  Pos:=Position;
  View:=iCoV-Pos;
  Normalize(View);
  iCoV:=Pos+View;
  // create orthogonal right
  Right:=CrossProduct(View,fUp);
  Normalize(Right);
  incAngle:=-((arccos(DotProduct(fUp,View))*(180/pi))-90);
end;

{ TAsmScene }

procedure TAsmScene.SetActiveCamera(const AValue: TAsmCamera);
begin
  fActiveCamera:=AValue;
  UpdateViewport;
end;

constructor TAsmScene.Create(Context: TOpenGLControl; AnOwner: TComponent
  );
begin
  inherited Create(AnOwner);
  Area:=Context;
  ObjectList:=TFPList.Create;
  Lightlist:=TFPList.Create;
  TextList:=TFPList.Create;
  fActiveCamera:=nil;
  Init;
end;

destructor TAsmScene.Destroy;
begin
  ObjectList.free;
  LightList.free;
  TextList.free;
  //glDeleteBuffersARB(3, buffers);
  inherited Destroy;
end;

procedure TAsmScene.Init;
var clean: array [0..3] of GLFloat; light: GLenum;
begin
  // get basic gl info
  Vendor:=glGetString(GL_VENDOR);
  Renderer:=glGetString(GL_RENDERER);
  Version:=glGetString(GL_VERSION);
  Extensions:=glGetString(GL_EXTENSIONS);
  // glState Handling
  //GLStateMachine:=GLStatus.Create;
  //glEnable(GL_TEXTURE_2D);
  glClearColor(0.0,0.0,0.0,1.0);
  glClearDepth(1.0);
  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);
  glShadeModel(GL_SMOOTH);
  glFrontFace(GL_CCW);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glEnable(GL_NORMALIZE);
  glDisable(GL_DITHER);
  clean[0]:=0.0;
  clean[1]:=0.0;
  clean[2]:=0.0;
  clean[3]:=0;
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, clean);
  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR);
  //glEnable(GL_LIGHTING);
  glEnable(GL_COLOR_MATERIAL);
  //glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
  //glEnable(GL_LINE_SMOOTH);
  //glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST);
  //glEnable(GL_POLYGON_SMOOTH);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
  glEnableClientState(GL_VERTEX_ARRAY);
  //glEnableClientState(GL_INDEX_ARRAY);
  //glEnableClientState(GL_NORMAL_ARRAY);
  //glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  //glEnableClientState(GL_NV_VERTEX_ARRAY_RANGE2);
  Light:=GL_LIGHT0;
  // Resetting OpenGL Light
  //SetAmbientColor(0,0,0,0);
  glLightfv(Light, GL_AMBIENT, clean);
  //SetDiffuseColor(0,0,0,0);
  glLightfv(Light, GL_DIFFUSE, clean);
  //SetSpecularColor(0,0,0,0);
  glLightfv(Light, GL_SPECULAR, clean);
  //SetPosition(0,0,0);
  glLightfv(Light, GL_POSITION, clean);
  //SetAttenuation(0,0,0);
  glLightf(Light, GL_CONSTANT_ATTENUATION, 1);
  glLightf(Light, GL_LINEAR_ATTENUATION, 0);
  glLightf(Light, GL_QUADRATIC_ATTENUATION, 0);
  //SetCutoff(180);
  glLightf(Light, GL_SPOT_CUTOFF, 180);
  //SetExponent(0);
  glLightf(Light, GL_SPOT_EXPONENT, 0);
  glDisable(Light);
end;

procedure TAsmScene.SetBackgroundColor(red, green, blue: GLfloat);
begin
  glClearColor(red, green, blue,1);
end;

procedure TAsmScene.SetBackgroundColor(newColor: glRGBColor);
begin
  glClearColor(newColor[0],newColor[1],newColor[2],1);
end;

procedure TAsmScene.RenderCameraIndependentObjects(Objects: TFPList);
var i: integer; ActualObject: TAsmObject; ActualShader: TAsmShader;
begin
  for i:=0 to Objects.Count-1 do begin
    ActualObject:=TAsmObject(Objects.Items[i]);
    ActualShader:=TAsmShader(ShaderList.Items[ActualObject.Shader]);
    GLStateMachine.SetState(ActualShader.States);
    with ActualObject do begin
      glColor4fv(Color);
      glMaterialfv(GL_FRONT,GL_SPECULAR,SpecularColor);
      glMateriali(GL_FRONT,GL_SHININESS,Shininess);
    end;
    glViewport(0,0,Area.Width div 6, Area.Height div 6);
    glPushMatrix;
    glLoadIdentity;
    ActualShader.Draw(ActualObject);
    glPopMatrix;
    UpdateViewport;
  end;
end;

procedure TAsmScene.UpdateViewport;
begin
  if (Area<>nil) and (ActiveCamera<>nil) then begin
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity;
    gluPerspective(TAsmCamera(ActiveCamera).FoV, Area.Width/Area.Height,
     TAsmCamera(ActiveCamera).NearPlane, TAsmCamera(ActiveCamera).FarPlane);
    glMatrixMode (GL_MODELVIEW);
    glViewport(0, 0, Area.Width, Area.Height);
  end;
end;

procedure TAsmScene.GetFrustum;
begin
end;

procedure TAsmScene.RenderScene;
var i: Integer; VisibleObjects, CameraIndependentObjects, EnabledLights: TFPList;
begin
  // make actual GL context current
  Area.MakeCurrent;
  // sent actual GL context Width&Height to ActiveCamera
  TAsmCamera(ActiveCamera).Width:=Area.Width;
  TAsmCamera(ActiveCamera).Height:=Area.Height;
  // Render view of the active camera
  VisibleObjects:=TFPList.Create;
  CameraIndependentObjects:=TFPList.Create;
  EnabledLights:=TFPList.Create;
  for i:=0 to ObjectList.Count-1 do
    if TAsmDrawable(ObjectList.Items[i]).Visible then
      if not TAsmDrawable(ObjectList.Items[i]).CameraIndependent then
        VisibleObjects.Add(ObjectList.Items[i])
      else
        CameraIndependentObjects.Add(ObjectList.Items[i]);
  for i:=0 to LightList.Count-1 do
    if TAsmLight(LightList.Items[i]).Enabled then
      EnabledLights.Add(LightList.Items[i]);
  TAsmCamera(ActiveCamera).RenderView(VisibleObjects, EnabledLights, GLStateMachine);
  // Render camera independent 3D (no frustum culling)
  if CameraIndependentObjects.Count>0 then
    for i:=0 to CameraIndependentObjects.Count-1 do begin
      RenderCameraIndependentObjects(CameraIndependentObjects);
    end;
  // Render 2D including bitmap text
  glGetIntegerv(GL_VIEWPORT,Viewport);
  glGetDoublev(GL_MODELVIEW_MATRIX,mm);
  glGetDoublev(GL_PROJECTION_MATRIX,pm);
  if TextList.count>0 then begin
    // setting ortho matrix
    GLStateMachine.SetState([]);
    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix;
    glLoadIdentity;
    gluOrtho2D(0, Area.Width, 0, Area.Height);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix;
    glLoadIdentity;
    for i:=0 to TextList.count-1 do begin
      TAsmBitmapText(TextList.Items[i]).Draw(mm,pm,Viewport);
    end;
    // restore matrix
    glMatrixMode(GL_PROJECTION);
    glPopMatrix;
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix;
    glEnable(GL_DEPTH_TEST);
  end;
  VisibleObjects.Free;
  CameraIndependentObjects.Free;
  EnabledLights.Free;
  Area.SwapBuffers;
end;

{ TAsmRotationCamera }

procedure TAsmRotationCamera.SetAliasCoV(const AValue: PVector3);
begin
  fAliasCoV:=AValue;
end;

procedure TAsmRotationCamera.SetAlpha(const AValue: GLfloat);
begin
  if fAlpha=AValue then exit;
  fAlpha:=AValue;
  if fAlpha>360 then fAlpha:=fAlpha-360;
  if fAlpha<0 then fAlpha:=fAlpha+360;
end;

procedure TAsmRotationCamera.SetBeta(const AValue: GLfloat);
begin
  if fBeta=AValue then exit;
  fBeta:=AValue;
  if fBeta>180 then fBeta:=179.999;
  if fBeta<=0 then fBeta:=0.001;
end;

procedure TAsmRotationCamera.SetHeight(const AValue: GLFloat);
var dHeight: GLFloat;
begin
  dHeight:=AValue-fHeight;
  fHeight:=AValue;
  Pos.y:=Pos.y+dHeight;
end;

procedure TAsmRotationCamera.SetRadius(const AValue: GLFloat);
begin
  if AValue>0 then begin
    fRadius:=AValue;
    UpdatePosition;
  end;
end;

procedure TAsmRotationCamera.UpdatePosition;
var pi180: GLfloat;
begin
  pi180:=pi/180;
  Pos.x:=sin(Beta*pi180)*sin(Alpha*pi180)*fRadius+fCoV.x;
  Pos.y:=cos(Beta*pi180)*fRadius+fCoV.y;
  Pos.z:=sin(Beta*pi180)*cos(Alpha*pi180)*fRadius+fCoV.z;
end;

constructor TAsmRotationCamera.Create(iRadius, iHeight, xCoV, yCoV, zCoV, iFoV,
  iNearPlane, iFarPlane: GLFloat; AnOwner: TComponent);
begin
  inherited Create(iFoV, iNearPlane, iFarPlane, AnOwner);
  fRadius:=iRadius;
  fHeight:=iHeight;
  fAlpha:=0;
  fBeta:=80;
  fUp.x:=0;
  fUp.y:=1;
  fUp.z:=0;
  fCoV.x:=xCoV;
  fCoV.y:=yCoV;
  fCoV.z:=zCoV;
  UpdatePosition;
end;

procedure TAsmRotationCamera.RotateAlphaAboutCoV(angle: GLFloat);
begin
  Alpha:=Alpha+angle;
  UpdatePosition;
end;

procedure TAsmRotationCamera.RotateBetaAboutCov(angle: GLfloat);
begin
  Beta:=Beta+angle;
  UpdatePosition;
end;

procedure TAsmRotationCamera.SetCoV(xCoV, yCoV, zCoV: GLFloat);
begin
  AliasCoV:=nil;
  fCoV.x:=xCoV;
  fCoV.y:=yCoV;
  fCoV.z:=zCoV;
  UpdatePosition;
end;

procedure TAsmRotationCamera.SetCoV(const iCoV: TVector3);
begin
  AliasCoV:=nil;
  fCoV.x:=iCoV.x;
  fCoV.y:=iCoV.y;
  fCoV.z:=iCoV.z;
  UpdatePosition;
end;

procedure TAsmRotationCamera.SetCoV(pCoV: PVector3);
begin
  AliasCoV:=pCoV;
  UpdatePosition;
end;

procedure TAsmRotationCamera.RenderView(ObjectList, Lightlist: TFPList; var GLStateMachine: GLStatus);
begin
  if AliasCoV<>nil then fCoV:=AliasCoV^;
  UpdatePosition;
  inherited RenderView(ObjectList, Lightlist, GLStateMachine);
end;

{ TAsmFreeCamera }
procedure TAsmFreeCamera.RotateAboutUp(angle: GLFloat);
begin
  Normalize(fUp);
  Rotate(View,fUp,angle);
  Normalize(View);
  // create orthogonal Right
  Right:=CrossProduct(View,fUp);
  Normalize(Right);
  fCoV:=Pos+View;
end;

procedure TAsmFreeCamera.RotateAboutRight(angle: GLFloat);
begin
  Normalize(Right);
  Rotate(fUp,Right,Angle);
  Normalize(fUp);
  // create orthogonal View
  View:=CrossProduct(fUp,Right);
  Normalize(View);
  fCoV:=Pos+View;
end;

procedure TAsmFreeCamera.MoveAlongView(speed: GLFloat);
begin
  Pos:=Pos+View*speed;
  fCoV:=Pos+View;
end;

procedure TAsmFreeCamera.MoveAlongUp(speed: GLFloat);
begin
  Pos:=Pos+fUp*speed;
  fCoV:=Pos+View;
end;

procedure TAsmFreeCamera.MoveAlongRight(speed: GLFloat);
begin
  Pos:=Pos+Right*speed;
  fCoV:=Pos+View;
end;

{ TAsmBitmapText }

procedure TAsmBitmapText.SetText(const AValue: String);
var i: byte;
begin
  fText:=AValue;
  for i:=1 to length(fText) do
    fText[i]:=chr(ord(fText[i])-32);
end;

constructor TAsmBitmapText.Create(Fontname: PGChar; AnOwner: TComponent);
var Font: PGdkFont; FontID: gint;
begin
  inherited Create(AnOwner);
  // load font and get id
  Font:=gdk_font_load(Fontname);
  FontID:=gdk_font_id(Font);
  // build display list from fontid
  Base:=glGenLists(96);
  glXUseXFont(FontID, 32, 96, Base);
end;

destructor TAsmBitmapText.Destroy;
begin
  glDeleteLists(Base, 96);
  inherited Destroy;
end;

procedure TAsmBitmapText.SetColor(red, green, blue: GLfloat);
begin
  fColor[0]:=red;
  fColor[1]:=green;
  fColor[2]:=blue;
end;

procedure TAsmBitmapText.Draw(const ModelMatrix, ProjMatrix: TGLUMatrixD; const Viewport: TGLUViewport);
var xp, yp, zp: GLDouble;
begin
  // draw text
  glColor3fv(fColor);
  if CoordinateIs3D then begin
    if gluProject(X,Y,Z,ModelMatrix,ProjMatrix,Viewport,xp,yp,zp)=GL_TRUE then begin
      glRasterPos3d(xp,yp,zp);
    end;
  end else glRasterPos2d(X, Y);
  glPushAttrib(GL_LIST_BIT);
  glListBase(Base);
  glCallLists(length(Text), GL_UNSIGNED_BYTE, pchar(Text));
  glPopAttrib;
end;

end.
