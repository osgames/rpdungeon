{
 *****************************************************************************
 *                                                                           *
 *  This source is free software. See the file COPYING.modifiedLGPL,         *
 *  included in this distribution, for details about the copyright.          *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************

 dyn_arrays.pp
 Author: Reimar Grabowski (2003)
}

unit dyn_arrays;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, nvGL, Vectors;

type
  Matrix3X3 = array[0..8] of GLfloat;

  // Dynamic Arrays
  ArrayOfInteger = object
  private
    Capacity: cardinal;
  public
    Data: ^Integer;
    Length: cardinal;
    procedure Add(Value: integer);
    destructor Free;
  end;

  { ArrayOfGLuint }

  ArrayOfGLuint = object
  private
    Capacity: cardinal;
  public
    Data: ^GLuint;
    Length: cardinal;
    procedure Add(Value: GLuint);
    procedure SetCapacity(Value: GLuint);
    constructor Create;
    procedure Remove(index: cardinal);
    destructor Free;
  end;

  ArrayOfGLfloat = object
  private
    Capacity: cardinal;
  public
    Data: ^GLfloat;
    Length: cardinal;
    procedure Add(Value: GLfloat);
    destructor Free;
  end;

  ArrayOfTVector2 = object
  private
    Capacity: cardinal;
  public
    Data: ^TVector2;
    Length: cardinal;
    procedure Add(const Value: TVector2); // mattias
    procedure Add(xValue, yValue: GLfloat);
    destructor Free;
  end;

  { ArrayOfTVector3 }

  ArrayOfTVector3 = object
  private
    Capacity: cardinal;
  public
    Data: ^TVector3;
    Length: cardinal;
    procedure Add(const Value: TVector3); // mattias
    procedure Add(xValue, yValue, zValue: GLfloat);
    constructor Create;
    destructor Free;
  end;
  
  ArrayOfMatrix3X3 = object
    Data: ^Matrix3X3;
    Length: cardinal;
    procedure SetSize(Size: integer);
    destructor free;
  end;

  V3Rec = record
    x, y, z: GLfloat;
    Position: GLuint;
  end;

  V3_2Rec = record
    x, y, z, x2, y2: GLFloat;
    Position: GLuint;
  end;

  V3_3Rec = record
    x, y, z, x2, y2, z2: GLfloat;
    Position: GLuint;
  end;

  V3_3_2Rec = record
    x, y, z, x2, y2, z2, x3, y3: GLfloat;
    Position: GLuint;
  end;

  PV3Rec = ^V3Rec;
  PV3_2Rec = ^V3_2Rec;
  PV3_3Rec = ^V3_3Rec;
  PV3_3_2Rec = ^V3_3_2Rec;

  ArrayOfV3Rec = object
  private
    Capacity: cardinal;
  public
    Data: ^V3Rec;
    Length: cardinal;
    procedure Add(const Value: V3Rec);
    destructor Free;
  end;

  ArrayOfV3_2Rec = object
  private
    Capacity: cardinal;
  public
    Data: ^V3_2Rec;
    Length: cardinal;
    procedure Add(const Value: V3_2Rec);
    destructor Free;
  end;

  ArrayOfV3_3Rec = object
  private
    Capacity: cardinal;
  public
    Data: ^V3_3Rec;
    Length: cardinal;
    procedure Add(const Value: V3_3Rec);
    destructor Free;
  end;
  
  ArrayOfV3_3_2Rec = object
  private
    Capacity: cardinal;
  public
    Data: PV3_3_2Rec;
    Length: cardinal;
    procedure Add(const Value: V3_3_2Rec);
    destructor Free;
  end;

implementation

{ ArrayOfInteger }

procedure ArrayOfinteger.Add(Value: integer);
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(integer)*Capacity)
    else
      GetMem(data, sizeof(integer)*Capacity);
  end;
  Data[Length-1]:=Value;
end;

destructor ArrayOfinteger.Free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

{ ArrayOfGLFloat }

procedure ArrayOfGLfloat.Add(Value: GLfloat);
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(GLFloat)*Capacity)
    else
      GetMem(data, sizeof(GLFloat)*Capacity);
  end;
  Data[Length-1]:=Value;
end;

destructor ArrayOfGLfloat.Free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

{ ArrayOfGLuint }

procedure ArrayOfGLuint.Add(Value: GLuint);
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(GLuint)*Capacity)
    else
      GetMem(data, sizeof(GLuint)*Capacity);
  end;
  Data[Length-1]:=Value;
end;

procedure ArrayOfGLuint.SetCapacity(Value: GLuint);
begin
  Capacity:=Value;
  ReallocMem(data, sizeof(GLuint)*Capacity);
end;

constructor ArrayOfGLuint.Create;
begin
  Length:=0;
  Data:=nil;
end;

procedure ArrayOfGLuint.Remove(index: cardinal);
begin
  if Length>1 then begin
    Move(data[index+1],data[index],SizeOf(GLuint)*(Length-1));
    Length:=Length-1;
  end else
    Free;
end;

destructor ArrayOfGLuint.Free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

{ ArrayOfTVector2 }

procedure ArrayOfTVector2.Add(const Value: TVector2);
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(TVector2)*Capacity)
    else
      GetMem(data, sizeof(TVector2)*Capacity);
  end;
  Data[Length-1]:=Value;
end;

procedure ArrayOfTVector2.Add(xValue, yValue: GLfloat);
var tmp: TVector2;
begin
  if data=nil then begin
    //writeln('data is NIL');
    length:=0; Capacity:=0;
  end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(TVector2)*Capacity)
    else
      GetMem(data, sizeof(TVector2)*Capacity);
  end;
  tmp.x:=xValue;
  tmp.y:=yValue;
  Data[Length-1]:=tmp;
end;

destructor ArrayOfTVector2.Free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

{ ArrayOfTVector }

procedure ArrayOfTVector3.Add(const Value: TVector3);
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(TVector3)*Capacity)
    else
      GetMem(data, sizeof(TVector3)*Capacity);
  end;
  Data[Length-1]:=Value;
end;

procedure ArrayOfTVector3.Add(xValue, yValue, zValue: GLfloat);
var tmp: TVector3;
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(TVector3)*Capacity)
    else
      GetMem(data, sizeof(TVector3)*Capacity);
  end;
  tmp.x:=xValue;
  tmp.y:=yValue;
  tmp.z:=zValue;
  Data[Length-1]:=tmp;
end;

constructor ArrayOfTVector3.Create;
begin
  Length:=0;
  Data:=nil;
end;

destructor ArrayOfTVector3.Free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

{ ArrayOfV3Rec }

procedure ArrayOfV3Rec.Add(const Value: V3Rec);
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(V3Rec)*Capacity)
    else
      GetMem(data, sizeof(V3Rec)*Capacity);
  end;
  Data[Length-1]:=Value;
end;

destructor ArrayOfV3Rec.Free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

{ ArrayOfV3_2Rec }

procedure ArrayOfV3_2Rec.Add(const Value: V3_2Rec);
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(V3_2Rec)*Capacity)
    else
      GetMem(data, sizeof(V3_2Rec)*Capacity);
  end;
  Data[Length-1]:=Value;
end;

destructor ArrayOfV3_2Rec.Free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

{ ArrayOfV3_3Rec }

procedure ArrayOfV3_3Rec.Add(const Value: V3_3Rec);
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(V3_3Rec)*Capacity)
    else
      GetMem(data, sizeof(V3_3Rec)*Capacity);
  end;
  Data[Length-1]:=Value;
end;

destructor ArrayOfV3_3Rec.Free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

{ ArrayOfV3_3_2Rec }

procedure ArrayOfV3_3_2Rec.Add(const Value: V3_3_2Rec);
begin
  if data=nil then begin length:=0; Capacity:=0; end;
  inc(Length);
  if Length>Capacity then begin
    Capacity:=Length*2;
    if data<>nil then
      ReallocMem(data, sizeof(V3_3_2Rec)*Capacity)
    else
      GetMem(data, sizeof(V3_3_2Rec)*Capacity);
  end;
  Data[Length-1]:=Value;
end;

destructor ArrayOfV3_3_2Rec.Free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

{ ArrayOfMatrix3X3 }

procedure ArrayOfMatrix3X3.SetSize(Size: integer);
begin
  if data<>nil then
    ReallocMem(data, sizeof(Matrix3X3)*Size)
  else
    GetMem(data, sizeof(Matrix3X3)*Size);
  Length:=Size;
end;

destructor ArrayOfMatrix3X3.free;
begin
  if data<>nil then begin
    FreeMem(data);
    data:=nil;
    Length:=0;
  end;
end;

end.

