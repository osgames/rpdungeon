{
 ***************************************************************************
 *                                                                         *
 *   This source is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This code is distributed in the hope that it will be useful, but      *
 *   WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   General Public License for more details.                              *
 *                                                                         *
 *   A copy of the GNU General Public License is available on the World    *
 *   Wide Web at <http://www.gnu.org/copyleft/gpl.html>. You can also      *
 *   obtain it by writing to the Free Software Foundation,                 *
 *   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.        *
 *                                                                         *
 ***************************************************************************

 asmodayeditor.pp
 Author: Reimar Grabowski (2003-2005)
}
unit asmodayeditorform;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs,
  Menus, StdCtrls, Buttons, vectors, nvgl, ExtCtrls, ComCtrls,
  MaskEdit, sgiglu, asmoday, asmtypes, asmshaders, asmutils, ExtDlgs,
  FPImage, FPReadPNG, FPReadJPEG, LazJPEG, asmmeshmanagerform, RTTICtrls,
  lcltype, math, OpenGLContext;

type
  GLint4 = array [0..3] of GLint;

type
  TActions = (Rotate, Translate, Scale, Select);
  TAction = Set of TActions;

type
  TAsmEditorObject = class(TComponent)
  private
    AsmObject: TAsmObject;
    AsmLight: TAsmLight;
    Color: glRGBAColor;
    EulerAngles: TVector3;
    SpecularColor: glRGBColor;
  public
    constructor Create(AnOwner: TComponent); override;
    destructor Destroy; override;
  end;
  
  TAsmEditorSelectionMesh = class(TAsmMesh)
  public
    procedure CreateSelection;
  end;


type

  { TAsmEditorMain }

  TAsmEditorMain = class(TForm)
    ObjectResetScaleButton: TButton;
    BackgroundColorButton: TColorButton;
    OpenGLControl1: TOpenGLControl;
    SelectButton: TButton;
    LightTypeComboBox: TComboBox;
    LightAmbientColorButton: TColorButton;
    LightDiffuseColorButton: TColorButton;
    LightSpecularColorButton: TColorButton;
    GroupBox2: TGroupBox;
    BackgroudLabel: TLabel;
    LightAmbientLabel: TLabel;
    LightDiffuseLabel: TLabel;
    LightSpecularLabel: TLabel;
    LightComboBox: TComboBox;
    ViewMenuItem: TMenuItem;
    ShowGridMenuItem: TMenuItem;
    NewLightButton: TButton;
    TextureTypeComboBox: TComboBox;
    GroupBox1: TGroupBox;
    MeshManagerMenuItem: TMenuItem;
    ScenePage: TPage;
    WindowsMenuItem: TMenuItem;
    ObjectResetTranslationButton: TButton;
    Image1: TIMAGE;
    NewObjectMenuitem: TMENUITEM;
    QuitMenuitem: TMENUITEM;
    ObjectColorbutton: TCOLORBUTTON;
    ObjectSpecColorbutton: TCOLORBUTTON;
    Openpicturedialog1: TOPENPICTUREDIALOG;
    Panel1: TPANEL;
    ShaderCombobox: TCOMBOBOX;
    ObjectTextureGroupbox: TGROUPBOX;
    ObjectColorGroupbox: TGROUPBOX;
    Groupbox3: TGROUPBOX;
    ObjectColorLabel: TLABEL;
    ObjectSpecColorLabel: TLABEL;
    ObjectShininessLabel: TLABEL;
    Maskedit1: TMASKEDIT;
    ObjectShininessMaskedit: TMASKEDIT;
    ObjectResetRotationButton: TBUTTON;
    ObjectResetMatrixButton: TBUTTON;
    ObjectResetGroupbox: TGROUPBOX;
    ObjectPositionGroupbox: TGROUPBOX;
    ObjectRotationGroupbox: TGROUPBOX;
    ObjectCoRGroupbox: TGROUPBOX;
    ObjectScaleGroupbox: TGROUPBOX;
    ObjectPageLabel: TLABEL;
    ObjectPositionXLabel: TLABEL;
    ObjectPositionYLabel: TLABEL;
    ObjectPositionZLabel: TLABEL;
    ObjectPitchLabel: TLABEL;
    ObjectYawLabel: TLABEL;
    ObjectRollLabel: TLABEL;
    ObjectCoRXLabel: TLABEL;
    ObjectCoRYLabel: TLABEL;
    ObjectCoRZLabel: TLABEL;
    ObjectScaleXLabel: TLABEL;
    ObjectScaleYLabel: TLABEL;
    ObjectScaleZLabel: TLABEL;
    ObjectPositionXMaskedit: TMASKEDIT;
    ObjectPositionYMaskedit: TMASKEDIT;
    ObjectPositionZMaskedit: TMASKEDIT;
    ObjectPitchMaskedit: TMASKEDIT;
    ObjectYawMaskedit: TMASKEDIT;
    ObjectRollMaskedit: TMASKEDIT;
    ObjectCoRXMaskedit: TMASKEDIT;
    ObjectCoRYMaskedit: TMASKEDIT;
    ObjectCoRZMaskedit: TMASKEDIT;
    ObjectScaleXMaskedit: TMASKEDIT;
    ObjectScaleYMaskedit: TMASKEDIT;
    ObjectScaleZMaskedit: TMASKEDIT;
    ObjectCombobox: TCOMBOBOX;
    ObjectTransformPage: TPAGE;
    ObjectNotebook: TNOTEBOOK;
    LightPage: TPAGE;
    Cameras: TPAGE;
    ObjectPropertiesPage: TPAGE;
    SpecColorButton: TBUTTON;
    ColorButton: TBUTTON;
    Mainmenu1: TMAINMENU;
    FileMenuItem: TMENUITEM;
    LoadObjectMenuItem: TMENUITEM;
    NewSceneMenuItem: TMENUITEM;
    MainNotebook: TNOTEBOOK;
    ObjectPage: TPAGE;
    Colordialog1: TCOLORDIALOG;
    Opendialog1: TOPENDIALOG;
    Statusbar1: TSTATUSBAR;
    TranslateTogglebox: TTOGGLEBOX;
    RotateTogglebox: TTOGGLEBOX;
    ScaleTogglebox: TTOGGLEBOX;
    ObjectShininessTrackbar: TTRACKBAR;
    ObjectPageUpDown: TUPDOWN;
    TextureUpDown: TUpDown;
    procedure AssignMesh(AssignedMesh: TAsmMesh);
    procedure BackgroundColorButtonColorChanged(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Gtkglareacontrol1KeyPress(Sender: TObject; var Key: Char);
    procedure Image1CLICK(Sender: TObject);
    procedure LightAmbientColorButtonColorChanged(Sender: TObject);
    procedure LightComboBoxChange(Sender: TObject);
    procedure LightDiffuseColorButtonColorChanged(Sender: TObject);
    procedure LightSpecularColorButtonColorChanged(Sender: TObject);
    procedure LightTypeComboBoxChange(Sender: TObject);
    procedure MeshManagerMenuItemClick(Sender: TObject);
    procedure NewLightButtonClick(Sender: TObject);
    procedure NewObjectMenuitemClick(Sender: TObject);
    procedure ObjectColorbuttonCOLORCHANGED(Sender: TObject);
    procedure ObjectResetScaleButtonClick(Sender: TObject);
    procedure ObjectResetTranslationButtonClick(Sender: TObject);
    procedure ObjectSpecColorbuttonCOLORCHANGED(Sender: TObject);
    procedure Form1CLOSE(Sender: TObject; var CloseAction: TCloseAction);
    procedure Form1CREATE(Sender: TObject);
    procedure OpenGLControl1KeyPress(Sender: TObject; var Key: char);
    procedure OpenGLControl1MOUSEDOWN(Sender: TOBject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OpenGLControl1MOUSEENTER(Sender: TObject);
    procedure OpenGLControl1MOUSELEAVE(Sender: TObject);
    procedure OpenGLControl1MOUSEMOVE(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure OpenGLControl1MOUSEUP(Sender: TOBject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OpenGLControl1PAINT(Sender: TObject);
    procedure OpenGLControl1RESIZE(Sender: TObject);
    procedure LoadObjectMenuItemCLICK(Sender: TObject);
    procedure ObjectComboboxCHANGE(Sender: TObject);
    procedure ObjectPitchMaskeditEXIT(Sender: TObject);
    procedure ObjectPositionXMaskeditEXIT(Sender: TObject);
    procedure ObjectPositionXMaskeditKEYPRESS(Sender: TObject; var Key: Char);
    procedure ObjectPositionYMaskeditEXIT(Sender: TObject);
    procedure ObjectPositionZMaskeditEXIT(Sender: TObject);
    procedure ObjectResetMatrixButtonCLICK(Sender: TObject);
    procedure ObjectResetRotationButtonCLICK(Sender: TObject);
    procedure ObjectRollMaskeditEXIT(Sender: TObject);
    procedure ObjectScaleXMaskeditEXIT(Sender: TObject);
    procedure ObjectScaleYMaskeditEXIT(Sender: TObject);
    procedure ObjectScaleZMaskeditEXIT(Sender: TObject);
    procedure ObjectShininessTrackbarCHANGE(Sender: TObject);
    procedure ObjectYawMaskeditEXIT(Sender: TObject);
    procedure QuitMenuitemClick(Sender: TObject);
    procedure RotateToggleboxCLICK(Sender: TObject);
    procedure ScaleToggleboxCLICK(Sender: TObject);
    procedure SelectButtonClick(Sender: TObject);
    procedure ShaderComboboxCLOSEUP(Sender: TObject);
    procedure TranslateToggleboxCLICK(Sender: TObject);
    procedure Updown1CLICK(Sender: TObject; Button: TUDBtnType);
    procedure ShowGridMenuItemCLICK(Sender: TObject);
  private
    fSelectedObject: TAsmEditorObject;
    procedure SetSelectedObject(const AValue: TAsmEditorObject);
  private
    { private declarations }
    Camera: TAsmRotationCamera;
    CS, Floor, Sphere: TAsmObject;
    LightList: TFPList;
    ObjectList: TFPList;
    ObjScale: TVector3;
    oldLength: GLfloat;
    oldScale: TVector3;
    oldx, oldy: integer;
    P1, P2, Center, oldCoR: TVector3;
    PlaneDistance: GLfloat;
    Scene: TAsmScene;
    SceneInitialized: boolean;
    property SelectedObject: TAsmEditorObject read fSelectedObject write SetSelectedObject;
    SelectedObjectChanged: boolean;
    SelectionObject: TAsmObject;
    SelectionMesh: TAsmEditorSelectionMesh;
    SelectedX, SelectedY: GLdouble;
    TransformAct: TAction;
    procedure CreateRayFromXY(WinX, WinY: GLdouble; var RayOrigin, Ray: TVector3);
    function NumericalInput(var NewValue: GLfloat; var InputField: string): boolean;
    procedure InitScene;
    procedure ShowObjectCoR;
    procedure ShowObjectPosition;
    procedure ShowObjectRotation;
    procedure ShowObjectScale;
    procedure ShowObjectSettings;
    procedure ShowObjectDefaults;
  public
    { public declarations }
  end; 

var
  AsmEditorMain: TAsmEditorMain;
  frp: array [0..7,0..2] of GLdouble;
  rMatrix: TMatrix4x4;
  // debugging
  ScalePoint1, ScalePoint2: TVector3;
  TestR0,TestRd: TVector3;

implementation

{type
  TFPSRecord = record
      LastWriteTime: integer;
      FrameCount: integer;
    end;

var
  FPSRecord: TFPSRecord;
  fps: string;

procedure FramesPerSecond(var AFPSRecord: TFPSRecord);
var
  CurrentTime, DiffTime: integer;
  hour, minutes, secs, msecs, usecs: word;
begin
  if AFPSRecord.FrameCount<0 then AFPSRecord.FrameCount:=0;
  inc(AFPSRecord.FrameCount);
  GetTime(hour, minutes, secs, msecs, usecs);
  // secs * 1000 + msecs
  CurrentTime:=secs;
  CurrentTime:=((((CurrentTime shl 5)-CurrentTime) shl 2)+CurrentTime) shl 3;
  inc(CurrentTime,msecs);
  if AFPSRecord.LastWriteTime=0 then
    AFPSRecord.LastWriteTime:=CurrentTime;
  // calculate time since last call:
  DiffTime:=CurrentTime-AFPSRecord.LastWriteTime;
  // if the minutes changed, the seconds restarts:
  if (DiffTime<0) then inc(DiffTime,60000);

  if DiffTime=0 then begin
    // no time difference
  end else if DiffTime<0 then begin
    // more than a minute
    writeln('FPS: too slow');
    AFPSRecord.LastWriteTime:=CurrentTime;
    AFPSRecord.FrameCount:=0;
  end else if DiffTime>=500 then begin
    // at least half a second past -> write fps
    fps:=IntToStr((AFPSRecord.FrameCount shl 10) div DiffTime);
    //:=(AFPSRecord.FrameCount shl 10) div DiffTime;
    AFPSRecord.LastWriteTime:=CurrentTime;
    AFPSRecord.FrameCount:=0;
  end;
end;}

function PointOnVirtualSphere(X, Y, ObjX, ObjY: GLdouble; const Viewport: GLint4): TVector3;
var v: TVector3; d: GLfloat;
begin
  if x>ObjX then
    v.x:=(X-ObjX)/(Viewport[2]-ObjX)
  else
    v.x:=(X-ObjX)/ObjX;
  Y:=Viewport[3]-Y;
  if y>ObjY then
    v.y:=(Y-ObjY)/(Viewport[3]-ObjY)
  else
    v.y:=(Y-ObjY)/ObjY;
  v.z:=0;
  d:=vLength(v);
  if d>1 then d:=1;
  v.z:=sqrt(1-d*d);
  Normalize(v);
  PointOnVirtualSphere:=v;
end;

procedure GetFrustumPoints(s: TAsmScene);
begin
  gluUnProject(0, s.viewport[3], 0, S.mm,S.pm,S.viewport,frp[0,0],frp[0,1],frp[0,2]);
  gluUnProject(0, 0, 0, S.mm,S.pm,S.viewport,frp[1,0],frp[1,1],frp[1,2]);
  gluUnProject(s.viewport[2], 0, 0, S.mm,S.pm,S.viewport,frp[2,0],frp[2,1],frp[2,2]);
  gluUnProject(s.viewport[2], s.viewport[3], 0, S.mm,S.pm,S.viewport,frp[3,0],frp[3,1],frp[3,2]);
  gluUnProject(0, s.viewport[3], 1, S.mm,S.pm,S.viewport,frp[4,0],frp[4,1],frp[4,2]);
  gluUnProject(0, 0, 1, S.mm,S.pm,S.viewport,frp[5,0],frp[5,1],frp[5,2]);
  gluUnProject(s.viewport[2], 0, 1, S.mm,S.pm,S.viewport,frp[6,0],frp[6,1],frp[6,2]);
  gluUnProject(s.viewport[2], s.viewport[3], 1, S.mm,S.pm,S.viewport,frp[7,0],frp[7,1],frp[7,2]);
  // debugging
  //for i:=3 to 7 do frp[i,2]:=-frp[i,2];
end;

{ TAsmEditorMain }

procedure TAsmEditorMain.Form1CREATE(Sender: TObject);
begin
  if Sender=nil then;
  writeln('Editor Form Creation');
  //Application.CaptureExceptions:=false;
  LightList:=TFPList.Create;
  LightCombobox.Items.Add('none');
  ObjectList:=TFPList.Create;
  ObjectCombobox.Items.Add('none');
  Scene:=TAsmScene.Create(OpenGLControl1, Self);
  ShaderCombobox.Items.Add('NoLighting');
  ShaderCombobox.Items.Add('NoLightingWireframe');
  ShaderCombobox.Items.Add('NoLightingTexture');
  ShaderCombobox.Items.Add('NoLightingColor');
  ShaderCombobox.Items.Add('Lighting');
  ShaderCombobox.Items.Add('LightingWireframe');
  ShaderCombobox.Items.Add('LightingTexture');
  ShaderCombobox.Items.Add('LightingNormal');
end;

procedure TAsmEditorMain.ObjectSpecColorbuttonCOLORCHANGED(Sender: TObject);
var RGBColor: glRGBColor;
begin
  if Sender=nil then;
  if SelectedObject<>nil then begin
    TColorToglRGB(ObjectSpecColorbutton.ButtonColor,RGBColor[0],RGBColor[1],
     RGBColor[2]);
    SelectedObject.AsmObject.SetSpecularColor(RGBColor);
  end;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectColorbuttonCOLORCHANGED(Sender: TObject);
var RGBAColor: glRGBAColor;
begin
  if Sender=nil then;
  if SelectedObject<>nil then begin
    TColorToglRGB(ObjectColorbutton.ButtonColor,RGBAColor[0],RGBAColor[1],
     RGBAColor[2]);
    RGBAColor[3]:=1;
    SelectedObject.AsmObject.SetColor(RGBAColor);
  end;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectResetScaleButtonClick(Sender: TObject);
begin
  if Sender=nil then;
  if SelectedObject<>nil then begin
    SelectedObject.AsmObject.ResetScale;
    SelectedObject.EulerAngles:=SelectedObject.AsmObject.GetEulerAngles;
    ShowObjectScale;
    //Sphere.ResetTranslation;
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmEditorMain.ObjectResetTranslationButtonClick(Sender: TObject);
begin
  if Sender=nil then;
  if SelectedObject<>nil then begin
    SelectedObject.AsmObject.ResetTranslation;
    SelectedObject.EulerAngles:=SelectedObject.AsmObject.GetEulerAngles;
    ShowObjectPosition;
    //Sphere.ResetTranslation;
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmEditorMain.AssignMesh(AssignedMesh: TAsmMesh);
var Object1: TAsmEditorObject;
begin
  writeln('new model');
  SelectedObjectChanged:=true;
  if (SelectedObject<>nil) and (SelectedObject.AsmLight=nil) then
    SelectedObject.AsmObject.Mesh:=AssignedMesh
  else begin
    Object1:=TAsmEditorObject.Create(self);
    Object1.AsmObject.Mesh:=AssignedMesh;
    Object1.AsmObject.Shader:=NoLightingWireframe;
    ShaderCombobox.ItemIndex:=NoLightingWireframe;
    Object1.AsmObject.SetColor(0,0,0,1);
    Object1.AsmObject.visible:=true;
    Object1.Name:='Object'+IntToStr(ObjectList.Count+1);
    ObjectList.Add(Object1);
    ObjectCombobox.Items.Add(Object1.Name);
    ObjectCombobox.ItemIndex:=ObjectCombobox.Items.Count-1;
    Scene.ObjectList.Add(Object1.AsmObject);
    SelectedObject:=TAsmEditorObject(ObjectList.Items[ObjectList.Count-1]);
    Object1:=nil;
  end;
  AsmMeshManager.Hide;
  ShowObjectCoR;
  ShowObjectPosition;
  ShowObjectRotation;
  ShowObjectScale;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.BackgroundColorButtonColorChanged(Sender: TObject);
var RGBColor: glRGBColor;
begin
  if Sender=nil then;
  TColorToglRGB(BackgroundColorbutton.ButtonColor,RGBColor[0],RGBColor[1],
   RGBColor[2]);
  Scene.SetBackgroundColor(RGBColor);
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.Button1Click(Sender: TObject);
begin
  if Sender=nil then;
  if SelectedObject<>nil then begin
    SelectedObject.AsmObject.ResetRotation;
    SelectedObject.EulerAngles:=SelectedObject.AsmObject.GetEulerAngles;
    ShowObjectRotation;
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmEditorMain.Gtkglareacontrol1KeyPress(Sender: TObject;
  var Key: Char);
begin

end;

procedure TAsmEditorMain.OpenGLControl1KeyPress(Sender: TObject;
  var Key: Char);
begin
  //if Key=#32 then TransformAct:=TransformAct+[Select];
  writeln('key pressed');
end;

procedure TAsmEditorMain.Image1CLICK(Sender: TObject);
var
  Texture: TAsmTexture;
  FileStream: TFileStream;
  MemStream: TMemoryStream;
  ReaderClass: TFPCustomImageReaderClass;
  Reader: TFPCustomImageReader;

begin
  if Sender=nil then;
  if (SelectedObject<>nil) and (ShaderSupported(2,SelectedObject.AsmObject
   .Mesh)) and (Openpicturedialog1.Execute) then begin
    // open file
    try
      ReaderClass:=GetFPImageReaderForFileExtension(ExtractFileExt(Openpicturedialog1.Filename));
      FileStream:=TFileStream.Create(Openpicturedialog1.Filename,fmOpenRead);
    except
      ShowMessage('Error opening file');
    end;
    MemStream:=nil;
    try
      // read file into mem
      MemStream:=TMemoryStream.Create;
      MemStream.CopyFrom(FileStream,FileStream.Size);
      // convert png stream to texture
      MemStream.Position:=0;
      Texture:=TAsmTexture.Create(0,0);
      if ReaderClass<>nil then begin
        Reader:=ReaderClass.Create;
        try
          Texture.LoadFromStream(MemStream,Reader);
          Texture.Bind;
          SelectedObject.AsmObject.Texture:=Texture;
          Image1.Picture.Assign(SelectedObject.AsmObject.Texture);
          Image1.Proportional:=true;
          Panel1.Text:='';
        except
          Texture.Free;
          ShowMessage('Error reading file');
        end;
      end;
    finally
      FileStream.Free;
      MemStream.Free;
      Reader.Free;
    end;
  end;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.LightAmbientColorButtonColorChanged(Sender: TObject);
var RGBAColor: glRGBAColor;
begin
  if Sender=nil then;
  if (SelectedObject<>nil) and (SelectedObject.AsmLight<>nil) then begin
    TColorToglRGB(LightAmbientColorbutton.ButtonColor,RGBAColor[0],RGBAColor[1],
     RGBAColor[2]);
    RGBAColor[3]:=1;
    SelectedObject.AsmLight.SetAmbientColor(RGBAColor);
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmEditorMain.LightComboBoxChange(Sender: TObject);
begin
  if Sender=nil then;
  if LightCombobox.ItemIndex<>0 then begin
    ObjectCombobox.ItemIndex:=0;
    SelectedObject:=TAsmEditorObject(LightList.Items[LightCombobox.ItemIndex-1]);
  end else
    SelectedObject:=nil;
  ShowObjectDefaults;
  // debugging
  if SelectedObject<>nil then
    writeln('Selected: ',SelectedObject.Name)
  else
    writeln('Selected: ','none');
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.LightDiffuseColorButtonColorChanged(Sender: TObject);
var RGBAColor: glRGBAColor;
begin
  if Sender=nil then;
  if (SelectedObject<>nil) and (SelectedObject.AsmLight<>nil) then begin
    TColorToglRGB(LightDiffuseColorbutton.ButtonColor,RGBAColor[0],RGBAColor[1],
     RGBAColor[2]);
    RGBAColor[3]:=1;
    SelectedObject.AsmLight.SetDiffuseColor(RGBAColor);
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmEditorMain.LightSpecularColorButtonColorChanged(Sender: TObject);
var RGBAColor: glRGBAColor;
begin
  if Sender=nil then;
  if (SelectedObject<>nil) and (SelectedObject.AsmLight<>nil) then begin
    TColorToglRGB(LightSpecularColorbutton.ButtonColor,RGBAColor[0],RGBAColor[1],
     RGBAColor[2]);
    RGBAColor[3]:=1;
    SelectedObject.AsmLight.SetSpecularColor(RGBAColor);
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmEditorMain.LightTypeComboBoxChange(Sender: TObject);
var Light: TAsmLight; Index: Integer;
begin
  if Sender<>nil then;
  if (SelectedObject<>nil) then
    with SelectedObject do begin
      if AsmLight<>nil then begin
        Index:=LightTypeComboBox.ItemIndex;
        if ((AsmLight is TAsmDirectionalLight) and (Index<>0))
         or ((AsmLight is TAsmPositionalLight) and (Index<>1)) then begin
          case Index of
            0: Light:=TAsmDirectionalLight.Create(self);
            1: Light:=TAsmPositionalLight.Create(self);
          end;
          Light.Name:=AsmLight.Name;
          Light.SetAmbientColor(AsmLight.AmbientColor);
          Light.SetDiffuseColor(AsmLight.DiffuseColor);
          Light.SetSpecularColor(AsmLight.SpecularColor);
          Light.Enabled:=AsmLight.Enabled;
          LightList.Delete(LightComboBox.ItemIndex-1);
          AsmLight.Free;
          AsmLight:=Light;
          AsmLight.SetPosition(@AsmObject.Position);
          LightList.Insert(LightComboBox.ItemIndex-1,SelectedObject);
          Scene.LightList.Delete(LightComboBox.ItemIndex-1);
          Scene.Lightlist.Insert(LightComboBox.ItemIndex-1,AsmLight);
          Light:=nil;
          OpenGLControl1.Invalidate;
        end;
      end;
    end;
end;

procedure TAsmEditorMain.MeshManagerMenuItemClick(Sender: TObject);
begin
  if Sender=nil then;
  AsmMeshManager.Show;
  AsmMeshManager.BringToFront;
end;

procedure TAsmEditorMain.NewLightButtonClick(Sender: TObject);
var LightObject: TAsmEditorObject; LightMesh: TAsmMesh; Light: TAsmLight;
begin
  if Sender=nil then;
  LightObject:=TAsmEditorObject.Create(self);
  // set up light model
  LightMesh:=TAsmMesh.Create;
  LightMesh.CreateSphere(1,10,10);
  with LightObject.AsmObject do begin
    Mesh:=LightMesh;
    SetColor(1,1,0,1);
    Visible:=true;
    Shader:=NoLighting;
  end;
  // set up light
  Light:=TAsmDirectionalLight.Create(self);
  Light.SetPosition(@LightObject.AsmObject.Position);
  Light.SetDiffuseColor(1,1,1,1);
  Light.enabled:=true;
  LightObject.AsmLight:=Light;
  LightObject.Name:='Light'+IntToStr(LightList.Count+1);
  //
  LightList.Add(LightObject);
  LightComboBox.Items.Add(LightObject.Name);
  LightComboBox.ItemIndex:=LightList.Count;
  LightTypeComboBox.ItemIndex:=0;
  Scene.Lightlist.Add(TAsmEditorObject(LightList.Items[LightList.Count-1]).AsmLight);
  Scene.ObjectList.Add(TAsmEditorObject(LightList.Items[LightList.Count-1]).AsmObject);
  SelectedObject:=TAsmEditorObject(LightList.Items[LightList.Count-1]);
  Light:=nil;
  LightMesh:=nil;
  LightObject:=nil;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.NewObjectMenuitemClick(Sender: TObject);
begin
  if Sender=nil then;
  ObjectCombobox.ItemIndex:=0;
  LightCombobox.ItemIndex:=0;
  SelectedObject:=nil;
  AsmMeshManager.Show;
  AsmMeshManager.BringToFront;
end;

procedure TAsmEditorMain.Form1CLOSE(Sender: TObject; var CloseAction: TCloseAction);
var i: integer;
begin
  if Sender=nil then;
  //Light1.Free;
  Floor.Mesh.Free;
  Floor.Free;
  //Sphere.Mesh.Free;
  //Sphere.Free;
  CS.Mesh.Free;
  CS.Free;
  AsmMeshManager.Close;
  for i:=0 to ObjectList.Count-1 do
    TAsmEditorObject(ObjectList.Items[i]).Free;
  ObjectList.Free;
  LightList.Free;
  Scene.Free;
end;

procedure TAsmEditorMain.OpenGLControl1MOUSEDOWN(Sender: TOBject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var Right, ViewXY, ScaleObjectCenter, NewPosition, R0, Rd, v0, v1, v2: TVector3;
    WinX, WinY, WinZ, dummy: GLdouble; BSRadius, t, tmin: GLfloat;
    i,j,k, IntersectedEditorObject, IntersectedLightObject: Integer;
    IntersectedBSs: array of integer;
    TestR0,TestRd: TVector3;
    t1, t2: GLfloat;
    //TestMatrix: glMatrix;
begin
  if Sender=nil then;
  oldx:=x;
  oldy:=y;
  if ssRight in Shift then begin
    // PopUpMenu
  end;
  // select - leftclick
  if (ssLeft in Shift) and (Select in TransformAct) then begin
    CreateRayFromXY(X, OpenGLControl1.Height-Y, R0, Rd);
    // todo?
    // first check all models and lights for bounding sphere intersection
    j:=0;
    SetLength(IntersectedBSs,ObjectList.Count);
    for i:=0 to ObjectList.Count-1 do
      with TAsmEditorObject(ObjectList[i]) do
        if RaySphereIntersection(R0, Rd, AsmObject.BS.Center+AsmObject.
         Position, AsmObject.BS.Radius,p1,p2) then begin
          //writeln('Object hit: ',Name);
          IntersectedBSs[j]:=i;
          inc(j);
          //SelectedObject:=TAsmEditorObject(ObjectList[i]);
          //TransformAct:=[];
        end;
        //writeln('Intersected BSs: ',j);
        // then check for triangle intersection
        tmin:=intpower(10,30); // just set any high value
        //writeln('tmin: ',tmin);
        for i:=0 to j-1 do begin
          k:=0;
          with TAsmEditorObject(ObjectList[IntersectedBSs[i]]) do begin
            // transform ray to object space
            TestR0:=TransformVector(R0,AsmObject.Matrix)
             -TransformVector(AsmObject.Position,AsmObject.Matrix);
            TestRd:=TransformVector(Rd,AsmObject.Matrix);
            with AsmObject do begin
              repeat
                //writeln('k: ',k,' Count: ',AsmObject.Mesh.Count);
                {v0.X:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k]*3];
                v0.Y:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k]*3+1];
                v0.Z:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k]*3+2];}
                v0:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k]];
                {v1.X:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k+1]*3];
                v1.Y:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k+1]*3+1];
                v1.Z:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k+1]*3+2];}
                v1:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k+1]];
                {v2.X:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k+2]*3];
                v2.Y:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k+2]*3+1];
                v2.Z:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k+2]*3+2];}
                v2:=Mesh.VertexArray.Data[Mesh.IndexArray.Data[k+2]];
                if RayTriangleIntersection(TestR0,TestRd,v0*Scale.x,
                 v1*Scale.y,v2*Scale.z,t) then begin
                  //writeln('Tri-Intersection: ',t);
                  if t<tmin then begin
                    tmin:=t;
                    //writeln('i: ',i);
                    IntersectedEditorObject:=i;
                  end;
                end;
                inc(k,3);
              until (k>=Mesh.Count);
            end;
          end;
        end;
    j:=0;
    IntersectedLightObject:=-1;
    for i:=0 to LightList.Count-1 do
      with TAsmEditorObject(LightList[i]) do
        if RaySphereIntersection(R0, Rd, AsmObject.BS.Center+AsmObject.
         Position, AsmObject.BS.Radius,p1,p2) then begin
          writeln('Object hit: ',Name);
          t1:=(p1.X-R0.X)/Rd.X;
          t2:=(p2.X-R0.X)/Rd.X;
          if t1<t2 then begin
            if t1<tmin then begin
              tmin:=t1;
              IntersectedLightObject:=i;
            end;
          end else
            if t2<tmin then begin
              tmin:=t2;
              IntersectedLightObject:=i;
            end;
          //SelectedObject:=TAsmEditorObject(ObjectList[i]);
          //TransformAct:=[];
        end;
    // then choose object with nearest intersection point
    if tmin<intpower(10,30) then
      if IntersectedLightObject>-1 then begin
        SelectedObject:=TAsmEditorObject(LightList[IntersectedLightObject]);
        LightCombobox.ItemIndex:=IntersectedLightObject+1;
        TransformAct:=TransformAct-[Select];
      end else begin
      // then choose object with nearest intersection point
        SelectedObject:=TAsmEditorObject(ObjectList[IntersectedBSs
         [IntersectedEditorObject]]);
        ObjectCombobox.ItemIndex:=IntersectedBSs[IntersectedEditorObject]+1;
        TransformAct:=TransformAct-[Select];
      end;
  end;
  if ssLeft in Shift then begin
    if (SelectedObject<>nil) and (not(Select in TransformAct)) then begin
      // Translate
      if Translate in TransformAct then begin
        gluProject(SelectedObject.AsmObject.Position.X,SelectedObject.AsmObject.
         Position.Y,SelectedObject.AsmObject.Position.Z,Scene.mm,Scene.pm,
         Scene.viewport,SelectedX,SelectedY,dummy);
        Right:=Normalized(CrossProduct((Camera.CoV-Camera.Pos),YAxis));
        ViewXY:=Normalized(Crossproduct(Right,YAxis));
        PlaneDistance:=DotProduct(ViewXY,SelectedObject.AsmObject.Position);
      end else
        // Rotate about origin
        if Rotate in TransformAct then begin
          gluProject(SelectedObject.AsmObject.Position.X,
                   SelectedObject.AsmObject.Position.Y,
                   SelectedObject.AsmObject.Position.Z,
                   Scene.mm,Scene.pm,Scene.Viewport,
                   WinX,WinY,WinZ);
          P1:=PointOnVirtualSphere(x,y,WinX,WinY,Scene.Viewport);
          P1:=TransformVector(P1,Scene.mm);
          rMatrix:=SelectedObject.AsmObject.Matrix;
        end else
          // Scale
          if Scale in TransformAct then begin
            ScaleObjectCenter:=SelectedObject.AsmObject.Position;
            //ScaleObjectRadius:=SelectedObject.AsmObject.Radius;
            gluProject(ScaleObjectCenter.X, ScaleObjectCenter.Y, ScaleObjectCenter.Z,
             Scene.mm, Scene.pm, Scene.viewport, SelectedX, SelectedY, dummy);
            Right:=Normalized(CrossProduct((Camera.CoV-Camera.Pos),YAxis));
            ViewXY:=Normalized(Crossproduct(Right,YAxis));
            PlaneDistance:=DotProduct(ViewXY,TransformVector(SelectedObject.AsmObject
             .BS.Center, SelectedObject.AsmObject.Matrix)+ScaleObjectCenter);
            ObjScale:=SelectedObject.AsmObject.Scale;
            //
            CreateRayFromXY(X, Scene.Viewport[3]-Y, R0, Rd);
            if RayPlaneIntersection(R0, Rd, ViewXY, PlaneDistance, NewPosition) then begin
              BSRadius:=SelectedObject.AsmObject.Mesh.BS.Radius;
              oldScale:=SelectedObject.AsmObject.Scale;
              oldLength:=vLength(NewPosition);
              // debugging
              ScalePoint1:=NewPosition;
              ShowObjectScale;
            end;
          end;
    end;
  end;
  // middleclick
  if ssMiddle in Shift then begin
    if SelectedObject<>nil then begin
      // Translate or Scale
      if (Translate in TransformAct) or (Scale in TransformAct) then begin
        gluProject(SelectedObject.AsmObject.Position.X,SelectedObject.AsmObject.
         Position.Y,SelectedObject.AsmObject.Position.Z,Scene.mm,Scene.pm,Scene.
         viewport,SelectedX,SelectedY,dummy);
        PlaneDistance:=DotProduct(YAxis,SelectedObject.AsmObject.Position);
      end else
        // Rotate about bscenter
        if Rotate in TransformAct then begin
          Center:=iTransformVector(SelectedObject.AsmObject.Mesh.BS.Center,
           SelectedObject.AsmObject.Matrix)*SelectedObject.AsmObject.maxScale
           +SelectedObject.AsmObject.Position;
          oldCoR:=SelectedObject.AsmObject.CoR;
          SelectedObject.AsmObject.SetCoR(Center);
          gluProject(Center.X,Center.Y,Center.Z,Scene.mm,Scene.pm,Scene.Viewport,
                   WinX,WinY,WinZ);
          P1:=PointOnVirtualSphere(x,y,WinX,WinY,Scene.Viewport);
          P1:=TransformVector(P1,Scene.mm);
          rMatrix:=SelectedObject.AsmObject.Matrix;
        end;
    end;
  end;
  //
  //if ssLeft in Shift then StatusLabel.Caption:='Move to rotate or to alter the height of the camera';
  //if ssRight in Shift then StatusLabel.Caption:='Move up and down to change distance to object';
end;

procedure TAsmEditorMain.OpenGLControl1MOUSEENTER(Sender: TObject);
begin
  if Sender=nil then;
  //StatusLabel.Caption:='Press left or right mousebutton to control the camera';
end;

procedure TAsmEditorMain.OpenGLControl1MOUSELEAVE(Sender: TObject);
begin
  if Sender=nil then;
  //StatusLabel.Caption:='';
end;

procedure TAsmEditorMain.OpenGLControl1MOUSEMOVE(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var Right, ViewXY, View, NewPosition, CenterProj, CenterVec: TVector3; WinX, WinY, WinZ: GLdouble;
    rotAxis, R0, Rd, newScale, tmpScale: TVector3; BSRadius, ScaleLength: GLfloat;
begin
  if Sender=nil then;
  if ssLeft in Shift then
    if (SelectedObject<>nil) and (TransformAct<>[]) then begin
      Right:=Normalized(CrossProduct((Camera.CoV-Camera.Pos),YAxis));
      ViewXY:=Normalized(Crossproduct(Right,YAxis));
      View:=Normalized(Camera.CoV-Camera.Pos);
      if (Rotate in TransformAct) then begin
        // rotate about origin
        gluProject(SelectedObject.AsmObject.Position.X,
                   SelectedObject.AsmObject.Position.Y,
                   SelectedObject.AsmObject.Position.Z,
                   Scene.mm,Scene.pm,Scene.Viewport,
                   WinX,WinY,WinZ);
        P2:=PointOnVirtualSphere(x,y,WinX,WinY,Scene.Viewport);
        P2:=TransformVector(P2,Scene.mm);
        rotAxis:=Normalized(CrossProduct(P1,P2));
        SelectedObject.AsmObject.Matrix:=rMatrix;
        SelectedObject.AsmObject.RotateAboutLocalOrigin(Angle(P1,P2),rotAxis);
        SelectedObject.EulerAngles:=SelectedObject.AsmObject.GetEulerAngles;
        ShowObjectRotation;
        ShowObjectPosition;
      end else
        // translate
        if (Translate in TransformAct) then begin
          CreateRayFromXY(SelectedX+(X-OldX), SelectedY+(OldY-Y), R0, Rd);
          if RayPlaneIntersection(R0, Rd, ViewXY, PlaneDistance, NewPosition) then begin
            SelectedObject.AsmObject.SetPosition(NewPosition);
            SelectedObject.AsmObject.SetCoR(NewPosition);
            ShowObjectPosition;
            ShowObjectCoR;
          end;
        end else
          // scale
          if (Scale in TransformAct) then
            begin
              CreateRayFromXY(X, Scene.Viewport[3]-Y, R0, Rd);
              if RayPlaneIntersection(R0, Rd, ViewXY, PlaneDistance, NewPosition) then begin
                // debugging
                ScalePoint2:=NewPosition;
                //
                with SelectedObject.AsmObject do begin
                  CenterProj:=ScalePoint2-ScalePoint1;
                  CenterVec:=ScalePoint1-iTransformVector(BS.Center,Matrix)-Position;
                  ScaleLength:=dotproduct(CenterProj,CenterVec)/vLength(CenterVec);
                  tmpScale.x:=oldScale.x*(ScaleLength/(Mesh.BS.Radius*oldScale.x)+1);
                  tmpScale.y:=oldScale.y*(ScaleLength/(Mesh.BS.Radius*oldScale.y)+1);
                  tmpScale.z:=oldScale.z*(ScaleLength/(Mesh.BS.Radius*oldScale.z)+1);
                  if tmpScale.x>0 then begin
                    newScale.x:=tmpScale.x;
                    if tmpScale.y>0 then begin
                      newScale.y:=tmpScale.y;
                      if tmpScale.z>0 then begin
                        newScale.z:=tmpScale.z;
                        SetScale(newScale);
                      end;
                    end;
                  end;
                end;
                ShowObjectScale;
              end;
            end;
    end else begin
      // move camera
      if ssCtrl in Shift then begin
        Camera.CoV.x:=Camera.CoV.x-(X-OldX);
        Camera.CoV.y:=Camera.CoV.y-(OldY-Y);
        //Sphere.SetPosition(Camera.CoV);
      end else begin
        Camera.RotateAlphaAboutCoV(-GLfloat(x-oldx)/2);
        Camera.RotateBetaAboutCoV(-GLfloat(y-oldy)/3);
        //Camera.Height:=Camera.Height+(GLfloat(y-oldy));
      end;
      oldx:=x;
      oldy:=y;
    end;
  if ssMiddle in Shift then
    if (SelectedObject<>nil) and (TransformAct<>[]) then begin
      if (Rotate in TransformAct) then begin
        // roate about bscenter
        gluProject(Center.X,Center.Y,Center.Z,Scene.mm,Scene.pm,Scene.Viewport,
                   WinX,WinY,WinZ);
        P2:=PointOnVirtualSphere(x,y,WinX,WinY,Scene.Viewport);
        P2:=TransformVector(P2,Scene.mm);
        rotAxis:=Normalized(CrossProduct(P1,P2));
        SelectedObject.AsmObject.Matrix:=rMatrix;
        SelectedObject.AsmObject.Rotate(Angle(P1,P2),rotAxis);
        SelectedObject.EulerAngles:=SelectedObject.AsmObject.GetEulerAngles;
        ShowObjectRotation;
        ShowObjectPosition;
        ShowObjectCor;
      end else
        // translate
        if (Translate in TransformAct) then begin
          CreateRayFromXY(SelectedX+(X-OldX), SelectedY+(OldY-Y), R0, Rd);
          if RayPlaneIntersection(R0, Rd, YAxis, PlaneDistance, NewPosition) then begin
            SelectedObject.AsmObject.SetPosition(NewPosition);
            SelectedObject.AsmObject.SetCoR(NewPosition);
            ShowObjectPosition;
            ShowObjectCoR;
          end;
        end;
    end else begin
      // move camera buggy workaround
      if ssCtrl in Shift then begin
        Camera.CoV.z:=Camera.CoV.z+Y-OldY;
        //Sphere.SetPosition(Camera.CoV);
      end else
        Camera.Radius:=Camera.Radius+(GLfloat(y-oldy));
      oldx:=x;
      oldy:=y;
    end;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.OpenGLControl1MOUSEUP(Sender: TOBject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Sender=nil then;
  if ((ssLeft in Shift) or (ssMiddle in Shift)) and (SelectedObject<>nil) then begin
    //P1selected:=false;
    // snap
    with SelectedObject do begin
      EulerAngles:=AsmObject.GetEulerAngles;
      AsmObject.SetEulerAngles(EulerAngles);
      if ssMiddle in Shift then AsmObject.SetCoR(oldCoR);
    end;
    ShowObjectRotation;
    ShowObjectPosition;
    ShowObjectCoR;
  end;
  OpenGLControl1.Invalidate;
  //StatusLabel.Caption:='Press left or right mousebutton to control the camera';
end;

procedure TAsmEditorMain.OpenGLControl1PAINT(Sender: TObject);
var i: integer; csm: TGLUMatrixd; cv, pv, bbtmp, bscentertemp: TVector3;
begin
  if Sender=nil then;
  if not SceneInitialized then InitScene;
  //StatusLabel.Caption:=fps;
  //FramesPerSecond(FPSRecord);
  
  // update selection
  if SelectedObject<>nil then begin
    with SelectedObject do begin
      bbtmp:=AsmObject.Mesh.BoundingBox;
      bscentertemp:=AsmObject.Mesh.BS.Center*AsmObject.maxScale;
      //SelectionObject.SetCoR(AsmObject.CoR);
      SelectionObject.SetPosition(AsmObject.Position);
      SelectionObject.Matrix:=AsmObject.Matrix;
      SelectionObject.SetScale(AsmObject.Scale);
      SelectionObject.Translate(iTransformVector(bscentertemp,AsmObject.Matrix));
    end;
    if SelectedObjectChanged then begin
      writeln('Selected Mesh Changed');
      with SelectedObject do begin
        SelectionMesh.BoundingBox:=bbtmp;
        SelectionMesh.BS.Center:=AsmObject.Mesh.BS.Center;
        SelectionMesh.CreateSelection;
        SelectionObject.Mesh:=SelectionMesh;
        SelectionObject.LineWidth:=3;
        // recreate Mesh
        SelectionObject.Visible:=true;
        SelectedObjectChanged:=false;
      end;
    end;
    end
  else SelectionObject.Visible:=false;
  
  // hack to display coordinate system - should be improved
  for i:=0 to 11 do
    csm[i]:=Scene.mm[i];
  for i:=12 to 15 do
    csm[i]:=CS.Matrix[3,i-12];
  for i:=0 to 15 do
    CS.Matrix[i div 4, i mod 4]:=csm[i];
  Scene.RenderScene;

  // debug stuff
  {if SelectedObject<>nil then begin
  glColor3f(1,1,0);
  glBegin(GL_LINES);
    glVertex3f(SelectedObject.AsmObject.Position.x,SelectedObject.AsmObject.
     Position.y,SelectedObject.AsmObject.Position.z);
    glVertex3f(SelectedObject.AsmObject.Position.x+P1.x*20,
               SelectedObject.AsmObject.Position.y+P1.y*20,
               SelectedObject.AsmObject.Position.z+P1.z*20);
    glColor3f(0,1,1);
    glVertex3f(SelectedObject.AsmObject.Position.x,SelectedObject.AsmObject.
     Position.y,SelectedObject.AsmObject.Position.z);
    glVertex3f(SelectedObject.AsmObject.Position.x+P2.x*20,
               SelectedObject.AsmObject.Position.y+P2.y*20,
               SelectedObject.AsmObject.Position.z+P2.z*20);
  glEnd;
  end;
  if SelectedObject<>nil then begin
  glColor3f(1,0,1);
  glBegin(GL_LINES);
    glVertex3f(SelectedObject.AsmObject.Position.x,SelectedObject.AsmObject.
     Position.y,SelectedObject.AsmObject.Position.z);
    glVertex3f(SelectedObject.AsmObject.Position.x+rotAxis.x*20,
               SelectedObject.AsmObject.Position.y+rotAxis.y*20,
               SelectedObject.AsmObject.Position.z+rotAxis.z*20);
  glEnd;
  end;}
  {
  glPointSize(15);
  glColor3f(1,0,0);
  glBegin(GL_LINES);
    for i:=0 to 3 do begin
      glVertex3dv(frp[i]);
      glVertex3dv(frp[i+4]);
    end;
    for i:=0 to 3 do
      glVertex3dv(frp[i]);
    glVertex3dv(frp[0]);
    for i:=4 to 7 do
      glVertex3dv(frp[i]);
    glVertex3dv(frp[4]);
  glEnd;
  }
  //
  // Mesh needed as public but should be private
  {if SelectedObject<>nil then begin
    m:=SelectedObject.AsmObject.Mesh;
    rl:=(m.BSRadius+m.BSCenter.length)*SelectedObject.AsmObject.maxScale;
    glTranslatef(SelectedObject.AsmObject.CoR.x,SelectedObject.AsmObject.CoR.y,
     SelectedObject.AsmObject.CoR.z);
    glMultMatrixf(SelectedObject.AsmObject.Matrix);
    glBegin(GL_LINES);
    glColor3f(1,0,0);
    glVertex3f(-rl,0,0);
    glVertex3f(rl,0,0);
    glColor3f(0,1,0);
    glVertex3f(0,-rl,0);
    glVertex3f(0,rl,0);
    glColor3f(0,0,1);
    glVertex3f(0,0,-rl);
    glVertex3f(0,0,rl);
    glEnd;
    // Scale
    glColor3f(0,0,0);
    glBegin(GL_LINES);
    glVertex3f(SelectedObject.AsmObject.Mesh.BSCenter.x,
               SelectedObject.AsmObject.Mesh.BSCenter.y,
               SelectedObject.AsmObject.Mesh.BSCenter.z);
    glVertex3f(ScalePoint.x,ScalePoint.y,ScalePoint.z);
    glEnd;
  end;
  }
  //
  //glPushMatrix;
  //glLoadIdentity;
  //
  {if SelectedObject<>nil then begin
    cv:=ScalePoint1-SelectedObject.AsmObject.Position;
    pv:=ScalePoint2-ScalePoint1;
    if cv.length<>0 then pv:=(dotproduct(pv,cv)/cv.length)*Normalize(cv);
    glBegin(GL_LINES);
    glColor3f(1,0,0);
    glVertex3f(SelectedObject.AsmObject.Position.x,SelectedObject.AsmObject.Position.y,SelectedObject.AsmObject.Position.z);
    glVertex3f(ScalePoint1.x,ScalePoint1.y,ScalePoint1.z);
    glColor3f(0,1,0);
    glVertex3f(ScalePoint1.x,ScalePoint1.y,ScalePoint1.z);
    glVertex3f(ScalePoint2.x,ScalePoint2.y,ScalePoint2.z);
    glColor3f(0,0,1);
    glVertex3f(ScalePoint1.x,ScalePoint1.y,ScalePoint1.z);
    glVertex3f(ScalePoint1.x+pv.x,ScalePoint1.y+pv.y,ScalePoint1.z+pv.z);
  glEnd;
  end;
  //glPopMatrix;
  //}
  {if TestR0<>zeroVector then begin
  glPushMatrix;
  glLoadIdentity;
  glBegin(GL_QUADS);
    glColor3f(0,1,0);
    glVertex3f(TestR0.x-5,TestR0.y+5,0);
    glVertex3f(TestR0.x-5,TestR0.y-5,0);
    glVertex3f(TestR0.x+5,TestR0.y-5,0);
    glVertex3f(TestR0.x+5,TestR0.y+5,0);
  glEnd;
  glPopMatrix;
  WriteVector(TestR0);
  writeln;
  end;}
  //gtk_gl_area_swap_buffers(PGtkGLArea(Gtkglareacontrol1.Widget));
  //GTKGLAreaControl1.Invalidate;
end;

procedure TAsmEditorMain.OpenGLControl1RESIZE(Sender: TObject);
begin
  if Sender=nil then;
  if Scene<>nil then Scene.UpdateViewport;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.LoadObjectMenuItemCLICK(Sender: TObject);
var Object1: TAsmEditorObject;
begin
  if Sender=nil then;
  try
    if OpenDialog1.Execute then begin
      Object1:=TAsmEditorObject.Create(self);
      Object1.AsmObject.Mesh.LoadMeshFromObjFile(OpenDialog1.FileName);
      Object1.AsmObject.BS.Center:=Object1.AsmObject.Mesh.BS.Center;
      Object1.AsmObject.BS.Radius:=Object1.AsmObject.Mesh.BS.Radius;
      Object1.AsmObject.Shader:=NoLightingWireframe;
      ShaderCombobox.ItemIndex:=NoLightingWireframe;
      Object1.AsmObject.SetColor(0,0,0,1);
      Object1.AsmObject.visible:=true;
      Object1.Name:='Object'+IntToStr(ObjectList.Count+1);
      ObjectList.Add(Object1);
      ObjectCombobox.Items.Add(Object1.Name);
      ObjectCombobox.ItemIndex:=ObjectCombobox.Items.Count-1;
      Scene.ObjectList.Add(Object1.AsmObject);
      SelectedObject:=TAsmEditorObject(ObjectList.Items[ObjectList.Count-1]);
      AsmEditorMain.Caption:=Opendialog1.FileName;
      OpenDialog1.InitialDir:='';
      Object1:=nil;
      ShowObjectCoR;
      ShowObjectPosition;
      ShowObjectRotation;
      ShowObjectScale;
      OpenGLControl1.Invalidate;
    end;
  except
    ShowMessage('Error while loading file');
  end;
end;

procedure TAsmEditorMain.ObjectComboboxCHANGE(Sender: TObject);
begin
  if Sender=nil then;
  if ObjectCombobox.ItemIndex<>0 then begin
    LightComboBox.ItemIndex:=0;
    SelectedObject:=TAsmEditorObject(ObjectList.Items[ObjectCombobox.ItemIndex-1]);
    ShowObjectCoR;
    ShowObjectPosition;
    ShowObjectRotation;
    ShowObjectScale;
    ShowObjectSettings;
  end else begin
    SelectedObject:=nil;
    ShowObjectDefaults;
  end;
  // debugging
  if SelectedObject<>nil then
    writeln('Selected: ',SelectedObject.Name)
  else
    writeln('Selected: ','none');
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectPitchMaskeditEXIT(Sender: TObject);
var NewPitch: GLfloat; TextField: string;
begin
  if Sender=nil then;
  TextField:=ObjectPitchMaskedit.Text;
  if NumericalInput(NewPitch, TextField) then
    with SelectedObject do begin
      TextField:=FloatToStr(NewPitch);
      EulerAngles:=AsmObject.GetEulerAngles;
      EulerAngles.y:=NewPitch-trunc(NewPitch/360)*360;
      AsmObject.SetEulerAngles(EulerAngles);
      EulerAngles:=AsmObject.GetEulerAngles;
    end;
  ObjectPitchMaskedit.Text:=TextField;
  ShowObjectRotation;
  OpenGLControl1.Invalidate;
end;


procedure TAsmEditorMain.ObjectPositionXMaskeditEXIT(Sender: TObject);
var NewX: GLfloat; TextField: string;
begin
  if Sender=nil then;
  TextField:=ObjectPositionXMaskedit.Text;
  if NumericalInput(NewX, TextField) then
    SelectedObject.AsmObject.SetPosition(NewX, SelectedObject.AsmObject.
     Position.y, SelectedObject.AsmObject.Position.z);
  ObjectPositionXMaskedit.Text:=TextField;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectPositionXMaskeditKEYPRESS(Sender: TObject; var Key: Char
  );
begin
  if Sender=nil then;
  if Key=#13 then ObjectPositionXMaskeditEXIT(Sender);
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectPositionYMaskeditEXIT(Sender: TObject);
var NewY: GLfloat; TextField: string;
begin
  if Sender=nil then;
  TextField:=ObjectPositionYMaskedit.Text;
  if NumericalInput(NewY, TextField) then
    SelectedObject.AsmObject.SetPosition(SelectedObject.AsmObject.Position.x,
     NewY, SelectedObject.AsmObject.Position.z);
  ObjectPositionYMaskedit.Text:=TextField;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectPositionZMaskeditEXIT(Sender: TObject);
var NewZ: GLfloat; TextField: string;
begin
  if Sender=nil then;
  TextField:=ObjectPositionZMaskedit.Text;
  if NumericalInput(NewZ, TextField) then
    SelectedObject.AsmObject.SetPosition(SelectedObject.AsmObject.Position.x,
     SelectedObject.AsmObject.Position.y, NewZ);
  ObjectPositionZMaskedit.Text:=TextField;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectResetMatrixButtonCLICK(Sender: TObject);
begin
  if Sender=nil then;
  if SelectedObject<>nil then begin
    SelectedObject.AsmObject.ResetMatrix;
    SelectedObject.AsmObject.SetPosition(zeroVector);
    SelectedObject.AsmObject.SetCoR(SelectedObject.AsmObject.Position);
    SelectedObject.EulerAngles:=SelectedObject.AsmObject.GetEulerAngles;
    ShowObjectPosition;
    ShowObjectRotation;
    ShowObjectCoR;
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmEditorMain.ObjectResetRotationButtonCLICK(Sender: TObject);
begin
  if Sender=nil then;
  if SelectedObject<>nil then begin
    SelectedObject.AsmObject.ResetRotation;
    SelectedObject.EulerAngles:=SelectedObject.AsmObject.GetEulerAngles;
    ShowObjectRotation;
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmEditorMain.ObjectRollMaskeditEXIT(Sender: TObject);
var NewRoll: GLfloat; TextField: string;
begin
  if Sender=nil then;
  TextField:=ObjectRollMaskedit.Text;
  if NumericalInput(NewRoll, TextField) then
    with SelectedObject do begin
      TextField:=FloatToStr(NewRoll);
      EulerAngles:=AsmObject.GetEulerAngles;
      EulerAngles.z:=NewRoll-trunc(NewRoll/360)*360;
      AsmObject.SetEulerAngles(EulerAngles);
      EulerAngles:=AsmObject.GetEulerAngles;
    end;
  ObjectRollMaskedit.Text:=TextField;
  ShowObjectRotation;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectScaleXMaskeditEXIT(Sender: TObject);
var NewX: GLfloat; TextField: string;
begin
  if Sender=nil then;
  TextField:=ObjectScaleXMaskedit.Text;
  if NumericalInput(NewX, TextField) then with SelectedObject do
    AsmObject.SetScale(NewX, AsmObject.Scale.y, AsmObject.Scale.z);
  ObjectScaleXMaskedit.Text:=TextField;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectScaleYMaskeditEXIT(Sender: TObject);
var NewY: GLfloat; TextField: string;
begin
  if Sender=nil then;
  TextField:=ObjectScaleYMaskedit.Text;
  if NumericalInput(NewY, TextField) then with SelectedObject do
    AsmObject.SetScale(AsmObject.Scale.x, NewY,AsmObject.Scale.z);
  ObjectScaleYMaskedit.Text:=TextField;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectScaleZMaskeditEXIT(Sender: TObject);
var NewZ: GLfloat; TextField: string;
begin
  if Sender=nil then;
  TextField:=ObjectScaleZMaskedit.Text;
  if NumericalInput(NewZ, TextField) then with SelectedObject do
    AsmObject.SetScale(AsmObject.Scale.x, AsmObject.Scale.y, NewZ);
  ObjectScaleZMaskedit.Text:=TextField;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.ObjectShininessTrackbarCHANGE(Sender: TObject);
begin
  if Sender=nil then;
  if SelectedObject<>nil then begin
    SelectedObject.AsmObject.Shininess:=ObjectShininessTrackbar.Position;
    ObjectShininessMaskedit.Text:=IntToStr(ObjectShininessTrackbar.Position);
    OpenGLControl1.Invalidate;
  end;
end;

procedure TAsmEditorMain.ObjectYawMaskeditEXIT(Sender: TObject);
var NewYaw: GLfloat; TextField: string;
begin
  if Sender=nil then;
  TextField:=ObjectYawMaskedit.Text;
  if NumericalInput(NewYaw, TextField) then
    with SelectedObject do begin
      TextField:=FloatToStr(NewYaw);
      EulerAngles:=AsmObject.GetEulerAngles;
      EulerAngles.x:=NewYaw-trunc(NewYaw/360)*360;
      AsmObject.SetEulerAngles(EulerAngles);
      EulerAngles:=AsmObject.GetEulerAngles;
    end;
  ObjectYawMaskedit.Text:=TextField;
  ShowObjectRotation;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.QuitMenuitemClick(Sender: TObject);
begin
  if Sender=nil then;
  close;
end;

procedure TAsmEditorMain.RotateToggleboxCLICK(Sender: TObject);
begin
  if Sender=nil then;
  if RotateTogglebox.Checked then begin
    TranslateTogglebox.Checked:=false;
    ScaleTogglebox.Checked:=false;
    TransformAct:=[Rotate];
    Statusbar1.SimpleText:='LMB rotate about origin - MMB rotate about Center';
  end else begin
    TransformAct:=[];
    Statusbar1.SimpleText:='';
  end;
end;

procedure TAsmEditorMain.ScaleToggleboxCLICK(Sender: TObject);
begin
  if Sender=nil then;
  if ScaleTogglebox.Checked then begin
    RotateTogglebox.Checked:=false;
    TranslateTogglebox.Checked:=false;
    TransformAct:=[Scale];
  end else TransformAct:=[];
end;

procedure TAsmEditorMain.SelectButtonClick(Sender: TObject);
begin
  TransformAct:=TransformAct+[Select];
end;

procedure TAsmEditorMain.ShaderComboboxCLOSEUP(Sender: TObject);
var NewShader: byte;
begin
  if Sender<>nil then;
  if SelectedObject<>nil then
    if ShaderCombobox.ItemIndex>-1 then begin
      NewShader:=byte(ShaderCombobox.ItemIndex);
      if ShaderSupported(NewShader, SelectedObject.AsmObject.Mesh) then begin
        SelectedObject.AsmObject.Shader:=NewShader;
        OpenGLControl1.Invalidate;
      end else begin
        ShaderCombobox.ItemIndex:=SelectedObject.AsmObject.Shader;
        ShowMessage('Shader not supported by this Mesh');
      end;
    end;
end;

procedure TAsmEditorMain.TranslateToggleboxCLICK(Sender: TObject);
begin
  if Sender=nil then;
  if TranslateTogglebox.Checked then begin
    RotateTogglebox.Checked:=false;
    ScaleTogglebox.Checked:=false;
    TransformAct:=[Translate];
    StatusBar1.SimpleText:='LMB move in camera plane - MMB move in XZ-plane';
  end else begin
    TransformAct:=[];
    Statusbar1.SimpleText:='';
  end;
end;

procedure TAsmEditorMain.Updown1CLICK(Sender: TObject; Button: TUDBtnType);
begin
  if Sender=nil then;
  if (Button=btNext) and (ObjectNotebook.PageIndex<ObjectNotebook.PageCount-1) then begin
    ObjectNotebook.PageIndex:=ObjectNotebook.PageIndex+1;
    ObjectPageLabel.Caption:='Properties';
  end;
  if (Button=btPrev) and (ObjectNotebook.PageIndex>0) then begin
    ObjectNotebook.PageIndex:=ObjectNotebook.PageIndex-1;
    ObjectPageLabel.Caption:='Transforms';
  end;
end;

procedure TAsmEditorMain.ShowGridMenuItemClick(Sender: TObject);
begin
  Floor.Visible:=not Floor.Visible;
  ShowGridMenuItem.Checked:=not ShowGridMenuItem.Checked;
  OpenGLControl1.Invalidate;
end;

procedure TAsmEditorMain.SetSelectedObject(const AValue: TAsmEditorObject);
var bbtmp, bscentertemp: TVector3;
begin
  if fSelectedObject=AValue then exit;
  fSelectedObject:=AValue;
  SelectedObjectChanged:=true;
end;

// creates a ray intersecting the near and far plane from window coordinates
procedure TAsmEditorMain.CreateRayFromXY(WinX, WinY: GLdouble; var RayOrigin,
  Ray: TVector3);
var UnProj1X, UnProj1Y, UnProj1Z, UnProj2X, UnProj2Y, UnProj2Z: GLdouble;
begin
  gluUnProject(WinX, WinY, 0, Scene.mm,Scene.pm,Scene.viewport,
   UnProj1X, UnProj1Y, UnProj1Z);
  gluUnProject(WinX, WinY, 1, Scene.mm,Scene.pm,Scene.viewport,
   UnProj2X, UnProj2Y, UnProj2Z);
  RayOrigin.X:=GLfloat(UnProj1X);
  RayOrigin.Y:=GLfloat(UnProj1Y);
  RayOrigin.Z:=GLfloat(UnProj1Z);
  Ray.X:=GLfloat(UnProj2X-UnProj1X);
  Ray.Y:=GLfloat(UnProj2Y-UnProj1Y);
  Ray.Z:=GLfloat(UnProj2Z-UnProj1Z);
  Normalize(Ray);
end;

function TAsmEditorMain.NumericalInput(var NewValue: GLfloat; var InputField: string): boolean;
begin
  if SelectedObject<>nil then begin
    try
      NewValue:=GLfloat(StrToFloat(InputField));
    except
      NewValue:=0;
      InputField:='0';
    end;
    NumericalInput:=true;
  end else begin
    InputField:='';
    NumericalInput:=false;
  end;
end;

procedure TAsmEditorMain.InitScene;
begin
  with Scene do begin
    Init;
    writeln('Vendor: ', Vendor);
    writeln('Renderer: ', Renderer);
    writeln('Version: ', Version);
    writeln('Extensions: ',Extensions);
  end;
  Scene.SetBackgroundColor(1,1,1);
  Camera:=TAsmRotationCamera.Create(100,20,0,0,0,45,1,100000,self);
  Scene.ActiveCamera:=Camera;
  //Light1:=TAsmDirectionalLight.Create(self);
  //Light1.SetPosition(0,0,1);
  //Light1.SetAmbientColor(0.1,0.1,0.1,1);
  //Light1.SetDiffuseColor(0.9,0.9,0.9,1);
  //Light1.SetSpecularColor(1,1,1,1);
  //Light1.Enabled:=true;
  //Scene.LightList.Add(Light1);
  Floor:=TAsmObject.Create(self);
  Floor.Mesh:=TAsmMesh.Create;
  Floor.Mesh.CreateGrid(1000,10);
  Floor.SetColor(0.8,0.8,0.83,0.5);
  Floor.RotateAboutLocalX(90);
  Floor.BS.Radius:=Floor.Mesh.BS.Radius;
  Floor.Visible:=true;
  Scene.ObjectList.Add(Floor);
  {Sphere:=TAsmObject.Create(self);
  Sphere.Mesh:=TAsmMesh.Create;
  Sphere.Mesh.CreateSphere(1,10,10);
  Sphere.SetColor(1,0,0,0.75);
  Sphere.Shader:=NoLighting;
  Sphere.Visible:=true;
  Scene.ObjectList.Add(Sphere);}
  CS:=TAsmObject.Create(self);
  CS.Mesh:=TAsmMesh.Create;
  CS.Mesh.CreateCoordinateSystem(15,false);
  CS.Shader:=NoLightingColor;
  CS.CameraIndependent:=true;
  CS.SetPosition(0,0,-50);
  CS.Visible:=true;
  Scene.ObjectList.Add(CS);
  SceneInitialized:=true;
  // init selection object
  SelectionObject:=TAsmObject.Create(self);
  SelectionObject.SetColor(1,0,0,1);
  SelectionMesh:=TAsmEditorSelectionMesh.Create;
  SelectionObject.Mesh:=SelectionMesh;
  SelectionObject.Shader:=NoLightingWireframe;
  Scene.ObjectList.Add(SelectionObject);
  // init MeshManager
  AsmMeshManager.OpenGLControl1.SharedControl:=OpenGLControl1;
  AsmMeshManager.OnAssignMesh:=@AsmEditorMain.AssignMesh;
end;

procedure TAsmEditorMain.ShowObjectCoR;
begin
  if SelectedObject<>nil then begin
    ObjectCoRXMaskedit.Text:=FloatToStr(SelectedObject.AsmObject.CoR.x);
    ObjectCoRYMaskedit.Text:=FloatToStr(SelectedObject.AsmObject.CoR.y);
    ObjectCoRZMaskedit.Text:=FloatToStr(SelectedObject.AsmObject.CoR.z);
  end;
end;

procedure TAsmEditorMain.ShowObjectPosition;
begin
  if SelectedObject<>nil then begin
    ObjectPositionXMaskedit.Text:=FloatToStr(SelectedObject.AsmObject.Position.x);
    ObjectPositionYMaskedit.Text:=FloatToStr(SelectedObject.AsmObject.Position.y);
    ObjectPositionZMaskedit.Text:=FloatToStr(SelectedObject.AsmObject.Position.z);
    if SelectedObject.AsmLight=nil then ;//Sphere.SetPosition(SelectedObject.AsmObject.Position);
  end;
end;

procedure TAsmEditorMain.ShowObjectRotation;
begin
  if SelectedObject<>nil then begin
    ObjectPitchMaskedit.Text:=FloatToStr(SelectedObject.EulerAngles.y);
    ObjectYawMaskedit.Text:=FloatToStr(SelectedObject.EulerAngles.x);
    ObjectRollMaskedit.Text:=FloatToStr(SelectedObject.EulerAngles.z);
  end;
end;

procedure TAsmEditorMain.ShowObjectScale;
begin
  if SelectedObject<>nil then begin
    ObjectScaleXMaskedit.Text:=FloatToStr(SelectedObject.AsmObject.Scale.x);
    ObjectScaleYMaskedit.Text:=FloatToStr(SelectedObject.AsmObject.Scale.y);
    ObjectScaleZMaskedit.Text:=FloatToStr(SelectedObject.AsmObject.Scale.z);
  end;
end;

procedure TAsmEditorMain.ShowObjectSettings;
var ActualObject: TAsmObject;
begin
  if SelectedObject<>nil then begin
    ActualObject:=SelectedObject.AsmObject;
    with ActualObject do begin
      ObjectColorbutton.ButtonColor:=glRGBToTColor(Color[0],Color[1],Color[2]);
      ObjectSpecColorbutton.ButtonColor:=glRGBToTColor(SpecularColor[0],
       SpecularColor[1],SpecularColor[2]);
      ObjectShininessTrackbar.Position:=Shininess;
      ObjectShininessMaskedit.Text:=IntToStr(Shininess);
      if Texture<>nil then begin
        Image1.Picture.Assign(Texture);
        Panel1.Text:='';
      end else begin
        Image1.Picture:=nil;
        Panel1.Text:='No Texture';
      end;
    end;
  end;
end;

procedure TAsmEditorMain.ShowObjectDefaults;
begin
  ObjectCoRXMaskedit.Text:='';
  ObjectCoRYMaskedit.Text:='';
  ObjectCoRZMaskedit.Text:='';
  ObjectPositionXMaskedit.Text:='';
  ObjectPositionYMaskedit.Text:='';
  ObjectPositionZMaskedit.Text:='';
  ObjectPitchMaskedit.Text:='';
  ObjectYawMaskedit.Text:='';
  ObjectRollMaskedit.Text:='';
  ObjectScaleXMaskedit.Text:='';
  ObjectScaleYMaskedit.Text:='';
  ObjectScaleZMaskedit.Text:='';
  ObjectColorbutton.ButtonColor:=glRGBToTColor(0,0,0);
  ObjectSpecColorbutton.ButtonColor:=glRGBToTColor(0,0,0);
  ObjectShininessTrackbar.Position:=0;
  ObjectShininessMaskedit.Text:='0';
  Image1.Picture:=nil;
  Panel1.Text:='No Texture';
end;

{ TAsmEditorObject }

constructor TAsmEditorObject.Create(AnOwner: TComponent);
begin
  inherited Create(AnOwner);
  AsmObject:=TAsmObject.Create(self);
end;

destructor TAsmEditorObject.Destroy;
begin
  AsmObject.Free;
  inherited Destroy;
end;

{ TAsmEditorSelectionMesh }

procedure TAsmEditorSelectionMesh.CreateSelection;
var bb: TVector3;
begin
  if FCount>0 then ClearArrays;
  //xDraw:= -(BoxWidth/2);
  //yDraw:= +(BoxHeight/2);
  //zDraw:= +(BoxDepth/2);
  Primitive:=GL_LINES;
  {BSRadius:=sqrt(xDraw*xDraw+yDraw*yDraw+zDraw*zDraw);
  BSCenter.x:=0; BSCenter.y:=0; BSCenter.z:=0;
  BoundingBox.X:=BoxWidth/2+0.1;
  BoundingBox.Y:=BoxHeight/2+0.1;
  BoundingBox.Z:=BoxDepth/2+0.1;}
  bb:=BoundingBox;
  IndexArray.Add(AddVertex3(bb.x,bb.y,bb.z));
  IndexArray.Add(AddVertex3(bb.x-bb.x/2,bb.y,bb.z));
  IndexArray.Add(AddVertex3(bb.x,bb.y,bb.z));
  IndexArray.Add(AddVertex3(bb.x,bb.y-bb.y/2,bb.z));
  IndexArray.Add(AddVertex3(bb.x,bb.y,bb.z));
  IndexArray.Add(AddVertex3(bb.x,bb.y,bb.z-bb.z/2));
  //
  IndexArray.Add(AddVertex3(-bb.x,bb.y,bb.z));
  IndexArray.Add(AddVertex3(-bb.x+bb.x/2,bb.y,bb.z));
  IndexArray.Add(AddVertex3(-bb.x,bb.y,bb.z));
  IndexArray.Add(AddVertex3(-bb.x,bb.y-bb.y/2,bb.z));
  IndexArray.Add(AddVertex3(-bb.x,bb.y,bb.z));
  IndexArray.Add(AddVertex3(-bb.x,bb.y,bb.z-bb.z/2));
  //
  IndexArray.Add(AddVertex3(bb.x,-bb.y,bb.z));
  IndexArray.Add(AddVertex3(bb.x-bb.x/2,-bb.y,bb.z));
  IndexArray.Add(AddVertex3(bb.x,-bb.y,bb.z));
  IndexArray.Add(AddVertex3(bb.x,-bb.y+bb.y/2,bb.z));
  IndexArray.Add(AddVertex3(bb.x,-bb.y,bb.z));
  IndexArray.Add(AddVertex3(bb.x,-bb.y,bb.z-bb.z/2));
  //
  IndexArray.Add(AddVertex3(bb.x,bb.y,-bb.z));
  IndexArray.Add(AddVertex3(bb.x-bb.x/2,bb.y,-bb.z));
  IndexArray.Add(AddVertex3(bb.x,bb.y,-bb.z));
  IndexArray.Add(AddVertex3(bb.x,bb.y-bb.y/2,-bb.z));
  IndexArray.Add(AddVertex3(bb.x,bb.y,-bb.z));
  IndexArray.Add(AddVertex3(bb.x,bb.y,-bb.z+bb.z/2));
  //
  IndexArray.Add(AddVertex3(-bb.x,-bb.y,bb.z));
  IndexArray.Add(AddVertex3(-bb.x+bb.x/2,-bb.y,bb.z));
  IndexArray.Add(AddVertex3(-bb.x,-bb.y,bb.z));
  IndexArray.Add(AddVertex3(-bb.x,-bb.y+bb.y/2,bb.z));
  IndexArray.Add(AddVertex3(-bb.x,-bb.y,bb.z));
  IndexArray.Add(AddVertex3(-bb.x,-bb.y,bb.z-bb.z/2));
  //
  IndexArray.Add(AddVertex3(-bb.x,bb.y,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x+bb.x/2,bb.y,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x,bb.y,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x,bb.y-bb.y/2,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x,bb.y,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x,bb.y,-bb.z+bb.z/2));
  //
  IndexArray.Add(AddVertex3(bb.x,-bb.y,-bb.z));
  IndexArray.Add(AddVertex3(bb.x-bb.x/2,-bb.y,-bb.z));
  IndexArray.Add(AddVertex3(bb.x,-bb.y,-bb.z));
  IndexArray.Add(AddVertex3(bb.x,-bb.y+bb.y/2,-bb.z));
  IndexArray.Add(AddVertex3(bb.x,-bb.y,-bb.z));
  IndexArray.Add(AddVertex3(bb.x,-bb.y,-bb.z+bb.z/2));
  //
  IndexArray.Add(AddVertex3(-bb.x,-bb.y,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x+bb.x/2,-bb.y,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x,-bb.y,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x,-bb.y+bb.y/2,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x,-bb.y,-bb.z));
  IndexArray.Add(AddVertex3(-bb.x,-bb.y,-bb.z+bb.z/2));
  //
  FCount:=IndexArray.Length;
  CreateBuffers;
end;

initialization
  {$I asmodayeditorform.lrs}

end.

