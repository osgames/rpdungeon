{
 *****************************************************************************
 *                                                                           *
 *  This source is free software. See the file COPYING.modifiedLGPL,         *
 *  included in this distribution, for details about the copyright.          *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************

 Filename: Vectors.pp
 Created 25-06-2001
 Updated 02-12-2005
 Written by Reimar Grabowski
 Unit providing basic Vector Types
}

{$MODE objfpc}  

Unit Vectors;

INTERFACE

Uses Math, SysUtils;

Type
  { THE Basic Math Type of Vectors.pp}
  TValue = single;

{ Basic Vector Objects }
  
    // Kartesic Coordinates
  TVector2 = packed record
    x, y: TValue;
  end;
  
  TVector3 = packed record
    x, y, z: TValue;
  end;
  
  TVector4 = packed record
    x, y, z, w: TValue;
  end;
  
  // Polar Coordinates
  TVector2DP = Object
    r, alpha: TValue;
  end;
  TVector3DP = Object
    r, alpha, beta: TValue;
  end;
  
  // Cylinder Coordinates
  TVector3DC = Object
    r, alpha, z: TValue;
  end;

  PVector3 = ^TVector3;

const
  XAxis: TVector3 = (x:1;y:0;z:0);
  YAxis: TVector3 = (x:0;y:1;z:0);
  ZAxis: TVector3 = (x:0;y:0;z:1);
  zeroVector: TVector3 = (x:0;y:0;z:0);
  
  { Basic Vector Operations }

// IO routines
Procedure ReadVector(Var Vector: TVector2);
Procedure ReadVector(Var Vector: TVector3);
Procedure ReadVector(Var Vector: TVector2DP);
Procedure ReadVector(Var Vector: TVector3DP);
Procedure ReadVector(Var Vector: TVector3DC);
Procedure WriteVector(Vector: TVector2);
Procedure WriteVector(Vector: TVector3);
Procedure WriteVector(Vector: TVector2DP);
Procedure WriteVector(Vector: TVector3DP);
Procedure WriteVector(Vector: TVector3DC);

// Conversion routines
Function Kar2Pol(const Vector: TVector2): TVector2DP;
//Function Kar2Pol(Vector: TVector3): TVector3DP;
Function Pol2Kar(const Vector: TVector2DP): TVector2;
//Function Pol2Kar(Vector: TVector3DP): TVector;
{Function Kar2Cyl(Vector: TVector3D): TVector3DC;
Function Cyl2Kar(Vector: TVector3DC): TVector;
Function Pol2Cyl(Vector: TVector3DP): TVector3DC;
Function Cyl2Pol(Vector: TVector3DC): TVector3DP;}

//
Operator =(const a, b: TVector2)Res: boolean;
Operator =(const a, b: TVector3)Res: boolean;

// Addition
Operator +(const a, b: TVector2)Res: TVector2;
Operator +(const a, b: TVector3)Res: TVector3;

// Subtraction
Operator -(const a: TVector2)Res: TVector2;
Operator -(const a: TVector3)Res: TVector3;
Operator -(const a, b: TVector2)Res: TVector2;
Operator -(const a, b: TVector3)Res: TVector3;

// Scalar Multiplication
Operator *(a: TValue; const b: TVector2)Res: TVector2;
Operator *(const a: TVector2; b: TValue)Res: TVector2;
Operator *(a: TValue; const b: TVector3)Res: TVector3;
Operator *(const a: TVector3; b: TValue)Res: TVector3;

Operator /(const a: TVector3; b: TValue)Res: TVector3;

// dot product - inner Product ( a*b = a*b*cos(ab) )
Function DotProduct(const a, b: TVector2): TValue;

Function DotProduct(const a, b: TVector3): TValue;

// cross product - outer product
Function CrossProduct(const a, b: TVector3): TVector3;

// misc functions
Function Angle(const a, b: TVector3): TValue;
Function vLength(const a: TVector2): TValue;
Function vLength(const a: TVector3): TValue; overload;
Procedure Normalize(var a: TVector3);
Function Normalized(const a: TVector3): TVector3;


IMPLEMENTATION

{ Basic Vectors }

{Function TVector2.length: TValue;
  begin
    length:=sqrt(sqr(x)+sqr(y));
  end;}

{Function TVector3.length: TValue;
  begin
    length:=sqrt(sqr(x)+sqr(y)+sqr(z));
  end;

procedure TVector3.normalize;
var tmp, l: TValue;
begin
  l:=length;
  if l>0 then begin
    tmp:=1/l;
    x:=x*tmp;
    y:=y*tmp;
    z:=z*tmp;
  end;
end;}

{ Basic Vector Operations }

// Vector IO routines
Procedure ReadVector(Var Vector: TVector2);
  begin
    write('X: ');
    readln(Vector.x);
    write('Y: ');
    readln(Vector.y);
  end;

Procedure ReadVector(Var Vector: TVector3);
  begin
    write('X: ');
    readln(Vector.x);
    write('Y: ');
    readln(Vector.y);
    write('Z: ');
    readln(Vector.z);
  end;
  
Procedure ReadVector(Var Vector: TVector2DP);
  begin
    write('R: ');
    readln(Vector.r);
    write('Alpha: ');
    readln(Vector.alpha);
    Vector.alpha:=TValue(DegToRad(Vector.alpha));
  end;  

Procedure ReadVector(Var Vector: TVector3DP);
  begin
    write('R: ');
    readln(Vector.r);
    write('Alpha: ');
    readln(Vector.alpha);
    write('Beta: ');
    readln(Vector.beta);
  end;
  
Procedure ReadVector(Var Vector: TVector3DC);
  begin
    write('R: ');
    readln(Vector.r);
    write('Alpha: ');
    readln(Vector.alpha);
    write('Z: ');
    readln(Vector.z)
  end;

Procedure WriteVector(Vector: TVector2);
  begin
    write('(X:',Vector.x,' ,Y:',Vector.y,')');
  end;

Procedure WriteVector(Vector: TVector3);
  begin
    write('(X:',Vector.x,' ,Y:',Vector.y,' ,Z:',Vector.z,')');
  end;
  
Procedure WriteVector(Vector: TVector2DP);
  begin
    write('(R:',Vector.r,', Alpha:',RadToDeg(Vector.alpha),')');
  end;  

Procedure WriteVector(Vector: TVector3DP);
  begin
    write('(R:',Vector.r,', Alpha:',Vector.alpha,', Beta:',Vector.beta,')');
  end;
  
Procedure WriteVector(Vector: TVector3DC);
  begin
    write('(R:',Vector.r,', Alpha:',Vector.alpha,', Z:',Vector.z,')');
  end;

//
Operator =(const a, b: TVector2)Res: boolean;
begin
  if (a.x=b.x) and (a.y=b.y) then Res:=true else Res:=false;
end;

Operator =(const a, b: TVector3)Res: boolean;
begin
  if (a.x=b.x) and (a.y=b.y) and (a.z=b.z) then Res:=true else Res:=false;
end;

// Vector Math routines
Operator +(const a, b: TVector2)Res:TVector2;
  begin
    with Res do
      begin
        x:=a.x+b.x;
        y:=a.y+b.y;
      end;
  end;
  
Operator +(const a, b: TVector3)Res: TVector3;
  begin
    with Res do
      begin
        x:=a.x+b.x;
        y:=a.y+b.y;
        z:=a.z+b.z;
      end;
  end;

Operator -(const a: TVector2)Res: TVector2;
  begin
    with Res do
      begin
        x:=-a.x;
        y:=-a.y;
      end
  end;

Operator -(const a: TVector3)Res: TVector3;
  begin
    with Res do
      begin
        x:=-a.x;
        y:=-a.y;
        z:=-a.z;
       end
  end;

Operator -(const a, b: TVector2)Res: TVector2;
  begin
    with Res do
      begin
        x:=a.x-b.x;
        y:=a.y-b.y;
      end;
  end;
  
Operator -(const a, b: TVector3)Res: TVector3;
  begin
    with Res do
      begin
        x:=a.x-b.x;
        y:=a.y-b.y;
        z:=a.z-b.z;
      end;
  end;

Operator *(a: TValue; const b: TVector2)Res: TVector2;
  begin
    with Res do
      begin
        x:=a*b.x;
        y:=a*b.y;
      end;
  end;

Operator *(const a: TVector2; b: TValue)Res: TVector2;
  begin
    with Res do
      begin
        x:=a.x*b;
        y:=a.y*b;
      end;
  end;

Operator *(a: TValue; const b: TVector3)Res: TVector3;
  begin
    with Res do
      begin
        x:=a*b.x;
        y:=a*b.y;
        z:=a*b.z;
      end;
  end;

Operator *(const a: TVector3; b: TValue)Res: TVector3;
  begin
    with Res do
      begin
        x:=a.x*b;
        y:=a.y*b;
        z:=a.z*b;
      end;
  end;

Operator /(const a: TVector3; b: TValue)Res: TVector3;
begin
  with Res do begin
    x:=a.x/b;
    y:=a.y/b;
    z:=a.z/b;
  end;
end;
  
Function DotProduct(const a, b: TVector2): TValue;
  begin
    DotProduct:=a.x*b.x+a.y*b.y;
  end;

Function DotProduct(const a, b: TVector3): TValue;
  begin
    DotProduct:=a.x*b.x+a.y*b.y+a.z*b.z;
  end;

Function CrossProduct(const a, b: TVector3): TVector3;
Var Vector: TVector3;
  begin
    Vector.x:=a.y*b.z-a.z*b.y;
    Vector.y:=a.z*b.x-a.x*b.z;
    Vector.z:=a.x*b.y-a.y*b.x;
    CrossProduct:=Vector;
  end;

// Vector Conversion routines
Function Kar2Pol(const Vector: TVector2): TVector2DP;
Var OutVector: TVector2DP; angle: TValue;   
  begin
    with Vector do
      begin
        OutVector.r:=sqrt(sqr(x)+sqr(y));
        if x<>0 then
          angle:=arctan(y/x)
        else
          if y>0 then 
            angle:=pi/2
          else
            if y<0 then 
              angle:=pi*1.5
            else
              angle:=0;
        OutVector.alpha:=angle;
      end;
    with Vector do
      begin
        OutVector.r:=sqrt(sqr(x)+sqr(y));
        if x=0 then
          if y=0 then
            angle:=0
          else
            if y>0 then 
              angle:=pi/2
            else
              angle:=pi*1.5
        else
          if x>0 then
            if y>0 then
              angle:=arctan(y/x)
            else
              angle:=(2*pi)+arctan(y/x)  
          else
            if y>0 then
              angle:=(pi*0.5)-arctan(x/y)
            else
              angle:=(pi*1.5)-arctan(x/y);
        OutVector.alpha:=angle;    
      end;
    Kar2Pol:=OutVector;
  end;
  
{Function Kar2Pol(Vector: TVector3): TVector3DP;
  begin
  end;}

Function Pol2Kar(const Vector: TVector2DP): TVector2;
Var OutVector: TVector2;
  begin
    with Vector do
      begin
        OutVector.x:=r*cos(alpha);
        OutVector.y:=r*sin(alpha);
      end;
    Pol2Kar:=OutVector;  
  end;

{Function Pol2Kar(Vector: TVector3DP): TVector3;
  begin
  end;}
  
//

Function Angle(const a, b: TVector3): TValue;
var s, ang: TValue;
begin
  s:=dotproduct(a,b);
  ang:=s/(vLength(a)*vLength(b));
  if abs(ang)>1 then round(ang);
  Angle:=TValue(RadToDeg(arccos(ang)));
end;

function vLength(const a: TVector2): TValue;
begin
  vlength:=sqrt(sqr(a.x)+sqr(a.y));
end;

Function vLength(const a: TVector3): Tvalue; overload;
begin
  vLength:=sqrt(sqr(a.x)+sqr(a.y)+sqr(a.z));
end;

Procedure Normalize(var a: TVector3);
var l, tmp: TValue;
begin
  l:=vLength(a);
  if l>0 then begin
    tmp:=1/l;
    a.x:=a.x*tmp;
    a.y:=a.y*tmp;
    a.z:=a.z*tmp;
  end;
end;

Function Normalized(const a: TVector3): TVector3;
var Res: TVector3; l, tmp: TValue;
begin
  l:=vLength(a);
  if l>0 then begin
    tmp:=1/l;
    Res.x:=a.x*tmp;
    Res.y:=a.y*tmp;
    Res.z:=a.z*tmp;
  end;
  Normalized:=Res;
end;

begin
end.
