{ This file was automatically created by Lazarus. Do not edit!
This source is only used to compile and install the package.
 }

unit AsmodayPkg; 

interface

uses
  asmshaders, asmoday, asmtypes, asmutils, dyn_arrays, sgiglu, nvGL, Vectors, 
    LazarusPackageIntf; 

implementation

procedure Register; 
begin
end; 

initialization
  RegisterPackage('AsmodayPkg', @Register); 
end.
