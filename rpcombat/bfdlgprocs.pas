unit BFDlgProcs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, Forms, Dialogs, FileUtil;

function RenameFileWithErrorDialogs(const SrcFilename, DestFilename: string;
                                    ExtraButtons: TMsgDlgButtons): TModalResult;
function CheckFileIsWritable(const Filename: string;
                             ErrorButtons: TMsgDlgButtons): TModalResult;
function ForceDirectoryInteractive(Directory: string;
                                   ErrorButtons: TMsgDlgButtons): TModalResult;
function BackupFileInteractive(const Filename: string;
                               ErrorButtons: TMsgDlgButtons): TModalResult;
function WarnOverwriteIfFileExists(const Filename: string;
                                   ErrorButtons: TMsgDlgButtons): TModalResult;

implementation

function RenameFileWithErrorDialogs(const SrcFilename, DestFilename: string;
  ExtraButtons: TMsgDlgButtons): TModalResult;
var
  DlgButtons: TMsgDlgButtons;
begin
  if CompareFilenames(SrcFilename,DestFilename)=0 then begin
    Result:=mrOk;
    exit;
  end;
  repeat
    if RenameFile(SrcFilename,DestFilename) then begin
      break;
    end else begin
      DlgButtons:=[mbCancel,mbRetry]+ExtraButtons;
      Result:=MessageDlg('Unable to rename file',
        'Unable to rename file "'+SrcFilename+'"'#13
        +'to "'+DestFilename+'".',
        mtError,DlgButtons,0);
      if (Result<>mrRetry) then exit;
    end;
  until false;
  Result:=mrOk;
end;

function CheckFileIsWritable(const Filename: string;
  ErrorButtons: TMsgDlgButtons): TModalResult;
begin
  Result:=mrOk;
  while not FileIsWritable(Filename) do begin
    Result:=MessageDlg('File is not writable',
      'Unable to write to file "'+Filename+'"',
      mtError,ErrorButtons+[mbCancel],0);
    if Result<>mrRetry then exit;
  end;
end;

function ForceDirectoryInteractive(Directory: string;
  ErrorButtons: TMsgDlgButtons): TModalResult;
var i: integer;
  Dir: string;
begin
  DoDirSeparators(Directory);
  Directory:=AppendPathDelim(Directory);
  i:=1;
  while i<=length(Directory) do begin
    if Directory[i]=PathDelim then begin
      Dir:=copy(Directory,1,i-1);
      if not DirPathExists(Dir) then begin
        while not CreateDir(Dir) do begin
          Result:=MessageDlg('Unable to create directory',
            'Unable to create directory "'+Dir+'"',
            mtError,ErrorButtons+[mbCancel],0);
          if Result<>mrRetry then exit;
        end;
      end;
    end;
    inc(i);
  end;
  Result:=mrOk;
end;

function BackupFileInteractive(const Filename: string;
  ErrorButtons: TMsgDlgButtons): TModalResult;
begin
  Result:=mrOk;
  if Filename='' then ;
  if ErrorButtons=[] then ;
end;

function WarnOverwriteIfFileExists(const Filename: string;
  ErrorButtons: TMsgDlgButtons): TModalResult;
begin
  Result:=mrOk;
  if not FileExists(Filename) then exit;
  Result:=MessageDlg('Overwrite?',
    'File "'+Filename+'" exists.'#13
    +'Overwrite?',mtWarning,[mbYes,mbNo]+ErrorButtons,0);
  if Result=mrYes then
    Result:=mrOk
  else if Result=mrNo then
    Result:=mrCancel;
end;

end.

