unit CombatantEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  Buttons, StdCtrls, RTTICtrls,
  Combatant,ArmorEditor, WeaponEditor, StatEditor;

type

  { TCombatantEditorDlg }

  TCombatantEditorDlg = class(TForm)
    AddWeaponButton: TButton;
    EditWeaponButton: TButton;
    ConstitutionLabel: TLabel;
    AgilityLabel: TLabel;
    EmpathyLabel: TLabel;
    IntuitionLabel: TLabel;
    StatIntuLabel: TLabel;
    StatEmpaLabel: TLabel;
    StatPresLabel: TLabel;
    StatQuicLabel: TLabel;
    StatStrenLabel: TLabel;
    StatMemoLabel: TLabel;
    StatReasLabel: TLabel;
    StatSelfLabel: TLabel;
    StatAgilLabel: TLabel;
    StatConsLabel: TLabel;
    PresenceLabel: TLabel;
    QuicknessLabel: TLabel;
    StrengthLabel: TLabel;
    MemoryLabel: TLabel;
    ReasoningLabel: TLabel;
    SelfdiciplineLabel: TLabel;
    RemoveWeaponButton: TButton;
    EditArmorButton: TButton;
    SizeLabel: TLabel;
    StatsGroupBox: TGroupBox;
    SizeTISpinEdit: TTISpinEdit;
    WeaponsListBox: TListBox;
    WeaponsGroupBox: TGroupBox;
    PenalitiesListBox: TListBox;
    PenalitiesGroupBox: TGroupBox;
    GroupsGroupBox: TGroupBox;
    HitsLabel: TLabel;
    HealHitsLabel: TLabel;
    GroupsListBox: TListBox;
    NameLabel: TLabel;
    MiscGroupBox: TGroupBox;
    OkButton: TButton;
    CancelButton: TButton;
    NameTIEdit: TTIEdit;
    HitsTIEdit: TTIEdit;
    HealHitsTIEdit: TTIEdit;
    procedure AddWeaponButtonClick(Sender: TObject);
    procedure CombatantEditorDlgCreate(Sender: TObject);
    procedure CombatantEditorDlgDestroy(Sender: TObject);
    procedure EditArmorButtonClick(Sender: TObject);
    procedure EditWeaponButtonClick(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure StatsGroupBoxDblClick(Sender: TObject);
    procedure UpdateStats;
    procedure UpdatePenalities;
  private
    FCombatant: TCombatant;
    procedure SetCombatant(const AValue: TCombatant);
    procedure FillWeaponsListBox;
  public
    property Combatant: TCombatant read FCombatant write SetCombatant;
  end;

function ShowCombatantEditor(ACombatant: TCombatant): TModalResult;

implementation

function ShowCombatantEditor(ACombatant: TCombatant): TModalResult;
var
  CombatantEditorDlg: TCombatantEditorDlg;
begin
  CombatantEditorDlg:=TCombatantEditorDlg.Create(nil);
  CombatantEditorDlg.Combatant:=ACombatant;
  Result:=CombatantEditorDlg.ShowModal;
  if Result=mrOk then
    ACombatant.Assign(CombatantEditorDlg.Combatant);
  CombatantEditorDlg.Free;
end;

{ TCombatantEditorDlg }

procedure TCombatantEditorDlg.CombatantEditorDlgCreate(Sender: TObject);
begin
  if Sender=nil then ;
  FCombatant:=TCombatant.Create;
  NameTIEdit.Link.SetObjectAndProperty(FCombatant,'Name');
  SizeTISpinEdit.Link.SetObjectAndProperty(FCombatant,'Size');
  HitsTIEdit.Link.SetObjectAndProperty(FCombatant.Hits,'MaxHits');
  HealHitsTIEdit.Link.SetObjectAndProperty(FCombatant.Hits,'HealApPerHit');
  //WeaponsListBox.Items.Add('bla');// TODO Weapons
 UpdateStats;
end;

procedure TCombatantEditorDlg.AddWeaponButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  ShowWeaponEditor(Combatant.Weapons,-1,true);
  FillWeaponsListBox;
end;

procedure TCombatantEditorDlg.CombatantEditorDlgDestroy(Sender: TObject);
begin
  if Sender=nil then ;
  FCombatant.Free;
end;

procedure TCombatantEditorDlg.EditArmorButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  ShowArmorEditor(Combatant.Armor);
end;

procedure TCombatantEditorDlg.EditWeaponButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  if WeaponsListBox.Items.Count=0 then exit;
  ShowWeaponEditor(Combatant.Weapons,WeaponsListBox.ItemIndex,false);
  FillWeaponsListBox;
end;

procedure TCombatantEditorDlg.OkButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  DebugLn('TCombatantEditorDlg.OkButtonClick ',Combatant.Name,' Size=',dbgs(Combatant.Size));
  ModalResult:=mrOk;
end;

procedure TCombatantEditorDlg.StatsGroupBoxDblClick(Sender: TObject);
begin
  if Sender=nil then ;
  //TODO neue unit Stateditor
  ShowStatEditor(Combatant.Stats);
  UpdateStats;
end;


procedure TCombatantEditorDlg.UpdateStats;
begin
  StatConsLabel.Caption:=IntToStr(FCombatant.Stats.Total[stConstitution]);
  StatAgilLabel.Caption:=IntToStr(FCombatant.Stats.Total[stAgility]);
  StatSelfLabel.Caption:=IntToStr(FCombatant.Stats.Total[stSelfdiscipline]);
  StatReasLabel.Caption:=IntToStr(FCombatant.Stats.Total[stReasoning]);
  StatMemoLabel.Caption:=IntToStr(FCombatant.Stats.Total[stMemory]);
  StatStrenLabel.Caption:=IntToStr(FCombatant.Stats.Total[stStrength]);
  StatQuicLabel.Caption:=IntToStr(FCombatant.Stats.Total[stQuickness]);
  StatPresLabel.Caption:=IntToStr(FCombatant.Stats.Total[stPresence]);
  StatEmpaLabel.Caption:=IntToStr(FCombatant.Stats.Total[stEmpathy]);
  StatIntuLabel.Caption:=IntToStr(FCombatant.Stats.Total[stIntuition]);
end;

procedure TCombatantEditorDlg.UpdatePenalities;
var
  i:Integer;
begin
  for i:=0 to Combatant.Penalties.Count-1 do
    PenalitiesListBox.Items.Add(Combatant.Penalties[i].Text);
end;

procedure TCombatantEditorDlg.SetCombatant(const AValue: TCombatant);
begin
  if FCombatant=AValue then exit;
  FCombatant.Assign(AValue);
  debugln(['TCombatantEditorDlg.SetCombatant Name=',FCombatant.Name,' ',
    NameTIEdit.Link.GetAsText,' ',ploReadOnIdle in NameTIEdit.Link.Options]);
  FillWeaponsListBox;
  UpdateStats;
end;

procedure TCombatantEditorDlg.FillWeaponsListBox;
var
  i: Integer;
begin
  WeaponsListBox.Items.BeginUpdate;
  WeaponsListBox.Items.Clear;
  for i:=0 to Combatant.Weapons.Count-1 do
    WeaponsListBox.Items.Add(Combatant.Weapons[i].WeaponName);
  WeaponsListBox.Items.BeginUpdate;
end;

initialization
  {$I combatanteditor.lrs}

end.

