program RPCombat;

{$mode objfpc}{$H+}

uses
  //MemCheck,
  Interfaces, // this includes the LCL widgetset
  Forms,
  MainUnit, Combatant, CombatGroups, CombatantEditor,
  ArmorEditor, WeaponEditor, StatEditor, HUGStats, BFProcs, BFDlgprocs,
  BFGroupEditor, StringInput, PenalitiesEditor;

begin
  Application.Title:='RPCombat';
  Application.Initialize;
  Application.CreateForm(TBattleForm, BattleForm);
  Application.Run;
end.

