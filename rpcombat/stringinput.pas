unit StringInput;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons;

type

  { TStringInputForm }

  TStringInputForm = class(TForm)
    OKButton: TButton;
    StringEdit: TEdit;
    StringInputLabel: TLabel;
    procedure OKButtonClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

function ShowStringInputDlg(Text: string): string;



implementation

function ShowStringInputDlg(Text: string): string;
  var
    StringInputDlg: TStringInputForm;
  begin
    StringInputDlg:=TStringInputForm.Create(nil);
    StringInputDlg.StringEdit.Text:=Text;
    if StringInputDlg.ShowModal=mrOk then
      Result:=StringInputDlg.StringEdit.Text
    else
      Result:='';
    StringInputDlg.Free;
  end;

{ TStringInputForm }

procedure TStringInputForm.OKButtonClick(Sender: TObject);
begin
  if Sender=nil then;
  ModalResult:=mrOK;
end;

initialization
  {$I stringinput.lrs}

end.

