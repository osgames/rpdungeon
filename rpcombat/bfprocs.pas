unit BFProcs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Laz_XMLCfg;

procedure LoadStrFromStream(s: TStream; var str: string);
procedure SaveStrToStream(s: TStream; const str: string);

procedure LoadRecentList(XMLConfig: TXMLConfig; List: TStrings;
  const Path: string);
procedure SaveRecentList(XMLConfig: TXMLConfig; List: TStrings;
  const Path: string);
function AddToRecentList(const s: string; RecentList: TStrings;
  Max: integer): boolean;
function RemoveFromRecentList(const s: string; RecentList: TStrings): boolean;
procedure LoadStringList(XMLConfig: TXMLConfig; List: TStrings;
  const Path: string);
procedure SaveStringList(XMLConfig: TXMLConfig; List: TStrings;
  const Path: string);
procedure RemoveDoubles(List: TStrings);

procedure LoadRectFromXMLConfig(XMLConfig: TXMLConfig; const Path: string;
                                var ARect: TRect);
procedure SaveRectToXMLConfig(XMLConfig: TXMLConfig; const Path: string;
                              const ARect: TRect);

implementation

procedure LoadStrFromStream(s: TStream; var str: string);
var
  l: Integer;
begin
  l:=0;
  s.Read(l,SizeOf(l));
  SetLength(str,l);
  if l<>0 then s.Read(str[1],l);
end;

procedure SaveStrToStream(s: TStream; const str: string);
var
  l: Integer;
begin
  l:=length(str);
  s.Write(l,SizeOf(l));
  if l<>0 then s.Write(str[1],l);
end;

procedure LoadRecentList(XMLConfig: TXMLConfig; List: TStrings;
  const Path: string);
begin
  LoadStringList(XMLConfig,List,Path);
end;

procedure SaveRecentList(XMLConfig: TXMLConfig; List: TStrings;
  const Path: string);
begin
  SaveStringList(XMLConfig,List,Path);
end;

function AddToRecentList(const s: string; RecentList: TStrings;
  Max: integer): boolean;
begin
  if (RecentList.Count>0) and (RecentList[0]=s) then begin
    Result:=false;
    exit;
  end else begin
    Result:=true;
  end;
  RemoveFromRecentList(s,RecentList);
  RecentList.Insert(0,s);
  if Max>0 then
    while RecentList.Count>Max do
      RecentList.Delete(RecentList.Count-1);
end;

function RemoveFromRecentList(const s: string; RecentList: TStrings): boolean;
var i: integer;
begin
  Result:=false;
  i:=RecentList.Count-1;
  while i>=0 do begin
    if RecentList[i]=s then begin
      RecentList.Delete(i);
      Result:=true;
    end;
    dec(i);
  end;
end;

procedure LoadStringList(XMLConfig: TXMLConfig; List: TStrings;
  const Path: string);
var i,Count: integer;
  s: string;
begin
  Count:=XMLConfig.GetValue(Path+'Count',0);
  List.Clear;
  for i:=1 to Count do begin
    s:=XMLConfig.GetValue(Path+'Item'+IntToStr(i)+'/Value','');
    if s<>'' then List.Add(s);
  end;
end;

procedure SaveStringList(XMLConfig: TXMLConfig; List: TStrings;
  const Path: string);
var i: integer;
begin
  XMLConfig.SetDeleteValue(Path+'Count',List.Count,0);
  for i:=0 to List.Count-1 do
    XMLConfig.SetDeleteValue(Path+'Item'+IntToStr(i+1)+'/Value',List[i],'');
end;

procedure RemoveDoubles(List: TStrings);
var
  i: Integer;
  j: Integer;
begin
  for i:=0 to List.Count-2 do begin
    for j:=List.Count-1 downto i+1 do
      if List[i]=List[j] then
        List.Delete(j);
  end;
end;

procedure LoadRectFromXMLConfig(XMLConfig: TXMLConfig; const Path: string;
  var ARect: TRect);
begin
  ARect.Left:=XMLConfig.GetValue(Path+'Left',0);
  ARect.Top:=XMLConfig.GetValue(Path+'Top',0);
  ARect.Right:=XMLConfig.GetValue(Path+'Right',0);
  ARect.Bottom:=XMLConfig.GetValue(Path+'Bottom',0);
end;

procedure SaveRectToXMLConfig(XMLConfig: TXMLConfig; const Path: string;
  const ARect: TRect);
begin
  XMLConfig.SetDeleteValue(Path+'Left',ARect.Left,0);
  XMLConfig.SetDeleteValue(Path+'Top',ARect.Top,0);
  XMLConfig.SetDeleteValue(Path+'Right',ARect.Right,0);
  XMLConfig.SetDeleteValue(Path+'Bottom',ARect.Bottom,0);
end;

end.

