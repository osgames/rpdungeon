unit BFGroupEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, ExtCtrls, ButtonPanel, Combatant, CombatGroups, StringInput,
  CombatantEditor;

type

  { TGroupEditorDlg }

  TGroupEditorDlg = class(TForm)
    AddToGroupBitBtn: TBitBtn;
    AddGroupBitBtn: TBitBtn;
    DeleteGroupBitBtn: TBitBtn;
    ButtonPanel1: TButtonPanel;
    RemoveFromGroupBitBtn: TBitBtn;
    CurGroupGroupBox: TGroupBox;
    CombatantsGroupBox: TGroupBox;
    GroupsGroupBox: TGroupBox;
    GroupsListBox: TListBox;
    GroupMembersListBox: TListBox;
    CombatantsListBox: TListBox;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    procedure AddGroupBitBtnClick(Sender: TObject);
    procedure AddToGroupBitBtnClick(Sender: TObject);
//    procedure CombatantsListBoxSelectionChange(Sender: TObject; User: boolean);
    procedure DeleteGroupBitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GroupMembersListBoxDblClick(Sender: TObject);
    procedure GroupsListBoxSelectionChange(Sender: TObject; User: boolean);
    procedure RemoveFromGroupBitBtnClick(Sender: TObject);
  private
    FCurGroup: TCombatantGroup;
    FGroups: TCombatantGroups;// temporary Group list
    FCombatants: TCombatantGroup;// global group list, do not free !
    function GetCombatantCount: integer;
    function GetCombatants(Index: integer): TCombatant;
    procedure SetCurGroup(const AValue: TCombatantGroup);
    procedure SetGroups(const AValue: TCombatantGroups);
    procedure UpDateCurGroup;
  public
    procedure FillAll;
    procedure FillGroupsListBox;
    procedure FillMembersListBox;
    procedure FillCombatantsListbox;
    property CurGroup: TCombatantGroup read FCurGroup write SetCurGroup;
    property Groups: TCombatantGroups read FGroups write SetGroups;
    property Combatants[Index: integer]: TCombatant read GetCombatants;
    property CombatantCount: integer read GetCombatantCount;
  end;


function ShowGroupEditorDlg(Groups: TCombatantGroups;
  ListOfCombatants: TCombatantGroup): TModalResult;

implementation

function ShowGroupEditorDlg(Groups: TCombatantGroups;
  ListOfCombatants: TCombatantGroup): TModalResult;
var
  GroupEditorDlg: TGroupEditorDlg;
begin
  GroupEditorDlg:=TGroupEditorDlg.Create(nil);
  GroupEditorDlg.Groups:=Groups;
  GroupEditorDlg.FCombatants:=ListOfCombatants;
  GroupEditorDlg.FillAll;
  Result:=GroupEditorDlg.ShowModal;
  if Result=mrOk then begin
    Groups.CopyGroups(GroupEditorDlg.Groups);
    Groups.Modified:=true;
  end;
  GroupEditorDlg.Free;
end;

{ TGroupEditorDlg }

procedure TGroupEditorDlg.SetGroups(const AValue: TCombatantGroups);
begin
  if FGroups=AValue then exit;
  FGroups.CopyGroups(AValue);
end;

procedure TGroupEditorDlg.UpDateCurGroup;
begin
  if Groups.Count < GroupsListBox.ItemIndex then exit;
  if GroupsListBox.ItemIndex < 0 then exit;
  CurGroup:=Groups.Items[GroupsListBox.ItemIndex];
end;

procedure TGroupEditorDlg.FillAll;
begin
  FillCombatantsListbox;
  writeln('filled combatants');
  if Groups.Count>0 then
    begin
      writeln('Gruppen.count');
      FillGroupsListBox;
      writeln('filled groups');
      GroupsListBox.ItemIndex:=0;
      FillMembersListBox;
      writeln('filled members');
    end
  else
    begin
      GroupsListBox.Clear;
      GroupMembersListBox.Clear;
    end;
end;

procedure TGroupEditorDlg.FillGroupsListBox;
var
  i:integer;
  OldItemIndex: LongInt;
begin
  //if Groups.Count=0 then exit;
  writeln('fill GruppeListe1');
  OldItemIndex:=GroupsListBox.ItemIndex;
  GroupsListBox.Items.BeginUpdate;
  GroupsListBox.Items.Clear;
  writeln('fill GruppeListe  :',GroupsListBox.Items.Count);
  if Groups.Count>0 then
      for i:=0 to Groups.Count-1 do
        GroupsListBox.Items.Add(Groups[i].Name);
  GroupsListBox.Items.EndUpdate;
  if Groups.Count=1 then GroupsListBox.ItemIndex:=0
  else
    if GroupsListBox.ItemIndex > Groups.Count-1 then
                                 GroupsListBox.ItemIndex := Groups.Count-1
    else GroupsListBox.ItemIndex := OldItemIndex
end;

procedure TGroupEditorDlg.FillMembersListBox;
var
  i:integer;
begin
  GroupMembersListBox.Items.BeginUpdate;
  GroupMembersListBox.Items.Clear;
  UpDateCurGroup;
  writeln('CurGroup.Count ',CurGroup.Count,' ', CurGroup.Name);
  if GroupsListBox.ItemIndex >= 0 then
    writeln('FillMembersListBox ItemIndex >= 0');
  for i:=0 to CurGroup.Count-1 do
    GroupMembersListBox.Items.Add(CurGroup.Items[i].Name);
  GroupMembersListBox.Items.EndUpdate;
end;

procedure TGroupEditorDlg.FillCombatantsListbox;
var
  i: Integer;
  OldItemIndex:integer;
begin
  OldItemIndex:=CombatantsListBox.ItemIndex;
  CombatantsListBox.Items.BeginUpdate;
  CombatantsListBox.Items.Clear;
  for i:=0 to CombatantCount-1 do
    CombatantsListBox.Items.Add(Combatants[i].Name);
  CombatantsListBox.Items.EndUpdate;
  CombatantsListBox.ItemIndex:=OldItemIndex;
end;

procedure TGroupEditorDlg.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;
  FGroups:=TCombatantGroups.Create;
end;

procedure TGroupEditorDlg.AddToGroupBitBtnClick(Sender: TObject);
var
  NewCombatant:TCombatant;
begin
  if CombatantsListBox.ItemIndex<0 then exit;
  UpDateCurGroup;
  NewCombatant:=TCombatant.Create;
  NewCombatant.Assign(Combatants[CombatantsListBox.ItemIndex]);
  CurGroup.Add(NewCombatant);
  writeln('zu Gruppe zugefuegt  ',CurGroup.Count);
  FillMembersListBox;
  CurGroup.Modified:=true;
end;

//procedure TGroupEditorDlg.CombatantsListBoxSelectionChange(Sender: TObject;
//  User: boolean);
//begin
  //if CombatantsListBox.ItemIndex<0 then exit;
 // FillCombatantsListbox;
//end;

procedure TGroupEditorDlg.DeleteGroupBitBtnClick(Sender: TObject);
begin
  if GroupsListBox.ItemIndex<0 then exit;
  Groups.RemoveGroup(GroupsListBox.ItemIndex);
  writeln('Gruppe geloescht :',Groups.Count);
  FillAll;
end;

procedure TGroupEditorDlg.AddGroupBitBtnClick(Sender: TObject);
var
  NewGroup:TCombatantGroup;
  TempName: string;
begin
  NewGroup:=TCombatantGroup.Create;
  writeln('vor StrInput');
  TempName:=ShowStringInputDlg('NoName');
  writeln('nach StrInput');
  NewGroup.Name:=Groups.NewName(TempName);
  Groups.Add(NewGroup);
  writeln('Gruppe zugefuegt');
  FillAll;
  NewGroup.Modified:=true;
end;

procedure TGroupEditorDlg.FormDestroy(Sender: TObject);
begin
  if Sender=nil then ;
  FGroups.Free;
end;

procedure TGroupEditorDlg.GroupMembersListBoxDblClick(Sender: TObject);
var
  SearchName:string;
begin
  if Sender = nil then ;
  SearchName := GroupMembersListBox.Items[GroupMembersListBox.ItemIndex];
  ShowCombatantEditor(CurGroup.FindCombatantByName(SearchName));
  FillMembersListBox;
end;

procedure TGroupEditorDlg.GroupsListBoxSelectionChange(Sender: TObject;
  User: boolean);
begin
  if GroupsListBox.ItemIndex < 0 then exit;
  if User then ;
  UpDateCurGroup;
  writeln('groupslistbox sel change  :', Groups.Count,'  :',GroupsListBox.ItemIndex);
  FillMembersListBox;
  writeln('Gruppe gewaechselt');
end;

procedure TGroupEditorDlg.RemoveFromGroupBitBtnClick(Sender: TObject);
begin
  if GroupMembersListBox.ItemIndex<0 then exit;
  UpDateCurGroup;
  CurGroup.Remove(GroupMembersListBox.ItemIndex);
  FillAll;
end;

function TGroupEditorDlg.GetCombatantCount: integer;
begin
  Result:=FCombatants.Count;
end;

function TGroupEditorDlg.GetCombatants(Index: integer): TCombatant;
begin
  Result:=FCombatants[Index];
end;

procedure TGroupEditorDlg.SetCurGroup(const AValue: TCombatantGroup);
begin
  if FCurGroup=AValue then exit;
  FCurGroup:=AValue;
end;

initialization
  {$I bfgroupeditor.lrs}

end.

