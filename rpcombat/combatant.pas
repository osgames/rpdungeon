unit Combatant;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, LCLProc, RMStats, Laz_XMLCfg;
  
type
  TCombatantGroup = class;

  TWuchtAspekt = (
    waStumpf,
    waScharf,
    waSpitz,
    waFeuer,
    waDruck,
    waBlitz
    );

  { TArmorPart }

  TArmorPart = class(TPersistent)
  private
    FModified: boolean;
    FPlace: string;
    FWucht: array[TWuchtAspekt] of integer;
    function GetWucht(Aspekt: TWuchtAspekt): integer;
    procedure SetModified(const AValue: boolean);
    procedure SetPlace(const AValue: string);
    procedure SetWucht(Aspekt: TWuchtAspekt; const AValue: integer);
  public
    constructor Create;
    constructor Create(const APlace: string;
                       AStumpf,AScharf,ASpitz,AFeuer,ADruck,ABlitz: integer);
    procedure Assign(Source: TPersistent); override;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
  public
    property Place: string read FPlace write SetPlace;
    property Wucht[Aspekt: TWuchtAspekt]: integer read GetWucht write SetWucht;
    property Modified: boolean read FModified write SetModified;
  end;

  
  { TArmorParts }

  TArmorParts = class(TPersistent)
  private
    FItems: TFPList;
    FModified: boolean;
    function GetCount: integer;
    function GetItems(Index: integer): TArmorPart;
    function GetModified: boolean;
    procedure SetItems(Index: integer; const AValue: TArmorPart);
    procedure SetModified(const AValue: boolean);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Assign(Source: TPersistent); override;
    function Add(ArmorPart: TArmorPart): integer;
    procedure Delete(Index: integer);
    procedure AddDefaultParts;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
  public
    property Items[Index: integer]: TArmorPart read GetItems write SetItems; default;
    property Count: integer read GetCount;
    property Modified: boolean read GetModified write SetModified;
  end;


  { TBattleArmor }

  TBattleArmor = class(TPersistent)
  private
    FArmorDB: integer;
    FArmorParts: TArmorParts;
    FElementalDB: integer;
    FQuicknessDB: integer;
    FShieldDB: integer;
    FModified: boolean;
    function GetModified: boolean;
    procedure SetArmorDB(const AValue: integer);
    procedure SetElementalDB(const AValue: integer);
    procedure SetModified(const AValue: boolean);
    procedure SetQuicknessDB(const AValue: integer);
    procedure SetShieldDB(const AValue: integer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Assign(Source: TPersistent); override;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    property Modified: boolean read GetModified write SetModified;
  published
    property ArmorDB: integer read FArmorDB write SetArmorDB;
    property ElementalDB: integer read FElementalDB write SetElementalDB;
    property QuicknessDB: integer read FQuicknessDB write SetQuicknessDB;
    property ShieldDB: integer read FShieldDB write SetShieldDB;
    property ArmorParts: TArmorParts read FArmorParts;
  end;


  { TCombatHits }

  TCombatHits = class(TPersistent)
  private
    FHealApPerHit: integer;
    FMaxHits: integer;
    FModified: boolean;
    procedure SetHealApPerHit(const AValue: integer);
    procedure SetMaxHits(const AValue: integer);
    procedure SetModified(const AValue: boolean);
  public
    procedure Clear;
    procedure Assign(Source: TPersistent); override;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    property Modified: boolean read FModified write SetModified;
  published
    property MaxHits: integer read FMaxHits write SetMaxHits;
    property HealApPerHit: integer read FHealApPerHit write SetHealApPerHit;
  end;


  {TCombatPenaltyReason = (
    cprNone,   // undefined
    cprCustom, // user defined
    cprCut,
    cprBruise,
    cprStabWound,
    cprBurn,    // burn or frost
    cprHitsPerRound, // hits per round (unspecific)
    cprBleeding,// hits per round due to cut
    cprBurning, // hits per round due to burning
    cprStatLoss, // stat is reduced, e.g. constitution loss
    cprExhaustion, // normal exhaustion
    cprParry,
    cprStun,
    cprStunNoParry,
    cprUnbalanced,
    cprKnockedDown,
    cprLostInitiative
    );}

  { TCombatPenalty }

  TCombatPenalty = class(TPersistent)
  private
    FDuration: integer;
    FHits: integer;
    FHitsPerRD: integer;
    FModified: boolean;
    FPenalty: integer;
    FText: string;
    FStartTime: integer;
    FStatDecrease: integer;
    procedure SetDuration(const AValue: integer);
    procedure SetModified(const AValue: boolean);
    procedure SetText(const AValue: string);
    procedure SetStartTime(const AValue: integer);
  public
    procedure Assign(Source: TPersistent); override;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
  public
    property Modified: boolean read FModified write SetModified;
  published
    property Duration: integer read FDuration write SetDuration;
    property StartTime: integer read FStartTime write SetStartTime;
    property Text: string read FText write SetText;
  end;
  
  
  { TCombatPenalties }

  TCombatPenalties = class(TPersistent)
  private
    FItems: TFPList;
    FModified: boolean;
    function GetCount: integer;
    function GetItems(Index: integer): TCombatPenalty;
    function GetModified: boolean;
    procedure SetItems(Index: integer; const AValue: TCombatPenalty);
    procedure SetModified(const AValue: boolean);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Assign(Source: TPersistent); override;
    function Add(Penalty: TCombatPenalty): integer;
    procedure Delete(Index: integer);
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    function AsString: string;
  public
    property Items[Index: integer]: TCombatPenalty read GetItems write SetItems; default;
    property Count: integer read GetCount;
    property Modified: boolean read GetModified write SetModified;
  end;
  
  
  { TCombatAttack }
  
  TCombatAttackType = (
    catOptional, // e.g. Stump/Scharf/Spitz of a Handaxe
    catAddional  // e.g. Flaming Weapon
    );
  
  TCombatAttack = class(TPersistent)
  private
    FAttack: TWuchtAspekt;
    FCritical: TWuchtAspekt;
    FTheType: TCombatAttackType;
    FWucht: integer;
    FModified: boolean;
    function GetModified: boolean;
    procedure SetAttack(const AValue: TWuchtAspekt);
    procedure SetCritical(const AValue: TWuchtAspekt);
    procedure SetModified(const AValue: boolean);
    procedure SetTheType(const AValue: TCombatAttackType);
    procedure SetWucht(const AValue: integer);
  public
    procedure Assign(Source: TPersistent); override;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    property Modified: boolean read GetModified write SetModified;
  published
    property Wucht: integer read FWucht write SetWucht;
    property Attack: TWuchtAspekt read FAttack write SetAttack;
    property Critical: TWuchtAspekt read FCritical write SetCritical;
    property TheType: TCombatAttackType read FTheType write SetTheType;
  end;
  
  
  { TCombatWeapon }
  
    TCombatWeapon = class(TPersistent)
  private
    FAttacks: TFPList;
    FAttackOrAim: integer;
    FDraw: integer;
    FFullParry: integer;
    FFumbleRange: integer;
    FOB: integer;
    FReload: integer;
    FWeaponName: string;
    FModified: boolean;
    function GetAttackCount: integer;
    function GetAttacks(Index: integer): TCombatAttack;
    function GetModified: boolean;
    procedure SetAttackOrAim(const AValue: integer);
    procedure SetAttacks(Index: integer; const AValue: TCombatAttack);
    procedure SetDraw(const AValue: integer);
    procedure SetFullParry(const AValue: integer);
    procedure SetFumbleRange(const AValue: integer);
    procedure SetModified(const AValue: boolean);
    procedure SetOB(const AValue: integer);
    procedure SetReload(const AValue: integer);
    procedure SetWeaponName(const AValue: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Assign(Source: TPersistent); override;
    function AddAttack(Attack: TCombatAttack): integer;
    procedure DeleteAttack(Index: integer);
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
  public
    property Attacks[Index: integer]: TCombatAttack read GetAttacks write SetAttacks; default;
    property AttackCount: integer read GetAttackCount;
    property Modified: boolean read GetModified write SetModified;
  published
    property AttackOrAim: integer read FAttackOrAim write SetAttackOrAim;
    property Draw: integer read FDraw write SetDraw;
    property Reload: integer read FReload write SetReload;
    property FullParry: integer read FFullParry write SetFullParry;
    property FumbleRange: integer read FFumbleRange write SetFumbleRange;
    property OB: integer read FOB write SetOB;
    property WeaponName : string read FWeaponName write SetWeaponName;
  end;
  

  { TBattleWeapons }

  TBattleWeapons = class(TPersistent)
  private
    FItems: TFPList;
    FModified: boolean;
    function GetCount: integer;
    function GetItems(Index: integer): TCombatWeapon;
    function GetModified: boolean;
    procedure SetItems(Index: integer; const AValue: TCombatWeapon);
    procedure SetModified(const AValue: boolean);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Assign(Source: TPersistent); override;
    function Add(Weapon: TCombatWeapon): integer;
    procedure Delete(Index: integer);
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
  public
    property Items[Index: integer]: TCombatWeapon read GetItems write SetItems; default;
    property Count: integer read GetCount;
    property Modified: boolean read GetModified write SetModified;
  end;
  
  
  { TCombatAktion }
  
  TCombatAktion = class(TPersistent)
  private
    FCancelled: boolean;
    FCancelTime: integer;
    FComment: string;
    FDuration: integer;
    FModified: boolean;
    procedure SetCancelled(const AValue: boolean);
    procedure SetCancelTime(const AValue: integer);
    procedure SetComment(const AValue: string);
    procedure SetDuration(const AValue: integer);
  protected
    function GetModified: boolean; virtual;
    procedure SetModified(const AValue: boolean); virtual;
  public
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string); virtual;
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string); virtual;
    property Modified: boolean read GetModified write SetModified;
  published
    property Duration: integer read FDuration write SetDuration;// in half AP (activity points)
    property Comment: string read FComment write SetComment;
    property Cancelled: boolean read FCancelled write SetCancelled;
    property CancelTime: integer read FCancelTime write SetCancelTime;
  end;
  
  
  { TCombatAktionAttack }
  
  TCombatAktionAttack = class(TCombatAktion)
  private
    FWeapon: TCombatWeapon;
    procedure SetWeapon(const AValue: TCombatWeapon);
  public
    property Weapon: TCombatWeapon read FWeapon write SetWeapon;
  end;


  { TCombatAktionSpell }

  TCombatAktionSpell = class(TCombatAktion)
  private
    FCastingTime: integer;
    FRecovery: integer;
    procedure SetCastingTime(const AValue: integer);
    procedure SetRecovery(const AValue: integer);
  public
    property CastingTime: integer read FCastingTime write SetCastingTime;
    property Recovery: integer read FRecovery write SetRecovery;
  end;


  { TCombatAktionPerception }

  TCombatAktionPerception = class(TCombatAktion)
  private
    FPerceptionTime: integer;
    procedure SetPerceptionTime(const AValue: integer);
  public
    property PerceptionTime: integer read FPerceptionTime write SetPerceptionTime;
  end;


  { TCombatAktionDelay }

  TCombatAktionDelay = class(TCombatAktion)
  public
  end;


  { TCombatAktionFullParry }

  TCombatAktionFullParry = class(TCombatAktion)
  private
    FWeapon: TCombatWeapon;
    procedure SetWeapon(const AValue: TCombatWeapon);
  public
    property Weapon: TCombatWeapon read FWeapon write SetWeapon;
  end;


  { TCombatAktionChargingAttack }

  TCombatAktionChargingAttack = class(TCombatAktionAttack)
  public
  end;


  { TCombatAktionDraw }

  TCombatAktionDraw = class(TCombatAktion)
  private
    FWeapon: TCombatWeapon;
    procedure SetWeapon(const AValue: TCombatWeapon);
  public
    property Weapon: TCombatWeapon read FWeapon write SetWeapon;
  end;


  { TCombatAktionReload }

  TCombatAktionReload = class(TCombatAktion)
  private
    FWeapon: TCombatWeapon;
    procedure SetWeapon(const AValue: TCombatWeapon);
  public
    property Weapon: TCombatWeapon read FWeapon write SetWeapon;
  end;


  { TCombatAktionMovement }

  TCombatAktionMovement = class(TCombatAktion)
  private
    FDistanceInM: double;
    procedure SetDistanceInM(const AValue: double);
  public
    property DistanceInM: double read FDistanceInM write SetDistanceInM;
  end;
  
  
  TStatType = (
    stConstitution:=0,
    stAgility,
    stSelfdiscipline,
    stReasoning,
    stMemory,
    stStrength,
    stQuickness,
    stPresence,
    stEmpathy,
    stIntuition
    );
  TArrayOfStatType = array[TStatType] of integer;
  
  { TCombatantStats }
  
  TCombatantStats = class(TPersistent)
  private
    FModified: boolean;
    FTemp, FPot, FRace, FMisc, FTotal, FBonus: TArrayOfStatType;
    function GetBonus(Index: TStatType): integer;
    function GetMisc(Index: TStatType): integer;
    function GetPot(Index: TStatType): integer;
    function GetRace(Index: TStatType): integer;
    function GetTemp(Index: TStatType): integer;
    function GetTotal(Index: TStatType): integer;
    procedure SetMisc(Index: TStatType; const AValue: integer);
    procedure SetModified(const AValue: boolean);
    procedure SetPot(Index: TStatType; const AValue: integer);
    procedure SetRace(Index: TStatType; const AValue: integer);
    procedure SetTemp(Index: TStatType; const AValue: integer);
  public
    procedure Clear;
    procedure CalcBonus(Index: TStatType);
    procedure CalcTotalBoni;
    procedure Assign(Source: TPersistent); override;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string); virtual;
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string); virtual;
  public
    constructor Create;
    destructor Destroy; override;
    property Temp[Index: TStatType]: integer read GetTemp write SetTemp;
    property Pot[Index: TStatType]: integer read GetPot write SetPot;
    property Race[Index: TStatType]: integer read GetRace write SetRace;
    property Misc[Index: TStatType]:integer read GetMisc write SetMisc;
    property Total[Index: TStatType]: integer read GetTotal;
    property Bonus[Index: TStatType]: integer read GetBonus;
    property Modified: boolean read FModified write SetModified;
  end;


  { TCombatant }

  TCombatant = class(TPersistent)
  private
    FAktionen: TFPList;
    FArmor: TBattleArmor;
    FFilename: string;
    FGroups: TFPList;                 //Namen der Gruppen in der der Char vorkommt
    FHits: TCombatHits;
    FModified: boolean;
    FName: string;
    FPenalties: TCombatPenalties;
    FSize: integer;
    FStats: TCombatantStats;
    FWeapons: TBattleWeapons;
    FCurrentFoe: TCombatant;
    FCurrentFoeName: string;
    function GetAktionen(Index: integer): TCombatAktion;
    function GetAktionenCount: integer;
    function GetCurrentFoeName: string;
    function GetGroupCount: integer;
    function GetGroups(Index: integer): TCombatantGroup;
    function GetModified: boolean;
    procedure SetAktionen(Index: integer; const AValue: TCombatAktion);
    procedure SetCurrentFoe(const AValue: TCombatant);
    procedure SetFilename(const AValue: string);
    procedure SetModified(const AValue: boolean);
    procedure SetName(const AValue: string);
    procedure SetSize(const AValue: integer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure ClearAktionen;
    procedure Assign(Source: TPersistent); override;
    procedure AddAktion(Aktion: TCombatAktion);
    procedure DeleteAktion(Aktion: TCombatAktion);
    procedure DeleteLastAktion;
    procedure CancelLastAktion(CancelTime: integer);
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    function LoadFromFile(const Filename: string): TModalResult;
    function SaveToFile(const Filename: string): TModalResult;
    function EndAP: integer;
  published
    property Name: string read FName write SetName;
    property Armor: TBattleArmor read FArmor;
    property Hits: TCombatHits read FHits;
    property Size: integer read FSize write SetSize;
    property CurrentFoeName: string read GetCurrentFoeName;
  public
    property Stats: TCombatantStats read FStats;
    property Penalties: TCombatPenalties read FPenalties;
    property Weapons: TBattleWeapons read FWeapons;
    property AktionenCount: integer read GetAktionenCount;
    property Aktionen[Index: integer]: TCombatAktion read GetAktionen write SetAktionen;
    property GroupCount: integer read GetGroupCount;
    property Groups[Index: integer]: TCombatantGroup read GetGroups;
    property Filename: string read FFilename write SetFilename;
    property Modified: boolean read GetModified write SetModified;
    property CurrentFoe: TCombatant read FCurrentFoe write SetCurrentFoe;
  end;
  
  
  { TCombatantGroup }

  TCombatantGroup = class(TPersistent)
  private
    FCombatants: TFPList;
    FFilename: string;
    FLoadedCombatantNames: TStrings;
    FName: string;
    FModified: boolean;
    function GetCombatants(Index: integer): TCombatant;
    function GetCount: integer;
    function GetModified: boolean;
    procedure SetCombatants(Index: integer; const AValue: TCombatant);
    procedure SetFilename(const AValue: string);
    procedure SetModified(const AValue: boolean);
    procedure SetName(const AValue: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure ClearCombatants;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    function LoadFromFile(const Filename: string): TModalResult;
    function FindCombatantByName(const AName: string):TCombatant;
    function SaveToFile(const Filename: string): TModalResult;
    procedure Assign(Source: TPersistent); override;
    procedure Add(Combatant: TCombatant);
    procedure Remove(Index:integer);
    function NewName(const StartName: string): string;
    procedure Move(FromIndex, ToIndex: integer);
  public
    property Count: integer read GetCount;
    property Items[Index: integer]: TCombatant read GetCombatants
                                               write SetCombatants; default;
    property LoadedCombatantNames: TStrings read FLoadedCombatantNames;
    property Modified: boolean read GetModified write SetModified;
    property Filename: string read FFilename write SetFilename;
  published
    property Name: string read FName write SetName;
  end;
  
const
  WuchtNames: array[TWuchtAspekt] of string = (
    'Stumpf',
    'Scharf',
    'Spitz',
    'Feuer',
    'Druck',
    'Blitz'
  );
  CombatAttackTypeNames: array[TCombatAttackType] of string =(
    'Optional',
    'Additional'
    );
  
  StatTypeNames: array[TStatType] of string = (
    'Constitution',
    'Agility',
    'Selfdiscipline',
    'Reasoning',
    'Memory',
    'Strength',
    'Quickness',
    'Presence',
    'Empathy',
    'Intuition'
    );


function WuchtNameToWuchtAspekt(const s: string): TWuchtAspekt;
function AttackTypeNameToAttackType(const s: string): TCombatAttackType;
function StatTypeNameToStatType(const s: string): TStatType;


implementation

function WuchtNameToWuchtAspekt(const s: string): TWuchtAspekt;
begin
  for Result:=Low(TWuchtAspekt) to high(TWuchtAspekt) do
    if CompareText(WuchtNames[Result],s)=0 then exit;
  Result:=waScharf;
end;

function AttackTypeNameToAttackType(const s: string): TCombatAttackType;
begin
  for Result:=Low(TCombatAttackType) to high(TCombatAttackType) do
    if CompareText(CombatAttackTypeNames[Result],s)=0 then exit;
  Result:=catOptional;
end;

function StatTypeNameToStatType(const s: string): TStatType;
begin
  for Result:=Low(TStatType) to high(TStatType) do
    if CompareText(StatTypeNames[Result],s)=0 then exit;
  Result:=stConstitution;
end;


{ TCombatant }

function TCombatant.GetAktionen(Index: integer): TCombatAktion;
begin
  Result:=TCombatAktion(FAktionen[Index]);
end;

function TCombatant.GetAktionenCount: integer;
begin
  Result:=FAktionen.Count;
end;

function TCombatant.GetCurrentFoeName: string;
begin
  if CurrentFoe<>nil then
    FCurrentFoeName:=CurrentFoe.Name;
  Result:=FCurrentFoeName;
end;

function TCombatant.GetGroupCount: integer;
begin
  Result:=FGroups.Count;
end;

function TCombatant.GetGroups(Index: integer): TCombatantGroup;
begin
  Result:=TCombatantGroup(FGroups[Index]);
end;

function TCombatant.GetModified: boolean;
begin
  Result:=FModified or Armor.Modified or Hits.Modified
          or Penalties.Modified or Weapons.Modified or Stats.Modified;
end;

procedure TCombatant.SetAktionen(Index: integer; const AValue: TCombatAktion);
begin
  if TCombatAktion(FAktionen[Index])=AValue then exit;
  FAktionen[Index]:=AValue;
  FModified:=true;
end;

procedure TCombatant.SetCurrentFoe(const AValue: TCombatant);
begin
  if FCurrentFoe=AValue then exit;
  FCurrentFoe:=AValue;
  if FCurrentFoe<>nil then
    fCurrentFoeName:=FCurrentFoe.Name
  else
    FCurrentFoeName:='';
  FModified:=true;
end;

procedure TCombatant.SetFilename(const AValue: string);
begin
  if FFilename=AValue then exit;
  FFilename:=AValue;
end;

procedure TCombatant.SetModified(const AValue: boolean);
begin
  if FModified=AValue then exit;
  FModified:=AValue;
  if not AValue then begin
    Armor.Modified:=false;
    Hits.Modified:=false;
    Penalties.Modified:=false;
    Weapons.Modified:=false;
    Stats.Modified:=false;
  end;
end;

procedure TCombatant.SetName(const AValue: string);
var
  i: Integer;
begin
  if FName=AValue then exit;
  FName:=AValue;
  FModified:=true;
  for i:=0 to GroupCount-1 do Groups[i].Modified:=true;
end;

procedure TCombatant.SetSize(const AValue: integer);
begin
  if FSize=AValue then exit;
  FSize:=AValue;
  FModified:=true;
end;

constructor TCombatant.Create;
begin
  FAktionen:=TFPList.Create;
  FArmor:=TBattleArmor.Create;
  FHits:=TCombatHits.Create;
  FPenalties:=TCombatPenalties.Create;
  FWeapons:=TBattleWeapons.Create;
  FGroups:=TFPList.Create;
  FStats:=TCombatantStats.Create;
end;

destructor TCombatant.Destroy;
begin
  Clear;
  FStats.Free;
  FArmor.Free;
  FHits.Free;
  FPenalties.Free;
  FWeapons.Free;
  ClearAktionen;
  FAktionen.Free;
  FGroups.Free;
  inherited Destroy;
end;

procedure TCombatant.Clear;
var
  i: Integer;
begin
  for i:=0 to FAktionen.Count-1 do TObject(FAktionen[i]).Free;
  FAktionen.Clear;
  FArmor.Clear;
  FGroups.Clear;
  FHits.Clear;
  FName:='';
  FPenalties.Clear;
  FSize:=0;
  FStats.Clear;
  FWeapons.Clear;
  FModified:=false;
end;

procedure TCombatant.ClearAktionen;
var
  i: Integer;
begin
  for i:=0 to FAktionen.Count-1 do
    TObject(FAktionen[i]).Free;
  FAktionen.Clear;
end;

procedure TCombatant.Assign(Source: TPersistent);
var
  Src: TCombatant;
begin
  if Source is TCombatant then begin
    Src:=TCombatant(Source);
    // TODO FAktionen: TFPList;
    FArmor.Assign(Src.Armor);
    // TODO FGroups: TFPList;
    FHits.Assign(Src.Hits);
    FName:=Src.Name;
    FPenalties.Assign(Src.Penalties);
    FSize:=Src.Size;
    FStats.Assign(Src.Stats);
    FWeapons.Assign(Src.Weapons);
    FCurrentFoeName:=Src.FCurrentFoeName;
    FCurrentFoe:=Src.FCurrentFoe;
  end else
    inherited Assign(Source);
end;

procedure TCombatant.AddAktion(Aktion: TCombatAktion);
begin
  FAktionen.Add(Aktion);
  FModified:=true;
end;

procedure TCombatant.DeleteAktion(Aktion: TCombatAktion);
begin
  FAktionen.Remove(Aktion);
  Aktion.Free;
  FModified:=true;
end;

procedure TCombatant.DeleteLastAktion;
begin
  if AktionenCount=0 then exit;
  DeleteAktion(Aktionen[AktionenCount-1]);
end;

procedure TCombatant.CancelLastAktion(CancelTime: integer);
var
  LastAktion: TCombatAktion;
begin
  if AktionenCount=0 then exit;
  LastAktion:=Aktionen[AktionenCount-1];
  LastAktion.Cancelled:=true;
  LastAktion.CancelTime:=LastAktion.Duration;
  LastAktion.Duration:=CancelTime-(EndAP-LastAktion.Duration);
  if LastAktion.Duration<1 then
    LastAktion.Duration:=1;
end;

procedure TCombatant.LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string
  );
var
  i: Integer;
  NewCount: LongInt;
  NewAktion: TCombatAktion;
begin
  Clear;
  FName:=XMLConfig.GetValue(Path+'Name/Value','');
  Armor.LoadFromXMLConfig(XMLConfig,Path+'Armor/');
  Hits.LoadFromXMLConfig(XMLConfig,Path+'Hits/');
  FSize:=XMLConfig.GetValue(Path+'Size/Value',0);
  Stats.LoadFromXMLConfig(XMLConfig,Path+'Stats/');
  Penalties.LoadFromXMLConfig(XMLConfig,Path+'Penalties/');
  Weapons.LoadFromXMLConfig(XMLConfig,Path+'Weapons/');
  FCurrentFoeName:=XMLConfig.GetValue(Path+'CurrentFoeName/Value','');

  NewCount:=XMLConfig.GetValue(Path+'Aktionen/Count',0);
  for i:=0 to NewCount-1 do begin
    NewAktion:=TCombatAktion.Create;
    NewAktion.LoadFromXMLConfig(XMLConfig,Path+'Aktionen/Item'+IntToStr(i)+'/');
    AddAktion(NewAktion);
  end;
  
  FModified:=false;
end;

procedure TCombatant.SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string
  );
var
  i: Integer;
begin
  DebugLn('TCombatant.SaveToXMLConfig ',Name);
  XMLConfig.SetDeleteValue(Path+'Name/Value',Name,'');
  Armor.SaveToXMLConfig(XMLConfig,Path+'Armor/');
  Hits.SaveToXMLConfig(XMLConfig,Path+'Hits/');
  XMLConfig.SetDeleteValue(Path+'Size/Value',Size,0);
  DebugLn('TCombatant.SaveToXMLConfig Size=',dbgs(Size));
  Stats.SaveToXMLConfig(XMLConfig,Path+'Stats/');
  Penalties.SaveToXMLConfig(XMLConfig,Path+'Penalties/');
  Weapons.SaveToXMLConfig(XMLConfig,Path+'Weapons/');
  XMLConfig.SetDeleteValue(Path+'CurrentFoeName/Value',CurrentFoeName,'');

  XMLConfig.SetDeleteValue(Path+'Aktionen/Count',AktionenCount,0);
  for i:=0 to AktionenCount-1 do
    Aktionen[i].SaveToXMLConfig(XMLConfig,Path+'Aktionen/Item'+IntToStr(i)+'/');

  FModified:=false;
end;

function TCombatant.LoadFromFile(const Filename: string): TModalResult;
var
  XMLConfig: TXMLConfig;
begin
  Result:=mrCancel;
  Clear;
  if not FileExists(Filename) then exit;
  XMLConfig:=TXMLConfig.Create(Filename);
  try
    LoadFromXMLConfig(XMLConfig,'Combatant/');
    Result:=mrOk;
  finally
    XMLConfig.Free;
  end;
end;

function TCombatant.SaveToFile(const Filename: string): TModalResult;
var
  XMLConfig: TXMLConfig;
begin
  if FileExists(Filename) and (not Modified) then exit(mrOk);
  Result:=mrCancel;
  XMLConfig:=TXMLConfig.CreateClean(Filename);
  try
    SaveToXMLConfig(XMLConfig,'Combatant/');
    Result:=mrOk;
  finally
    XMLConfig.Free;
  end;
end;

function TCombatant.EndAP: integer;
var
  i: Integer;
begin
  Result:=0;
  for i:=0 to AktionenCount-1 do inc(Result,Aktionen[i].Duration);
end;

{ TCombatHits }

procedure TCombatHits.SetMaxHits(const AValue: integer);
begin
  if FMaxHits=AValue then exit;
  FMaxHits:=AValue;
  FModified:=true;
end;

procedure TCombatHits.SetModified(const AValue: boolean);
begin
  if FModified=AValue then exit;
  FModified:=AValue;
end;

procedure TCombatHits.Clear;
begin
  FHealApPerHit:=0;
  FMaxHits:=0;
  FModified:=false;
end;

procedure TCombatHits.Assign(Source: TPersistent);
var
  Src: TCombatHits;
begin
  if Source is TCombatHits then begin
    Src:=TCombatHits(Source);
    FMaxHits:=Src.MaxHits;
    FHealApPerHit:=Src.HealApPerHit;
  end else
    inherited Assign(Source);
end;

procedure TCombatHits.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  FHealApPerHit:=XMLConfig.GetValue(Path+'HealApPerHit/Value',0);
  FMaxHits:=XMLConfig.GetValue(Path+'MaxHits/Value',0);
  FModified:=false;
end;

procedure TCombatHits.SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string
  );
begin
  XMLConfig.SetDeleteValue(Path+'HealApPerHit/Value',FHealApPerHit,0);
  XMLConfig.SetDeleteValue(Path+'MaxHits/Value',FMaxHits,0);
  FModified:=true;
end;

procedure TCombatHits.SetHealApPerHit(const AValue: integer);
begin
  if FHealApPerHit=AValue then exit;
  FHealApPerHit:=AValue;
  FModified:=true;
end;

{ TBattleArmor }

procedure TBattleArmor.SetArmorDB(const AValue: integer);
begin
  if FArmorDB=AValue then exit;
  FArmorDB:=AValue;
  FModified:=true;
end;

function TBattleArmor.GetModified: boolean;
begin
  Result:=FModified or ArmorParts.Modified;
end;

procedure TBattleArmor.SetElementalDB(const AValue: integer);
begin
  if FElementalDB=AValue then exit;
  FElementalDB:=AValue;
  FModified:=true;
end;

procedure TBattleArmor.SetModified(const AValue: boolean);
begin
  FModified:=AValue;
  ArmorParts.Modified:=AValue;
end;

procedure TBattleArmor.SetQuicknessDB(const AValue: integer);
begin
  if FQuicknessDB=AValue then exit;
  FQuicknessDB:=AValue;
  FModified:=true;
end;

procedure TBattleArmor.SetShieldDB(const AValue: integer);
begin
  if FShieldDB=AValue then exit;
  FShieldDB:=AValue;
  FModified:=true;
end;

constructor TBattleArmor.Create;
begin
  FArmorParts:=TArmorParts.Create;
end;

destructor TBattleArmor.Destroy;
begin
  FArmorParts.Free;
  inherited Destroy;
end;

procedure TBattleArmor.Clear;
begin
  FArmorDB:=0;
  FArmorParts.Clear;
  FElementalDB:=0;
  FQuicknessDB:=0;
  FShieldDB:=0;
end;

procedure TBattleArmor.Assign(Source: TPersistent);
var
  Src: TBattleArmor;
begin
  if Source is TBattleArmor then begin
    Src:=TBattleArmor(Source);
    FArmorDB:=Src.ArmorDB;
    FArmorParts.Assign(Src.ArmorParts);
    FElementalDB:=Src.ElementalDB;
    FQuicknessDB:=Src.QuicknessDB;
    FShieldDB:=Src.ShieldDB;
  end else
    inherited Assign(Source);
end;

procedure TBattleArmor.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  ArmorDB:=XMLConfig.GetValue(Path+'ArmorDB/Value',0);
  ArmorParts.LoadFromXMLConfig(XMLConfig,Path+'ArmorParts/');
  ElementalDB:=XMLConfig.GetValue(Path+'ElementalDB/Value',0);
  QuicknessDB:=XMLConfig.GetValue(Path+'QuicknessDB/Value',0);
  ShieldDB:=XMLConfig.GetValue(Path+'ShieldDB/Value',0);
  FModified:=false;
end;

procedure TBattleArmor.SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string
  );
begin
  XMLConfig.SetDeleteValue(Path+'ArmorDB/Value',ArmorDB,0);
  ArmorParts.SaveToXMLConfig(XMLConfig,Path+'ArmorParts/');
  XMLConfig.SetDeleteValue(Path+'ElementalDB/Value',ElementalDB,0);
  XMLConfig.SetDeleteValue(Path+'QuicknessDB/Value',QuicknessDB,0);
  XMLConfig.SetDeleteValue(Path+'ShieldDB/Value',ShieldDB,0);
  FModified:=false;
end;

{ TArmorParts }

function TArmorParts.GetItems(Index: integer): TArmorPart;
begin
  Result:=TArmorPart(FItems[Index]);
end;

function TArmorParts.GetModified: boolean;
var
  i: Integer;
begin
  if FModified then exit(true);
  for i:=0 to Count-1 do
    if Items[i].Modified then exit(true);
  Result:=false;
end;

function TArmorParts.GetCount: integer;
begin
  Result:=FItems.Count;
end;

procedure TArmorParts.SetItems(Index: integer; const AValue: TArmorPart);
begin
  if TArmorPart(FItems[Index])=AValue then exit;
  FItems[Index]:=AValue;
  FModified:=true;
end;

procedure TArmorParts.SetModified(const AValue: boolean);
var
  i: Integer;
begin
  FModified:=AValue;
  if not AValue then
    for i:=0 to Count-1 do
      Items[i].Modified:=AValue;
end;

constructor TArmorParts.Create;
begin
  FItems:=TFPList.Create;
  AddDefaultParts;
end;

destructor TArmorParts.Destroy;
begin
  Clear;
  FItems.Free;
  inherited Destroy;
end;

procedure TArmorParts.Clear;
var
  i: Integer;
begin
  for i:=0 to FItems.Count-1 do
    TObject(FItems[i]).Free;
  FItems.Clear;
  FModified:=false;
end;

procedure TArmorParts.Assign(Source: TPersistent);
var
  Src: TArmorParts;
  i: Integer;
begin
  if Source is TArmorParts then begin
    Src:=TArmorParts(Source);
    while Src.Count>Count do Add(TArmorPart.Create);
    while Src.Count<Count do Delete(Count-1);
    for i:=0 to Src.Count-1 do
      Items[i].Assign(Src[i]);
  end else
    inherited Assign(Source);
end;

function TArmorParts.Add(ArmorPart: TArmorPart): integer;
begin
  Result:=FItems.Add(ArmorPart);
  FModified:=true;
end;

procedure TArmorParts.Delete(Index: integer);
begin
  Items[Index].Free;
  FItems.Delete(Index);
  FModified:=true;
end;

procedure TArmorParts.AddDefaultParts;

  procedure AddPart(const s: string);
  begin
    Add(TArmorPart.Create(s,0,0,0,0,0,0));
  end;

begin
  AddPart('Schaedel');
  AddPart('Auge');
  AddPart('Gesicht');
  AddPart('Hals');
  AddPart('Schulter');
  AddPart('Oberarm');
  AddPart('Ellbogen');
  AddPart('Unterarm');
  AddPart('Hand');
  AddPart('Fluegel');
  AddPart('Brustkorb');
  AddPart('Unterleib');
  AddPart('Huefte');
  AddPart('Genitalien');
  AddPart('Schwanz');
  AddPart('Oberschenkel');
  AddPart('Knie');
  AddPart('Wade');
  AddPart('Fuss');
  FModified:=false;
end;

procedure TArmorParts.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  NewCount: LongInt;
  i: Integer;
  NewArmorPart: TArmorPart;
begin
  Clear;
  NewCount:=XMLConfig.GetValue(Path+'Count',0);
  for i:=0 to NewCount-1 do begin
    NewArmorPart:=TArmorPart.Create;
    NewArmorPart.LoadFromXMLConfig(XMLConfig,Path+'Item'+IntToStr(i)+'/');
    Add(NewArmorPart);
  end;
  FModified:=false;
end;

procedure TArmorParts.SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string
  );
var
  i: Integer;
begin
  XMLConfig.SetDeleteValue(Path+'Count',Count,0);
  for i:=0 to Count-1 do begin
    Items[i].SaveToXMLConfig(XMLConfig,Path+'Item'+IntToStr(i)+'/');
  end;
  FModified:=false;
end;

{ TArmorPart }

procedure TArmorPart.SetPlace(const AValue: string);
begin
  if FPlace=AValue then exit;
  FPlace:=AValue;
  FModified:=true;
end;

function TArmorPart.GetWucht(Aspekt: TWuchtAspekt): integer;
begin
  Result:=FWucht[Aspekt];
end;

procedure TArmorPart.SetModified(const AValue: boolean);
begin
  if FModified=AValue then exit;
  FModified:=AValue;
end;

procedure TArmorPart.SetWucht(Aspekt: TWuchtAspekt; const AValue: integer);
begin
  if FWucht[Aspekt]=AValue then exit;
  FWucht[Aspekt]:=AValue;
  FModified:=true;
end;

constructor TArmorPart.Create;
begin

end;

constructor TArmorPart.Create(const APlace: string;
  AStumpf, AScharf, ASpitz, AFeuer, ADruck, ABlitz: integer);
begin
  FPlace:=APlace;
  FWucht[waStumpf]:=AStumpf;
  FWucht[waScharf]:=AScharf;
  FWucht[waSpitz]:=ASpitz;
  FWucht[waFeuer]:=AFeuer;
  FWucht[waDruck]:=ADruck;
  FWucht[waBlitz]:=ABlitz;
end;

procedure TArmorPart.Assign(Source: TPersistent);
var
  a: TWuchtAspekt;
  Src: TArmorPart;
begin
  if Source is TArmorPart then begin
    Src:=TArmorPart(Source);
    FPlace:=Src.Place;
    for a:=Low(TWuchtAspekt) to High(TWuchtAspekt) do
      FWucht[a]:=Src.Wucht[a];
  end else
  inherited Assign(Source);
end;

procedure TArmorPart.LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string
  );
var
  w: TWuchtAspekt;
begin
  Place:=XMLConfig.GetValue(Path+'Place/Value','');
  for w:=Low(TWuchtAspekt) to High(TWuchtAspekt) do begin
    Wucht[w]:=XMLConfig.GetValue(Path+'Wucht/'+WuchtNames[w],0);
  end;
end;

procedure TArmorPart.SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string
  );
var
  w: TWuchtAspekt;
begin
  XMLConfig.SetDeleteValue(Path+'Place/Value',Place,'');
  for w:=Low(TWuchtAspekt) to High(TWuchtAspekt) do begin
    XMLConfig.SetDeleteValue(Path+'Wucht/'+WuchtNames[w],Wucht[w],0);
  end;
end;

{ TCombatPenalties }

function TCombatPenalties.GetItems(Index: integer): TCombatPenalty;
begin
  Result:=TCombatPenalty(FItems[Index]);
end;

function TCombatPenalties.GetModified: boolean;
var
  i: Integer;
begin
  if FModified then exit(true);
  for i:=0 to Count-1 do if Items[i].Modified then exit(true);
  Result:=false;
end;

function TCombatPenalties.GetCount: integer;
begin
  Result:=FItems.Count;
end;

procedure TCombatPenalties.SetItems(Index: integer;
  const AValue: TCombatPenalty);
begin
  if TCombatPenalty(FItems[Index])=AValue then exit;
  FItems[Index]:=AValue;
  FModified:=true;
end;

procedure TCombatPenalties.SetModified(const AValue: boolean);
var
  i: Integer;
begin
  FModified:=AValue;
  if not AValue then
    for i:=0 to Count-1 do Items[i].Modified:=AValue;
end;

constructor TCombatPenalties.Create;
begin
  FItems:=TFPList.Create;
end;

destructor TCombatPenalties.Destroy;
begin
  Clear;
  FItems.Free;
  inherited Destroy;
end;

procedure TCombatPenalties.Clear;
var
  i: Integer;
begin
  for i:=0 to FItems.Count-1 do
    TObject(FItems[i]).Free;
  FItems.Clear;
  FModified:=false;
end;

procedure TCombatPenalties.Assign(Source: TPersistent);
var
  Src: TCombatPenalties;
  i: Integer;
begin
  if Source is TCombatPenalties then begin
    Src:=TCombatPenalties(Source);
    while Src.Count>Count do Add(TCombatPenalty.Create);
    while Src.Count<Count do Delete(Count-1);
    for i:=0 to Src.Count-1 do
      Items[i].Assign(Src[i]);
  end else
    inherited Assign(Source);
end;

function TCombatPenalties.Add(Penalty: TCombatPenalty): integer;
begin
  Result:=FItems.Add(Penalty);
  FModified:=true;
end;

procedure TCombatPenalties.Delete(Index: integer);
begin
  Items[Index].Free;
  FItems.Delete(Index);
  FModified:=true;
end;

procedure TCombatPenalties.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  NewCount: LongInt;
  i: Integer;
  NewPenalty: TCombatPenalty;
begin
  Clear;
  NewCount:=XMLConfig.GetValue(Path+'Count',0);
  for i:=0 to NewCount-1 do begin
    NewPenalty:=TCombatPenalty.Create;
    NewPenalty.LoadFromXMLConfig(XMLConfig,Path+'Item'+IntToStr(i)+'/');
    Add(NewPenalty);
  end;
  FModified:=false;
end;

procedure TCombatPenalties.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  i: Integer;
begin
  XMLConfig.SetDeleteValue(Path+'Count',Count,0);
  for i:=0 to Count-1 do begin
    Items[i].SaveToXMLConfig(XMLConfig,Path+'Item'+IntToStr(i)+'/');
  end;
  FModified:=false;
end;

function TCombatPenalties.AsString: string;
var
  i: Integer;
begin
  Result:='';
  for i:=0 to Count-1 do begin
    if Result<>'' then Result:=Result+',';
    Result:=Result+Items[i].Text;
  end;
end;

{ TCombatPenalty }

procedure TCombatPenalty.SetModified(const AValue: boolean);
begin
  if FModified=AValue then exit;
  FModified:=AValue;
end;

procedure TCombatPenalty.SetDuration(const AValue: integer);
begin
  if FDuration=AValue then exit;
  FDuration:=AValue;
  FModified:=true;
end;

procedure TCombatPenalty.SetText(const AValue: string);
begin
  if FText=AValue then exit;
  FText:=AValue;
  FModified:=true;
end;

procedure TCombatPenalty.SetStartTime(const AValue: integer);
begin
  if FStartTime=AValue then exit;
  FStartTime:=AValue;
  FModified:=true;
end;

procedure TCombatPenalty.Assign(Source: TPersistent);
var
  Src: TCombatPenalty;
begin
  if Source is TCombatPenalty then begin
    Src:=TCombatPenalty(Source);
    FDuration:=Src.Duration;
    FText:=Src.Text;
    FStartTime:=Src.StartTime;
  end else
    inherited Assign(Source);
end;

procedure TCombatPenalty.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  i: Integer;
begin
  FDuration:=XMLConfig.GetValue(Path+'Duration/Value',0);
  FText:=XMLConfig.GetValue(Path+'Text/Value','');
  for i:=1 to length(FText) do begin
    case FText[i] of
    ',': FText[i]:=';';
    #0..#31: FText[i]:=' ';
    end;
  end;
  FStartTime:=XMLConfig.GetValue(Path+'StartTime/Value',0);
  FModified:=false;
end;

procedure TCombatPenalty.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  XMLConfig.SetDeleteValue(Path+'Duration/Value',FDuration,0);
  XMLConfig.SetDeleteValue(Path+'Text/Value',FText,'');
  XMLConfig.SetDeleteValue(Path+'StartTime/Value',FStartTime,0);
  FModified:=false;
end;

{ TBattleWeapons }

function TBattleWeapons.GetItems(Index: integer): TCombatWeapon;
begin
  Result:=TCombatWeapon(FItems[Index]);
end;

function TBattleWeapons.GetModified: boolean;
var
  i: Integer;
begin
  if FModified then exit(true);
  for i:=0 to Count-1 do if Items[i].Modified then exit(true);
  Result:=false;
end;

function TBattleWeapons.GetCount: integer;
begin
  Result:=FItems.Count;
end;

procedure TBattleWeapons.SetItems(Index: integer; const AValue: TCombatWeapon);
begin
  if TCombatWeapon(FItems[Index])=AValue then exit;
  FItems[Index]:=AValue;
  FModified:=true;
end;

procedure TBattleWeapons.SetModified(const AValue: boolean);
var
  i: Integer;
begin
  FModified:=AValue;
  if not AValue then
    for i:=0 to Count-1 do Items[i].Modified:=AValue;
end;

constructor TBattleWeapons.Create;
begin
  FItems:=TFPList.Create;
end;

destructor TBattleWeapons.Destroy;
begin
  Clear;
  FItems.Free;
  inherited Destroy;
end;

procedure TBattleWeapons.Clear;
var
  i: Integer;
begin
  for i:=0 to FItems.Count-1 do
    TObject(FItems[i]).Free;
  FItems.Clear;
  FModified:=false;
end;

procedure TBattleWeapons.Assign(Source: TPersistent);
var
  Src: TBattleWeapons;
  i: Integer;
begin
  if Source is TBattleWeapons then begin
    Src:=TBattleWeapons(Source);
    while Src.Count>Count do Add(TCombatWeapon.Create);
    while Src.Count<Count do Delete(Count-1);
    for i:=0 to Src.Count-1 do
      Items[i].Assign(Src[i]);
  end else
    inherited Assign(Source);
end;

function TBattleWeapons.Add(Weapon: TCombatWeapon): integer;
begin
  Result:=FItems.Add(Weapon);
  FModified:=true;
end;

procedure TBattleWeapons.Delete(Index: integer);
begin
  Items[Index].Free;
  FItems.Delete(Index);
  FModified:=true;
end;

procedure TBattleWeapons.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  NewCount: LongInt;
  i: Integer;
  NewWeapon: TCombatWeapon;
begin
  Clear;
  NewCount:=XMLConfig.GetValue(Path+'Count',0);
  for i:=0 to NewCount-1 do begin
    NewWeapon:=TCombatWeapon.Create;
    NewWeapon.LoadFromXMLConfig(XMLConfig,Path+'Item'+IntToStr(i)+'/');
    Add(NewWeapon);
  end;
  FModified:=false;
end;

procedure TBattleWeapons.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  i: Integer;
begin
  XMLConfig.SetDeleteValue(Path+'Count',Count,0);
  for i:=0 to Count-1 do
    Items[i].SaveToXMLConfig(XMLConfig,Path+'Item'+IntToStr(i)+'/');
  FModified:=false;
end;

{ TCombatWeapon }

function TCombatWeapon.GetAttacks(Index: integer): TCombatAttack;
begin
  Result:=TCombatAttack(FAttacks[Index]);
end;

function TCombatWeapon.GetModified: boolean;
var
  i: Integer;
begin
  Result:=FModified;
  if not Modified then begin
    for i:=0 to AttackCount-1 do if Attacks[i].Modified then exit(true);
  end;
end;

function TCombatWeapon.GetAttackCount: integer;
begin
  Result:=FAttacks.Count;
end;

procedure TCombatWeapon.SetAttackOrAim(const AValue: integer);
begin
  if FAttackOrAim=AValue then exit;
  FAttackOrAim:=AValue;
  FModified:=true;
end;

procedure TCombatWeapon.SetAttacks(Index: integer; const AValue: TCombatAttack);
begin
  if TCombatAttack(FAttacks[Index])=AValue then exit;
  FAttacks[Index]:=AValue;
  FModified:=true;
end;

procedure TCombatWeapon.SetDraw(const AValue: integer);
begin
  if FDraw=AValue then exit;
  FDraw:=AValue;
  FModified:=true;
end;

procedure TCombatWeapon.SetFullParry(const AValue: integer);
begin
  if FFullParry=AValue then exit;
  FFullParry:=AValue;
  FModified:=true;
end;

procedure TCombatWeapon.SetFumbleRange(const AValue: integer);
begin
  if FFumbleRange=AValue then exit;
  FFumbleRange:=AValue;
  FModified:=true;
end;

procedure TCombatWeapon.SetModified(const AValue: boolean);
var
  i: Integer;
begin
  FModified:=AValue;
  if not AValue then begin
    for i:=0 to AttackCount-1 do Attacks[i].Modified:=AValue;
  end;
end;

procedure TCombatWeapon.SetOB(const AValue: integer);
begin
  if FOB=AValue then exit;
  FOB:=AValue;
  FModified:=true;
end;

procedure TCombatWeapon.SetReload(const AValue: integer);
begin
  if FReload=AValue then exit;
  FReload:=AValue;
  FModified:=true;
end;

procedure TCombatWeapon.SetWeaponName(const AValue: string);
begin
  if FWeaponName=AValue then exit;
  FWeaponName:=AValue;
  FModified:=true;
end;

constructor TCombatWeapon.Create;
begin
  FAttacks:=TFPList.Create;
  SetAttackOrAim(0);
  SetDraw(0);
  SetFullParry(0);
  SetFumbleRange(0);
  SetOB(0);
  SetReload(0);
  SetWeaponName('noname');
end;

destructor TCombatWeapon.Destroy;
begin
  Clear;
  FAttacks.Free;
  inherited Destroy;
end;

procedure TCombatWeapon.Clear;
var
  i: Integer;
begin
  for i:=0 to FAttacks.Count-1 do
    TObject(FAttacks[i]).Free;
  FAttacks.Clear;
  FModified:=false;
end;

procedure TCombatWeapon.Assign(Source: TPersistent);
var
  Src: TCombatWeapon;
  i: Integer;
begin
  if Source is TCombatWeapon then begin
    Src:=TCombatWeapon(Source);
    FWeaponName:=Src.FWeaponName;
    FAttackOrAim:=Src.AttackOrAim;
    FDraw:=Src.Draw;
    FFullParry:=Src.FullParry;
    FFumbleRange:=Src.FumbleRange;
    FOB:=Src.OB;
    FReload:=Src.Reload;
    while Src.AttackCount>AttackCount do AddAttack(TCombatAttack.Create);
    while Src.AttackCount<AttackCount do DeleteAttack(AttackCount-1);
    for i:=0 to Src.AttackCount-1 do
      Attacks[i].Assign(Src[i]);
  end else
    inherited Assign(Source);
end;

function TCombatWeapon.AddAttack(Attack: TCombatAttack): integer;
begin
  Result:=FAttacks.Add(Attack);
  FModified:=true;
end;

procedure TCombatWeapon.DeleteAttack(Index: integer);
begin
  Attacks[Index].Free;
  FAttacks.Delete(Index);
  FModified:=true;
end;

procedure TCombatWeapon.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  NewCount: LongInt;
  i: Integer;
  NewAttack: TCombatAttack;
begin
  Clear;
  NewCount:=XMLConfig.GetValue(Path+'Attacks/Count',0);
  for i:=0 to NewCount-1 do begin
    NewAttack:=TCombatAttack.Create;
    NewAttack.LoadFromXMLConfig(XMLConfig,Path+'Attacks/Item'+IntToStr(i)+'/');
    AddAttack(NewAttack);
  end;

  FAttackOrAim:=XMLConfig.GetValue(Path+'AttackOrAim/Value',0);
  FDraw:=XMLConfig.GetValue(Path+'Draw/Value',0);
  FFullParry:=XMLConfig.GetValue(Path+'FullParry/Value',0);
  FFumbleRange:=XMLConfig.GetValue(Path+'FumbleRange/Value',0);
  FOB:=XMLConfig.GetValue(Path+'OB/Value',0);
  FReload:=XMLConfig.GetValue(Path+'Reload/Value',0);
  FWeaponName:=XMLConfig.GetValue(Path+'WeaponName/Value','');
  FModified:=false;
end;

procedure TCombatWeapon.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  i: Integer;
begin
  XMLConfig.SetDeleteValue(Path+'Attacks/Count',AttackCount,0);
  for i:=0 to AttackCount-1 do
    Attacks[i].SaveToXMLConfig(XMLConfig,Path+'Attacks/Item'+IntToStr(i)+'/');

  XMLConfig.SetDeleteValue(Path+'AttackOrAim/Value',FAttackOrAim,0);
  XMLConfig.SetDeleteValue(Path+'Draw/Value',FDraw,0);
  XMLConfig.SetDeleteValue(Path+'FullParry/Value',FFullParry,0);
  XMLConfig.SetDeleteValue(Path+'FumbleRange/Value',FFumbleRange,0);
  XMLConfig.SetDeleteValue(Path+'OB/Value',FOB,0);
  XMLConfig.SetDeleteValue(Path+'Reload/Value',FReload,0);
  XMLConfig.SetDeleteValue(Path+'WeaponName/Value',FWeaponName,'');
  FModified:=false;
end;

{ TCombatAttack }

procedure TCombatAttack.SetAttack(const AValue: TWuchtAspekt);
begin
  if FAttack=AValue then exit;
  FAttack:=AValue;
  FModified:=true;
end;

function TCombatAttack.GetModified: boolean;
begin
  Result:=FModified;
end;

procedure TCombatAttack.SetCritical(const AValue: TWuchtAspekt);
begin
  if FCritical=AValue then exit;
  FCritical:=AValue;
  FModified:=true;
end;

procedure TCombatAttack.SetModified(const AValue: boolean);
begin
  FModified:=AValue;
end;

procedure TCombatAttack.SetTheType(const AValue: TCombatAttackType);
begin
  if FTheType=AValue then exit;
  FTheType:=AValue;
  FModified:=true;
end;

procedure TCombatAttack.SetWucht(const AValue: integer);
begin
  if FWucht=AValue then exit;
  FWucht:=AValue;
  FModified:=true;
end;

procedure TCombatAttack.Assign(Source: TPersistent);
var
  Src: TCombatAttack;
begin
  if Source is TCombatAttack then begin
    Src:=TCombatAttack(Source);
    FAttack:=Src.Attack;
    FCritical:=Src.Critical;
    FWucht:=Src.Wucht;
    FTheType:=Src.TheType;
  end else
    inherited Assign(Source);
end;

procedure TCombatAttack.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  FAttack:=WuchtNameToWuchtAspekt(
                    XMLConfig.GetValue(Path+'Attack/Value',WuchtNames[Attack]));
  FCritical:=WuchtNameToWuchtAspekt(
                  XMLConfig.GetValue(Path+'Critical/Value',WuchtNames[Attack]));
  FTheType:=AttackTypeNameToAttackType(
          XMLConfig.GetValue(Path+'Type/Value',CombatAttackTypeNames[TheType]));
  FWucht:=XMLConfig.GetValue(Path+'Wucht/Value',0);
  FModified:=false;
end;

procedure TCombatAttack.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  XMLConfig.SetDeleteValue(Path+'Attack/Value',WuchtNames[Attack],'');
  XMLConfig.SetDeleteValue(Path+'Critical/Value',WuchtNames[Critical],'');
  XMLConfig.SetDeleteValue(Path+'Type/Value',CombatAttackTypeNames[TheType],'');
  XMLConfig.SetDeleteValue(Path+'Wucht/Value',FWucht,0);
  FModified:=false;
end;

{ TCombatAktionAttack }

procedure TCombatAktionAttack.SetWeapon(const AValue: TCombatWeapon);
begin
  if FWeapon=AValue then exit;
  FWeapon:=AValue;
end;

{ TCombatAktion }

procedure TCombatAktion.SetComment(const AValue: string);
begin
  if FComment=AValue then exit;
  FComment:=AValue;
  FModified:=true;
end;

procedure TCombatAktion.SetCancelled(const AValue: boolean);
begin
  if FCancelled=AValue then exit;
  FCancelled:=AValue;
  FModified:=true;
end;

function TCombatAktion.GetModified: boolean;
begin
  Result:=FModified;
end;

procedure TCombatAktion.SetCancelTime(const AValue: integer);
begin
  if FCancelTime=AValue then exit;
  FCancelTime:=AValue;
  FModified:=true;
end;

procedure TCombatAktion.SetDuration(const AValue: integer);
begin
  if FDuration=AValue then exit;
  FDuration:=AValue;
  FModified:=true;
end;

procedure TCombatAktion.SetModified(const AValue: boolean);
begin
  FModified:=AValue;
end;

procedure TCombatAktion.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  FCancelled:=XMLConfig.GetValue(Path+'Cancelled/Value',false);
  FCancelTime:=XMLConfig.GetValue(Path+'CancelTime/Value',0);
  FComment:=XMLConfig.GetValue(Path+'Comment/Value','');
  FDuration:=XMLConfig.GetValue(Path+'Duration/Value',0);
  FModified:=false;
end;

procedure TCombatAktion.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  XMLConfig.SetDeleteValue(Path+'Cancelled/Value',FCancelled,false);
  XMLConfig.SetDeleteValue(Path+'CancelTime/Value',FCancelTime,0);
  XMLConfig.SetDeleteValue(Path+'Comment/Value',FComment,'');
  XMLConfig.SetDeleteValue(Path+'Duration/Value',FDuration,0);
  FModified:=false;
end;

{ TCombatAktionSpell }

procedure TCombatAktionSpell.SetCastingTime(const AValue: integer);
begin
  if FCastingTime=AValue then exit;
  FCastingTime:=AValue;
end;

procedure TCombatAktionSpell.SetRecovery(const AValue: integer);
begin
  if FRecovery=AValue then exit;
  FRecovery:=AValue;
end;

{ TCombatAktionPerception }

procedure TCombatAktionPerception.SetPerceptionTime(const AValue: integer);
begin
  if FPerceptionTime=AValue then exit;
  FPerceptionTime:=AValue;
end;

{ TCombatAktionFullParry }

procedure TCombatAktionFullParry.SetWeapon(const AValue: TCombatWeapon);
begin
  if FWeapon=AValue then exit;
  FWeapon:=AValue;
end;

{ TCombatAktionDraw }

procedure TCombatAktionDraw.SetWeapon(const AValue: TCombatWeapon);
begin
  if FWeapon=AValue then exit;
  FWeapon:=AValue;
end;

{ TCombatAktionReload }

procedure TCombatAktionReload.SetWeapon(const AValue: TCombatWeapon);
begin
  if FWeapon=AValue then exit;
  FWeapon:=AValue;
end;

{ TCombatAktionMovement }

procedure TCombatAktionMovement.SetDistanceInM(const AValue: double);
begin
  if FDistanceInM=AValue then exit;
  FDistanceInM:=AValue;
end;

{ TCombatantGroup }

function TCombatantGroup.GetCombatants(Index: integer): TCombatant;
begin
  Result:=TCombatant(FCombatants[Index]);
end;

function TCombatantGroup.GetCount: integer;
begin
  Result:=FCombatants.Count;
end;

function TCombatantGroup.GetModified: boolean;
begin
  Result:=FModified;
end;

procedure TCombatantGroup.SetCombatants(Index: integer;
  const AValue: TCombatant);
begin
  FCombatants[Index]:=AValue;
end;

procedure TCombatantGroup.SetFilename(const AValue: string);
begin
  if FFilename=AValue then exit;
  FFilename:=AValue;
end;

procedure TCombatantGroup.SetModified(const AValue: boolean);
begin
  FModified:=AValue;
end;

procedure TCombatantGroup.SetName(const AValue: string);
begin
  if FName=AValue then exit;
  FName:=AValue;
end;

constructor TCombatantGroup.Create;
begin
  FCombatants:=TFPList.Create;
  FLoadedCombatantNames:=TStringList.Create;
end;

destructor TCombatantGroup.Destroy;
begin
  Clear;
  FLoadedCombatantNames.Free;
  FCombatants.Free;
  inherited Destroy;
end;

procedure TCombatantGroup.Clear;
begin
  FLoadedCombatantNames.Clear;
  ClearCombatants;
end;

procedure TCombatantGroup.ClearCombatants;
begin
  FCombatants.Clear;
end;

procedure TCombatantGroup.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  NewCount: LongInt;
  i: Integer;
  NewCombatantName: String;
begin
  Clear;
  NewCount:=XMLConfig.GetValue(Path+'Count',0);
  Name:=XMLConfig.GetValue(Path+'Name','');
  for i:=0 to NewCount-1 do begin
    NewCombatantName:=XMLConfig.GetValue(
                                   Path+'Item'+IntToStr(i)+'/CombatantName','');
    FLoadedCombatantNames.Add(NewCombatantName);
  end;
  writeln('LoadFromXMLConfig Names :',FLoadedCombatantNames.Text);
end;

procedure TCombatantGroup.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
var
  i: Integer;
begin
  XMLConfig.SetDeleteValue(Path+'Count',Count,0);
  XMLConfig.SetDeleteValue(Path+'Name',Name,'');
  for i:=0 to Count-1 do begin
    
    XMLConfig.SetDeleteValue(Path+'Item'+IntToStr(i)+'/CombatantName',
                             Items[i].Name,'');
  end;
end;

function TCombatantGroup.LoadFromFile(const Filename: string): TModalResult;
var
  XMLConfig: TXMLConfig;
begin
  Result:=mrCancel;
  Clear;
  if not FileExists(Filename) then exit;
  XMLConfig:=TXMLConfig.Create(Filename);
  try
    LoadFromXMLConfig(XMLConfig,'CombatantGroup/');
    Result:=mrOk;
  finally
    XMLConfig.Free;
  end;
end;

function TCombatantGroup.FindCombatantByName(const AName: string): TCombatant;
var
  i: Integer;
begin
  i:=FCombatants.Count-1;
  while (i>=0) do begin
    if (CompareText(AName,Items[i].Name)=0) then begin
      Result:=Items[i];
      exit;
    end;
    dec(i);
  end;
  Result:=nil;
end;

function TCombatantGroup.SaveToFile(const Filename: string): TModalResult;
var
  XMLConfig: TXMLConfig;
begin
  if FileExists(Filename) and (not Modified) then exit(mrOk);
  Result:=mrCancel;
  XMLConfig:=TXMLConfig.CreateClean(Filename);
  try
    SaveToXMLConfig(XMLConfig,'CombatantGroup/');
    XMLConfig.Flush;
    Result:=mrOk;
  finally
    XMLConfig.Free;
  end;
end;

procedure TCombatantGroup.Assign(Source: TPersistent);
var
  Src: TCombatantGroup;
begin
  if Source is TCombatantGroup then begin
    Src:=TCombatantGroup(Source);
    Name:=Src.Name;
    LoadedCombatantNames.Assign(Src.LoadedCombatantNames);
    FCombatants.Assign(Src.FCombatants);
  end else begin
    inherited Assign(Source);
  end;
end;

procedure TCombatantGroup.Add(Combatant: TCombatant);
begin
  FCombatants.Add(Combatant);
  if (Combatant.CurrentFoeName<>'') then
    Combatant.CurrentFoe:=FindCombatantByName(Combatant.CurrentFoeName);
  FModified:=true;
end;

procedure TCombatantGroup.Remove(Index: integer);
begin
  Items[Index].Free;
  FCombatants.Delete(Index);
  Modified:=true;
end;

function TCombatantGroup.NewName(const StartName: string): string;
begin
  Result:=StartName;
  while FindCombatantByName(Result)<>nil do
    Result:=CreateNextIdentifier(Result);
end;

procedure TCombatantGroup.Move(FromIndex, ToIndex: integer);
begin
  FCombatants.Move(FromIndex,ToIndex);
end;

{ TCombatantStats }

function TCombatantStats.GetPot(Index: TStatType): integer;
begin
  Result:=FPot[Index];
end;

function TCombatantStats.GetMisc(Index: TStatType): integer;
begin
  Result:=FMisc[Index];
end;

function TCombatantStats.GetBonus(Index: TStatType): integer;
begin
  Result:=FBonus[Index];
end;

function TCombatantStats.GetRace(Index: TStatType): integer;
begin
  Result:=FRace[Index];
end;

function TCombatantStats.GetTemp(Index: TStatType): integer;
begin
  Result:=FTemp[Index];
end;

function TCombatantStats.GetTotal(Index: TStatType): integer;
begin
  Result:=FTotal[Index];
end;

procedure TCombatantStats.SetMisc(Index: TStatType; const AValue: integer);
begin
  if FMisc[Index]=AValue then exit;
  FMisc[Index]:=AValue;
  FModified:=true;
end;

procedure TCombatantStats.SetModified(const AValue: boolean);
begin
  if FModified=AValue then exit;
  FModified:=AValue;
end;

procedure TCombatantStats.SetPot(Index: TStatType; const AValue: integer);
begin
  if FPot[Index]=AValue then exit;
  FPot[Index]:=AValue;
  FModified:=true;
end;

procedure TCombatantStats.SetRace(Index: TStatType; const AValue: integer);
begin
  if FRace[Index]=AValue then exit;
  FRace[Index]:=AValue;
  FModified:=true;
end;

procedure TCombatantStats.SetTemp(Index: TStatType; const AValue: integer);
begin
  if FTemp[Index]=AValue then exit;
  FTemp[Index]:=AValue;
  FModified:=true;
end;

procedure TCombatantStats.Clear;

  procedure ClearArrayOfStatType(var Stats: TArrayOfStatType);
  var
    s: TStatType;
  begin
    for s:=Low(TStatType) to High(TStatType) do
      Stats[s]:=0;
  end;

begin
  ClearArrayOfStatType(FTemp);
  ClearArrayOfStatType(FPot);
  ClearArrayOfStatType(FRace);
  ClearArrayOfStatType(FMisc);
  CalcTotalBoni;
  FModified:=false;
end;

{ Liefert zum temporary Bonus einen Stat }
procedure TCombatantStats.CalcBonus(Index: TStatType);
begin
  FBonus[Index]:=Stat2Bonus(temp[Index]);
  FTotal[Index]:=FBonus[Index]+FRace[Index]+FMisc[Index];
end;

procedure TCombatantStats.CalcTotalBoni;
var
  s: TStatType;
begin
  for s:=Low(TStatType) to High(TStatType) do
    CalcBonus(s);
end;

procedure TCombatantStats.Assign(Source: TPersistent);
var
  Src: TCombatantStats;
  s: TStatType;
begin
  if Source is TCombatantStats then begin
    Src:=TCombatantStats(Source);
    for s:=Low(TStatType) to High(TStatType) do begin
      FTemp[s]:=Src.Temp[s];
      FPot[s]:=Src.Pot[s];
      FRace[s]:=Src.Race[s];
      FMisc[s]:=Src.Misc[s];
      CalcBonus(s);
      //writeln('TCombatantStats.Assign=',FTotal[s],';',integer(s));
    end;
  end else
    inherited Assign(Source);
end;

procedure TCombatantStats.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);

  procedure LoadArrayOfStatType(const SubPath: string;
    var Stats: TArrayOfStatType);
  var
    s: TStatType;
  begin
    for s:=Low(TStatType) to High(TStatType) do
      Stats[s]:=XMLConfig.GetValue(Path+SubPath+StatTypeNames[s],0);
  end;

begin
  LoadArrayOfStatType('Temp/',FTemp);
  LoadArrayOfStatType('Pot/',FPot);
  LoadArrayOfStatType('Race/',FRace);
  LoadArrayOfStatType('Misc/',FMisc);
  CalcTotalBoni;
  FModified:=false;
end;

procedure TCombatantStats.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
  
  procedure SaveArrayOfStatType(const SubPath: string;
    const Stats: TArrayOfStatType);
  var
    s: TStatType;
  begin
    for s:=Low(TStatType) to High(TStatType) do
      XMLConfig.SetDeleteValue(Path+SubPath+StatTypeNames[s],Stats[s],0);
  end;
  
begin
  SaveArrayOfStatType('Temp/',FTemp);
  SaveArrayOfStatType('Pot/',FPot);
  SaveArrayOfStatType('Race/',FRace);
  SaveArrayOfStatType('Misc/',FMisc);
  FModified:=false;
end;

constructor TCombatantStats.Create;
var
  s:TStatType;
begin
    for s:=Low(TStatType) to High(TStatType) do begin
      FTemp[s]:=0;
      FPot[s]:=0;
      FRace[s]:=0;
      FMisc[s]:=0;
      CalcBonus(s);
      //writeln('TCombatantStats.Assign=',FTotal[s],';',integer(s));
    end;
end;

destructor TCombatantStats.Destroy;
begin

end;

end.

