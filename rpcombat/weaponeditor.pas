unit WeaponEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  Buttons, Combatant, RTTICtrls, StdCtrls, RTTIGrids;

type

  { TWeaponEditorDlg }

  TWeaponEditorDlg = class(TForm)
    AttackLabel: TLabel;
    AddAttackButton: TButton;
    RemoveAtttackButton: TButton;
    PreButton: TButton;
    NextButton: TButton;
    NameLabel: TLabel;
    OKButton: TButton;
    CancelButton: TButton;
    DrawLabel: TLabel;
    FullParryLabel: TLabel;
    FumbleRangeLabel: TLabel;
    OBLabel: TLabel;
    ReloadLabel: TLabel;
    AttackTIEdit: TTIEdit;
    DrawTIEdit: TTIEdit;
    ReloadTIEdit: TTIEdit;
    FullParryTIEdit: TTIEdit;
    FumbleRangeTIEdit: TTIEdit;
    OBTIEdit: TTIEdit;
    PositionTILabel: TTILabel;
    TIGrid1: TTIGrid;
    WeaponNameTIEdit: TTIEdit;
    procedure AddAttackButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure NextButtonClick(Sender: TObject);
    procedure PreButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure RemoveAtttackButtonClick(Sender: TObject);
    procedure TIGrid1GetObject(Sender: TTICustomGrid; Index: integer;
      var TIObject: TPersistent);
    procedure TIGrid1GetObjectCount(Sender: TTICustomGrid; ListObject: TObject;
      var ObjCount: integer);
    procedure TIGrid1GetObjectName(Sender: TObject; Index: integer;
                                   TIObject: TPersistent; var ObjName: string);
  private
    FWeaponposition: integer;
    FWeapons: TBattleWeapons;
    FTheWeapon: TCombatWeapon;
    procedure FillAttacksStringGrid;
    procedure SetWeaponPosition(const AValue: integer);
    procedure SetWeapons(const AValue: TBattleWeapons);
    procedure SaveChanges;
  public
    property Weapons: TBattleWeapons read FWeapons write SetWeapons;
    property CurWeapon: TCombatWeapon read FTheWeapon;
    property WeaponPosition: integer read FWeaponposition write SetWeaponPosition;
  end; 

var
  WeaponEditorDlg: TWeaponEditorDlg;

function ShowWeaponEditor(Weapons: TBattleWeapons; Index: integer;
                          Add: Boolean): TModalResult;

implementation

function ShowWeaponEditor(Weapons: TBattleWeapons; Index: integer;
  Add: boolean): TModalResult;
var
  WeaponEditorDlg: TWeaponEditorDlg;
  NewWeapon: TCombatWeapon;
begin
  WeaponEditorDlg:=TWeaponEditorDlg.Create(nil);
  WeaponEditorDlg.Weapons:=Weapons;
  if Add then begin
    NewWeapon:=TCombatWeapon.Create;
    NewWeapon.WeaponName:='Test';
    WeaponEditorDlg.Weapons.Add(NewWeapon);
    WeaponEditorDlg.Weaponposition:=WeaponEditorDlg.Weapons.Count-1;
  end else begin
    WeaponEditorDlg.Weaponposition:=Index;
  end;
  //DebugLn('ShowWeaponEditor Pos=',dbgs(WeaponEditorDlg.Weaponposition),' Count=',dbgs(WeaponEditorDlg.Weapons.Count));

  //writeln('WeaponEditorDlg.FTheWeapon.Draw',WeaponEditorDlg.FTheWeapon.Draw);
  Result:=WeaponEditorDlg.ShowModal;
  if Result=mrOk then
    Weapons.Assign(WeaponEditorDlg.Weapons);
  WeaponEditorDlg.Free;
end;


{ TWeaponEditorDlg }

procedure TWeaponEditorDlg.OKButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  SaveChanges;
  //SetWeapons(Weapons);
   //CurArmorPart.Wucht[Aspect]:=StrToIntDef(PartsStringGrid.Cells[ord(Aspect)+1,Zeile],
     //                          CurArmorPart.Wucht[Aspect]);
  ModalResult:=mrOK;
end;

procedure TWeaponEditorDlg.RemoveAtttackButtonClick(Sender: TObject);
begin

end;

procedure TWeaponEditorDlg.TIGrid1GetObject(Sender: TTICustomGrid;
  Index: integer; var TIObject: TPersistent);
begin
  if Sender=nil then ;
  if (Index>=0) and (Index<CurWeapon.AttackCount) then
    TIObject:=CurWeapon.Attacks[Index];
  //DebugLn('TWeaponEditorDlg.TIGrid1GetObject Index=',dbgs(Index),' CurWeapon.AttackCount=',dbgs(CurWeapon.AttackCount),' TIObject=',DbgSName(TIObject));
end;

procedure TWeaponEditorDlg.TIGrid1GetObjectCount(Sender: TTICustomGrid;
  ListObject: TObject; var ObjCount: integer);
begin
  if Sender=nil then ;
  if ListObject=nil then ;
  ObjCount:=CurWeapon.AttackCount;
  //DebugLn('TWeaponEditorDlg.TIGrid1GetObjectCount ',DbgSName(ListObject),' ObjCount=',dbgs(ObjCount));
end;

procedure TWeaponEditorDlg.TIGrid1GetObjectName(Sender: TObject; Index: integer;
  TIObject: TPersistent; var ObjName: string);
begin
  if Sender=nil then ;
  if TIObject=nil then ;
  ObjName:=IntToStr(Index);
end;

procedure TWeaponEditorDlg.CancelButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  ModalResult:=mrCancel;
end;

procedure TWeaponEditorDlg.PreButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  SaveChanges;
  if FWeaponposition>0 then FWeaponposition-=1;
  CurWeapon.Assign(Weapons[FWeaponposition]);
  PositionTILabel.RealSetText(IntToStr(WeaponPosition+1));
  FillAttacksStringGrid;
end;

procedure TWeaponEditorDlg.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;
  FWeapons:=TBattleWeapons.Create;
  FTheWeapon:=TCombatWeapon.Create;
end;

procedure TWeaponEditorDlg.AddAttackButtonClick(Sender: TObject);
var
  NewAttack:TCombatAttack;
begin
  NewAttack:=TCombatAttack.Create;
  CurWeapon.AddAttack(NewAttack);
  TIGrid1.ReloadTIList;
  FillAttacksStringGrid;
end;

procedure TWeaponEditorDlg.FormDestroy(Sender: TObject);
begin
  if Sender=nil then ;
  FTheWeapon.Free;
  FWeapons.Free;
end;

procedure TWeaponEditorDlg.NextButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  SaveChanges;
  if FWeaponposition<Weapons.Count-1 then FWeaponposition+=1;
  if FWeaponposition>=0 then
    CurWeapon.Assign(Weapons[FWeaponposition]);
  PositionTILabel.RealSetText(IntToStr(WeaponPosition+1));
  FillAttacksStringGrid;
end;


//-----------------------------


procedure TWeaponEditorDlg.SetWeapons(const AValue: TBattleWeapons);
begin
  if FWeapons=AValue then exit;
  FWeapons.Assign(AValue);
  //writeln('WeaponEditorDlg.SetWeapon');
  if Weapons.Count>0 then
    FTheWeapon.Assign(Weapons[0])
  else
    FTheWeapon.Clear;
  FillAttacksStringGrid;
  PositionTILabel.RealSetText(IntToStr(WeaponPosition+1));
  WeaponNameTIEdit.Link.SetObjectAndProperty(FTheWeapon,'WeaponName');
  AttackTIEdit.Link.SetObjectAndProperty(FTheWeapon,'AttackOrAim');
  DrawTIEdit.Link.SetObjectAndProperty(FTheWeapon,'Draw');
  ReloadTIEdit.Link.SetObjectAndProperty(FTheWeapon,'Reload');
  FullParryTIEdit.Link.SetObjectAndProperty(FTheWeapon,'FullParry');
  FumbleRangeTIEdit.Link.SetObjectAndProperty(FTheWeapon,'FumbleRange');
  OBTIEdit.Link.SetObjectAndProperty(FTheWeapon,'OB');
end;

procedure TWeaponEditorDlg.SaveChanges;
begin
  if (FWeaponposition>=0) and (FWeaponposition<Weapons.Count) then
  begin
    Weapons[FWeaponposition].Assign(CurWeapon);
  end;
end;

procedure TWeaponEditorDlg.FillAttacksStringGrid;
begin
  TIGrid1.ListObject:=CurWeapon;
end;

procedure TWeaponEditorDlg.SetWeaponPosition(const AValue: integer);
begin
  if (Weapons.Count>AValue) and (AValue>=0) then begin
    FWeaponposition:=AValue;
    FTheWeapon.Assign(Weapons[AValue]);
    
    // update focused control
    GetPropertyLinkOfComponent(ActiveControl).LoadFromProperty;
    
    PositionTILabel.Caption:=IntToStr(WeaponPosition+1);
    FillAttacksStringGrid;
  end;
end;

initialization
  {$I weaponeditor.lrs}

end.

