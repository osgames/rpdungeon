unit PenalitiesEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  RTTICtrls;

type

  { TPenalitiesEditorForm }

  TPenalitiesEditorForm = class(TForm)
    HitsLabel: TLabel;
    HitsPerRoundLabel: TLabel;
    ReasonLabel: TLabel;
    StartTimeLabel: TLabel;
    StatDecreaseLabel: TLabel;
    DurationLabel: TLabel;
    PenalityLabel: TLabel;
    PenalityTIEdit: TTIEdit;
    HitsTIEdit: TTIEdit;
    HitsPerRoundTIEdit: TTIEdit;
    StatDecreaseTIEdit: TTIEdit;
    DurationTIEdit: TTIEdit;
    StartTimeTIEdit: TTIEdit;
    ReasonTIEdit: TTIEdit;
  private
    { private declarations }
  public
    { public declarations }
    procedure SetPenality;
  end; 

var
  PenalitiesEditorForm: TPenalitiesEditorForm;

implementation

{ TPenalitiesEditorForm }

procedure TPenalitiesEditorForm.SetPenality;
begin
  //PenalityTIEdit.Link.SetObjectAndProperty(FTheWeapon,'WeaponName');
end;

initialization
  {$I penalitieseditor.lrs}

end.

