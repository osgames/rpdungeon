unit ArmorEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, Buttons, Combatant,
  RTTICtrls, StdCtrls, Grids;

type

  { TArmorEditorDlg }

  TArmorEditorDlg = class(TForm)
    CancelButton: TButton;
    ArmorDBLabel: TLabel;
    ElemDBLabel: TLabel;
    ShieldDBLabel: TLabel;
    QuDBLabel: TLabel;
    OKButton: TButton;
    ArmorDBTIEdit: TTIEdit;
    QuDBTIEdit: TTIEdit;
    ShieldDBTIEdit: TTIEdit;
    ElemDBTIEdit: TTIEdit;
    PartsStringGrid: TStringGrid;
    procedure ArmorEditorDlgCreate(Sender: TObject);
    procedure ArmorEditorDlgDestroy(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
  private
    FArmor: TBattleArmor;
    procedure SetArmor(const AValue: TBattleArmor);
    { private declarations }
    procedure InitPartStringGrid;
  public
    { public declarations }
    property Armor:TBattleArmor read FArmor write SetArmor;
  end; 

var
  ArmorEditorDlg: TArmorEditorDlg;

function ShowArmorEditor(AArmor: TBattlearmor): TModalResult;

implementation


function ShowArmorEditor(AArmor: TBattleArmor): TModalResult;
var
  ArmorEditorDlg: TArmorEditorDlg;
begin
  ArmorEditorDlg:=TArmorEditorDlg.Create(nil);
  ArmorEditorDlg.Armor:=AArmor;
  Result:=ArmorEditorDlg.ShowModal;
  if Result=mrOk then
    AArmor.Assign(ArmorEditorDlg.Armor);
  ArmorEditorDlg.Free;
end;


{ TArmorEditorDlg }

procedure TArmorEditorDlg.OkButtonClick(Sender: TObject);
var
  Zeile: Integer;
  Aspect: TWuchtAspekt;
  CurArmorPart: TArmorPart;
begin
  if Sender=nil then ;
  for Zeile:=1 to Armor.ArmorParts.Count do
  begin
    CurArmorPart:=Armor.ArmorParts[Zeile-1];
    for Aspect:=low(TWuchtAspekt) to high(TWuchtAspekt) do begin
     CurArmorPart.Wucht[Aspect]:=StrToIntDef(PartsStringGrid.Cells[ord(Aspect)+1,Zeile],
                               CurArmorPart.Wucht[Aspect]);
    end;
  end;

  ModalResult:=mrOK;
end;

procedure TArmorEditorDlg.SetArmor(const AValue: TBattleArmor);
begin
  if FArmor=AValue then exit;
  FArmor.Assign(AValue);
  ArmorDBTIEdit.Link.SetObjectAndProperty(Armor,'ArmorDB');
  ShieldDBTIEdit.Link.SetObjectAndProperty(Armor,'ShieldDB');
  ElemDBTIEdit.Link.SetObjectAndProperty(Armor,'ElementalDB');
  QuDBTIEdit.Link.SetObjectAndProperty(Armor,'QuicknessDB');
  InitPartStringGrid;
end;

procedure TArmorEditorDlg.InitPartStringGrid;
var
  Zeile: Integer;
  CurArmorPart: TArmorPart;
begin
  PartsStringGrid.RowCount:=Armor.ArmorParts.Count+1;
  
  PartsStringGrid.Cells[0,0]:='Place';
  PartsStringGrid.ColWidths[0]:=100;
  PartsStringGrid.Cells[1,0]:='Stumpf';
  PartsStringGrid.ColWidths[1]:=40;
  PartsStringGrid.Cells[2,0]:='Spitz';
  PartsStringGrid.ColWidths[2]:=40;
  PartsStringGrid.Cells[3,0]:='Scharf';
  PartsStringGrid.ColWidths[3]:=40;
  PartsStringGrid.Cells[4,0]:='Feuer';
  PartsStringGrid.ColWidths[4]:=40;
  PartsStringGrid.Cells[5,0]:='Druck';
  PartsStringGrid.ColWidths[5]:=40;
  PartsStringGrid.Cells[6,0]:='Blitz';
  PartsStringGrid.ColWidths[6]:=40;

  for Zeile:=1 to Armor.ArmorParts.Count do
  begin
    CurArmorPart:=Armor.ArmorParts[Zeile-1];
    PartsStringGrid.Cells[0,Zeile]:=CurArmorPart.Place;
    PartsStringGrid.Cells[1,Zeile]:=IntToStr(CurArmorPart.Wucht[waStumpf]);
    PartsStringGrid.Cells[2,Zeile]:=IntToStr(CurArmorPart.Wucht[waSpitz]);
    PartsStringGrid.Cells[3,Zeile]:=IntToStr(CurArmorPart.Wucht[waScharf]);
    PartsStringGrid.Cells[4,Zeile]:=IntToStr(CurArmorPart.Wucht[waFeuer]);
    PartsStringGrid.Cells[5,Zeile]:=IntToStr(CurArmorPart.Wucht[waDruck]);
    PartsStringGrid.Cells[6,Zeile]:=IntToStr(CurArmorPart.Wucht[waBlitz]);
  end;
end;

procedure TArmorEditorDlg.CancelButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  ModalResult:=mrCancel;
end;

procedure TArmorEditorDlg.ArmorEditorDlgCreate(Sender: TObject);
begin
  if Sender=nil then ;
  FArmor:=TBattleArmor.Create;
  
  InitPartStringGrid;
end;

procedure TArmorEditorDlg.ArmorEditorDlgDestroy(Sender: TObject);
begin
  if Sender=nil then ;
  FArmor.Free;
end;

initialization
  {$I armoreditor.lrs}

end.

