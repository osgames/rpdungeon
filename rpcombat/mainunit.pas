unit MainUnit; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, LCLProc, LCLType, LCLIntf,
  LResources, Forms, Controls, Graphics,
  Dialogs,  StdCtrls, RTTICtrls, Menus, Grids, Combatant, CombatantEditor,
  Laz_XMLCfg, FileUtil, BFProcs, BFDlgProcs, CombatGroups, BFGroupEditor,
  Buttons;

type
  TSaveCharacterFlag = (
     scfSaveEvenIfNotModified,
     scfSaveAs,      // always show 'save as' dialog
     scfAddToRecent
     );
  TSaveCharacterFlags = set of TSaveCharacterFlag;

  TOpenCharacterFlag = (
     ocfAddToRecent
     );
  TOpenCharacterFlags = set of TOpenCharacterFlag;
{  TSaveGroupFlag = (
     scfSaveEvenIfNotModified,
     scfSaveAs,
     scfAddToRecent
     );}
  TSaveGroupFlags = set of TSaveCharacterFlag;
  
  { TBattleFieldOptions }

  TRPCombatOptions = class
  private
    FFileDlgHeight: integer;
    FFileDlgHistoryList: TStringList;
    FFileDlgInitialDir: string;
    FFileDlgMaxHistory: integer;
    FFileDlgWidth: integer;
    FLoadedCharacters: TStrings;
    FMainFormBounds: TRect;
    FMaxRecentCharacterFilenames: integer;
    FRecentCharacterFilenames: TStringList;
    FModified: boolean;
    function GetModified: boolean;
    procedure SetFileDlgHeight(const AValue: integer);
    procedure SetFileDlgHistoryList(const AValue: TStringList);
    procedure SetFileDlgInitialDir(const AValue: string);
    procedure SetFileDlgMaxHistory(const AValue: integer);
    procedure SetFileDlgWidth(const AValue: integer);
    procedure SetLoadedCharacters(const AValue: TStrings);
    procedure SetMainFormBounds(const AValue: TRect);
    procedure SetMaxRecentCharacterFilenames(const AValue: integer);
    procedure SetModified(const AValue: boolean);
    procedure SetRecentCharacterFilenames(const AValue: TStringList);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure LoadFromXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure SaveToXMLConfig(XMLConfig: TXMLConfig; const Path: string);
    procedure LoadFromFile(const Filename: string);
    procedure SaveToFile(const Filename: string);
    procedure InitializeFileDialog(FileDialog: TFileDialog);
    procedure SaveFileDialogSettings(FileDialog: TFileDialog);
    procedure AddToRecentCharacterFilenames(const Filename: string);
    procedure RemoveFromRecentCharacterFilenames(const Filename: string);
    procedure ReplaceLoadedCharacter(const OldFilename, NewFilename: string);
  public
    property FileDlgInitialDir: string read FFileDlgInitialDir write SetFileDlgInitialDir;
    property FileDlgWidth: integer read FFileDlgWidth write SetFileDlgWidth;
    property FileDlgHeight: integer read FFileDlgHeight write SetFileDlgHeight;
    property FileDlgHistoryList: TStringList read FFileDlgHistoryList write SetFileDlgHistoryList;
    property FileDlgMaxHistory: integer read FFileDlgMaxHistory write SetFileDlgMaxHistory;
    property Modified: boolean read GetModified write SetModified;
    property RecentCharacterFilenames: TStringList read FRecentCharacterFilenames write SetRecentCharacterFilenames;
    property MaxRecentCharacterFilenames: integer read FMaxRecentCharacterFilenames write SetMaxRecentCharacterFilenames;
    property MainFormBounds: TRect read FMainFormBounds write SetMainFormBounds;
    property LoadedCharacters: TStrings read FLoadedCharacters write SetLoadedCharacters;
  end;

  { TBattleForm }

  TBattleForm = class(TForm)
    AllPenalitiesEdit: TEdit;
    CancelActionButton: TButton;
    EditCombatantMenuItem: TMenuItem;
    PopupMenu1: TPopupMenu;
    TargetComboBox: TComboBox;
    DeleteActionButton: TButton;
    CurCombatantGroupBox: TGroupBox;
    AddCustomAPGroupBox: TGroupBox;
    TargetLabel: TLabel;
    PenalitiesLabel: TLabel;
    MainMenu1: TMainMenu;
    FileMenuItem: TMenuItem;
    ExitMenuItem: TMenuItem;
    CharLoadMenuItem: TMenuItem;
    CharNewMenuItem: TMenuItem;
    CharEditMenuItem: TMenuItem;
    CharSaveMenuItem: TMenuItem;
    CharSaveAsMenuItem: TMenuItem;
    CharOpenRecentMenuItem: TMenuItem;
    GroupsMenuItem: TMenuItem;
    EditGroupsMenuItem: TMenuItem;
    MultiplyCharacterMenuItem: TMenuItem;
    SaveGroupsMenuItem: TMenuItem;
    OpenGroupMenuItem: TMenuItem;
    Sep2MenuItem: TMenuItem;
    Sep1MenuItem: TMenuItem;
    SaveAllMenuItem: TMenuItem;
    APListStringGrid: TStringGrid;
    procedure APListStringGridColRowMoved(Sender: TObject; IsColumn: Boolean;
      sIndex, tIndex: Integer);
    procedure APListStringGridDrawCell(Sender: TObject; Col, Row: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure APListStringGridSelection(Sender: TObject; Col, Row: Integer);
    procedure AddCustomAPButtonClick(Sender: TObject);
    procedure CancelActionButtonClick(Sender: TObject);
    procedure CharEditMenuItemClick(Sender: TObject);
    procedure CharLoadMenuItemClick(Sender: TObject);
    procedure CharNewMenuItemClick(Sender: TObject);
    procedure CharSaveAsMenuItemClick(Sender: TObject);
    procedure CharSaveMenuItemClick(Sender: TObject);
    procedure DeleteActionButtonClick(Sender: TObject);
    procedure EditCombatantMenuItemClick(Sender: TObject);
    procedure EditGroupsMenuItemClick(Sender: TObject);
    procedure ExitMenuItemClick(Sender: TObject);
    procedure BattleCreate(Sender: TObject);
    procedure BattleDestroy(Sender: TObject);
    procedure CharOpenRecentClicked(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure MultiplyCharacterMenuItemClick(Sender: TObject);
    procedure OpenGroupMenuItemClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure SaveAllMenuItemClick(Sender: TObject);
    procedure SaveGroupsMenuItemClick(Sender: TObject);
    procedure TargetComboBoxEditingDone(Sender: TObject);
  private
    FCombatants: TCombatantGroup;
    FGroups: TCombatantGroups;
    FCurrentAP: integer;
    FHistoryAP: integer;
    FFutureAP: integer;
    FOptions: TRPCombatOptions;
    FOptionsFilename: String;
    fCurrentCombatant: TCombatant;
    function GetCombatants(Index: integer): TCombatant;
    procedure SetCurrentAP(const AValue: integer);
    procedure SetFutureAP(const AValue: integer);
    procedure SetHistoryAP(const AValue: integer);
    procedure SetOptionsFilename(const AValue: String);
    procedure UpdateCurrentAP;
    procedure UpdateAPList;
    procedure SwitchCurCombatant;
    procedure UpdateCurrentCombatant;
    procedure UpdateCaption;
    procedure UpdatePenalities;
    procedure UpdateTargetComboBox;
    procedure CreateAddCustomAPButtons;
    function CreateUniqueCombatantName: string;
    function FindCombatantByName(const AName: string): TCombatant;
    function GetCurrentCombatant: TCombatant;
    procedure SaveOptions;
    procedure SaveAll;
    procedure SaveAllCombatants;
    procedure SaveAllGroups;
    procedure SaveLoadedCharacterList;
    procedure SelectNextCombatant;
  public
    function CreateNewCharacter: TModalResult;
    function AskToSaveCharacterIfModified: TModalResult;
    function SaveCharacter(ACombatant: TCombatant; Filename: string;
                           const Flags: TSaveCharacterFlags): TModalResult;
    function SaveGroup(AGroup:TCombatantGroup; Filename: string;
                           const Flags: TSaveGroupFlags):TModalResult;
    function OpenCharacter(const Filename: string;
                           const Flags: TOpenCharacterFlags): TModalResult;
    function OpenLastCharacters: TModalResult;
    function OpenGroup(const Filename: string;
                       const Flags: TOpenCharacterFlags): TModalResult;
    procedure AddRecentCharacterFilename(const Filename: string);
    procedure SetRecentSubMenu(ParentMenuItem: TMenuItem; FileList: TStringList;
                               OnClickEvent: TNotifyEvent);
    procedure EditCurCombatant;

    // show and update data
    procedure UpdateRecentCharacterFilenamesMenu;

    property Combatants: TCombatantGroup read FCombatants;
    property Options: TRPCombatOptions read FOptions;
    property OptionsFilename: String read FOptionsFilename write SetOptionsFilename;
    property Groups: TCombatantGroups read FGroups;
    
    property CurrentAP: integer read FCurrentAP write SetCurrentAP;// what is the global AP
    property HistoryAP: integer read FHistoryAP write SetHistoryAP;// how much history AP to show
    property FutureAP: integer read FFutureAP write SetFutureAP;// how much future AP to show
  end;

const
  CombatantExt = '.bfc';
  GroupExt = '.bfg';

var
  BattleForm: TBattleForm;

implementation

{ TBattleForm }

procedure TBattleForm.BattleCreate(Sender: TObject);
var
  NewBounds: TRect;
begin
  if Sender=nil then ;
  FCombatants:=TCombatantGroup.Create;
  
  OptionsFilename:='rpcombat.xml';
  FOptions:=TRPCombatOptions.Create;
  Options.LoadFromFile(OptionsFilename);
  
  FGroups:=TCombatantGroups.Create;
  
  FCurrentAP:=0;
  FHistoryAP:=10;
  FFutureAP:=30;

  UpdateCaption;
  NewBounds:=Options.MainFormBounds;
  if (NewBounds.Left+200<NewBounds.Right)
  and (NewBounds.Left>=-10) and (NewBounds.Right<Screen.Width+10)
  and (NewBounds.Top+200<NewBounds.Bottom)
  and (NewBounds.Top>=-10) and (NewBounds.Bottom<Screen.Height+10)
  then
    BoundsRect:=NewBounds;
    
  CreateAddCustomAPButtons;

  UpdateRecentCharacterFilenamesMenu;
  UpdateCurrentCombatant;

  OpenLastCharacters;
  
  UpdateCurrentAP;
  UpdateAPList;
  SelectNextCombatant;
end;

procedure TBattleForm.BattleDestroy(Sender: TObject);
begin
  if Sender=nil then ;
  FCombatants.Free;
  FGroups.Free;
  FOptions.Free;
end;

procedure TBattleForm.CharOpenRecentClicked(Sender: TObject);
var
  AFileName: String;
begin
  if Sender is TMenuItem then begin
    AFileName:=ExpandFilename(TMenuItem(Sender).Caption);
    if OpenCharacter(AFilename,[ocfAddToRecent])<>mrOk then begin
      // open failed
      if not FileExists(AFilename) then
        Options.RemoveFromRecentCharacterFilenames(AFilename);
    end;
  end;
end;

procedure TBattleForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if CloseAction=caNone then ;
  SaveOptions;
end;

procedure TBattleForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_S) then begin
    SaveAll;
    Key:=VK_UNKNOWN;
  end;
  if (Shift=[ssCtrl]) and (Key=VK_E) then begin
    EditCurCombatant;
    Key:=VK_UNKNOWN;
  end;
end;

procedure TBattleForm.MultiplyCharacterMenuItemClick(Sender: TObject);
var
  SrcCombatant: TCombatant;
  CountStr: String;
  NewCount: LongInt;
  i: Integer;
  NewCombatant: TCombatant;
begin
  SrcCombatant:=GetCurrentCombatant;
  if SrcCombatant=nil then exit;
  CountStr:=InputBox('Vermehre '+SrcCombatant.Name,'Anzahl:','1');
  NewCount:=StrToIntDef(CountStr,0);
  if NewCount>0 then begin
    for i:=0 to NewCount-1 do begin
      NewCombatant:=TCombatant.Create;
      NewCombatant.Assign(SrcCombatant);
      NewCombatant.Name:=FCombatants.NewName(SrcCombatant.Name);
      FCombatants.Add(NewCombatant);
    end;
    // update views
    UpdateAPList;
    UpdateCurrentCombatant;
  end;
end;

procedure TBattleForm.OpenGroupMenuItemClick(Sender: TObject);
var
  OpenDialog: TOpenDialog;
  ok: Boolean;
begin
  if Sender=nil then ;
  OpenDialog:=TOpenDialog.Create(nil);
  try
    OpenDialog.Title:='Open Combat Group (*'+GroupExt+')';
    Options.InitializeFileDialog(OpenDialog);
    OpenDialog.Options:=OpenDialog.Options+[ofFileMustExist];
    OpenDialog.Filter:='Group (*'+GroupExt+')|*'+GroupExt+'|'
                       +'All files|'+GetAllFilesMask;
    ok:=OpenDialog.Execute;
    Options.SaveFileDialogSettings(OpenDialog);
    SaveOptions();
    if ok then
      OpenGroup(OpenDialog.Filename,[ocfAddToRecent]);
  finally
    OpenDialog.Free;
  end;
end;

procedure TBattleForm.PopupMenu1Popup(Sender: TObject);
var
  ACombatant: TCombatant;
begin
  ACombatant:=GetCurrentCombatant;
  if ACombatant<>nil then begin
    EditCombatantMenuItem.Caption:='Editiere '+ACombatant.Name;
    EditCombatantMenuItem.Visible:=true;
  end else begin

  end;
end;

procedure TBattleForm.SaveAllMenuItemClick(Sender: TObject);
begin
  if Sender=nil then ;
  SaveAll;
end;

procedure TBattleForm.SaveGroupsMenuItemClick(Sender: TObject);
begin
  SaveAllGroups;
end;

procedure TBattleForm.TargetComboBoxEditingDone(Sender: TObject);
var
  ACombatant: TCombatant;
begin
  ACombatant:=GetCurrentCombatant;
  if ACombatant=nil then exit;
  ACombatant.CurrentFoe:=FCombatants.FindCombatantByName(TargetComboBox.Text);
end;

function TBattleForm.GetCombatants(Index: integer): TCombatant;
begin
  Result:=FCombatants[Index];
end;

procedure TBattleForm.SetCurrentAP(const AValue: integer);
begin
  if FCurrentAP=AValue then exit;
  FCurrentAP:=AValue;
end;

procedure TBattleForm.SetFutureAP(const AValue: integer);
begin
  if FFutureAP=AValue then exit;
  FFutureAP:=AValue;
end;

procedure TBattleForm.SetHistoryAP(const AValue: integer);
begin
  if FHistoryAP=AValue then exit;
  FHistoryAP:=AValue;
end;

procedure TBattleForm.SetOptionsFilename(const AValue: String);
begin
  if FOptionsFilename=AValue then exit;
  FOptionsFilename:=AValue;
end;

procedure TBattleForm.UpdateCurrentAP;
var
  i: Integer;
  EndAP: LongInt;
  AP: Integer;
  APValid: Boolean;
begin
  AP:=0;
  APValid:=false;
  for i:=0 to FCombatants.Count-1 do begin
    EndAP:=Combatants[i].EndAP;
    if (not APValid) or (EndAP<AP) then begin
      AP:=EndAP;
      APValid:=true;
    end;
  end;
  CurrentAP:=AP;
end;

procedure TBattleForm.UpdateAPList;
var
  i: Integer;
  CurCombatant: TCombatant;
begin
  // setup columns
  APListStringGrid.ColCount:=3;
  APListStringGrid.FixedCols:=1;
  APListStringGrid.ColWidths[0]:=100;
  APListStringGrid.ColWidths[2]:=200;
  APListStringGrid.ColWidths[1]:=APListStringGrid.ClientWidth-2-100-200;

  // setup rows
  APListStringGrid.RowCount:=FCombatants.Count+1;
  APListStringGrid.FixedRows:=1;

  APListStringGrid.Cells[0,0]:='AP';
  for i:=1 to FCombatants.Count do begin
    CurCombatant:=Combatants[i-1];
    APListStringGrid.Cells[0,i]:=CurCombatant.Name;
  end;
  
  APListStringGrid.Invalidate;
end;

procedure TBattleForm.SwitchCurCombatant;
var
  c: LongInt;
begin
  c:=APListStringGrid.Row;
  if (c>0) and (c<=FCombatants.Count) then
    fCurrentCombatant:=Combatants[c-1]
  else
    fCurrentCombatant:=nil;
end;

procedure TBattleForm.UpdateCurrentCombatant;
var
  ACombatant: TCombatant;
begin
  ACombatant:=GetCurrentCombatant;
  if ACombatant<>nil then begin
    CurCombatantGroupBox.Caption:=ACombatant.Name;
  end else begin
    CurCombatantGroupBox.Caption:='';
  end;
  UpdatePenalities;
  UpdateTargetComboBox;
end;

procedure TBattleForm.UpdateCaption;
begin
  Caption:='Battlefield';
end;

procedure TBattleForm.UpdatePenalities;
var
  ACombatant: TCombatant;
begin
  ACombatant:=GetCurrentCombatant;
  if ACombatant<>nil then
    AllPenalitiesEdit.Caption:=ACombatant.Penalties.AsString
  else
    AllPenalitiesEdit.Caption:='n/a';
end;

procedure TBattleForm.UpdateTargetComboBox;
var
  sl: TStringList;
  CurCombatant: TCombatant;
  i: Integer;
begin
  CurCombatant:=GetCurrentCombatant;
  sl:=TStringList.Create;
  if CurCombatant<>nil then begin
    for i:=0 to FCombatants.Count-1 do begin
      if FCombatants[i]<>CurCombatant then
        sl.Add(FCombatants[i].Name);
    end;
  end;
  TargetComboBox.Items.Assign(sl);
  sl.Free;
  if CurCombatant<>nil then begin
    TargetComboBox.Text:=CurCombatant.CurrentFoeName;
  end else begin
    TargetComboBox.Text:='n/a';
  end;
end;

procedure TBattleForm.CreateAddCustomAPButtons;
var
  i: Integer;
  NewButton: TButton;
begin
  for i:=1 to 12 do begin
    NewButton:=TButton.Create(Self);
    with NewButton do begin
      Name:='AddCustomAPButton'+IntToStr(i);
      Width:=20;
      Height:=20;
      Caption:=IntToStr(i);
      Align:=alLeft;
      OnClick:=@AddCustomAPButtonClick;
      Parent:=AddCustomAPGroupBox;
    end;
  end;
end;

function TBattleForm.CreateUniqueCombatantName: string;
var
  Prefix: String;
  i: Integer;
begin
  Prefix:='Combatant';
  i:=1;
  repeat
    Result:=Prefix+IntToStr(i);
    inc(i);
  until FindCombatantByName(Result)=nil;
end;

function TBattleForm.FindCombatantByName(const AName: string): TCombatant;
begin
  Result:=FCombatants.FindCombatantByName(AName);
end;

function TBattleForm.GetCurrentCombatant: TCombatant;
var
  c: LongInt;
begin
  Result:=fCurrentCombatant;
  c:=APListStringGrid.Row;
  if (c>0) and (c<=FCombatants.Count) then
    Result:=Combatants[c-1];
end;

procedure TBattleForm.SaveOptions;
begin
  Options.MainFormBounds:=BoundsRect;
  try
    Options.SaveToFile(OptionsFilename);
  except
    on E: Exception do begin
      DebugLn('TBattleForm.SaveOptions ',E.Message);
    end;
  end;
end;

procedure TBattleForm.SaveAll;
begin
  SaveAllCombatants;
  SaveAllGroups;
end;

procedure TBattleForm.SaveAllCombatants;
var
  i: Integer;
begin
  for i:=0 to FCombatants.Count-1 do
    SaveCharacter(Combatants[i],Combatants[i].Filename,[]);
end;

procedure TBattleForm.SaveAllGroups;
var
  i: Integer;
begin
  for i:=0 to FGroups.Count-1 do
    SaveGroup(Groups[i],Groups[i].Filename,[]);
end;

procedure TBattleForm.SaveLoadedCharacterList;
var
  i: Integer;
  CurFilename: string;
  j: Integer;
begin
  j:=0;
  for i:=0 to FCombatants.Count-1 do begin
    CurFilename:=FCombatants[i].Filename;
    if not FilenameIsAbsolute(CurFilename) then continue;
    // add all characters with a filename
    if Options.LoadedCharacters.Count>j then begin
      if Options.LoadedCharacters[j]<>CurFilename then begin
        Options.LoadedCharacters[j]:=CurFilename;
        Options.Modified:=true;
      end;
    end else begin
      Options.LoadedCharacters.Add(CurFilename);
      Options.Modified:=true;
    end;
    inc(j);
  end;
  // delete the rest
  while Options.LoadedCharacters.Count>j do begin
    Options.LoadedCharacters.Delete(Options.LoadedCharacters.Count-1);
    Options.Modified:=true;
  end;
end;

procedure TBattleForm.SelectNextCombatant;
var
  i: Integer;
begin
  for i:=0 to Combatants.Count-1 do begin
    if Combatants[i].EndAP<=CurrentAP then begin
      APListStringGrid.Row:=i+1;
      exit;
    end;
  end;
end;

function TBattleForm.CreateNewCharacter: TModalResult;
var
  i:integer;
  NewCombatant: TCombatant;
begin
  Result:=mrOk;
  NewCombatant:=TCombatant.Create;
  NewCombatant.Name:=CreateUniqueCombatantName;
  FCombatants.Add(NewCombatant);
  ShowCombatantEditor(NewCombatant);
  //debugln('MainUnit.NewMenuItem nach Combatanteditor; NewCombatant ',NewCombatant.Name);//debug
  UpdateAPList;
  UpdateCurrentCombatant;
  for i:=0 to FCombatants.Count-1 do begin //debug
    debugln('Combatants: ', Combatants[i].Name);//debug
   // debugln('Combatants.Weapons: ', Combatants[i].Weapons.Items[0]);//debug
  end;//debug
end;

function TBattleForm.AskToSaveCharacterIfModified: TModalResult;
var
  ACombatant: TCombatant;
begin
  ACombatant:=GetCurrentCombatant;
  if (ACombatant<>nil) and ACombatant.Modified then begin
    Result:=QuestionDlg('Save changes?',
      'Character '+ACombatant.Name+' has been modified.',
      mtConfirmation,[mrYes,'Save',mrNo,'Discard',mrCancel],0);
    if Result=mrYes then begin
      Result:=SaveCharacter(ACombatant,ACombatant.Filename,[]);
    end else if Result=mrNo then
      Result:=mrOk;
  end else
    Result:=mrOk;
end;

function TBattleForm.SaveCharacter(ACombatant: TCombatant; Filename: string;
  const Flags: TSaveCharacterFlags): TModalResult;
var
  SaveDialog: TSaveDialog;
  ok: Boolean;
  SaveAs: Boolean;
  OldFilename: String;
begin
  if ACombatant=nil then exit;

  OldFilename:=ACombatant.Filename;
  SaveAs:=(scfSaveAs in Flags) or (not FilenameIsAbsolute(ACombatant.Filename));

  if Filename='' then begin
    // create default filename
    Filename:=ACombatant.Filename;
    if Filename='' then begin
      Filename:=ACombatant.Name+CombatantExt;
    end;
  end;

  debugln('TMainForm.SaveCharacter B "',Filename,'"');
  if SaveAs then begin
    SaveDialog:=TSaveDialog.Create(nil);
    try
      SaveDialog.Title:='Charakter '+ACombatant.Name+' speichern als ... (*'+CombatantExt+')';
      Options.InitializeFileDialog(SaveDialog);
      // set initial directory
      if FilenameIsAbsolute(Filename) then
        SaveDialog.InitialDir:=ExtractFilePath(Filename);
      // show default filename
      if Filename<>'' then
        SaveDialog.Filename:=ExtractFileName(Filename);
      SaveDialog.Options:=SaveDialog.Options+[ofPathMustExist];
      SaveDialog.Filter:='Charakter (*'+CombatantExt+')|*'+CombatantExt+'|'
                         +'All files|'+GetAllFilesMask;
      ok:=SaveDialog.Execute;
      Filename:=SaveDialog.Filename;
      Options.SaveFileDialogSettings(SaveDialog);
      SaveOptions();
      if not ok then
      begin
        Result:=mrCancel;
        exit;
      end;

      if (ExtractFileExt(Filename)='') then
        Filename:=ChangeFileExt(Filename,CombatantExt);
    finally
      SaveDialog.Free;
    end;

    // warn for overwrite
    Result:=WarnOverwriteIfFileExists(Filename,[]);
    if Result<>mrOk then exit;
  end;

  // save
  Result:=ACombatant.SaveToFile(Filename);
  ACombatant.Filename:=Filename;
  if Result=mrOk then begin
    Options.ReplaceLoadedCharacter(OldFilename,Filename);
  end;
  // add to recent
  if scfAddToRecent in Flags then begin
    AddRecentCharacterFilename(Filename);
    SaveOptions();
  end;

  Result:=mrOk;
end;

function TBattleForm.SaveGroup(AGroup: TCombatantGroup; Filename: string;
  const Flags: TSaveGroupFlags): TModalResult;
var
  SaveDialog: TSaveDialog;
  ok: Boolean;
  SaveAs: Boolean;
begin
  if AGroup=nil then exit;

  SaveAs:=(scfSaveAs in Flags) or (not FilenameIsAbsolute(AGroup.Filename));

  if Filename='' then begin
    // create default filename
    Filename:=AGroup.Filename;
    if Filename='' then begin
      Filename:=AGroup.Name+GroupExt;
    end;
  end;

  debugln('TMainForm.SaveGroup B "',Filename,'"');
  if SaveAs then begin
    SaveDialog:=TSaveDialog.Create(nil);
    try
      SaveDialog.Title:='Gruppe speichern als ... (*'+GroupExt+')';
      Options.InitializeFileDialog(SaveDialog);
      // set initial directory
      if FilenameIsAbsolute(Filename) then
        SaveDialog.InitialDir:=ExtractFilePath(Filename);
      // show default filename
      if Filename<>'' then
        SaveDialog.Filename:=ExtractFileName(Filename);
      SaveDialog.Options:=SaveDialog.Options+[ofPathMustExist];
      SaveDialog.Filter:='Group (*'+GroupExt+')|*'+GroupExt+'|'
                         +'All files|'+GetAllFilesMask;
      ok:=SaveDialog.Execute;
      Filename:=SaveDialog.Filename;
      Options.SaveFileDialogSettings(SaveDialog);
      SaveOptions();
      if not ok then begin
        Result:=mrCancel;
        exit;
      end;

      if (ExtractFileExt(Filename)='') then
        Filename:=ChangeFileExt(Filename,GroupExt);
    finally
      SaveDialog.Free;
    end;

    // warn for overwrite
    Result:=WarnOverwriteIfFileExists(Filename,[]);
    if Result<>mrOk then exit;
  end;

  // save
  Result:=AGroup.SaveToFile(Filename);
  //if Result=mrOk then begin
    //Options.ReplaceLoadedCharacter(OldFilename,Filename);
  //end;
  // add to recent
  //if scfAddToRecent in Flags then begin
  //  AddRecentCharacterFilename(Filename);
  //  SaveOptions();
  //end;
  debugln('Gruppen gespeichert');
  Result:=mrOk;
end;


function TBattleForm.OpenCharacter(const Filename: string;
  const Flags: TOpenCharacterFlags): TModalResult;
var
  ACombatant: TCombatant;
begin
  if (not FileExists(Filename)) or (DirPathExists(Filename)) then begin
    Result:=mrCancel;
    exit;
  end;
  DebugLn('TBattleForm.OpenCharacter "',Filename,'"');

  // load a character
  ACombatant:=TCombatant.Create;
  Result:=ACombatant.LoadFromFile(Filename);
  //debugln('TMainForm.OpenCharacter ',ACharacter.Name,' ACharacter=',dbgs(ACharacter.Items.Count),' ',dbgs(Result=mrOk));
  if Result=mrOk then begin
    ACombatant.Filename:=Filename;
    Options.ReplaceLoadedCharacter('',Filename);
  end;
  if ocfAddToRecent in Flags then begin
    AddRecentCharacterFilename(Filename);
    SaveOptions();
  end;
  if Result<>mrOk then begin
    // loading failed => clean up and abort
    ACombatant.Free;
    exit;
  end;
  ACombatant.Name:=FCombatants.NewName(ACombatant.Name);
  ACombatant.Modified:=false;
  FCombatants.Add(ACombatant);

  // update views
  UpdateAPList;
  UpdateCurrentCombatant;

  Result:=mrOk;
end;

function TBattleForm.OpenLastCharacters: TModalResult;
var
  i: Integer;
begin
  for i:=0 to Options.LoadedCharacters.Count-1 do begin
    Result:=OpenCharacter(Options.LoadedCharacters[i],[]);
    if Result=mrAbort then exit;
  end;
  Result:=mrOk;
end;

function TBattleForm.OpenGroup(const Filename: string;
  const Flags: TOpenCharacterFlags): TModalResult;
var
  AGroup: TCombatantGroup;
  i:integer;
  ACombatant: TCombatant;
begin
  if (not FileExists(Filename)) or (DirPathExists(Filename)) then begin
    Result:=mrCancel;
    exit;
  end;
  DebugLn('TBattleForm.OpenCharacter "',Filename,'"');

  // load a character
  AGroup:=TCombatantGroup.Create;
  Result:=AGroup.LoadFromFile(Filename);
  //debugln('TMainForm.OpenCharacter ',ACharacter.Name,' ACharacter=',dbgs(ACharacter.Items.Count),' ',dbgs(Result=mrOk));
  if Result=mrOk then begin
    AGroup.Filename:=Filename;
    debugln('xmlLoad names:',AGroup.LoadedCombatantNames.Text);
    //Options.ReplaceLoadedCharacter('',Filename);
  end;
  if ocfAddToRecent in Flags then begin
    //AddRecentCharacterFilename(Filename);
    SaveOptions();
  end;
  if Result<>mrOk then begin
    // loading failed => clean up and abort
    AGroup.Free;
    exit;
  end;
  debugln(['OpenGroup count :',AGroup.LoadedCombatantNames.Count]);
  for i:=0 to AGroup.LoadedCombatantNames.Count-1 do begin
    ACombatant:=FCombatants.FindCombatantByName(AGroup.LoadedCombatantNames[i]);
    if ACombatant<>nil then
      AGroup.Add(ACombatant);
  end;
  AGroup.Modified:=false;
  FGroups.Add(AGroup);

  // update views
  //UpdateAPList;
  //UpdateCurrentCombatant;

  Result:=mrOk;
end;

procedure TBattleForm.AddRecentCharacterFilename(const Filename: string);
begin
  Options.AddToRecentCharacterFilenames(Filename);
  UpdateRecentCharacterFilenamesMenu;
end;

procedure TBattleForm.SetRecentSubMenu(ParentMenuItem: TMenuItem;
  FileList: TStringList; OnClickEvent: TNotifyEvent);
var
  i: integer;
  AMenuItem: TMenuItem;
begin
  // create enough menuitems
  while ParentMenuItem.Count<FileList.Count do begin
    AMenuItem:=TMenuItem.Create(Self);
    AMenuItem.Name:=
      ParentMenuItem.Name+'Recent'+IntToStr(ParentMenuItem.Count);
    ParentMenuItem.Add(AMenuItem);
  end;
  // delete unused menuitems
  while ParentMenuItem.Count>FileList.Count do
    ParentMenuItem.Items[ParentMenuItem.Count-1].Free;
  // set captions and event
  for i:=0 to FileList.Count-1 do begin
    AMenuItem:=ParentMenuItem.Items[i];
    AMenuItem.Caption := FileList[i];
    AMenuItem.OnClick := OnClickEvent;
  end;
  // set parent's visibility
  ParentMenuItem.Visible:=ParentMenuItem.Count>0;
end;

procedure TBattleForm.EditCurCombatant;
var
  ACombatant: TCombatant;
begin
  ACombatant:=GetCurrentCombatant;
  if ACombatant=nil then exit;
  ShowCombatantEditor(ACombatant);
  UpdateAPList;
  UpdateCurrentCombatant;
end;

procedure TBattleForm.UpdateRecentCharacterFilenamesMenu;
begin
  SetRecentSubMenu(CharOpenRecentMenuItem,
                   Options.RecentCharacterFilenames,
                   @CharOpenRecentClicked);
end;

procedure TBattleForm.ExitMenuItemClick(Sender: TObject);
begin
  if Sender=nil then ;
  Close;
end;

procedure TBattleForm.CharNewMenuItemClick(Sender: TObject);
begin
  if Sender=nil then ;
  CreateNewCharacter;
end;

procedure TBattleForm.CharSaveAsMenuItemClick(Sender: TObject);
begin
  if Sender=nil then ;
  SaveCharacter(GetCurrentCombatant,'',[scfSaveAs,scfAddToRecent]);
end;

procedure TBattleForm.CharSaveMenuItemClick(Sender: TObject);
begin
  if Sender=nil then ;
  SaveCharacter(GetCurrentCombatant,'',[scfAddToRecent]);
end;

procedure TBattleForm.DeleteActionButtonClick(Sender: TObject);
var
  CurCombatant: TCombatant;
begin
  CurCombatant:=GetCurrentCombatant;
  if (CurCombatant=nil) or (CurCombatant.AktionenCount=0) then exit;
  CurCombatant.DeleteLastAktion;
  UpdateCurrentAP;
  debugln(['TBattleForm.DeleteActionButtonClick ',CurrentAP]);
  UpdateAPList;
  SelectNextCombatant;
end;

procedure TBattleForm.EditCombatantMenuItemClick(Sender: TObject);
begin
  EditCurCombatant;
end;

procedure TBattleForm.EditGroupsMenuItemClick(Sender: TObject);
begin
  if Sender=nil then ;
  ShowGroupEditorDlg(Groups,FCombatants);
end;

procedure TBattleForm.APListStringGridColRowMoved(Sender: TObject;
  IsColumn: Boolean; sIndex, tIndex: Integer);
begin
  if sIndex=tIndex then exit;
  if IsColumn then begin

  end else begin
    //debugln('TBattleForm.APListStringGridColRowMoved ',sIndex,' ',tIndex);
    FCombatants.Move(sIndex-1,tIndex-1);
    SaveLoadedCharacterList;
    UpdateCurrentCombatant;
  end;
end;

procedure TBattleForm.APListStringGridDrawCell(Sender: TObject; Col,
  Row: Integer; aRect: TRect; aState: TGridDrawState);
var
  CurCombatant: TCombatant;
  a: Integer;
  CurAktion: TCombatAktion;
  LeftAP: Integer;
  EndAP: Integer;
  CellWidth: Integer;
  DrawAP: Integer;
  APRect: TRect;
  NullPos: Integer;
  NameRect: TRect;
  OldPenColor: Integer;
begin
  if Row<1 then exit;
  if aState=[] then ;
  CurCombatant:=Combatants[Row-1];
  CellWidth:=aRect.Right-aRect.Left;
  OldPenColor:=APListStringGrid.Canvas.Pen.Color;
  
  DrawAP:=HistoryAP+FutureAP;
  if Col=0 then begin
    // Name
    if CurCombatant.EndAP<=CurrentAP then begin
      // current combatant
      APListStringGrid.Canvas.Brush.Color:=clGreen;
      NameRect:=ARect;
      APListStringGrid.Canvas.FillRect(NameRect);
    end;
    if Row=APListStringGrid.Row then begin
      // selected
      //APListStringGrid.Canvas.Font.Color:=clRed;
    end;
  end else if Col=1 then begin
    // AP
    LeftAP:=0;
    for a:=0 to CurCombatant.AktionenCount-1 do begin
      CurAktion:=CurCombatant.Aktionen[a];
      EndAP:=LeftAP+CurAktion.Duration;
      if (LeftAP<CurrentAP+FutureAP)
      and (EndAP>CurrentAP-HistoryAP) then begin
        APRect.Top:=aRect.Top+2;
        APRect.Bottom:=aRect.Bottom-2;
        APRect.Left:=((LeftAP-CurrentAP+HistoryAP)*CellWidth) div DrawAP;
        APRect.Right:=((EndAP-CurrentAP+HistoryAP)*CellWidth) div DrawAP;
        APRect.Left:=Max(APRect.Left,0);
        APRect.Right:=Min(APRect.Right,CellWidth);
        APRect.Left+=aRect.Left;
        APRect.Right+=aRect.Left;
        APListStringGrid.Canvas.Brush.Color:=clYellow;
        APListStringGrid.Canvas.Pen.Color:=clBlue;
        APListStringGrid.Canvas.Rectangle(APRect);
        InflateRect(APRect,-1,-1);
        APListStringGrid.Canvas.Frame(APRect);
      end;
      LeftAP:=EndAP;
    end;

    // draw current AP line
    APListStringGrid.Canvas.Brush.Color:=clBlack;
    NullPos:=aRect.Left+((HistoryAP*CellWidth) div DrawAP);
    APListStringGrid.Canvas.FillRect(Rect(NullPos,aRect.Top,NullPos+2,aRect.Bottom));
  end;

  APListStringGrid.Canvas.Pen.Color:=OldPenColor;
end;

procedure TBattleForm.APListStringGridSelection(Sender: TObject; Col,
  Row: Integer);
begin
  if Sender=nil then ;
  if (Col=0) and (Row=0) then ;
  SwitchCurCombatant;
  UpdateCurrentCombatant;
  APListStringGrid.Invalidate;
end;

procedure TBattleForm.AddCustomAPButtonClick(Sender: TObject);
var
  AP: LongInt;
  NewAktion: TCombatAktion;
  CurCombatant: TCombatant;
begin
  AP:=StrToIntDef(TButton(Sender).Caption,1);
  NewAktion:=TCombatAktion.Create;
  NewAktion.Duration:=AP*2;
  CurCombatant:=GetCurrentCombatant;
  CurCombatant.AddAktion(NewAktion);
  
  UpdateCurrentAP;
  UpdateAPList;
  SelectNextCombatant;
end;

procedure TBattleForm.CancelActionButtonClick(Sender: TObject);
var
  CurCombatant: TCombatant;
begin
  CurCombatant:=GetCurrentCombatant;
  if (CurCombatant=nil) or (CurCombatant.AktionenCount=0) then exit;
  CurCombatant.CancelLastAktion(CurrentAP);
  UpdateCurrentAP;
  UpdateAPList;
  SelectNextCombatant;
end;

procedure TBattleForm.CharEditMenuItemClick(Sender: TObject);
begin
  EditCurCombatant;
end;

procedure TBattleForm.CharLoadMenuItemClick(Sender: TObject);
var
  OpenDialog: TOpenDialog;
  ok: Boolean;
  i: Integer;
begin
  if Sender=nil then ;
  OpenDialog:=TOpenDialog.Create(nil);
  try
    OpenDialog.Title:='Open Charakter (*'+CombatantExt+')';
    Options.InitializeFileDialog(OpenDialog);
    OpenDialog.Options:=OpenDialog.Options+[ofFileMustExist,ofAllowMultiSelect];
    OpenDialog.Filter:='Charakter (*'+CombatantExt+')|*'+CombatantExt+'|'
                       +'All files|'+GetAllFilesMask;
    ok:=OpenDialog.Execute;
    Options.SaveFileDialogSettings(OpenDialog);
    SaveOptions();
    if ok then begin
      for i:=0 to OpenDialog.Files.Count-1 do begin
        if OpenCharacter(OpenDialog.Files[i],[ocfAddToRecent])<>mrOk then break;
      end;
    end;
  finally
    OpenDialog.Free;
  end;
end;

{ TRPCombatOptions }

function TRPCombatOptions.GetModified: boolean;
begin
  Result:=FModified;
end;

procedure TRPCombatOptions.SetFileDlgHeight(const AValue: integer);
begin
  if FFileDlgHeight=AValue then exit;
  FFileDlgHeight:=AValue;
end;

procedure TRPCombatOptions.SetFileDlgHistoryList(const AValue: TStringList);
begin
  if FFileDlgHistoryList=AValue then exit;
  FFileDlgHistoryList.Assign(AValue);
  Modified:=true;
end;

procedure TRPCombatOptions.SetFileDlgInitialDir(const AValue: string);
begin
  if FFileDlgInitialDir=AValue then exit;
  FFileDlgInitialDir:=AValue;
end;

procedure TRPCombatOptions.SetFileDlgMaxHistory(const AValue: integer);
begin
  if FFileDlgMaxHistory=AValue then exit;
  FFileDlgMaxHistory:=AValue;
end;

procedure TRPCombatOptions.SetFileDlgWidth(const AValue: integer);
begin
  if FFileDlgWidth=AValue then exit;
  FFileDlgWidth:=AValue;
end;

procedure TRPCombatOptions.SetLoadedCharacters(const AValue: TStrings);
begin
  if FLoadedCharacters=AValue then exit;
  FLoadedCharacters.Assign(AValue);
end;

procedure TRPCombatOptions.SetMainFormBounds(const AValue: TRect);
begin
  if CompareRect(@FMainFormBounds,@AValue) then exit;
  FMainFormBounds:=AValue;
  Modified:=true;
end;

procedure TRPCombatOptions.SetMaxRecentCharacterFilenames(
  const AValue: integer);
begin
  if FMaxRecentCharacterFilenames=AValue then exit;
  FMaxRecentCharacterFilenames:=AValue;
  Modified:=true;
end;

procedure TRPCombatOptions.SetModified(const AValue: boolean);
begin
  FModified:=AValue;
end;

procedure TRPCombatOptions.SetRecentCharacterFilenames(
  const AValue: TStringList);
begin
  if FRecentCharacterFilenames=AValue then exit;
  FRecentCharacterFilenames.Assign(AValue);
  Modified:=true;
end;

constructor TRPCombatOptions.Create;
begin
  FFileDlgHistoryList:=TStringList.Create;
  FRecentCharacterFilenames:=TStringList.Create;
  FLoadedCharacters:=TStringList.Create;
  Clear;
end;

destructor TRPCombatOptions.Destroy;
begin
  FreeAndNil(FRecentCharacterFilenames);
  FreeAndNil(FFileDlgHistoryList);
  FreeAndNil(FLoadedCharacters);
  inherited Destroy;
end;

procedure TRPCombatOptions.Clear;
begin
  FFileDlgInitialDir:='';
  FFileDlgWidth:=400;
  FFileDlgHeight:=300;
  FFileDlgMaxHistory:=15;
  FFileDlgHistoryList.Clear;
  FRecentCharacterFilenames.Clear;
  FMaxRecentCharacterFilenames:=20;
  FLoadedCharacters.Clear;
end;

procedure TRPCombatOptions.LoadFromXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  FFileDlgInitialDir:=XMLConfig.GetValue(Path+'FileDlg/InitialDir/Value','');
  FFileDlgWidth:=XMLConfig.GetValue(Path+'FileDlg/Width/Value',400);
  FFileDlgHeight:=XMLConfig.GetValue(Path+'FileDlg/Height/Value',300);
  FFileDlgMaxHistory:=XMLConfig.GetValue(Path+'FileDlg/MaxHistory/Value',15);
  LoadRecentList(XMLConfig,FFileDlgHistoryList,Path+'FileDlg/History/');
  FMaxRecentCharacterFilenames:=XMLConfig.GetValue(
                                        Path+'RecentCharacterFilenames/Max',20);
  LoadRecentList(XMLConfig,FRecentCharacterFilenames,
                 Path+'RecentCharacterFilenames/Files/');
  LoadRectFromXMLConfig(XMLConfig,Path+'MainFormBounds/',FMainFormBounds);
  LoadStringList(XMLConfig,LoadedCharacters,Path+'LoadedCharacters/');
  RemoveDoubles(LoadedCharacters);

  Modified:=false;
end;

procedure TRPCombatOptions.SaveToXMLConfig(XMLConfig: TXMLConfig;
  const Path: string);
begin
  XMLConfig.SetDeleteValue(Path+'FileDlg/InitialDir/Value',FFileDlgInitialDir,'');
  XMLConfig.SetDeleteValue(Path+'FileDlg/Width/Value',FFileDlgWidth,400);
  XMLConfig.SetDeleteValue(Path+'FileDlg/Height/Value',FFileDlgHeight,300);
  XMLConfig.SetDeleteValue(Path+'FileDlg/MaxHistory/Value',FFileDlgMaxHistory,15);
  SaveRecentList(XMLConfig,FFileDlgHistoryList,Path+'FileDlg/History/');
  XMLConfig.SetDeleteValue(Path+'RecentCharacterFilenames/Max',
                           FMaxRecentCharacterFilenames,20);
  SaveRecentList(XMLConfig,FRecentCharacterFilenames,
                 Path+'RecentCharacterFilenames/Files/');
  SaveRectToXMLConfig(XMLConfig,Path+'MainFormBounds/',FMainFormBounds);
  SaveStringList(XMLConfig,LoadedCharacters,Path+'LoadedCharacters/');

  Modified:=false;
end;

procedure TRPCombatOptions.LoadFromFile(const Filename: string);
var
  XMLConfig: TXMLConfig;
begin
  Clear;
  if not FileExists(Filename) then exit;
  XMLConfig:=TXMLConfig.Create(Filename);
  try
    LoadFromXMLConfig(XMLConfig,'Battlefield/');
  finally
    XMLConfig.Free;
  end;
end;

procedure TRPCombatOptions.SaveToFile(const Filename: string);
var
  XMLConfig: TXMLConfig;
begin
  if FileExists(Filename) and (not Modified) then exit;
  XMLConfig:=TXMLConfig.CreateClean(Filename);
  try
    SaveToXMLConfig(XMLConfig,'Battlefield/');
    XMLConfig.Flush;
  finally
    XMLConfig.Free;
  end;
end;

procedure TRPCombatOptions.InitializeFileDialog(FileDialog: TFileDialog);
begin
  FileDialog.InitialDir:=FileDlgInitialDir;
  FileDialog.Width:=FileDlgWidth;
  FileDialog.Height:=FileDlgHeight;
  FileDialog.HistoryList:=FFileDlgHistoryList;
end;

procedure TRPCombatOptions.SaveFileDialogSettings(FileDialog: TFileDialog);
var s: string;
begin
  FFileDlgInitialDir:=FileDialog.InitialDir;
  FFileDlgWidth:=FileDialog.Width;
  FFileDlgHeight:=FileDialog.Height;
  s:=ExtractFilePath(FFileDlgInitialDir);
  if s<>'' then
    AddToRecentList(s,FFileDlgHistoryList,FFileDlgMaxHistory);
end;

procedure TRPCombatOptions.AddToRecentCharacterFilenames(
  const Filename: string);
begin
  if AddToRecentList(Filename,RecentCharacterFilenames,
    MaxRecentCharacterFilenames)
  then
    Modified:=true;
end;

procedure TRPCombatOptions.RemoveFromRecentCharacterFilenames(
  const Filename: string);
begin
  if RemoveFromRecentList(Filename,RecentCharacterFilenames) then
    Modified:=true;
end;

procedure TRPCombatOptions.ReplaceLoadedCharacter(const OldFilename,
  NewFilename: string);
var
  i: LongInt;
begin
  repeat
    i:=LoadedCharacters.IndexOf(OldFilename);
    if i>=0 then LoadedCharacters.Delete(i);
  until i<0;
  LoadedCharacters.Add(NewFilename);
  Modified:=true;
end;

initialization
  {$I mainunit.lrs}

end.

