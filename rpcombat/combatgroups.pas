unit CombatGroups;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Combatant, LCLProc;

type
  { TCombatantGroups }

  TCombatantGroups = class(TPersistent)
  private
    FGroups: TFPList;
    FModified: boolean;
    function GetCount: integer;
    function GetGroups(Index: integer): TCombatantGroup;
    procedure SetGroups(Index: integer; const AValue: TCombatantGroup);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure ClearGroups;
    procedure CopyGroups(Source: TCombatantGroups);
    procedure Add(Group: TCombatantGroup);
    function NewName(const StartName: string): string;
    function FindGroup(const Name: string):TCombatantGroup;
    procedure RemoveGroup(Index: integer);
  public
    property Items[Index: integer]: TCombatantGroup read GetGroups write SetGroups; default;
    property Count: integer read GetCount;
    property Modified: boolean read FModified write FModified;
  end;


implementation

{ TCombatantGroups }

function TCombatantGroups.GetGroups(Index: integer): TCombatantGroup;
begin
  Result:=TCombatantGroup(FGroups[Index]);
end;

function TCombatantGroups.GetCount: integer;
begin
  Result:=FGroups.Count;
end;

procedure TCombatantGroups.SetGroups(Index: integer;
  const AValue: TCombatantGroup);
begin
  if TCombatantGroup(FGroups[Index])=AValue then exit;
  FGroups[Index]:=AValue;
  FModified:=true;
end;

procedure TCombatantGroups.RemoveGroup(Index: integer);
begin
  Items[Index].Free;
  FGroups.Delete(Index);
  FModified:=true;
end;

constructor TCombatantGroups.Create;
begin
  FGroups:=TFPList.Create;
end;

destructor TCombatantGroups.Destroy;
begin
  Clear;
  FreeAndNil(FGroups);
  inherited Destroy;
end;

procedure TCombatantGroups.Clear;
begin
  ClearGroups;
end;

procedure TCombatantGroups.ClearGroups;
var
  i: Integer;
begin
  for i:=0 to Count-1 do TObject(FGroups[i]).Free;
  FGroups.Clear;
end;

procedure TCombatantGroups.CopyGroups(Source: TCombatantGroups);
var
  i: Integer;
  NewGroup: TCombatantGroup;
begin
  Clear;
  for i:=0 to Source.Count-1 do begin
    NewGroup:=TCombatantGroup.Create;
    NewGroup.Assign(Source[i]);
    Add(NewGroup);
  end;
end;

procedure TCombatantGroups.Add(Group: TCombatantGroup);
begin
  FGroups.Add(Group);
  FModified:=true;
end;

function TCombatantGroups.NewName(const StartName: string): string;
begin
  result:=StartName;
  while FindGroup(result)<>nil do
    result:=CreateNextIdentifier(result);
end;

function TCombatantGroups.FindGroup(const Name: string): TCombatantGroup;
var
  i: Integer;
begin
  i:=0;
  while i < count do
    begin
      if CompareText( Name , Items[i].Name) = 0 then
        exit(Items[i]);
      i+=1;
    end;
  result:=nil;
end;

end.

