unit StatEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc,  LResources, Forms, Controls, Graphics, Dialogs,
  Buttons, Combatant, RTTICtrls, StdCtrls, Grids, RMStats;

type

  { TStatEditorDlg }

  TStatEditorDlg = class(TForm)
    OKButton: TButton;
    CancelButton: TButton;
    ConstitutionLabel: TLabel;
    AgilityLabel: TLabel;
    EmpathyLabel: TLabel;
    IntuitionLabel: TLabel;
    StatsStringGrid: TStringGrid;
    TotalLabel: TLabel;
    MiscLabel: TLabel;
    RaceLabel: TLabel;
    PotentialLabel: TLabel;
    TempLabel: TLabel;
    PresenceLabel: TLabel;
    QuicknessLabel: TLabel;
    StrengthLabel: TLabel;
    MemoryLabel: TLabel;
    ReasoningLabel: TLabel;
    SelfdisciplineLabel: TLabel;
    procedure CancelButtonClick(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure StatsStringGridBeforeSelection(Sender: TObject; Col, Row: Integer
      );
    procedure StatsStringGridEditingDone(Sender: TObject);
    procedure StatsStringGridSelection(Sender: TObject; Col, Row: Integer);
  private
    FStats: TCombatantStats;
    procedure SetStats(const AValue: TCombatantStats);
    procedure SaveChanges;
    procedure FillStatsStringGrid;
  public
    property Stats: TCombatantStats read FStats write SetStats;
  end; 

var
  StatEditorDlg: TStatEditorDlg;

function ShowStatEditor(CombatantStats: TCombatantStats): TModalResult;
implementation

function ShowStatEditor(CombatantStats: TCombatantStats): TModalResult;
var
  StatEditorDlg: TStatEditorDlg;
begin
  StatEditorDlg:=TStatEditorDlg.Create(nil);
  StatEditorDlg.Stats:=CombatantStats;
  StatEditorDlg.FillStatsStringGrid;
  Result:=StatEditorDlg.ShowModal;
  if Result=mrOK then
    CombatantStats.Assign(StatEditorDlg.Stats);
  StatEditorDlg.Free;
end;

{ TStatEditorDlg }

procedure TStatEditorDlg.CancelButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  ModalResult:=mrCancel;
end;

procedure TStatEditorDlg.OKButtonClick(Sender: TObject);
begin
  if Sender=nil then ;
  SaveChanges;
  ModalResult:=mrOK;
end;

procedure TStatEditorDlg.StatsStringGridBeforeSelection(Sender: TObject; Col,
  Row: Integer);
begin
  if Sender=nil then ;
  if (Col=0) and (Row=0) then ;
  //DebugLn('TStatEditorDlg.StatsStringGridSelection ',dbgs(StatsStringGrid.Col),' ',dbgs(StatsStringGrid.Row));
end;

procedure TStatEditorDlg.StatsStringGridEditingDone(Sender: TObject);
var
  Row: LongInt;
  Col: LongInt;
  NewValue: LongInt;
  AStat: TStatType;
begin
  if Sender=nil then ;
  Row:=StatsStringGrid.Row;
  Col:=StatsStringGrid.Col;
  //DebugLn('TStatEditorDlg.StatsStringGridEditingDone ',dbgs(Col),' ',dbgs(Row));
  NewValue:=StrToIntDef(StatsStringGrid.Cells[Col,Row],0);
  AStat:=TStatType(Row);
  try
    case Col of
    0:
      begin
        if (NewValue<0) or (NewValue>150) then exit;
        // temp stat changed
        if NewValue=FStats.Temp[AStat] then exit;
        //DebugLn('TStatEditorDlg.StatsStringGridEditingDone ',dbgs(NewValue));
        Stats.Temp[AStat]:=NewValue;
        if Stats.Temp[AStat]>Stats.Pot[AStat] then
          Stats.Pot[AStat]:=Stats.Temp[AStat];
      end;
    1:
      begin
        // pot stat changed
        if (NewValue<0) or (NewValue>150) then exit;
        if NewValue=FStats.Pot[AStat] then exit;
        Stats.Pot[AStat]:=NewValue;
      end;
    2:
      begin
        // race bonus changed
        if (NewValue<-50) or (NewValue>150) then exit;
        if NewValue=FStats.Race[AStat] then exit;
        Stats.Race[AStat]:=NewValue;
      end;
    3:
      begin
        // misc bonus changed
        if (NewValue<-150) or (NewValue>150) then exit;
        if NewValue=FStats.Misc[AStat] then exit;
        Stats.Misc[AStat]:=NewValue;
      end;
    else
      exit;
    end;
    Stats.CalcBonus(AStat);
    //DebugLn('TStatEditorDlg.StatsStringGridEditingDone ',dbgs(Stats.Temp[AStat]),' ',dbgs(Stat2Bonus(Stats.Temp[AStat])),' ',dbgs(Stats.Bonus[AStat]));
  finally
    FillStatsStringGrid;
  end;
end;

procedure TStatEditorDlg.StatsStringGridSelection(Sender: TObject; Col,
  Row: Integer);
begin
  if Sender=nil then ;
  if (Col=0) and (Row=0) then ;
  //DebugLn('TStatEditorDlg.StatsStringGridSelection ',dbgs(Col),' ',dbgs(Row));
end;

procedure TStatEditorDlg.SetStats(const AValue: TCombatantStats);
begin
  if FStats=AValue then exit;
  FStats:=AValue;
end;

procedure TStatEditorDlg.SaveChanges;
var
  Zeile: TStatType;
begin
  for Zeile := low(TStatType) to high(TStatType) do
  begin
    FStats.Temp[Zeile]:=StrToInt(StatsStringGrid.Cells[0,ord(Zeile)]);
    FStats.Pot[Zeile]:=StrToInt(StatsStringGrid.Cells[1,ord(Zeile)]);
    FStats.Race[Zeile]:=StrToInt(StatsStringGrid.Cells[2,ord(Zeile)]);
    FStats.Misc[Zeile]:=StrToInt(StatsStringGrid.Cells[3,ord(Zeile)]);
  end;
end;

procedure TStatEditorDlg.FillStatsStringGrid;
var
  Zeile: TStatType;
begin
  for Zeile:=low(TStatType) to high(TStatType) do
    begin
      StatsStringGrid.Cells[0,ord(Zeile)]:=IntToStr(Stats.Temp[Zeile]);
      StatsStringGrid.Cells[1,ord(Zeile)]:=IntToStr(Stats.Pot[Zeile]);
      StatsStringGrid.Cells[2,ord(Zeile)]:=IntToStr(Stats.Race[Zeile]);
      StatsStringGrid.Cells[3,ord(Zeile)]:=IntToStr(Stats.Misc[Zeile]);
      StatsStringGrid.Cells[4,ord(Zeile)]:=IntToStr(Stats.Total[Zeile]);
    end;
end;

initialization
  {$I stateditor.lrs}

end.

